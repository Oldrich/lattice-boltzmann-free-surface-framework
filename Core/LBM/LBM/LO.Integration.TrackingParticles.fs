﻿namespace LBM.LO.Integration

open LBM
open LBM.LO.Types
open LBM.LO

open Kit
open Kit.Collections
open Kit.Math

type internal TrackingParticles =

  static member run (duration: float, loA: LO [], ufA: float triple [][]) =    

    loA |> Array.mapi (fun i lo0 -> 
      if lo0._Interaction = TrackingParticle then
        let dxA0 = lo0.Nodes.LocalDX |> Array.map (Versor.rotateL2G lo0.Rot)
        let lo1 =
          let ufCog1, omegafCog1 =
            let sumV, sumUfCog, sumOmegafCog =
              dxA0 |> Array.foldi (fun j (sumV, sumUf, sumOmegaf) dx0 ->
                let vj = lo0.Nodes.V.[j]
                let newOmega =
                  let n2 = Triple.norm2 dx0
                  if n2 = 0. then sumOmegaf else sumOmegaf + vj * Triple.cross(dx0 / n2, ufA.[i].[j])
                sumV + vj, sumUf + vj * ufA.[i].[j], newOmega
              ) (0., Triple.zerof, Triple.zerof)
            if sumV = 0. then failwith "TP: SumV = 0."
            sumUfCog / sumV, sumOmegafCog / sumV
          let cog1 = lo0.CoG + 0.5 * duration * (ufCog1 + lo0.U)
          let rot1 = 
            let drotQuat = duration * (omegafCog1 |> Versor.ofAngularVelocityVector |> Quaternion.normalize)
            drotQuat * lo0.Rot
          lo0.copy (coG = cog1, rot = rot1, u = ufCog1, omegaGlob = omegafCog1)
        lo1
      else lo0 )