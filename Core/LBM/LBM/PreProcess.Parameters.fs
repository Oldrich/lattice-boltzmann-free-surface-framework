﻿namespace LBM.PreProcess

open LBM.Naming.Input

type Parameters =
 
  static member general1 (?slipCoeff, ?maxTau, ?isMRT, ?anStep, ?binStep, ?vtkStep, ?fiberAspectRatio, ?realFiberLength, ?seed) =
    let isMRT = defaultArg isMRT false
    let binStep = defaultArg binStep 0.1 // secs
    let vtkStep = defaultArg vtkStep (binStep * 50.0)
    let anStep2 = binStep / 50.0
    [ General.seed, string (defaultArg seed 8451235)
      General.analyzeEveryReal, string (defaultArg anStep anStep2)
      General.saveVtkLBMEveryReal, string vtkStep
      General.saveVtkLOAEveryReal, string vtkStep
      General.saveLOAEveryReal, string binStep
      General.saveFluidShapeEveryReal, string binStep

      Fluid.slipCoeff, string (defaultArg slipCoeff 0.9) 
      Fluid.isMRT, string isMRT
      Fluid.maxTau, string (defaultArg maxTau (if isMRT then 10. else 6.))
      Fluid.lbmMaxPotentialSpeed, string 0.1

      Solids.Other.coeffOfRestitution, string 0.0
      Solids.Other.dynamicFrictionCoef, string 0.6
      Solids.Other.staticFrictionCoef, string 0.6

      Solids.Fibers.realFiberDensity, string 7850.
      Solids.Fibers.fiberAspectRatio, string (defaultArg fiberAspectRatio 80.)
      Solids.Fibers.realFiberLength, string (defaultArg realFiberLength 0.06) ]

