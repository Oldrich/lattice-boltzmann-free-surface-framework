﻿namespace LBM.LO.Types

open LBM.BC.Types.Internal

open Kit

type FrictionLawType = 
  /// dynamic coefficient of friction * static coefficient of friction
  | CoulombFriction of float * float
  | CoulombFrictionFn of (Shape * Shape -> (float * float) option)
  | CoulombFrictionFnBc of (Shape * BcObject -> (float * float) option)

type ForceInteractionType = 
  | LubricationFnBc of (Shape * BcObject -> (float * float) option)
  | LubricationFn of (Shape * Shape -> (float * float) option)
  /// lubrication distance * cut-off distance
  | Lubrication of float * float
  /// hook distance * force magnitude
  | HookForce of float * float 

type InteractionsPP = 
  { forceInteraction : ForceInteractionType seq
    friction: FrictionLawType option
    /// coefficient of restitution
    cR: float
    /// function to decide if the solids collide or not based on a distance
    collisionFnP: LO -> LO -> float -> bool
    /// maximum distance of interaction
    interactingDistance: float
    /// function to generate a fictitious particle for a colliding pair (distance -> fictitious diameter)
    fictitiousParticleFn: ((float -> float) * (float -> uint16 * uint16 -> float)) option
  }

type InteractionsBC = 
  { forceInteraction : ForceInteractionType seq
    friction: FrictionLawType option
    /// coefficient of restitution
    cR: float
    /// function to decide if the solids collide or not based on a distance
    collisionFn: LO -> BcObject -> float -> bool
    /// maximum distance of interaction
    interactingDistance: float
    fictitiousParticleFn: (float -> float) option
  } 

type CompacterType =
  | MoveToPoint of (float triple * int option * int option * float option)
  | LargestDistance of float

type ParticleGenerator =
  { FluidVolume: float
    /// VolumeFraction * generateLO
    SphereData: (float * (float -> LO option)) option
    /// VolumeFraction * generateLO
    FiberData: (float * (float -> LO option)) option
    /// VolumeFraction * generateLO
    EllipsoidData: (float * (float -> LO option)) option
    gapFnP: Shape -> Shape -> float
    gapFnBc: Shape -> BcShape -> float
    Compacter: CompacterType option
  }
