﻿namespace LBM.PostProcess.Asymptote

open LBM.Naming
open LBM.LO.Types
open Utilities.PostProcess
open Utilities.Math
open Utilities.PostProcess.U3D
open Utilities.IO
open Utilities.PostProcess.U3D.Types

open System
open System.Collections.Generic
open System.IO
open System.Text.RegularExpressions

type Graph<[<Measure>] 'a, [<Measure>] 'b> (graphName: string, xLabel, xUnits, yLabel, yUnits, ?isLogY, ?scaleXY: (float<'a> * float<'b>), ?width, ?height, ?isLegend, ?y2Units) =
  let width, height = defaultArg width "18cm", defaultArg height "12cm"
  let isLogY = defaultArg isLogY false
  let scale = if isLogY then "scale(Linear, Log);" else ""

  let mutable isLegend = defaultArg isLegend true
  
  let preamble = 
    sprintf "import graph;
import geometry;
texpreamble(\"\\def\Arg{\\mathop {\\rm Arg}\\nolimits}\");

size(%s,%s,IgnoreAspect);
%s;
real x0 = 1e99;
real x1 = -1e99;
real y0 = 1e99;
real y1 = -1e99;
real a;" width height scale

  let curves = List ()
  let fileName = Regex.Replace (graphName.Replace (" ", "_"), @"[\]\[;\\$><]", "")

  let index = ref 0

  member o.IsLegend with get () = isLegend and set v = isLegend <- v

  member o.Name = graphName
  
  member o.addCurve (label: string, data: (float * float[]) [], column, ?pen, ?fill) =
    let dataStr = data |> Array.map (fun (time, vA) -> sprintf "%e\t%e" time vA.[column])
    curves.Add (label, dataStr, pen, fill)

  member o.saveAsy outputPath =
    if Directory.Exists outputPath = false then Directory.CreateDirectory outputPath |> ignore
    
    let commands = curves |> Seq.map (fun (label, data, pen, fill) ->
      let dataFile =
        let fileName = sprintf "%s_%d.asyData" fileName (incr index; !index)
        Path.Combine (outputPath, fileName)
      File.WriteAllLines (dataFile, data)
      let pen = defaultArg pen "black"
      let fill = 
        match fill with 
          | Some p -> sprintf "fill((min(data[0]),0)--graph(data[0],data[1])--(max(data[0]),0)--cycle, %s);" p
          | None -> ""
      sprintf "
file fin=input(\"%s\");
real [][] data = fin.dimension(%d,2);
data = transpose(data);
%s
draw(graph(data[0],data[1]),%s,Label(\"%s\", fontsize(10pt)));
a = min(data[0]);
if (a < x0) {x0 = a;}
a = max(data[0]);
if (a > x1) {x1 = a;};
a = min(data[1]);
if (a < y0) {y0 = a;}
a = max(data[1]);
if (a > y1) {y1 = a;};" dataFile data.Length fill pen label ) |> String.concat "\n\n"

    let axes = sprintf "
if (x0 == x1 ) {x0 = 0.999 * x0; x1 = 1.001 * x1;}
if (y0 == y1 ) {y0 = 0.999 * y0; y1 = 1.001 * y1;}
if (y0 == 0 && y1 == 0) {y1 = 0.001;}
y1 = 1.005 * y1;
y0 = 0.995 * y0;
xlimits(x0,x1);
ylimits(y0,y1);
xaxis(\"%s [%s]\",Bottom,LeftTicks, above = true);
yaxis(\"%s [%s]\",Left,RightTicks, above = true);" xLabel xUnits yLabel yUnits
    
    let secondaryAxes =
      let y2Units = defaultArg y2Units "LU"
      match scaleXY with
        | Some (scaleX, scaleY) ->
          let scY = if isLogY then "Log" else "Linear"
          sprintf "real scaleX = %g;
real scaleY = %g;
picture secondary=secondaryY(new void(picture pic) {
    scale(pic,Linear(1./scaleX),%s);
    real [] x = {x0 * scaleX, x1 * scaleX};
    real [] y = {y0 * scaleY, y1 * scaleY};
    draw(pic,graph(pic,x,y),nullpen);
    yaxis(pic,\"%s [%s]\", Right,gray, LeftTicks(begin=false,end=false), above = true);
    xaxis(pic,\"%s [LU]\", Top,gray, RightTicks(begin=false,end=false), above = true);
    });
add(secondary);" (float scaleX) (float scaleY) scY yLabel y2Units xLabel
        | None -> ""

    let finalString =
      let isLegendStr = if isLegend then sprintf "\nattach(legend(nullpen,linelength = 25, perline = 0),(0,y0 + 0.15 * (y0 - y1)));" else ""
      sprintf "%s\n%s\n%s\n%s%s" preamble commands axes secondaryAxes isLegendStr
    File.WriteAllText(Path.Combine(outputPath, sprintf "%s.asy" fileName), finalString)

  member o.savePdf outputPath =
    o.saveAsy outputPath
    System.Console.WriteLine "Do not cancel"
    let p, output =
      let args = sprintf "-cd \"%s\" -nointeractiveView -nobatchView -f pdf \"%s.asy\"" outputPath fileName
      Utilities.Shell.shellExecute ("asy.exe", args)
    System.Console.WriteLine "Free to cancel"
    if p.ExitCode <> 0 then printfn "%s" output
    fileName

type Graphs (problemDir: string, outputDir) =

  let relW = "0.9"

  let dirName = (DirectoryInfo outputDir).Name
  let subDirName = "ASY"

  let rc = LBM.PostProcess.ReadAndCombine problemDir
  let globZaml = Zaml.parse (File.ReadAllText (Path.Combine(problemDir, General.Files.report)))
  let units = LBM.Units.ofZaml globZaml

  let getFigure (graph: Graph<_,_>) =
    let fileName = graph.savePdf (Path.Combine (outputDir, subDirName))
    let description =
      let found = LBM.PostProcess.Helping.tryFind (graph.Name, "Graphs.tex")
      match found with Some v -> v + "\n" | _ -> ""
    let fig = Latex.Figure.get (sprintf @"%s/%s/%s.pdf" dirName subDirName fileName, width = relW + "\\textwidth", label = sprintf "fig:%s" graph.Name, caption = graph.Name)
    description, fig

  let graphs = System.Collections.Concurrent.ConcurrentDictionary ()

  member private o.addGraph (id, data:(_*_[])[] option, graph: Graph<_,_>, ?labels: _ list, ?iList) =
    data |> Option.iter (fun data ->
      if data.Length > 0 then
        let colors = ["black"; "red"; "blue"; "heavygreen"; "purple"; "olive"; "orange"]
        let length = (snd data.[0]).Length
        let iter = defaultArg iList [0 .. length - 1]
        iter |> List.iteri (fun ii i ->
          let name = match labels with Some l -> l.[ii] | _ -> sprintf "%s %d" graph.Name ii
          graph.addCurve (name, data, i, "linewidth(1.0) + " + colors.[ii]) )
        if iter.Length = 1 then graph.IsLegend <- false
        if graphs.TryAdd(id, getFigure graph) = false then printfn "Cannot add id = %d" id )

  member private o.addGraphMinMaxMean (id, data, graph: Graph<_,_>) = o.addGraph (id, data, graph, ["Min"; "Max"; "Average"])

  member o.addFluidMassReal() = o.addGraph (20, rc.fluidMassReal, Graph("Fluid mass", "Time", "s", "Mass", "kg", scaleXY = (units.scalingT, units.scalingKg)))
  member o.addMinMaxMeanPressureDiffReal() = o.addGraphMinMaxMean (21, rc.minMaxMeanPressureDiffReal, Graph("Pressure difference", "Time", "s", "Pressure difference", "Pa", scaleXY = (units.scalingT, 100. * units.toLbm.Stress 1.<Pa>), y2Units = "\\%"))
  member o.addMeanViscosityReal() = o.addGraph (24, rc.meanViscosityReal, Graph("Apparent viscosity", "Time", "s", "Viscosity", "Pa.s", isLogY = true, scaleXY = (units.scalingT, units.toLbm.DynamicViscosity 1.<Pa s>)))
  member o.addMinMaxMeanSpeedReal() = o.addGraphMinMaxMean (22, rc.minMaxMeanSpeedReal, Graph("Fluid speed", "Time", "s", "Speed", "ms$^{-1}$", scaleXY = (units.scalingT, units.toLbm.Speed 1.<m/s>)))
  member o.addMinMaxMeanShearRateReal() = o.addGraphMinMaxMean (23, rc.minMaxMeanShearRateReal, Graph("Shear rate", "Time", "s", "Shear rate", "s$^{-1}$", scaleXY = (units.scalingT, units.toLbm.ShearRate 1.</s>)))

  member o.addDissipatedEnergyPerLOReal() = o.addGraph (53, rc.numberOfCollisionsDissipatedEnergyPerLOReal, Graph("Dissipated energy during collisions per particle", "Time", "s", "Energy", "J", scaleXY = (units.scalingT, units.toLbm.Energy 1.<J>)), [""], [1])
  member o.addKineticEnergyLOReal() = o.addGraph (54, rc.kineticEnergyLOReal, Graph("Kinetic energy of particles", "Time", "s", "Energy", "J", scaleXY = (units.scalingT, units.toLbm.Energy 1.<J>)))
  member o.addNumberOfCollisionsPerLOReal() = o.addGraph (52, rc.numberOfCollisionsDissipatedEnergyPerLOReal, Graph("Number of collisions per particle", "Time", "s", "Number of collisions", "-", scaleXY = (units.scalingT, 1.0)), [""], [0])
  member o.addNumberOfParticlesReal() = o.addGraph (50, rc.numberOfParticles, Graph("Number of particles", "Time", "s", "Number of particles", "-", scaleXY = (units.scalingT, 1.0)), ["Fibers"; "Spheres"; "Ellipsoids"])
  
  member o.addParticleVolumeFractionFiberSphereEllipsoidReal() =
    o.addGraph (51,
      rc.particleVolumeFractionFiberSphereEllipsoidReal,
      Graph("Particle volume fraction", "Time", "s", "Fraction", "-", scaleXY = (units.scalingT, 1.0)),
      ["Fibers"; "Spheres"; "Ellipsoids"] )

  member o.fractionOfCellsAboveTauReal () =
    rc.fractionOfCellsAboveTauReal |> Option.iter (fun data -> 
      let fillA = [| "paleblue"; "palered"; "palegreen"; "paleyellow"; "pink" |]
      //let penA = [| "red"; "green"; "Yellow"; "blue"; "magenta" |]
      let graph =
        let graphName = "Fraction of fluid having certain viscosity"
        Graph (graphName, "Time", "s", "Fraction","-", scaleXY = (units.scalingT, 1.0))
      let viscosities = 
        let n, tauMin, fn = 4, 0.5, fun vMin vMax i -> vMin + float (i + 1) / 4.0 * (vMax - vMin) // from Runtime.Evaluate
        let vMin, vMax =
          let tauMax =
            let inp = LBM.Naming.Input.ML.parse (File.ReadAllText (problemDir + "\\" + "_input.txt"))
            inp.[Input.Fluid.maxTau] |> Utilities.String.findAllFloats |> List.head
          let fn tau = (tau - 0.5) / 3. |> units.toReal.DynamicViscosity |> float
          fn tauMin, fn tauMax
        Array.init n (fun i ->
          if i = 0 then String.Format ("$\\mu \\le$ {0:0.##} [Pa.s]", fn vMin vMax (i+1))
          elif i = n - 1 then String.Format ("$\\mu >$ {0:0.##} [Pa.s]", fn vMin vMax i)
          else String.Format ("{0:0.##} [Pa.s] $< \\mu \\le$ {1:0.##} [Pa.s]", fn vMin vMax i, fn vMin vMax (i+1)) ) |> Array.rev
      let data = data |> Array.map (fun (t, vA) -> t, vA |> Array.rev |> Array.map (fun v -> 1. - v))
      viscosities |> Array.iteri (fun i label -> graph.addCurve (label, data, i, "linewidth(1.0) + " + fillA.[i], fillA.[i]))
      if graphs.TryAdd(25, getFigure graph) = false then printfn "Cannot add id = 5" )

  member o.addAllGraphs =
    [|  o.addFluidMassReal; o.addMinMaxMeanPressureDiffReal;
        o.addMinMaxMeanSpeedReal; o.addMinMaxMeanShearRateReal;
        o.addMeanViscosityReal; o.fractionOfCellsAboveTauReal;

        o.addDissipatedEnergyPerLOReal; o.addKineticEnergyLOReal; 
        o.addNumberOfCollisionsPerLOReal; o.addNumberOfParticlesReal
        o.addParticleVolumeFractionFiberSphereEllipsoidReal
         |]
      |> Array.Parallel.iter (fun fn -> fn())
  
  member o.addNumberOfFibersCrossingPlanes (pointNormalA: (triple * triple) [], ?acceptTime, ?acceptLO) =
    
    let acceptTime = defaultArg acceptTime (fun _ -> true)
    let acceptLO = defaultArg acceptLO (fun _ -> true)

    let distancePointPlane (point: triple) (pointOnPlane: triple, normal: triple) =
      match normal.X, normal.Y, normal.Z with
        | nx, 0., 0. -> (point.X - pointOnPlane.X - nx * 0.5) * nx
        | 0., ny, 0. -> (point.Y - pointOnPlane.Y - ny * 0.5) * ny
        | 0., 0., nz -> (point.Z - pointOnPlane.Z - nz * 0.5) * nz
        | _ -> (point - pointOnPlane) * normal
      |> abs

    let counts = pointNormalA |> Array.map (fun _ -> Dictionary())
    rc.readLoA |> Seq.iter (fun (KeyValue(time, data)) ->
      if acceptTime time then
        data.Value |> Array.iter (fun (_, (cog, rot), shape, _ as lo) ->
          if acceptLO (time, lo) then 
            let rot = Utilities.Rotation.QuaternionRotation rot
            match shape with
              | Fiber f ->
                pointNormalA |> Array.iteri (fun i pN ->
                  let p1, p2 = 
                    let r1, r2 = 0.5 * f.Length * Triple.X |> rot.rotateL2G, -0.5 * f.Length * Triple.X |> rot.rotateL2G
                    cog + r1, cog + r2
                  if distancePointPlane p1 pN < f.Length || distancePointPlane p2 pN < f.Length then 
                    if counts.[i].ContainsKey time then counts.[i].[time] <- counts.[i].[time] + 1
                    else counts.[i].[time] <- 1 )
              | _ -> () ) )
    
    counts |> Array.Parallel.iteri (fun i tc ->
      let name = sprintf "Number of fibers crossing plane with point %A and normal %A" (fst pointNormalA.[i]) (snd pointNormalA.[i])
      let data =
        tc|> Seq.sortBy (fun v -> v.Key)
          |> Seq.map (fun (KeyValue(time, n)) -> float (units.toReal.Time (float time)), [| float n |])
          |> Seq.toArray
      o.addGraph (0, Some data, Graph (name, "Time", "s", "Count", "-")) )

  member o.save () =
    let texPath = Path.Combine (outputDir, "graphs.tex")
    let descriptionA, figA = graphs |> Seq.sortBy (fun v -> v.Key) |> Seq.map (fun v -> v.Value) |> Array.ofSeq |> Array.unzip
    let str = Array.append descriptionA figA |> String.concat "\n%\n"
    File.WriteAllText (texPath, str)