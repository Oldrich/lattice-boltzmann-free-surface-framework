﻿namespace LBM.Parallel

module Time =
  let out = System.TimeSpan (20, 0, 0, 0) // NESAHAT!!!!!

open System.ServiceModel
open System.ServiceModel.Description
open System.IO
open System.Diagnostics
open System.Collections.Generic
open System.Collections.Concurrent
open System.Threading
open System.Text.RegularExpressions
open System.Runtime.Serialization
open System.Runtime.Serialization.Formatters.Binary

open LBM
open LBM.Basic

open Kit
open Kit.Math
open Kit.Collections

type internal LOWCF =
  byte[] * int * ((float * float * float * bool) option * float option * (float * float * float) option) *
  float * float * float [] * float [] * (float [] [] * float []) *
  float [] * float [] * int * float [] * float [] * float [] * float [] * float [] * (float * float [])
type internal FluidWCF = (float [][][] * float [] option [][][] * int [][] * byte [] * byte [] * byte [] * byte [] * byte []) option

type internal Lo2Lbm = (int [][] * byte []) option

type internal IWCFCallback =

  [<OperationContract(IsOneWay = true)>]
  abstract ReceiveExchange1D: UID:int * name:string * Data:byte [] -> unit


[<ServiceContract(CallbackContract = typeof<IWCFCallback>)>]
type internal IWCF =
  
  [<OperationContract(IsOneWay = true)>]
  abstract _Start: idI:int * idJ:int * idK:int -> unit
  
  [<OperationContract(IsOneWay = true)>]
  abstract _Ping: Nothing:unit -> unit

  [<OperationContract(IsOneWay = true)>]
  abstract SendExchange1D:   UID:int * Name:string * Caller:(int*int*int)  * Data:byte [] -> unit

  [<OperationContract(IsOneWay = true)>]
  abstract SendExchange3D:   UID:int * Name:string * Caller:(int*int*int)  * Data:byte [] -> unit

type internal Stuff3D<'T> =
  { main: 'T option ref [,,]
    mutable n: int
    stop: ManualResetEvent }

type internal Stuff1D<'T, 'Res> =
  { mutable data: 'T option
    mutable result: 'Res option
    mutable n: int
    callbacks: IWCFCallback List
    stop: ManualResetEvent }

[<ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)>]
type internal LocalWCF (domainID: (int*int*int) option [,,], mainId, timer: LBM.Timer) =
  
  let myLock = new System.Object()

  let me = domainID.[1,1,1].Value
  let nIsSomeOtherDomains = (domainID |> Array3D.sumBy (fun v -> if Option.isSome v then 1 else 0)) - 1
  let otherRunning = List ()

  let exchangeStuff1D, exchangeStuff3D = Dictionary (), Dictionary ()

  let getStuff (uid, dict: Dictionary<_,_>, getDefault) =
    lock myLock (fun () ->
      if dict.ContainsKey uid then dict.[uid]
      else 
        let data = getDefault ()
        dict.Add (uid, data); data )

  let getStuff1D (uid, dict) =
    getStuff (uid, dict, fun () -> {data = None; result = None; n = 0; callbacks = List (); stop = new ManualResetEvent false})
  let getStuff3D (uid, dict) =
    getStuff (uid, dict, fun () -> {main = Array3D.init 3 3 3 (fun _ _ _ -> ref None); n = 0; stop = new ManualResetEvent false})

  let mutable exchangeFn = Dictionary ()

  member o.ExchangeFn = exchangeFn
 
  member o.addExchangeFn1D name value = if me = mainId then exchangeFn.[name] <- value

  member o.getAndResetExchange3D (name, uid) =
    let v = getStuff (name, exchangeStuff3D, fun () -> Dictionary ())
    let s = getStuff3D (uid, v)
    if nIsSomeOtherDomains > 0 then
      timer.tick 3
      if s.stop.WaitOne Time.out = false then failwithf "getAndReset3D: Timeout.\nuid = %A\ndict = %A" uid dict
      timer.tockPrint 3 "Waiting - getAndReset3D"
    lock myLock (fun () -> if v.Remove uid = false then failwith "getAndReset3D: Cannot remove")
    s.main

  member o.getAndResetExchange1D uid name =
    let v = getStuff (name, exchangeStuff1D, fun () -> Dictionary ())
    let s = getStuff1D (uid, v)
    if nIsSomeOtherDomains > 0 then
      timer.tick 3
      if s.stop.WaitOne Time.out = false then failwith "getAndReset1D: Timeout"
      timer.tockPrint 3 "Waiting - getAndResetExchange1D"
    lock myLock (fun () -> if v.Remove uid = false then failwith "getAndReset1D: Cannot remove")
    match s.result with Some v -> v | None -> failwith "getAndReset1D: result should not be None"

  interface IWCF with
    member o._Ping () = ()
    member o._Start (i,j,k) = otherRunning.Add (i,j,k)

    member o.SendExchange3D (uid, name, caller, data) = 
      lock myLock (fun () ->
        let v = getStuff (name, exchangeStuff3D, fun () -> Dictionary ())
        let s = getStuff3D (uid, v)
        let i,j,k = caller
        s.main.[i,j,k] := Some (uid, data)
        s.n <- s.n + 1
        if s.n = nIsSomeOtherDomains then
          if s.stop.Set () = false then failwith "send: Cannot set" )

    member o.SendExchange1D (uid, name, caller, newData) =
      lock myLock (fun () ->
        let v = getStuff (name, exchangeStuff1D, fun () -> Dictionary ())
        let s = getStuff1D (uid, v)
        s.n <- s.n + 1
        s.data <- Some (newData :: defaultArg s.data [])
        if caller <> me then
          s.callbacks.Add (OperationContext.Current.GetCallbackChannel<IWCFCallback>())
        if s.n = otherRunning.Count + 1 then
          let back = exchangeFn.[name] s.data.Value
          (o :> IWCFCallback).ReceiveExchange1D (uid, name, back)
          for c in s.callbacks do c.ReceiveExchange1D (uid, name, back) )

  interface IWCFCallback with

    member o.ReceiveExchange1D (uid, name, data) =
      lock myLock (fun () ->
        let s = getStuff1D (uid, getStuff (name, exchangeStuff1D, fun () -> Dictionary()))
        s.result <- Some data
        if s.stop.Set () = false then failwith "ReceiveExchange: Cannot set" )

type Exchange1D<'ToMain, 'FromMain> internal (me:int*int*int, mainWCF: IWCF, locWcf: LocalWCF, name: string, mainFn: ('ToMain list -> 'FromMain)) =
  let newFn (data: byte [] list): byte [] =
    let data2 = data |> List.map (fun d ->
      use s = new MemoryStream(d)
      unbox (BinaryFormatter().Deserialize(s)) )
    use s = new MemoryStream()
    BinaryFormatter().Serialize (s, mainFn data2)
    s.ToArray()
  do locWcf.addExchangeFn1D name newFn

  member o.sendToMain (time, data: 'ToMain): 'FromMain =
    use s = new MemoryStream()
    BinaryFormatter().Serialize (s, data)
    let out: byte [] =
      mainWCF.SendExchange1D (time, name, me, s.ToArray())
      locWcf.getAndResetExchange1D time name
    use s2 = new MemoryStream(out)
    unbox(BinaryFormatter().Deserialize(s2))

and WCF (domainID: (int*int*int) option [,,], isSerial, mainId, args: IDictionary<string,string>, timer) =

  let mutable uid = 0

  let me = domainID.[1,1,1].Value

  let nbrs: IWCF option [,,] = Array3D.create 3 3 3 None
  let limit = 52428800

  let id2Address (i,j,k) =
    let uid = (DirectoryInfo (Directory.GetCurrentDirectory())).Parent.FullName.GetHashCode()
    sprintf "net.pipe://localhost/LBM_%d%d%d_%d" i j k uid

  let getBinding () =
    let t = Time.out
    let b = NetNamedPipeBinding ( MaxReceivedMessageSize = int64 limit, MaxBufferPoolSize = int64 limit, MaxBufferSize = limit,
                                  CloseTimeout = t, OpenTimeout = t, ReceiveTimeout = t, SendTimeout = t )
    b.ReaderQuotas.MaxStringContentLength <- limit; b.ReaderQuotas.MaxDepth <- limit; b.ReaderQuotas.MaxArrayLength <- limit; b.ReaderQuotas.MaxBytesPerRead <- limit; b.ReaderQuotas.MaxNameTableCharCount <- limit
    b.Security.Mode <- NetNamedPipeSecurityMode.None
    b

  let setLimit (e: ServiceEndpoint) =
    for i in e.Contract.Operations do
      i.Behaviors.Find<DataContractSerializerOperationBehavior>().MaxItemsInObjectGraph <- limit

  let locWcf = LocalWCF (domainID, mainId, timer)

  do
    if isSerial = false then
      let svh = new ServiceHost (locWcf)
      let _serviceEndPoint = svh.AddServiceEndpoint (typeof<IWCF>, getBinding(), id2Address me)
      match svh.Description.Behaviors.Find<ServiceDebugBehavior>() with
        | null -> svh.Description.Behaviors.Add (new ServiceDebugBehavior(IncludeExceptionDetailInFaults = true))
        | beh -> beh.IncludeExceptionDetailInFaults <- true
      for e in svh.Description.Endpoints do setLimit e
      svh.Open ()

  let mainWCF =
    if mainId <> me then
      let fact = new DuplexChannelFactory<IWCF> (new InstanceContext(locWcf), getBinding(), id2Address mainId)
      setLimit fact.Endpoint
      let ch = fact.CreateChannel()
      ch._Start me
      ch
    else locWcf :> IWCF

  member o.Exchange1D(name, mainFn: 'ToMain list -> 'FromMain) = Exchange1D(me, mainWCF, locWcf, name, mainFn)

  member internal o.createProcess (globI, globJ, globK) =
    let globID_Dir = sprintf @"%s\..\Domain_%d-%d-%d" (Directory.GetCurrentDirectory()) globI globJ globK

    if Directory.Exists globID_Dir = false then
      let proc = new Process()
      proc.StartInfo.FileName <- Process.GetCurrentProcess().MainModule.FileName
      proc.StartInfo.Arguments <-
        args.[LBM.Naming.General.Args.paralel] <- sprintf "globID-%d-%d-%d" globI globJ globK
        Utilities.IO.CMD.ofDict args
      proc.StartInfo.UseShellExecute <- false
      if Directory.Exists globID_Dir = false then
        System.Console.WriteLine ("Starting new domain: {0}, {1}, {2}", globI, globJ, globK)
        if proc.Start() = false then failwith "createProcess: Cannot start"

  member private o.createProcess2 glob =
    let proces = lazy (o.createProcess glob)
    let doPing () =
      try
        let fact = new DuplexChannelFactory<IWCF>(new InstanceContext(locWcf), getBinding(), id2Address glob)
        setLimit fact.Endpoint
        let ch = fact.CreateChannel()
        ch._Ping()
        domainID |> Array3D.iteri (fun i j k globID ->
          globID |> Option.iter (fun globID ->
            if globID = glob then nbrs.[i,j,k] <- Some ch ))
        true
      with _ -> proces.Value; false
    let time = Stopwatch()
    time.Start()
    while doPing () = false do
      if time.Elapsed > Time.out then failwith "timeout"
      Thread.Sleep 50

  member private o.sendAndReceive sendFn =
    nbrs |> Array3D.iteri (fun i j k wcf ->
      if (i,j,k) <> (1,1,1) then
        let op = 
          let op x = match x with 0 -> 2 | 2 -> 0 | _ -> 1
          op i, op j, op k
        domainID.[i,j,k] |> Option.iter (fun globID ->
          let wcf =
            if globID = me then locWcf :> IWCF
            else defaultFn wcf (fun () -> o.createProcess2 globID; nbrs.[i,j,k].Value)
          sendFn i j k op wcf ))

  member internal o.IsRunning = nbrs |> Array3D.mapi (fun i j k v -> v |> Option.isSome || domainID.[i, j, k] |> Option.exists (fun id -> id = me) )

  member internal o.sendAndReceive_Exchange3D<'T>(name, data: 'T [,,]) =
    uid <- uid + 1
    o.sendAndReceive (fun i j k op (wcf: IWCF) ->
      let formatter = BinaryFormatter()
      use m = new MemoryStream()
      formatter.Serialize(m, Some data.[i,j,k])
      wcf.SendExchange3D (uid, name, op, m.ToArray()))
    let out = locWcf.getAndResetExchange3D (name, uid)
    out |> Array3D.map (fun v ->
      v.Value |> Option.map (fun (time, res) ->
        let out: 'T option =
          if uid <> time then failwithf "The times do not match %d <> %d" uid time
          let formatter = BinaryFormatter()
          use m = new MemoryStream(res)
          unbox (formatter.Deserialize(m))
        out.Value ))