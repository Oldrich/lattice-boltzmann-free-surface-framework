﻿namespace LBM

open LBM.Naming
open LBM.Basic

open Kit
open Kit.IO
open Kit.Collections
open Kit.Math

type ParallelType = {
  curId: int * int * int
  mainId: int * int * int
  nDomains: int * int * int
  loOffsetD: int }

type Lattice(lengthA_Glob: int [,], maxNodesZ_Glob, consts: DxQx, boundary, parallelization, args, ?timer, ?startAll) =

  let timer = defaultArg timer (Timer [])

  let nodesXg, nodesYg = lengthA_Glob.GetLength 0, lengthA_Glob.GetLength 1

  let p = parallelization
  let (nDomX, nDomY, nDomZ), (idX, idY, idZ) = p.nDomains, p.curId

  do if idX >= nDomX || idY >= nDomY || idZ >= nDomZ then failwithf "p.curId %A >= p.nDomains %A" p.curId p.nDomains

  let isSerial = nDomX = 1 && nDomY = 1 && nDomZ = 1

  let (nodesXl, originX), (nodesYl, originY), (maxNodesZl, originZ) =
    let getNOX, getNOY, getNOZ =
      let fn nodesXg nDomX idX = 
        let originX = (nodesXg / nDomX) * idX
        if idX = nDomX - 1 then nodesXg - originX, originX
        else nodesXg / nDomX, originX
      fn nodesXg nDomX, fn nodesYg nDomY, fn maxNodesZ_Glob nDomZ
    getNOX idX, getNOY idY, getNOZ idZ
  let originT = triple (originX, originY, originZ)

  let mutable lengthAl = Array2D.init nodesXl nodesYl (fun x y ->
    min maxNodesZl (lengthA_Glob.[x + originX, y + originY] - originZ) )

  let domainID = Array3D.init 3 3 3 (fun il jl kl ->
    let ig, jg, kg = il + idX - 1, jl + idY - 1, kl + idZ - 1
    match boundary with
      | LTPeriodic ->
        let fn length pos =
          if pos >= length then pos % length
          elif pos < 0 then (1000 * length + pos) % length
          else pos
        Some (fn nDomX ig, fn nDomY jg, fn nDomZ kg)
      | LTFail ->
        if ig < 0 || jg < 0 || kg < 0 || ig >= nDomX || jg >= nDomY || kg >= nDomZ then None
        else Some (ig, jg, kg) )

  let origins =
    let offSetX, offSetY, offSetZ =
      let fn nodesXg nDomX idX = 
        let sizeReg = (nodesXg / nDomX)
        let sizeLast = nodesXg - (nodesXg / nDomX) * (nDomX - 1)
        if idX = -1 then -sizeLast
        elif idX = nDomX then nodesXg
        else sizeReg * idX
      fn nodesXg nDomX, fn nodesYg nDomY, fn maxNodesZ_Glob nDomZ
    Array3D.init 3 3 3 (fun i j k -> 
      triple( offSetX (idX + i - 1), offSetY (idY + j - 1), offSetZ (idZ + k - 1)))

  let par = Parallel.LbmMain (domainID, p.loOffsetD, (nodesXl, nodesYl, maxNodesZl), isSerial, p.mainId, args, timer)

  let _startNbrs =
    System.Threading.Thread.Sleep 500
    match startAll with
      | Some false -> ()
      | _ ->
        if p.curId = p.mainId then
          Array3D.init nDomX nDomY nDomZ (fun i j k ->
            if (i,j,k) <> p.curId then par.WCF.createProcess (i, j, k) ) |> ignore
  
  let massExhange = System.Collections.Generic.Dictionary<(int*int*int),float>()

  member o.MassExchange = massExhange
  member o.Timer = timer

  member o.LoNtimes =
    let nx, ny, nz = Tuple.map (parallelization.nDomains, float)
    int ((nx*nx + ny*ny + nz*nz)**0.5) + 1

  member o.Consts = consts
  member o.MainID = p.mainId
  member o.LengthA_Glob = lengthA_Glob

  member o.OriginsA = origins

  member o.OriginX = originX
  member o.OriginY = originY
  member o.OriginZ = originZ

  member o.NodesX_Glob = nodesXg
  member o.NodesY_Glob = nodesYg
  member o.NodesZ_Glob = maxNodesZ_Glob

  member o.NodesX_Loc = nodesXl
  member o.NodesY_Loc = nodesYl
  member o.NodesZ_Loc = maxNodesZl
  member o.NodesXf_Loc = float nodesXl
  member o.NodesYf_Loc = float nodesYl
  member o.NodesZf_Loc = float maxNodesZl

  member o.DomId = idX, idY, idZ
  member o.DomId3D = domainID

  member o.Par = par

  member o.LengthA_Loc  with get () = lengthAl
                        and set v =
                          lengthAl <- v
                          v |> Array2D.iteri (fun xl yl h ->
                            lengthA_Glob.[xl + originX, yl + originY] <- originZ + h )

  member o.x_dx ((x, y, z), aDim) = let c = consts.C.[aDim] in x - c.[0], y - c.[1], z - c.[2]
  member o.xdx  ((x, y, z), aDim) = let c = consts.C.[aDim] in x + c.[0], y + c.[1], z + c.[2]

  member o.xdx  (xyz: int triple, aDim) =
    let c = consts.Cf.[aDim] |> Triple.map int
    xyz + c

  member o.x_dx  (xyz: int triple, aDim) =
    let c = consts.Cf.[aDim] |> Triple.map int
    xyz - c

  member o.xyzLoc2Glob ((x,y,z)) = originX + x, originY + y, originZ + z
  member o.xyzLoc2Glob (x,y,z) = originX + x, originY + y, originZ + z
  member o.xyzLoc2Glob (t: int triple) = originT + t
  member o.xyzLoc2Glob (t: float triple) = Triple.map float originT + t

  member o.xyzGlob2Loc ((x,y,z)) = x - originX, y - originY, z - originZ
  member o.xyzGlob2Loc (x,y,z) = x - originX, y - originY, z - originZ
  member o.xyzGlob2Loc (t: int triple) = t - originT
  
  member o.initializeLBM (pA: _ array3, uRhoTauInit) =
    let fA, rho, ux, uy, uz, tau = Array3.init6 pA.LengthA (fun x y z ->
      match pA.[x,y,z] with
        | Fluid | Interface _ ->
          let (rho, u: triple, tau) = uRhoTauInit(x,y,z)
          Some (consts.getfEqA(rho, u)), Some rho, Some u.X, Some u.Y, Some u.Z, Some tau
        | Gas | BoundaryCondition _ -> None, None, None, None, None, None ) 
    o.Par.setLBMA7 (pA |> Array3.copy, fA, rho, ux, uy, uz, tau)
  
  member inline private o.checkPeriodic (length: int, pos: int) =
    if pos >= length then pos % length
    elif pos < 0 then (1000 * length + pos) % length
    else pos

  member o.tryCheckGlobalPeriodic (xyz: int triple) =
    match boundary with
      | LTPeriodic ->
        let x = o.checkPeriodic(nodesXg, xyz.x)
        let y = o.checkPeriodic(nodesYg, xyz.y)
        let z = o.checkPeriodic(maxNodesZ_Glob, xyz.z)
        Some(triple (x, y, z))
      | LTFail ->
        if xyz.x >= 0 && xyz.x < nodesXg && xyz.y >= 0 && xyz.y < nodesYg && xyz.z >= 0 && xyz.z < lengthA_Glob.[xyz.x,xyz.y] then Some xyz
        else None
  
  member o.checkGlobalPeriodic xyz =
    match o.tryCheckGlobalPeriodic xyz with
      | Some v -> v
      | None -> failwithf "Current position (%d, %d, %d) does not exist - use different LTBoundary type" x y z

  member inline private o.shortestDistanceX (nodeNum, v1, v2) =
    let dx = v2 - v1
    let adx = abs dx
    if adx > nodeNum * 0.5 then
      if v2 > v1 then nodeNum - adx else adx - nodeNum
    else dx

  ///Compute shortest normal vector from v1 to v2 in periodic Lattice,
  ///i.e. corner to corner normal is [0.0;0.0].
  member internal o.shortestGlobalNormal (v1: float triple) (v2: float triple): float triple =
    match boundary with 
      | LTPeriodic ->
        let x = o.shortestDistanceX (float nodesXg, v1.x, v2.x)
        let y = o.shortestDistanceX (float nodesYg, v1.y, v2.y)
        let z = o.shortestDistanceX (float maxNodesZ_Glob, v1.z, v2.z)
        triple(x, y, z)
      | LTFail -> v2 - v1
   
  ///Compute length of shortest normal vector from v1 to v2 in periodic Lattice,
  ///i.e. corner to corner distance is 0.0.
  member internal o.shortestGlobalDistance v1 v2 = 
    match boundary with 
      | LTPeriodic -> Triple.norm (o.shortestGlobalNormal v1 v2)
      | LTFail -> Triple.norm(v2 - v1)

  member o.ToZaml isLocal =
    Report.Lattice.lattice, ZamlSeq [
      yield Report.Lattice.globDim, ZamlT (triple(nodesXg, nodesYg, maxNodesZ_Glob))
      if isLocal then
        yield Report.Lattice.locDim, ZamlT (triple(nodesXl, nodesYl, maxNodesZl))
        yield Report.Lattice.origin, ZamlT originT
        yield Report.Lattice.domainIndex, ZamlT (triple p.curId)
      yield Report.Lattice.latticeType, ZamlS <| sprintf "D%dQ%d" consts.Dim consts.ADim
      yield Report.Lattice.isInCompressible, ZamlB consts.IsIncompressible ]
