﻿module LBM.PostProcess.Orientations

open System.IO

open Utilities.Math

let m = Utilities.Matlab false

let getSimulationLines (fiberLength, n, path) =
    let file = File.ReadAllLines path
    let lst = System.Collections.Generic.Stack()
    for line in file do
      let data = line.Split [| '\t' |] |> Array.map float
      let p1 = triple (data.[1], data.[2], data.[3])
      let v = triple (data.[4], data.[5], data.[6])
      let dx = fiberLength / float n
      for i in 0 .. n - 1 do lst.Push (p1 + float i * dx * v, p1 + float (i + 1) * dx * v)
    lst.ToArray()

let getSimLines (fiberLength, fiberPieces) (refresh, path) =
  let dir = DirectoryInfo path
  let losDir = Path.Combine (dir.FullName, "los-cog-axis")
  if refresh && Directory.Exists losDir then Directory.Delete (losDir, true)
  let losDir = DirectoryInfo losDir
  if losDir.Exists = false then
    let mapLosFiles (fileA: FileInfo []) = [| fileA |> Array.maxBy (fun file -> file.CreationTime) |]
    LBM.PostProcess.Report.Combine.particlesTypeCoGandAxis (dir, mapLosFiles)
  let losFile = losDir.GetFiles "losReal*"
  if losFile.Length <> 1 then failwith "bad"
  else
    getSimulationLines (fiberLength, fiberPieces, losFile.[0].FullName)

let linesToArray3D (nX, nY, nZ) (minX, minY, minZ) (maxX, maxY, maxZ) (res: (triple * triple) seq) =

    let dx, dy, dz = (maxX - minX) / float nX, (maxY - minY) / float nY, (maxZ - minZ) / float nZ

    let out = Array3D.create nX nY nZ []
    res |> Seq.iter (fun (p1, p2) ->
      let idX, idY, idZ =
        let cog = (p1 + p2) / 2.
        min (max (min (int ((cog.X - minX) / dx)) (nX - 1)) 0) (nX - 1),
        min (max (min (int ((cog.Y - minY) / dy)) (nY - 1)) 0) (nY - 1),
        min (max (min (int ((cog.Z - minZ) / dz)) (nZ - 1)) 0) (nZ - 1)
      out.[idX, idY, idZ] <- (p2 - p1) :: out.[idX, idY, idZ] )

    out, dx, dy, dz

let plotSimulationEllipses (refresh, path, size, lineSpec, contours) =
  m.holdOn
  m.execute "cm = colormap('gray(256)'); colormap(cm(256:-1:128,:))"
  if contours then m.colorbar ()
  let plateX, plateY, plateZ, nX, nY, nZ = size, size, 0.15, 10, 10, 3
  let fiberLength, fiberAspectRatio, fiberPieces = 0.06, 80., 6
  let fiberVolume = System.Math.PI * (fiberLength / fiberAspectRatio / 2.)**2. * fiberLength
  let dX, dY, dZ = plateX / float nX, plateY / float nY, plateZ / float nZ
  let lines = getSimLines (fiberLength, fiberPieces) (refresh, path)
  for cogX, iZ in [0.,0.; size + 0.1,2.] do

    let lines = lines |> Array.filter (fun (a,b) ->
      let minZ, maxZ = iZ * dZ, (iZ + 1.) * dZ
      a.Z > minZ && b.Z > minZ && a.Z < maxZ && b.Z < maxZ )

    let minLine = lines |> Array.minBy (fun (a,b) -> min a.Z b.Z)
    printfn "%A" minLine

    let arr3D, _,_,_ = linesToArray3D (nX, nY, 1) (0.,0.,0.) (plateX, plateY, plateZ) lines

    if contours then
      let xA, yA, zA = Array2D.init3 (arr3D.GetLength 0) (arr3D.GetLength 1) (fun i j ->
        let n = float arr3D.[i,j,0].Length
        let volumePercentage = n * fiberVolume / float fiberPieces  / (dX * dY * dZ)
        float i * dX + 0.5 * dX + cogX, float j * dY + 0.5 * dY, volumePercentage )
      m.contourf (xA, yA, zA, 100, other = ",'edgecolor','none'")

    arr3D |> Array3D.iteri (fun i j k fibers ->
      let fibers2D = fibers |> List.map (fun fiber ->
        let length = sqrt (fiber.X**2. + fiber.Y**2.)
        fiber.X / length, fiber.Y / length ) 
      let ell = Utilities.LBM.Fibers.pToEllipse fibers2D
      ell |> Option.iter (fun (phi, a, b) ->
        let scale = 0.0468
        m.plotEllipse (float i * dX + 0.5 * dX + cogX, float j * dY + 0.5 * dY, scale * a, scale * b, phi, linespec = lineSpec, other = ",'LineWidth', 1.0") ))