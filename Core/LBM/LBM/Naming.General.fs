﻿namespace LBM.Naming.General

module Files  =

  let report = "_report.txt"
  let input = "_input.txt"
    
  let fluidMass = "#fluidMass.txt"
  let shape = "#shape.txt"
  let spread = "#spread.txt"
  let fractionOfCellsAboveTau = "#fractionOfCellsAboveTau.txt"
  let minMaxMeanLbmSpeed = "#minMaxMeanLbmSpeed.txt"
  let minMaxMeanShearRate = "#minMaxMeanShearRate.txt"
  let meanTau = "#meanTau.txt"
  let kineticEnergyOfLO = "#kineticEnergyOfLO.txt"
  let numberOfCollisionsDissipatedEnergy = "#numberOfCollisionsDissipatedEnergy.txt"
  let numberOfParticles = "#numberOfParticles.txt"
  let particleVolumeFractionFiberSphereEllipsoid = "#particleVolumeFractionFiberSphereEllipsoid.txt"
  //let terminalVelocity = "#terminalVelocity.txt"
  //let dragForce = "#dragForce.txt"
  let velocityProfile = "#velocityProfile.txt"
  //let collisions = "#collisions.txt"
  let minMaxMeanRhoDiff = "#minMaxMeanRhoDiff.txt"

module Args =
  let inputFile = "InputFile"
  let dirPath = "DirectoryPath"
  let name = "Name"
  let paralel = "Parallel"