﻿namespace LBM.LO

open LBM
open LBM.LO.Types
open LBM.BC.Types.Internal

open Kit
open Kit.Collections
open Kit.Math

type internal Collisions =

  static member private collideBC (h, n: float triple, rp: float triple) (lo: LO) (bc: BcObject) (behavior: InteractionsBC) =
    match bc.Purpose with
      | BcBounceBackI (uPres, _) ->
        let vp, vpn =
          let vp = lo.U + (Triple.cross(lo.OmegaGlob, rp)) - uPres
          vp, Triple.dot(vp, n)
        if vpn < 0. then
          let mInvSum = 1. / lo.Mass // bc.Mass -> inf
          let Iinv =
            matrix [
              [1. / lo.I.x; 0.; 0.]
              [0.; 1. / lo.I.y; 0.]
              [0.; 0.; 1. / lo.I.z]] |> lo.Rot.rotateL2G
          let impulseN = 
            let j0 = (1. + behavior.cR) * vpn
            let IrNrSum =
              let rpN = Triple.cross(rp, n)
              Triple.cross (Triple.matrixMult Iinv rpN, rp) //bc.Iinv = zeroMatrix
            j0 / ( mInvSum + Triple.dot(IrNrSum, n) )
          let F,T =
            let t0 = Triple.cross(n, vp)
            let friction (muDyn, muStat) =
              let t = Triple.cross(t0, n) |> Triple.normalize
              let impulseT =
                let rpT = Triple.cross(rp, t)
                let j0T = Triple.dot(vp, t)
                let IrNrSum = Triple.cross (Triple.matrixMult Iinv rpT, rp)
                j0T / ( mInvSum + Triple.dot(IrNrSum, t) ) 
              let reduction = if abs impulseT < muStat * abs impulseN then 1.0 else muDyn * abs impulseN / abs impulseT
              let f = - impulseN * n - reduction * impulseT * t
              f, Triple.cross(rp, f)
            match behavior.friction with
              | Some (CoulombFriction(muDyn, muStat)) when Triple.norm t0 > 10e-10 -> friction (muDyn, muStat)
              | Some (CoulombFrictionFnBc fn) when Triple.norm t0 > 10e-10 ->
                match fn (lo.Shape, bc) with
                  | Some (muDyn, muStat) -> friction (muDyn, muStat)
                  | None ->
                    let F = - impulseN * n
                    F, Triple.cross(rp, F)
              | _ ->
                let F = - impulseN * n
                F, Triple.cross(rp, F)
          let mult = min (1. - 1.0 * h) 1.2
          Some (mult * F,T)
        else None
      | _ -> failwith "colliding unsupported BC type"  
      
  static member private collidePP (h, n: float triple, rpi, rpj) (loi: LO, loj: LO) (behavior: InteractionsPP) =
    let vpi, vpj = loi.U + Triple.cross(loi.OmegaGlob, rpi), loj.U + Triple.cross(loj.OmegaGlob, rpj)
    let dv = vpi - vpj
    if Triple.dot(dv, n) > 0.0 then
      let IiInv, IjInv = 
        let Iinv (I: float triple) =
          matrix [
            [1. / I.x; 0.; 0.]
            [0.; 1. / I.y; 0.]
            [0.; 0.; 1. / I.z]] 
        (Iinv loi.I) |> Versor.rotateL2G loi.Rot, (Iinv loj.I) |> Versor.rotateL2G loj.Rot
      let mInvSum = 1./loi.Mass + 1./loj.Mass
      let impulseN =         
        let rpiN, rpjN = Triple.cross(rpi, n), Triple.cross(rpj, n)
        let j0 = - (1. + behavior.cR) * Triple.dot(dv, n)
        let IrNrSum = Triple.cross (Triple.matrixMult IiInv rpiN, rpi) + Triple.cross(Triple.matrixMult IjInv rpjN, rpj)
        j0 / ( mInvSum + Triple.dot(IrNrSum, n) )            
      let impulseN = if h < - 0.05 then impulseN - 2. * impulseN * h else impulseN
      let FiTi, FjTj =
        let friction (muDyn, muStat) =
          let t = 
            let t0 = Triple.cross(n, dv)
            Triple.cross(t0, n) |> Triple.normalize
          let impulseT =
            let rpiT, rpjT = Triple.cross(rpi, t), Triple.cross(rpj, t)
            let j0T = Triple.dot(-dv, t)
            let IrNrSum = (Triple.cross (Triple.matrixMult IiInv rpiT, rpi) + Triple.cross (Triple.matrixMult IjInv rpjT, rpj) )
            j0T / ( mInvSum + Triple.dot(IrNrSum, t) ) 
          let reduction = if abs impulseT < muStat * abs impulseN then 1.0 else muDyn * abs impulseN / abs impulseT
          let f = impulseN * n + reduction * impulseT * t
          (f, Triple.cross(rpi, f)), (-f, Triple.cross(rpj, -f))
        match behavior.friction with
          | Some (CoulombFriction(muDyn, muStat)) -> friction (muDyn, muStat)
          | Some (CoulombFrictionFn fn) ->
            match fn (loi.Shape, loj.Shape) with
              | Some (muDyn, muStat) -> friction (muDyn, muStat)
              | None ->
                let f = impulseN * n
                (f, Triple.cross(rpi, f)), (-f, Triple.cross(rpj, -f))
          | _ ->
            let f = impulseN * n
            (f, Triple.cross(rpi, f)), (-f, Triple.cross(rpj, -f))
      Some(FiTi, FjTj)
    else None

  static member private updateLO (lo: LO) (F,T) workOfCollisions =
    if lo._Interaction = TwoWay then
      let newU, newOmegaGlob = 
        let duGlob, domegaGlob = Dynamics.getDU F lo, Dynamics.getDOmega T lo
        lo.U + duGlob, lo.OmegaGlob + domegaGlob
      workOfCollisions := (
        let omegaLoc, newOmegaLoc = lo.OmegaGlob |> Versor.rotateG2L lo.Rot, newOmegaGlob |> Versor.rotateG2L lo.Rot             
        !workOfCollisions + 0.5 * lo.Mass * Triple.norm2 (newU - lo.U) + 0.5 * Triple.dot(lo.I, Triple.map2(fun o oo -> (o - oo)**2.0) newOmegaLoc omegaLoc))
      lo.copy (u = newU, omegaGlob = newOmegaGlob)
    else
      if LBM.Basic.trashFn.ContainsKey "collisionForceAddF0" then
        LBM.Basic.trashFn.["collisionForceAddF0"] (box F)
      lo
  
  static member private updateLOBC (lo: LO) (F,T) workOfCollisions =
    if lo._Interaction = TwoWay then
      let newU, newOmegaGlob = 
        let duGlob, domegaGlob = Dynamics.getDU F lo, Dynamics.getDOmega T lo
        lo.U + duGlob, lo.OmegaGlob + domegaGlob
      workOfCollisions := (
        let omegaLoc, newOmegaLoc = lo.OmegaGlob |> Versor.rotateG2L lo.Rot, newOmegaGlob |> Versor.rotateG2L lo.Rot
        !workOfCollisions + 0.5 * lo.Mass * Triple.norm2 (newU - lo.U) + 0.5 * Triple.dot(lo.I, Triple.map2 (fun o oo -> (o - oo)**2.0) newOmegaLoc omegaLoc ))
      lo.copy (u = newU, omegaGlob = newOmegaGlob)//, coG = cog)
    else lo

  static member private updateTrackingParticle (lo: LO) (F,T) (duration: float) =
    if lo._Interaction = TrackingParticle then
      let newU, newOmegaGlob = 
        let duGlob, domegaGlob = Dynamics.getDU F lo, Dynamics.getDOmega T lo
        lo.U + duGlob, lo.OmegaGlob + domegaGlob
      let cog1 = lo.CoG + duration * newU
      let rot1 = 
        let drotQuat = duration * (newOmegaGlob |> Versor.ofAngularVelocityVector|> Quaternion.normalize)
        drotQuat * lo.Rot
      lo.copy (u = newU, omegaGlob = newOmegaGlob, coG = cog1, rot = rot1)
    else lo

  static member private updateTrackingParticleBC (lo: LO) (F,T) (duration: float) (h: float) (n: float triple) =
    if lo._Interaction = TrackingParticle then
      let newU, newOmegaGlob = 
        let duGlob, domegaGlob = Dynamics.getDU F lo, Dynamics.getDOmega T lo
        lo.U + duGlob, lo.OmegaGlob + domegaGlob
      let cog =
        let cog1 = lo.CoG + duration * newU
        let dh = h + 0.05
        if dh < 0. then cog1 - n * dh else cog1
      let rot = 
        let drotQuat = duration * (newOmegaGlob |> Versor.ofAngularVelocityVector|> Quaternion.normalize)
        drotQuat * lo.Rot
      lo.copy (u = newU, omegaGlob = newOmegaGlob, coG = cog, rot = rot)
    else lo

  static member modifyLObyCollisions (time, lp: LagrangianProblem, loA: LO [], bcA: BcObject []) =
    
    let numOfIPCollisions, workOfCollisions = ref 0., ref 0.
    
    lp.iter3D (fun x y z ->
      let xyz = (x, y, z)      
      if xyz = (1, 1, 1) then  
        lp.getPairsPP xyz time false |> Array.iter (fun (i, j, globIJ) ->
          let (h, _, _, _ as data) = HelpingFunctions.getDistanceNormalDxiDxj loA.[i] loA.[j]
          if lp.collisionFnPP xyz globIJ loA.[i] loA.[j] h then
            Collisions.collidePP data (loA.[i], loA.[j]) lp.InteractionsPP |> Option.iter (fun (iFT, jFT) ->
                numOfIPCollisions := !numOfIPCollisions + 1.0
                loA.[i] <- Collisions.updateLO loA.[i] iFT workOfCollisions
                loA.[j] <- Collisions.updateLO loA.[j] jFT workOfCollisions
               )                  
        )
      else
        lp.getPairsPG xyz time false |> Array.iter (fun (i, ghost, globIJ) ->
          if loA.[i]._Interaction = TwoWay && ghost._Interaction <> TrackingParticle then
            let (h, _, _, _ as data) = HelpingFunctions.getDistanceNormalDxiDxj loA.[i] ghost
            if lp.collisionFnPP xyz globIJ loA.[i] ghost h then
              Collisions.collidePP data (loA.[i], ghost) lp.InteractionsPP |> Option.iter (fun (iFT, _) ->
                numOfIPCollisions := !numOfIPCollisions + 0.5
                loA.[i] <- Collisions.updateLO loA.[i] iFT workOfCollisions )
          )
      )

    lp.getPairsBC time false |> Array.iter (fun (i, j, globIJ) ->
      let lo, bc = loA.[i], bcA.[j]
      if lo._Interaction = TwoWay then 
        let (h, _, _ as data) = HelpingFunctions.getDistanceNormalDxi_BC lo bc
        if lp.collisionFnBC globIJ lo bc h then
          Collisions.collideBC data lo bc lp.InteractionsBC |> Option.iter (fun FT ->
            numOfIPCollisions := !numOfIPCollisions + 1.0
            loA.[i] <- Collisions.updateLOBC lo FT workOfCollisions
            )
      )

    !numOfIPCollisions, !workOfCollisions
  
  static member modifyLPByInitialCollisions (time, duration, lp: LagrangianProblem, loA: LO [], bcA: BcObject []) =

    let numOfIPCollisions, workOfCollisions = ref 0., ref 0.

    lp.iter3D (fun x y z ->
      let xyz = (x, y, z)      
      let globIJdurationA = 
        if xyz = (1, 1, 1) then  
          lp.getPairsPP xyz time true |> Array.map (fun (i, j, globIJ) ->
            let (h, _, _, _ as data) = HelpingFunctions.getDistanceNormalDxiDxj loA.[i] loA.[j]
            if lp.collisionFnPP xyz globIJ loA.[i] loA.[j] h then
              Collisions.collidePP data (loA.[i], loA.[j]) lp.InteractionsPP |> Option.iter (fun (iFT, jFT) ->
                match loA.[i]._Interaction, loA.[j]._Interaction with
                  | TwoWay, TwoWay ->
                      numOfIPCollisions := !numOfIPCollisions + 1.0
                      loA.[i] <- Collisions.updateLO loA.[i] iFT workOfCollisions
                      loA.[j] <- Collisions.updateLO loA.[j] jFT workOfCollisions
                  | TrackingParticle, _ | _, TrackingParticle ->
                    loA.[i] <- Collisions.updateTrackingParticle loA.[i] iFT duration
                    loA.[j] <- Collisions.updateTrackingParticle loA.[j] jFT duration )
            globIJ, lp.getDurationPP xyz globIJ loA.[i] loA.[j] h )
        else
          lp.getPairsPG xyz time true |> Array.map (fun (i, ghost, globIJ) ->
            let (h, _, _, _ as data) = HelpingFunctions.getDistanceNormalDxiDxj loA.[i] ghost
            if lp.collisionFnPP xyz globIJ loA.[i] ghost h then
              Collisions.collidePP data (loA.[i], ghost) lp.InteractionsPP |> Option.iter (fun (iFT, _) ->
                match loA.[i]._Interaction, ghost._Interaction with
                  | TwoWay, TwoWay ->
                    numOfIPCollisions := !numOfIPCollisions + 0.5
                    loA.[i] <- Collisions.updateLO loA.[i] iFT workOfCollisions
                  | TrackingParticle, _  ->
                    loA.[i] <- Collisions.updateTrackingParticle loA.[i] iFT duration
                  | _ -> ()
              )
            globIJ, lp.getDurationPP xyz globIJ loA.[i] ghost h )
      lp.replacePairsPP xyz time globIJdurationA ) 

    let globIJdurationA = 
      lp.getPairsBC time true |> Array.choose (fun (i, j, globIJ) ->
        let lo, bc = loA.[i], bcA.[j]
        let (h, n, _ as data) = HelpingFunctions.getDistanceNormalDxi_BC lo bc
        if lp.collisionFnBC globIJ lo bc h then
          Collisions.collideBC data lo bc lp.InteractionsBC |> Option.iter (fun FT ->
            match lo._Interaction with
              | TwoWay ->
                numOfIPCollisions := !numOfIPCollisions + 1.0
                loA.[i] <- Collisions.updateLOBC lo FT workOfCollisions
              | TrackingParticle -> 
                loA.[i] <- Collisions.updateTrackingParticleBC lo FT duration h n )
        Some (globIJ, lp.getDurationBC globIJ lo h) )
    lp.replacePairsBC time globIJdurationA

    !numOfIPCollisions, !workOfCollisions