﻿namespace LBM.LO.Types

open LBM
open LBM.Naming
open LBM.BC.Types.Internal

open Kit
open Kit.Math
open Kit.IO

open System.Collections.Generic
open System.IO

type LagrangianProblem (lt: LBM.Lattice, bcAInit: BcObject [], _loA: LO [], controls: UptadeCtrls, interactionsPPO: InteractionsPP option,
                        interactionsBC: InteractionsBC, maxMutualSpeed, maxDiameter, plasticViscosity, ?fiberFit, ?deactivatePairsPP) =

  let nBoxes = 1000
  let mutable isGhost = Dictionary ()
  let mutable ghostIdsTheyWant = null
  let mutable idAall = dict []
  let mutable idAactive = dict []
  let mutable lastStepDuration = 0.005

  let deactivatePairsPP = defaultArg deactivatePairsPP false

  let defFitFn (d) =
    (5.0 * plasticViscosity + 5.5) / (d*d/plasticViscosity)**0.89 + 10.0 * plasticViscosity / (d*d/plasticViscosity)**0.1

  let fitFn = defaultArg fiberFit defFitFn

  let loTimeExch = lt.Par.WCF.Exchange1D("____LOtime____", List.fold (fun (oldIsFail, oldDur) (newIsFail, newDur) -> oldIsFail || newIsFail, min oldDur newDur) (false, 1000.))

  let lodA = Array3D.init 3 3 3 (fun _ _ _ -> Dictionary() )
  let pairsPP = Array3D.init 3 3 3 (fun _ _ _ -> 
    match interactionsPPO with 
      | Some inter -> PairsP (maxMutualSpeed, nBoxes, inter.interactingDistance, deactivatePairsPP)
      | None -> PairsP (maxMutualSpeed, 0, 0., deactivatePairsPP))
  let fictitiousDistances = Array3D.init 3 3 3 (fun _ _ _ -> Dictionary())

  let pairsBC = PairsBC (bcAInit, maxMutualSpeed, nBoxes, interactionsBC.interactingDistance)
  let mutable fictitiousDistancesBC = Dictionary()

  let ghostsdA = Array3D.init 3 3 3 (fun _ _ _ -> Dictionary<int,LO>() )
  let removedGhosts = Array3D.init 3 3 3 (fun _ _ _ -> Dictionary<int,LO>() )

  let fnOuter (p: float triple) =
    let bcFn x nodesX = if x < -0.6 then 0 elif x > nodesX - 0.4 then 2 else 1
    bcFn p.x lt.NodesXf_Loc, bcFn p.y lt.NodesYf_Loc, bcFn p.z lt.NodesZf_Loc
    
  let _addLoA =
    let currId = ref -1
    fun time (newLoA: LO []) ->
      let newLoAd = 
        newLoA |> Array.choose (fun lo ->
          if fnOuter lo.CoG = (1, 1, 1) then
            incr currId; Some (!currId, lo)
          else None ) |> dict
      if Option.isSome interactionsPPO then
         pairsPP |> Array3D.iteri (fun i j k p ->
          if (i, j, k) = (1,1,1) then p.addNewPairs_LoLo (time, lodA.[1,1,1], newLoAd)
          else p.addNewPairs_LoGh (time, newLoAd, ghostsdA.[i,j,k]) )
      pairsBC.addNewLOs (time, newLoAd)
      newLoAd |> Seq.iter (fun v -> 
        lodA.[1,1,1].Add (v.Key, v.Value)
        isGhost.Add (v.Key, Array3D.create 3 3 3 false) )

  do if _loA.Length > 0 then _addLoA 1 _loA

  let cogMap i j k (oldCog: float triple) = // map between domains (other to mine)
    oldCog + (lt.OriginsA.[i, j, k] - lt.OriginsA.[1,1,1]) |> Triple.map float
  
  let loA2ghostData (_idsToSend: int [], loA: LO [], idLoc: IDictionary<int, int>) =
    let idsToSend = _idsToSend |> Array.filter (fun id -> idLoc.ContainsKey id)
    use m = new MemoryStream ()
    use w = new BinaryWriter (m)
    w.Write idsToSend.Length
    for id in idsToSend do
      let lo = loA.[idLoc.[id]]
      w.Write id
      w.Write lo.CoG.x; w.Write lo.CoG.y; w.Write lo.CoG.z
      w.Write lo.U.x; w.Write lo.U.y; w.Write lo.U.z
      w.Write lo.OmegaGlob.x; w.Write lo.OmegaGlob.y; w.Write lo.OmegaGlob.z
      w.Write lo.Rot.w; w.Write lo.Rot.t.x; w.Write lo.Rot.t.y; w.Write lo.Rot.t.z
    m.ToArray()
  
  let updateGhostData ((ii, jj, kk), arr: byte []) =
    lodA.[ii, jj, kk].Clear()
    if Option.isSome interactionsPPO then
      use r = new BinaryReader (new MemoryStream (arr))
      for _ in 0 .. r.ReadInt32 () - 1 do
        let id = r.ReadInt32 ()
        let cog = cogMap ii jj kk (triple(r.ReadDouble(), r.ReadDouble(), r.ReadDouble()))
        let u = triple ( r.ReadDouble(), r.ReadDouble(), r.ReadDouble() )
        let omega = triple ( r.ReadDouble(), r.ReadDouble(), r.ReadDouble() )
        let rot = quaternion ( r.ReadDouble() , triple ( r.ReadDouble(), r.ReadDouble(), r.ReadDouble() ))
        if ghostsdA.[ii,jj,kk].ContainsKey id then
          ghostsdA.[ii,jj,kk].[id] <- ghostsdA.[ii, jj, kk].[id].copy (coG = cog, u = u, omegaGlob = omega, rot = rot)
        else ghostsdA.[ii,jj,kk].Add (id, removedGhosts.[ii, jj, kk].[id].copy (coG = cog, u = u, omegaGlob = omega, rot = rot))
        lodA.[ii, jj, kk].[id] <- ghostsdA.[ii,jj,kk].[id]

  member o.FiberFit d = fitFn d

  member o.InteractionsPP = interactionsPPO.Value
  member o.InteractionsBC = interactionsBC
  member o.Controls = controls

  member o.Particles = lodA.[1,1,1] |> Seq.map (fun v -> v.Key, v.Value) |> dict
  //member o.SetParticles loD = lodA.[1,1,1] <- loD
  //member o.AllParticles = lodA |> Array3D.map (fun dic -> dic |> Seq.map (fun v -> v.Key, v.Value) |> dict )

  member o.addLoA (time, newLoA) = _addLoA time newLoA

  member internal o.LOTimeExch = loTimeExch

  member internal p.MyParticles =
      idAall <- dict (lodA.[1,1,1].Keys |> Seq.mapi (fun i k -> k, i))
      lodA.[1,1,1].Values |> Seq.toArray
  member internal p.SetMyParticles (loAall: LO [], loAactive: LO []) =
      lodA.[1,1,1].Clear() 
      for v in idAall do 
        if loAall.[v.Value]._Interaction = TrackingParticle then lodA.[1,1,1].Add (v.Key, loAall.[v.Value])
      for v in idAactive do lodA.[1,1,1].Add (v.Key, loAactive.[v.Value])

  member internal p.removeTrackingParticles (loA: LO []) =
    let locIds = List()
    let loA, ids = 
      idAall
        |> Seq.filteri (fun i v -> if loA.[v.Value]._Interaction <> TrackingParticle then locIds.Add i; true else false)
        |> Seq.mapi (fun i v -> loA.[v.Value], (v.Key, i))
        |> Array.ofSeq |> Array.unzip
    idAactive <- dict ids
    loA, locIds.ToArray()

  member o.LastStepDuration with get() = lastStepDuration and set (v: float) = lastStepDuration <- v

  member internal o.iter3D f = for x = 0 to 2 do for y = 0 to 2 do for z = 0 to 2 do f x y z

  member internal o.moveToTheFuture curNextTime =
    pairsBC.moveToTheFuture curNextTime
    match interactionsBC.fictitiousParticleFn with
      | Some _ ->
        let iG, jG = pairsBC.getPairs (snd curNextTime)
        fictitiousDistancesBC <-
          let fd = List()
          for i = 0 to iG.Count - 1 do
            let ijG = iG.[i], jG.[i]
            if fictitiousDistancesBC.ContainsKey ijG then fd.Add (ijG, fictitiousDistancesBC.[ijG])
          Dictionary (dict fd) 
      | None -> ()
    match interactionsPPO with
      | Some inter ->
        pairsPP |> Array3D.iteri (fun x y z p -> 
          p.moveToTheFuture curNextTime
          match inter.fictitiousParticleFn with
            | Some _ ->
              let iG, jG = p.getPairs (snd curNextTime)
              fictitiousDistances.[x, y, z] <-
                let fd = List()
                for i = 0 to iG.Count - 1 do
                  let ijG = iG.[i], jG.[i]
                  if fictitiousDistances.[x, y, z].ContainsKey ijG then fd.Add (ijG, fictitiousDistances.[x, y, z].[ijG])
                Dictionary (dict fd)  
            | None -> () )
      | None -> ()
    
  /// Mutates lt.Par.ForceLO !!!
  member internal o.propagateLO (loA: LO [], initial: bool) =
    
    interactionsPPO |> Option.iter (fun _ ->
      lt.Timer.tick 4

      let simpleLoA =
        ghostIdsTheyWant |> Array3D.map (fun theyWantIDAO ->
          theyWantIDAO |> Option.map (fun theyWantIDA -> 
            if initial then loA2ghostData (theyWantIDA, loA, idAall)
            else loA2ghostData (theyWantIDA, loA, idAactive)))

      lt.Timer.tockPrintTick 4 "WCF.preprocess"

      if lt.Timer.isActive 4 then
        simpleLoA |> Array3D.iteri (fun i j k v -> v |> Option.iter (fun bA -> 
          let loALength = (bA.Length - 4) / 108
          if loALength > 0 then lt.Timer.printAny 4 (sprintf "WCF.simpleLoA.Length (%d,%d,%d)" i j k) loALength ))

      //let incomming = lt.Par.WCF.sendAndReceive_GhostsLO3D (timeID, simpleLoA)
      let incomming = lt.Par.WCF.sendAndReceive_Exchange3D ("-GhostExchange-", simpleLoA)

      lt.Timer.tockPrintTick 4 "WCF.sendAndReceive_LO"
    
      incomming |> Array3D.iteri (fun i j k incom ->
        incom |> Option.iter (fun d ->
          if (i, j, k) = (1,1,1) then failwith "received ghosts from (1,1,1)"
          d |> Option.iter (fun arrivedGhostsData -> updateGhostData ((i, j, k), arrivedGhostsData) ) ) )

      lt.Timer.tockPrint 4 "WCF.posprocess" )


  member internal o.propagateLBM_LO time =

    lt.Timer.tick 1
    lt.Timer.tick 2
            
    let simpleLoaA =
      let lostLoaA, ghostsTheyGet = Array3D.init 3 3 3 (fun _ _ _ -> List()), Array3D.init 3 3 3 (fun _ _ _ -> List())
      lodA.[1,1,1] |> Seq.iter (fun v ->
        let id, lo = v.Key, v.Value
        let iBc, jBc, kBc = fnOuter lo.CoG         
        if (iBc,jBc,kBc) = (1,1,1) then
          if Option.isSome interactionsPPO then
            o.iter3D (fun i j k ->
              let ni, nj, nk =              
                let p = lo.CoG +  (maxDiameter + interactionsPPO.Value.interactingDistance + 0.1) * triple (i-1, j-1, k-1)
                fnOuter p
              if (ni, nj, nk) <> (1, 1, 1) && isGhost.[id].[ni, nj, nk] = false then
                isGhost.[id].[ni, nj, nk] <- true
                ghostsTheyGet.[ni,nj,nk].Add (id, lo.lo2simpleLO) )  
        else lostLoaA.[iBc,jBc,kBc].Add (id, lo) )

      let lostLoaA = lostLoaA |> Array3D.map (fun loId ->
        Array.init loId.Count (fun i ->
          let id, lo = loId.[i]
          if lodA.[1, 1, 1].Remove id = false then failwith "Object not in dictionary"
          if isGhost.Remove id = false then failwith "isGhost not in dictionary"
          lo.lo2simpleLO ))

      Array3D.init 3 3 3 (fun i j k -> 
        if (i, j, k) = (1, 1, 1) then None
        else Some (lostLoaA.[i,j,k], ghostsTheyGet.[i,j,k].ToArray()) )

    lt.Timer.tockPrintTick 2 "Preprocess"
       
    let incom = lt.Par.propagate (simpleLoaA)

    lt.Timer.tockPrintTick 2 "Propagate"

    incom |> Array3D.iteri (fun i j k dataO ->
      dataO |> Option.iter (fun (arrivedLoA, arrivedGhostsA) ->
        if arrivedLoA.Length > 0 then
          let newLoA = arrivedLoA |> Array.map (fun sLO -> LO.simpleLO2lo (cogMap i j k, sLO))
          o.addLoA (time, newLoA)
        let newGA =
          let dic = Dictionary ()
          (for id, sLO in arrivedGhostsA do 
            let idlo = id, LO.simpleLO2lo (cogMap i j k, sLO)
            ghostsdA.[i, j, k].Add idlo
            dic.Add idlo); dic
        pairsPP.[i, j, k].addNewPairs_LoGh (time, lodA.[1,1,1], newGA)
        ))
    
    lt.Timer.tockPrintTick 2 "Arrived"

    if Option.isSome interactionsPPO then

      let ghostsIwant = Array3D.init 3 3 3 (fun i j k ->
        if (i,j,k) <> (1,1,1) then
          let _, ghostIdA = pairsPP.[i,j,k].getPairs time
          set ghostIdA |> Set.toArray |> Array.map int
        else [||] )

      lt.Timer.tockPrintTick 2 "ghostsIwant"
    
      //let incom = lt.Par.WCF.sendAndReceive_IdsLO3D (time, ghostsIwant)
      let incom = lt.Par.WCF.sendAndReceive_Exchange3D ("-propagateLBM_LO-", ghostsIwant)
      ghostIdsTheyWant <- incom 

      lt.Timer.tockPrintTick 2 "sendAndReceive_LOids"
    lt.Timer.tockPrint 1 "propagateLBM_LO"

    lt.Timer.printAny 1 "Number of solids" lodA.[1,1,1].Count
  
  member o.distributeLoA (time, loAinit: LO []) =
    lt.Timer.tick 0
    
    o.addLoA (time, loAinit)

    let actualLoA = ref loAinit

    for iT = 0 to lt.LoNtimes - 1 do     

      let simpleLoaA =
        let lostLoaA = Array3D.init 3 3 3 (fun _ _ _ -> List())
        (!actualLoA) |> Array.iter (fun lo ->
          let iBc, jBc, kBc = fnOuter lo.CoG
          if (iBc,jBc,kBc) <> (1,1,1) then lostLoaA.[iBc,jBc,kBc].Add <| lo.lo2simpleLO )        
        Array3D.init 3 3 3 (fun i j k -> if (i, j, k) = (1, 1, 1) then None else Some (lostLoaA.[i,j,k].ToArray(), [||]) )

      //let incom = lt.Par.WCF.sendAndReceive_LBM3D (timeId, Array3D.create 3 3 3 None, simpleLoaA)
      let incom = lt.Par.WCF.sendAndReceive_Exchange3D ("-distributeLoA-", simpleLoaA)
      actualLoA := [||]
      incom |> Array3D.iteri (fun i j k dataO ->
        dataO |> Option.iter (Option.iter (fun (arrivedLoA, _) ->
            actualLoA := Array.append !actualLoA (arrivedLoA |> Array.map (fun sLO -> LO.simpleLO2lo (cogMap i j k, sLO)))
        )))
      o.addLoA (time, !actualLoA)

    lt.Timer.tockPrint 0 "distributeLoA"

  member private o.getPairs (x,y,z) time (idLoc: IDictionary<int,int>) (v: IDictionary<_,_>) =
    if Option.isSome interactionsPPO then
      let ijIJ = List()
      pairsPP.[x, y, z].getPairs time ||> Seq.iter2 (fun i j ->
        let inti, intj = int i, int j
        if idLoc.ContainsKey inti && v.ContainsKey intj then
          ijIJ.Add (idLoc.[inti], v.[intj], (i, j) )
        elif idLoc.ContainsKey intj = false && ghostsdA.[x,y,z].ContainsKey intj then 
          removedGhosts.[x,y,z].[intj] <- ghostsdA.[x,y,z].[intj]
          ghostsdA.[x,y,z].Remove intj |> ignore
         )
      ijIJ.ToArray()
    else [||]

  member internal o.getPairsPP xyz time initial =
    if xyz <> (1,1,1) then failwith "pairsPP not available at %A" xyz
    if initial then o.getPairs xyz time idAall idAall
    else o.getPairs xyz time idAactive idAactive
  
  member internal o.getPairsPG (x,y,z as xyz) time initial =
    if xyz = (1,1,1) then failwith "pairsPP not available at (1,1,1)"
    if initial then o.getPairs xyz time idAall lodA.[x,y,z]
    else o.getPairs xyz time idAactive lodA.[x,y,z]

  member internal o.getPairsBC time initial =
    let iJIJ = List()
    pairsBC.getPairs time ||> Seq.iter2 (fun i j ->
      if lodA.[1,1,1].ContainsKey (int i) then 
        if initial then iJIJ.Add (idAall.[int i], int j, (i, j) )
        elif idAactive.ContainsKey (int i) then iJIJ.Add (idAactive.[int i], int j, (i, j) ) )
    iJIJ.ToArray()

  member internal o.getDurationPP (x, y, z) ijG loi loj h = 
    let gap =
      match interactionsPPO.Value.fictitiousParticleFn with
        | Some _ -> h - max fictitiousDistances.[x, y, z].[ijG] interactionsPPO.Value.interactingDistance
        | None -> h - interactionsPPO.Value.interactingDistance
    pairsPP.[x, y, z].getDuration loi loj gap
  member internal o.getDurationBC ijG loi h = 
    let gap =
      match interactionsBC.fictitiousParticleFn with
        | Some _ -> h - max fictitiousDistancesBC.[ijG] interactionsBC.interactingDistance
        | None -> h - interactionsBC.interactingDistance
    pairsBC.getDuration loi gap
  member internal o.replacePairsPP (x, y, z) time ijDurA = if Option.isSome interactionsPPO then pairsPP.[x, y, z].replacePairs (time, ijDurA)
  member internal o.replacePairsBC time iDurA = pairsBC.replacePairs (time, iDurA)

  member internal o.collisionFnPP ((x, y, z) as xyz) ijG loi loj h = 
    match interactionsPPO.Value.fictitiousParticleFn with
      | Some (rndFn, detFn) ->
        let fd = 
          if fictitiousDistances.[x, y, z].ContainsKey ijG then fictitiousDistances.[x, y, z].[ijG]
          else
            let fd =
              if xyz = (1,1,1) then rndFn h              
              else detFn h ijG
            fictitiousDistances.[x, y, z].Add (ijG, fd)
            fd
        interactionsPPO.Value.collisionFnP loi loj (h - fd)
      | None -> interactionsPPO.Value.collisionFnP loi loj h
  
  member internal o.collisionFnBC ijG lo bc h = 
    match interactionsBC.fictitiousParticleFn with
      | Some rndFn ->
        let fd = 
          if fictitiousDistancesBC.ContainsKey ijG then fictitiousDistancesBC.[ijG]
          else
            let fd = rndFn h 
            fictitiousDistancesBC.Add (ijG, fd)
            fd
        interactionsBC.collisionFn lo bc (h - fd)
      | None -> interactionsBC.collisionFn lo bc h
  
  static member groupShapes (loA: LO []) =
    loA |> Array.fold (fun (s, f, e) lo ->
      match lo.Shape with
        | Sphere _ -> lo::s, f, e
        | Fiber _ -> s, lo::f, e
        | SymmetricEllipsoid _ -> s, f, lo::e ) ([], [], [])
    
  static member amountsOfShape (loL: LO list)=
    let rec computeAmounts (nameBoxList: _ list) (loA: LO list) =
      if loA.Length > 0 then
        let identical, different =
          let first = loA.[0].ToString()
          loA |> List.partition (fun lo -> lo.ToString() = first)
        let newEntry = Report.LP.kind nameBoxList.Length, ZamlSeq [Report.LP.totalNumOfObjs, ZamlI identical.Length; loA.[0].ToZaml()]
        computeAmounts (newEntry::nameBoxList) different
      else nameBoxList
    computeAmounts [] loL

  member o.ToZaml (isLocal) =

    let IPbehZ =    
      match interactionsPPO with
        | Some inter ->
          let IPinteractions =
            inter.forceInteraction |> Seq.fold (fun lst rule ->
              match rule with
                | Lubrication(hLub, hCut) -> 
                  (Report.LP.lubrication, ZamlSeq [
                    Report.LP.lubricationDistance, sprintf "%g LU" hLub |> ZamlS
                    Report.LP.lbmCutOffDistance, sprintf "%g LU" hCut |> ZamlS] ) :: lst
                | LubricationFn _ -> 
                  (Report.LP.lubrication, ZamlS "Function") :: lst
                | _ -> failwith "unsupported type of behavior" )[]
          let friction =
            match inter.friction with
              | Some (CoulombFriction(muDyn, muStat)) -> 
                Report.LP.coulombFriction, ZamlSeq [
                  Report.LP.dynamicFrictionCoef, ZamlF muDyn
                  Report.LP.staticFrictionCoef, ZamlF muStat]
              | Some (CoulombFrictionFn _ ) -> Report.LP.coulombFriction, ZamlS "Function"
              | Some (CoulombFrictionFnBc _ ) -> Report.LP.coulombFriction, ZamlS "Function"
              | None -> Report.LP.coulombFriction, ZamlS Report.none
          Report.LP.interparticleInteraction, ZamlSeq [ 
            if IPinteractions.Length > 0 then yield Report.LP.forceInteraction, ZamlSeq IPinteractions
            yield friction
            yield Report.LP.coeffOfRestitution, ZamlF inter.cR ]
        | None -> Report.LP.interparticleInteraction, ZamlS Report.none
    let BCbehZ =
      let BCinteractions =
        interactionsBC.forceInteraction |> Seq.fold (fun lst rule ->
          match rule with
            | Lubrication(hLub, hCut) -> 
              (Report.LP.lubrication, ZamlSeq [
                Report.LP.lubricationDistance, sprintf "%g LU" hLub |> ZamlS
                Report.LP.lbmCutOffDistance, sprintf "%g LU" hCut |> ZamlS] ) :: lst
            | LubricationFnBc _ -> 
              (Report.LP.lubrication, ZamlS "Function") :: lst
            | _ -> failwith "unsupported type of behavior" )[]
      let friction =
        match interactionsBC.friction with
          | Some (CoulombFriction(muDyn, muStat)) -> 
            Report.LP.coulombFriction, ZamlSeq [
              Report.LP.dynamicFrictionCoef, ZamlF muDyn
              Report.LP.staticFrictionCoef, ZamlF muStat]
          | Some (CoulombFrictionFn _ ) -> Report.LP.coulombFriction, ZamlS "Function"
          | Some (CoulombFrictionFnBc _ ) -> Report.LP.coulombFriction, ZamlS "Function"
          | None -> Report.LP.coulombFriction, ZamlS Report.none
      Report.LP.partilceBoundaryInteraction, ZamlSeq [ 
        if BCinteractions.Length > 0 then yield Report.LP.forceInteraction, ZamlSeq BCinteractions
        yield friction
        yield Report.LP.coeffOfRestitution, ZamlF interactionsBC.cR ]
    let controls = 
      let zmlTW =
        let kind, c = o.Controls.TwoWayIntegrationMethod
        kind.Name, ZamlSeq [c.ToZaml()]
      Report.LP.controls, ZamlSeq [ 
        Report.LP.twoWayIntegrationType, ZamlSeq [zmlTW]
        Report.LP.twoWayDiracDelta, ZamlS <| sprintf "%A" o.Controls.TwoWayDiracDelta3D
        Report.LP.tractkingDiracDelta, ZamlS <| sprintf "%A" o.Controls.TrackingDiracDelta3D ]
    
    let spheres, fibers, ells = LagrangianProblem.groupShapes (lodA.[1,1,1] |> Array.ofSeq |> Array.map (fun v -> v.Value))
    let objectsTotalAmounts = 
      Report.LP.totalNumOfObjs, ZamlSeq [
        Report.LP.totalNumOfObjs, ZamlI lodA.[1,1,1].Count
        Report.LP.spheres, ZamlI spheres.Length
        Report.LP.fibers, ZamlI fibers.Length
        Report.LP.symEllipsoids, ZamlI ells.Length ]        
    Report.LP.LagrangianProblem, ZamlSeq [ if isLocal then yield objectsTotalAmounts else yield! [IPbehZ; BCbehZ; controls] ]

    
