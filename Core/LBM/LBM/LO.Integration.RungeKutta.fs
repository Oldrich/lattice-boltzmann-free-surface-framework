﻿module internal LBM.LO.Integration.RungeKutta

open LBM
open LBM.LO.Types
open LBM.LO

open Kit
open Kit.Collections
open Kit.Math

open System.Collections.Generic

let newStepDuration_45 orderOfAccuracy (loAA: LO [] list, stepDuration, maxDuration, lt:Lattice) =
  let timeScale = 
    let relAllowedError = 0.1
    loAA.[0] |> Array.foldi (fun i minTimeScale loi ->
      let loj = loAA.[1].[i]

      let timeScale (a: float triple, b: float triple) =
        let an, bn, cn = Triple.norm a, Triple.norm b, Triple.norm(a-b)
        if cn < 1e-15 then 1e100 else relAllowedError * (max an bn) / cn

      let e2, e3 = timeScale (loi.U, loj.U), timeScale (loi.OmegaGlob, loj.OmegaGlob)

      min (min e2 e3) minTimeScale ) 1e100
  let newDuration = 0.9 * stepDuration * timeScale**(1. / float orderOfAccuracy)

  lt.Timer.printAny 3 "minTimeScale" timeScale
  lt.Timer.printAny 3 "newStepDurationLoc" newDuration
  timeScale < 0.1, min newDuration maxDuration

let fehlberg =
  { Name = "Runge-Kutta-Fehlberg"
    ConstA =
      [ []
        [1./4.]
        [3./32.; 9./32.]
        [1932./2197.; -7200./2197.; 7296./2197.]
        [439./216.; -8.; 3680./513.; -845./4104.]
        [-8./27.; 2.; -3544./2565.; 1859./4104.; -11./40.] ]
    ResConstA =
      [ [25./216.; 0.; 1408./2565.; 2197./4104.; -1./5.; 0.] // more precise
        [16./135.; 0.; 6656./12825.; 28561./56430.; -9./50.; 2./55.] ] // less precise
    TimeA = [0.; 1./4.; 3./8.; 12./13.; 1.; 1./2.]
    NewStepDuration = newStepDuration_45 5
    ResId = 0 }

let cashKarp =
  { Name = "Runge-Kutta-Cash-Karp"
    ConstA =
      [ []
        [1./5.]
        [3./40.; 9./40.]
        [3./10.; -9./10.; 6./5.]
        [-11./54.; 5./2.; -70./27.; 35./27.]
        [1631./55296.; 175./512.; 575./13824.; 44275./110592.; 253./4096.] ]
    ResConstA =
      [ [37./378.; 0.; 250./621.; 125./594.; 0.; 512./1771.]
        [2825./27648.; 0.; 18575./48384.; 13525./55296.; 277./14336.; 1./4.] ]
    TimeA = [0.; 1./5.; 3./10.; 3./5.; 1.; 7./8.]
    NewStepDuration = newStepDuration_45 5
    ResId = 0 }

let dormandPrince =
  { Name = "Runge-Kutta-Dormand-Prince"
    ConstA =
      [ []
        [1./5.]
        [3./40.; 9./40.]
        [44./45.; -56./15.; 32./9.]
        [19372./6561.; -25360./2187.; 64448./6561.; -212./729.]
        [9017./3168.; -355./33.; 46732./5247.; 49./176.; -5103./18656.]
        [35./384.; 0.; 500./1113.; 125./192.; -2187./6784.; 11./84.] ]
    TimeA = [0.; 1./5.; 3./10.; 4./5.; 8./9.; 1.; 1.]
    ResConstA =
      [ [5179./57600.; 0.; 7571./16695.; 393./640.; -92097./339200.; 187./2100.; 1./40.]
        [35./384.; 0.; 500./1113.; 125./192.; -2187./6784.; 11./84.] ]
    NewStepDuration = newStepDuration_45 5
    ResId = 1 }

let heunEuler =
  { Name = "Runge-Kutta-Heun-Euler"
    ConstA = [ [1.] ]
    TimeA = [1.]
    ResConstA = [ [1./2.; 1./2.]; [1.; 0.] ]
    NewStepDuration = newStepDuration_45 2
    ResId = 0 }
  
let constantSubStepping =
  { Name = "Constant sub-stepping"
    ConstA = []
    TimeA = [0.0]
    ResConstA = [ [1.0] ]
    NewStepDuration = fun (_loAA, oldStepDuration, _maxStepDuration, _lt) -> false, oldStepDuration
    ResId = 0 }

let tolerance = 1e-10

let rec iterate (integration, endTime, LP: LagrangianProblem, lbmData, bcA as v)
                (loA: LO [], sumFfsiA: float triple [] [], (sumNC, sumWC) as old)
                (acc, currentTime, oldStepDurationIn, failG) =

  let (rk: RKType, ctrl: IntegrationControlsType) = integration

  let mutable acc = acc
  let dAcc = rk.ConstA.Length + 1
  if acc / dAcc > 500 then failwith "Too many RK iterations"
    
  let stepDuration =
    if (endTime - currentTime - oldStepDurationIn) / oldStepDurationIn > 0.1 then oldStepDurationIn
    else endTime - currentTime

  let _, _, (lt: Lattice), lbmTime,_ = lbmData
    
  let uid = 100 * lbmTime + acc

  lt.Timer.printString 3 (sprintf "---- RK Time = %g ----" currentTime)
    
  lt.Timer.tick 3

  let nNC, nWC =
    if not failG && acc <> 0 then
      LP.propagateLO (loA, false)
      let dNC, dWC = Collisions.modifyLObyCollisions (lbmTime, LP, loA, bcA)
      sumNC + dNC, sumWC + dWC
    else sumNC, sumWC

  lt.Timer.tockPrintTick 3 "propagateLO_Collisions"

  let rhoV, uNbrs, lt, time, ponorA = lbmData

  let derA, FfsiAA =
    let newDerA, newFfsiAA = List rk.ConstA.Length, List rk.ConstA.Length
    rk.ConstA |> List.iteri (fun i consts ->
      let currentState = Dynamics.updateState(stepDuration * rk.TimeA.[i], consts, newDerA, (currentTime = 0.0), loA)
      let newDer, newFfsiA = Dynamics.getDerivatives(currentState, ponorA, lt, time, LP, bcA, rhoV, uNbrs)
      newDerA.Add newDer; newFfsiAA.Add newFfsiA )
    newDerA, newFfsiAA

  let newLOAA = rk.ResConstA |> List.map (fun consts ->
    Dynamics.updateState (stepDuration, consts, derA, (currentTime = 0.0), loA) )
        
  lt.Timer.tockPrintTick 3 "Dynamics"

  let failLoc, newStepDurationLoc = rk.NewStepDuration (newLOAA, stepDuration, ctrl.MaxTimeStep, lt)
  let fail, newStepDuration = LP.LOTimeExch.sendToMain (uid, (failLoc, newStepDurationLoc))

  if fail then
    let newStepDuration = if newStepDuration < stepDuration then newStepDuration else 0.5 * stepDuration
    iterate v old (acc + dAcc, currentTime, newStepDuration, true)
  else

    if currentTime = 0.0 then LP.LastStepDuration <- newStepDuration
      
    if LBM.Basic.trashFn.ContainsKey "collisionForceAddF1" then LBM.Basic.trashFn.["collisionForceAddF1"] (box stepDuration)

    let newFfsiA = newLOAA.[rk.ResId] |> Array.mapi (fun i newLO ->
      Array.init newLO.Nodes.LocalDX.Length (fun j ->
        let force = rk.ResConstA.[rk.ResId] |> List.sumByi (fun k resC -> resC * FfsiAA.[k].[i].[j])
        stepDuration * force + (if sumFfsiA.Length > 0 then sumFfsiA.[i].[j] else Triple.Zero) ))
    let newTime = currentTime + stepDuration

    if abs (newTime - endTime) > tolerance then
      iterate v (newLOAA.[rk.ResId], newFfsiA, (nNC, nWC)) (acc + dAcc, newTime, newStepDuration, false)
    else
      lt.Timer.printAny 3 "Number of iterations" (acc + dAcc)
      printfn "iter %d" (acc + dAcc)
      (newLOAA.[rk.ResId], newFfsiA, (nNC, nWC))

let inline run (integration, endTime, LP, loA, lbmData, bcA) =
  iterate (integration, endTime, LP, lbmData, bcA) (loA, [||], (0., 0.)) (0, 0., LP.LastStepDuration, false)  

  