﻿namespace LBM.Runtime

open LBM
open LBM.Basic
open LBM.LO.Types
open Kitware.VTK
open Utilities.Math

type VTK (folderPath, lt: Lattice, ?lPO: LagrangianProblem) =

  let realOrigin = lt.OriginsA.[1,1,1]

  do
    use fileOutputWindow = vtkFileOutputWindow.New()
    fileOutputWindow.SetFileName "-vtkError.txt"
    fileOutputWindow.FlushOn()
    vtkOutputWindow.SetInstance fileOutputWindow

  member o.saveLBM time =

    lt.Timer.tick 1

    let dx = 1.
    try
      let index = Array2D.create lt.NodesX_Loc lt.NodesY_Loc lt.NodesZ_Loc
      let convert (defVal: float32) (arr: float option GArray3D) =
        GArray3D.init index (fun x y z ->
          match arr.tryGet (x,y,z) with
            | Some vo -> match vo with Some v -> float32 v | None -> defVal
            | None -> defVal )

      let f0 = float32 0.
      let rhoA = lt.Par.RhoA |> convert f0
      let uxA = lt.Par.UxA |> convert f0
      let uyA = lt.Par.UyA |> convert f0
      let uzA = lt.Par.UzA |> convert f0
      let tauA = lt.Par.TauA |> convert f0
        
      let phase = GArray3D.init index (fun x y z ->
        match lt.Par.PA.tryGet (x,y,z) with
          | Some v -> float32 (Phase.toFloat v)
          | None -> float32 -10. )

      use vtk = new Utilities.PostProcess.VTK.ImageData (dx, dx, dx, realOrigin)
      vtk.addCellData(phase, "Domain")
      vtk.addCellData(rhoA, "Rho")
      vtk.addCellData(tauA, "Tau") 
      vtk.addCellData(uxA, uyA, uzA, "U")
      vtk.saveToFile (sprintf @"%s\vtk\lbm%A_%d.vti" folderPath lt.DomId time)
    with e -> printfn "error occurred during vtk\n%A" e
    
    lt.Timer.tockPrint 1 "saveLBM"  

  member o.saveLOA (time, ?filterFn: LO -> bool) = lPO |> Option.iter (fun lP ->
    let filterFn = defaultArg filterFn (fun _ -> true)
    use poly = new Utilities.PostProcess.VTK.PolyData (realOrigin, true)
    lt.Timer.tick 1

    let plotLO (lo: LO) =
      let mat = Quaternion.toRotMatrix lo.Rot.Quaternion
      let cog = lo.CoG
      match lo.Shape with
        | Sphere s ->
          let r = s.Radius
          poly.addSphere (cog, r, mat, [255uy,255uy,255uy,255uy; 200uy,200uy,200uy,255uy])
          None
        | Fiber f ->
          let p1, p2 = f.P1P2
          if f.IsCylinder then
            poly.addCylinder (lo.CoG, f.R, f.Length, mat, [220uy, 220uy, 220uy, 255uy])
            None
          else Some (p1, p2)
        | SymmetricEllipsoid e ->
          let l, r = e.MainHalfAxis, e.Radius
          poly.addEllipsoid (cog, [|l; r; r|], mat, [200uy, 230uy, 255uy, 255uy])
          None

    poly.addSphere (Triple.Zero, 0.0)

    let lines = lP.Particles.Values |> Array.ofSeq |> Array.choose (fun lo -> if filterFn lo then plotLO lo else None)
    poly.addLines (lines, Array.create lines.Length (255uy, 200uy, 255uy, 255uy))
      
    let path = sprintf @"%s\vtk\solids%A_%d.vtp.temp" folderPath lt.DomId time
    poly.saveToFile path
    let newPath = path.Replace(".temp", "")
    if System.IO.File.Exists newPath then System.IO.File.Delete newPath
    System.IO.File.Move (path, newPath)
    
    lt.Timer.tockPrint 1 "saveLOA" )
  
  member o.saveBoth time = o.saveLBM time; o.saveLOA time

//  member o.saveTrackingParticles (time, tpA: LBM.TrackingParticles.TrackingParticle []) =
//    let timer = Utilities.Basic.Timer ()
//    let displayLbmRadius = 0.1
//    timer.tick
//    polyTrack.deleteData ()
//    if tpA.Length > 0 then
//        for p in tpA do
//          let cog = match units with Some units -> p.CoG |> Triple.map (units.toReal.Length >> float) | _ -> p.CoG
//          let r = match units with Some units -> displayLbmRadius |> units.toReal.Length |> float | _ -> displayLbmRadius
//          polyTrack.addSphere (cog, r)
//        let path = sprintf @"%s\trackingParticles_%d.vtp" folderPath time
//        poly.saveToFile path
//        //if display then System.IO.File.WriteAllText (displayFile, path
//    timer.tockPrint "saveTrackingParticles"