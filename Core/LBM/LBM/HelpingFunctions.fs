﻿namespace LBM.HelpingFunctions

open System.IO
open System.Collections.Generic

open Utilities.Basic
open Utilities.Math
open Utilities.IO

open LBM
open LBM.Basic
open LBM.BC.Types
open LBM.BC.Shapes
open LBM.LO.Types
open LBM.Naming

module DiracDelta =

  let linear = 
    1.,
    (fun (x, y, z) ->
      let aX, aY, aZ = abs x, abs y, abs z
      if aX < 1.f && aY < 1.f && aZ < 1.f then (1.f - aX) * (1.f - aY) * (1.f - aZ) else 0.f )

  let nearestNbr = 0.5, (fun _ -> 1.f)

  let smoothenLinear = 
    1.5,
    (fun (x, y, z) ->
      let aX, aY, aZ = abs x, abs y, abs z
      let fn aX = if aX < 0.5f then 0.75f - aX * aX elif aX < 1.5f then 1.125f - 1.5f * aX + 0.5f * aX * aX else 0.f
      fn aX * fn aY * fn aZ )

  let cosine =
    let cose05 =
      let B, C = 2.f, -1.f
      fun x ->
        let a = if x > 1.f then x - 3.f else x + 1.f
        B * a + C * a * abs a
    2., 
      (fun (x, y, z) ->
        let aX, aY, aZ = abs x, abs y, abs z
        if aX < 2.f && aY < 2.f && aZ < 2.f then
          0.015625f * (1.f + cose05 aX) * (1.f + cose05 aY) * (1.f + cose05 aZ)
        else 0.0f )

module Optimize =
  let domain marginLength (lt: Lattice) dontOptimize =  

    let dontOptimize = defaultArg dontOptimize (fun _ _ -> false)

    let pAtemp = JArray3D.init lt.NodesX_Loc lt.NodesY_Loc lt.NodesZ_Loc (fun x y z ->
      match lt.Par.PA.tryGet(x, y, z) with
        | Some (Interface _) -> Interface marginLength
        | Some other -> other
        | None -> Gas )

    let pAtemp =
      let npA = JArray3D.copy pAtemp

      let rec modifyPA x y z newM =
        if newM >= 0. then lt.Consts.ADimA |> Array.iter (fun i ->
          let xdx, ydy, zdz as idi = lt.xdx ((x,y,z),i)
          if lt.Par.PA.exists idi then
            match npA.[xdx].[ydy].[zdz] with
              | Gas ->
                npA.[xdx].[ydy].[zdz] <- Interface newM
                modifyPA xdx ydy zdz (newM - 1.)
              | Interface oldM when newM > oldM ->
                npA.[xdx].[ydy].[zdz] <- Interface newM
                modifyPA xdx ydy zdz (newM - 1.)
              | _ -> ()
          else
            let xdx, ydy, zdz =
              let c = lt.Consts.C.[i]
              x + c.[0], y + c.[1], z + c.[2]
            if xdx >= 0 && ydy >= 0 && zdz >= 0 && xdx < npA.Length && ydy < npA.[xdx].Length && zdz < npA.[xdx].[ydy].Length then
              npA.[xdx].[ydy].[zdz] <- Interface newM
              modifyPA xdx ydy zdz (newM - 1.)
        )

      pAtemp |> JArray3D.iteri (fun x y z phase -> match phase with Interface m -> modifyPA x y z (m - 1.) | _ -> ())
      npA

    let newLengthA_Loc = Array2D.init lt.NodesX_Loc lt.NodesY_Loc (fun x y ->
      if dontOptimize x y then pAtemp.[x].[y].Length
      else
        let rp = pAtemp.[x].[y] |> Array.rev
        let index = rp |> Array.tryFindIndex (function BoundaryCondition _ | Fluid | Interface _ -> true | Gas -> false )
        match index with Some i -> rp.Length - i | None -> 0 )
    
    lt.LengthA_Loc <- newLengthA_Loc
    let newData = GArray3D.init7 lt.LengthA_Loc (fun x y z ->
      let xyz = x, y, z
      if lt.Par.PA.exists xyz then lt.Par.PA.[xyz], lt.Par.FA.[xyz], lt.Par.RhoA.[xyz], lt.Par.UxA.[xyz], lt.Par.UyA.[xyz], lt.Par.UzA.[xyz], lt.Par.TauA.[xyz]
      else Gas, None, None, None, None, None, None)
    lt.Par.setLBMA7 newData
