﻿namespace LBM.LO.Types

open LBM
open LBM.Basic

open Kit
open Kit.IO
open Kit.Collections
open Kit.Math

open System.Collections.Generic

type Nodes =
  { LocalDX: PositionVector []
    V: float [] } 

type FiberType(length, aspectRatio, ?dLength, ?vtkIsCylinder, ?doNotFill) = 

  let dLength = defaultArg dLength 1.0
  let isCylinder = defaultArg vtkIsCylinder false
  
  let d = length / aspectRatio
  let r = 0.5 * d
  let p1p2 = defaultArg doNotFill (triple(0.,0.,0.), triple(0.,0.,0.))

  member o.Length = length
  member o.AspectRatio = aspectRatio
  member o.DLength = dLength
  member o.IsCylinder = isCylinder
  member o.D = d
  member o.R = r
  member o.volume = pi * r * r * length
  member o.P1P2 = p1p2

  member o.update cog (rot: versor) =
    let dl = triple (0.5 * length, 0., 0.) |> Versor.rotateL2G rot
    FiberType(length, aspectRatio, dLength, isCylinder, (cog - dl, cog + dl) )

  override o.ToString() =
    sprintf "================
Fiber
----------------
Length = %f
AspectRatio = %f
DLength = %f
Diameter = %f" o.Length o.AspectRatio o.DLength d

  member o.ToZaml() = 
    Naming.Report.LO.fiber, ZamlSeq [
      Naming.Report.LO.length, ZamlF o.Length
      Naming.Report.LO.diameter, ZamlF d
      Naming.Report.LO.fiberAspectRatio, ZamlF o.AspectRatio
      Naming.Report.LO.dLength, ZamlF o.DLength ]

type SphereType(?radius: float, ?diameter: float) =
  let r, d =
    match radius, diameter with
      | Some r, Some d when d = 2. * r-> r, d
      | Some r, None -> r, 2. * r
      | None, Some d -> 0.5 * d, d
      | _ -> failwith "Sphere type not defined correctly" 
  member o.Volume = 4. / 3. * pi * r**3.
  member o.Radius = r
  member o.Diameter = d
  override o.ToString() =
    sprintf "================
Sphere
----------------
Radius = %f Diameter = %f" o.Radius o.Diameter

  member o.ToZaml() = 
    Naming.Report.LO.sphere, ZamlSeq [
      Naming.Report.LO.radius, ZamlF o.Radius
      Naming.Report.LO.diameter, ZamlF o.Diameter ]

type SymmetricEllipsoidType(dLength: float, MainHalfAxis: float, ?radius: float, ?diameter: float) =
  let r, d =
    match radius, diameter with
      | Some r, Some d when d = 2. * r-> r, d
      | Some r, None -> r, 2. * r
      | None, Some d -> 0.5 * d, d
      | _ -> failwith "Sphere type not defined correctly" 
  let boundingSpheres = lazy (
    let n = int (MainHalfAxis / dLength / 1.5) + 1
    let dh = MainHalfAxis / float n
    let bc = Array.create (2 * n) (triple(0.,0.,0.))
    let br = Array.create (2 * n) 0.
    for i = 0 to n - 1 do
      let x = (float i + 0.5) * dh
      let rLoc = 
        let r = sqrt (r * r * (1. - x * x / (MainHalfAxis * MainHalfAxis) ) )
        min r (MainHalfAxis - x)
      if r < 0. then failwith "r < 0. "
      bc.[i] <- triple (x, 0., 0.)
      br.[i] <- rLoc
      bc.[n+i] <- triple (-x, 0., 0.)
      br.[n+i] <- rLoc
    bc, br )
  member o.Radius = r
  member o.L2 = [| MainHalfAxis * MainHalfAxis; r * r; r * r |]
  member o.Volume = 4./3. * pi * MainHalfAxis * r * r
  member o.Diameter = d
  member o.MainHalfAxis = MainHalfAxis
  member o.DLength = dLength
  member o.BoundingSpheresCog = fst boundingSpheres.Value
  member o.BoundingSpheresR = snd boundingSpheres.Value
  override o.ToString() =
    sprintf "================
Symmetric Ellipsoid
----------------
Main Half-axis = %f
Radius = %f" o.MainHalfAxis o.Radius
  member o.ToZaml() = 
    Naming.Report.LO.symEllipsoid, ZamlSeq [
      Naming.Report.LO.mainHalfAxis, ZamlF o.MainHalfAxis
      Naming.Report.LO.radius, ZamlF o.Radius ]
    
type Shape =
  | Sphere of SphereType         
  | Fiber of FiberType
  | SymmetricEllipsoid of SymmetricEllipsoidType

type Interaction =
  /// fluid <-> particle, collides with TwoWay and FrozenParticle
  | TwoWay
  /// fluid -> particle, collides with all
  | TrackingParticle

type LO ( Shape: Shape, Mass: float, Density: float, I: float triple, GAcc: float triple,
          Nodes : Nodes, _Rotate: float triple, _Move: float triple, _Interaction: Interaction, CoG: PositionVector,
          U: VelocityVector, OmegaGlob: VelocityVector, Favg: ForceVector, Tavg: ForceVector,
          Rot: versor, Marker: int, Guid: System.Guid) as o =
  let sh = match Shape with Fiber f -> Fiber (f.update CoG Rot) | x -> x

  let mutable u, cog = U, CoG
  
  let _Mass, _I = Mass, I
  do o.checkNaN ()
  
  member o.Mass = _Mass
  member o.Density = Density
  member o.I = _I
  member o.GAcc = GAcc
  member o.Nodes = Nodes
  member o._Rotate = _Rotate
  member o._Move = _Move
  member o._Interaction = _Interaction
  member o.CoG with get () = cog and set v = cog <- v
  member o.U with get () = u and set v = u <- v
  member o.OmegaGlob = OmegaGlob
  member o.Favg = Favg
  member o.Tavg = Tavg
  member o.Rot = Rot
  member o.Shape = sh
  member o.Marker = Marker
  member o.Guid = Guid

  member o.copy ( ?shape, ?mass, ?density, ?i, ?gAcc, ?nodes, ?_rotate, ?_move, ?_interaction, ?coG,
                  ?u, ?omegaGlob, ?f, ?t, ?rot, ?marker, ?guid ) =
    let d a b = defaultArg a b
    LO (  d shape Shape, d mass Mass, d density Density, d i I, d gAcc GAcc,
          d nodes Nodes, d _rotate _Rotate, d _move _Move, d _interaction _Interaction, d coG o.CoG, d u o.U,
          d omegaGlob OmegaGlob, d f Favg, d t Tavg, d rot Rot, d marker Marker, d guid Guid)

  member o.lo2simpleLO : LBM.Parallel.LOWCF =
    let shape =
      match o.Shape with
        | Fiber f -> Some (f.Length, f.AspectRatio, f.DLength, f.IsCylinder), None, None
        | Sphere s -> None, Some s.Radius, None
        | SymmetricEllipsoid e -> None, None, Some (e.DLength, e.MainHalfAxis, e.Radius)
    let intaraction = match o._Interaction with TwoWay -> 0 | TrackingParticle -> 1
    let fn = Triple.toArray
    let nodes = o.Nodes.LocalDX |> Array.map fn, o.Nodes.V
    o.Guid.ToByteArray(), o.Marker, shape, o.Mass, o.Density, fn o.I, fn o.GAcc, nodes, fn o._Rotate, fn o._Move,
    intaraction, fn o.CoG, fn o.U, fn o.OmegaGlob, fn o.Favg, fn o.Tavg, (o.Rot.w, fn o.Rot.t)

  static member simpleLO2lo (cogFn, data: LBM.Parallel.LOWCF) =
    let guid, marker, shape, mass, density, i, gacc, (nodesX, nodesV), rotate, move, interaction, cog, u, omega, favg, tavg, (quatW, quatT) = data
    let shape =
      match shape with
        | Some (l, a, dl, isC), _, _ -> Fiber (FiberType(l,a,dl,isC))
        | _, Some r, _ -> Sphere (SphereType r)
        | _, _, Some (dl, ha, r) -> SymmetricEllipsoid (SymmetricEllipsoidType (dl, MainHalfAxis = ha, radius = r))
        | _ -> failwith "error"
    let fn = Triple.ofArray
    let nodes = { LocalDX = nodesX |> Array.map fn; V = nodesV}
    let rot = quaternion(quatW, fn quatT)
    let interaction = if interaction = 0 then TwoWay elif interaction = 1 then TrackingParticle else failwith "error"
    LO (shape, mass, density, fn i, fn gacc, nodes, fn rotate, fn move, interaction, cogFn cog, fn u , fn omega, fn favg, fn tavg, rot, marker, System.Guid guid)
  
  member private o.checkNaN () =
    let isBad v = System.Double.IsNaN v || System.Double.IsInfinity v
    if o.CoG.isNaNInf || o.U.isNaNInf || o.OmegaGlob.isNaNInf || 
        isBad o.Rot.Quaternion.W || o.Rot.Quaternion.T.isNaNInf
    then failwithf "cog = %A\nu = %A\nomega = %A\nrot = %A, %A" o.CoG o.U o.OmegaGlob o.Rot.w o.Rot.t

//  override o.ToString() = 
//    
//    let shapeS = 
//      match sh with 
//        | Sphere s -> sprintf "Sphere: radius = %g LU; " s.Radius
//        | Fiber f -> sprintf "Fiber: length = %g LU; diameter = %g LU; cylinder = %A; DLength = %g; " f.Length f.D f.IsCylinder f.DLength
//        | SymmetricEllipsoid se -> sprintf "Symmetric ellipsoid: length = %g LU; radius = %g LU; " se.MainHalfAxis se.Radius
//    sprintf "%sMass = %g LU; Density = %g LU; GAcc = %A LU; number of nodes = %d; move = %A; rotate = %A; update = %A" shapeS o.Mass o.Density o.GAcc o.Nodes.LocalDX.Length o._Move o._Rotate o._Interaction

  member o.ToZaml() = 
    let shape = 
      match sh with 
        | Sphere s -> s.ToZaml()    
        | Fiber f -> f.ToZaml()
        | SymmetricEllipsoid se -> se.ToZaml()
    Naming.Report.LO.shape, ZamlSeq [shape]

type IntegrationControlsType (maxTimeStep: float, ?errors: float * float * float * float)  =

  let errors = defaultArg errors (1e-3, 1e-3, 1e-3, 1e-3)
  
  member o.MaxTimeStep = maxTimeStep
  member o.MaxErrors = errors
  member o.ToZaml() = 
    let maxErrorCoG, maxErrorRot, maxErrorU, maxErrorOmegaGlob = errors
    Naming.Report.LP.integrationControls, ZamlSeq [
      Naming.Report.LP.maxStepSize, ZamlF maxTimeStep
      Naming.Report.LP.maxErrors, ZamlSeq [
        Naming.Report.LP.CoG, ZamlF maxErrorCoG
        Naming.Report.LP.rotation, ZamlF maxErrorRot
        Naming.Report.LP.speed, ZamlF maxErrorU
        Naming.Report.LP.rotationSpeed, ZamlF maxErrorOmegaGlob] ]

type RKType = {
  Name: string
  ConstA: float list list
  ResConstA: float list list
  TimeA: float list
  ResId: int
  /// (loAA, oldStepDuration, maxDuration, lt) -> isFail * Duration
  NewStepDuration: (LO [] list * float * float * Lattice) -> (bool * float)
}

type UptadeCtrls = 
  { TwoWayIntegrationMethod: (RKType * IntegrationControlsType)
    /// Use: HelpingFunctions.DiracDelta
    TwoWayDiracDelta3D: (float * (float32 * float32 * float32 -> float32))
    /// Use: HelpingFunctions.DiracDelta
    TrackingDiracDelta3D: (float * (float32 * float32 * float32 -> float32)) }