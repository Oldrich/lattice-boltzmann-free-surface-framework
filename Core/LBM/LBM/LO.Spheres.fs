﻿module LBM.LO.Spheres

open LBM.Basic
open LBM.LO.Types

open Kit
open Kit.Collections
open Kit.Math

let internal nodesDict = System.Collections.Generic.Dictionary()

let sphereToLo (shape: Shape) (_CoG: float triple, angle: float triple) (density: float) (gravityAcceleration: float triple) (dl:float) (u, omegaYPR) (move, rotate, interaction) prec marker =
  
  let r = match shape with Sphere s -> s.Radius | _ -> failwith "only spheres can be discretized by sphereToLo"

  let rec layers nodes ri r=    
    let N = 
      let S = 4. * pi * ri**2.0
      let N = S / dl**2.0 |> ceil |> int
      if N > 3 then N else 4
    let dx = 
      if nodesDict.ContainsKey N then nodesDict.[N] |> Array.map (fun point -> point * ri)
      else
        let nodes = Utilities.Geometry.uniformPointsSphere N prec
        nodesDict.Add(N, nodes)
        nodes |> Array.map (fun point -> point * ri)
    let dV = 
      if ri = r then 4. / 3. * pi * ( r**3.0 - (r - 0.5*dl)**3.0) / float N
      else 4. / 3. * pi * ( (ri + 0.5 * dl)**3.0 - (ri - 0.5*dl)**3.0) / float N
    let newNodes =
      { LocalDX = Array.append nodes.LocalDX dx
        V = Array.append nodes.V <| Array.create N dV
      }
    if ri - dl > 0.5 * dl then layers newNodes (ri - dl) r
    else
      let dV = 4. / 3. * pi * (ri - 0.5 * dl)**3.0
      {newNodes with LocalDX = Array.append newNodes.LocalDX [| Triple.Zero |]; V = Array.append newNodes.V [|dV|] }
 
  let V = 4./ 3. * pi * r**3.0
  let mass = density * V
  let I = 2./5. * mass * r * r
  let rot = Utilities.Rotation.QuaternionRotation(angle |> Quaternion.ofYawPitchRoll)
  let omegaGlob = omegaYPR |> Quaternion.ofYawPitchRoll |> Quaternion.toAxisAngle |> Quaternion.angularVelocityVectorFromAxisAngle
  LO (Shape = Sphere(SphereType(radius = r)), CoG = _CoG, Mass = mass, Density = density, U = u,
                    Favg = Triple.Zero, Tavg = Triple.Zero, OmegaGlob = omegaGlob, I = triple(I,I,I), Rot = rot,
                    GAcc = gravityAcceleration, Nodes = layers {LocalDX = [||]; V = [||]} r r, _Rotate = rotate, _Move = move, _Interaction = interaction, Marker = marker, Guid = System.Guid.NewGuid())