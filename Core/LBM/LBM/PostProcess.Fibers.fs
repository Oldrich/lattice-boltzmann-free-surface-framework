﻿namespace LBM.PostProcess

open System.IO
open System.Collections.Generic

type Fibers =

  static member serializeProblemIntoFibers (problemDir, targetDir, ?acceptFiberFn, ?chooseFiles) =
    let acceptFiberFn = defaultArg acceptFiberFn (fun _ -> true)
    if Directory.Exists targetDir = false then Directory.CreateDirectory targetDir |> ignore
    let rA = LBM.PostProcess.Report.Combine.particlesTypeCoGandAxis2 (DirectoryInfo problemDir, ?chooseFiles = chooseFiles)
    for domDir, files in rA do
      printfn "serializeProblemIntoFibers: %s" domDir.Name
      for file, lines in files do
        use stream = File.Open (Path.Combine (targetDir, file.Name + ".fib"), FileMode.Append)
        use bw = new BinaryWriter (stream)
        let w (v: float) = bw.Write (float32 v)
        for kind, cog, p in lines do
          if kind = 1 && acceptFiberFn (cog, p) then w cog.[0]; w cog.[1]; w cog.[2]; w p.[0]; w p.[1];w p.[2]
        bw.Close()

  /// cog, p []
  static member deserializeFibers (file, processFn) =
    use stream = File.Open (file, FileMode.Open, FileAccess.Read)
    use bw = new BinaryReader (stream)
    let r () = bw.ReadSingle ()
    let numOfFibers = stream.Length / 24L
    for i in 0L .. numOfFibers - 1L do processFn (triple (r(),r(),r()), triple (r(),r(),r()))
    bw.Close ()

  static member getEllipsoidsAndNFibers file (origin: triple, domSize: triple, (boxes0: int, boxes1, boxes2), acceptFiberFn) =
    let acceptFiberFn = defaultArg acceptFiberFn (fun _ -> true)
    let boxesT = triple (boxes0, boxes1, boxes2)
    let dx = domSize ./ boxesT
    let boxes = Array3D.init boxes0 boxes1 boxes2 (fun _ _ _ -> List ())
    Fibers.deserializeFibers (file, fun (cog, p) ->
      if acceptFiberFn (file, cog, p) then
        let i, j, k =
          let i, j, k = (boxesT .* ((cog - origin) ./ domSize)).toTuple
          int i, int j, int k
        if i >= 0 && j >= 0 && k >= 0 && i < boxes.GetLength 0 && j < boxes.GetLength 1 && k < boxes.GetLength 2 then
          boxes.[int i, int j, int k].Add p )
    let els = boxes |> Array3D.map (fun fibers ->
      Utilities.Math.OrientationEllipse.pToEllipsoid fibers |> Option.map (fun (vals, vecs) ->
        vals |> Array.map float32, vecs |> Array2D.map float32 ))
    let nFibs = boxes |> Array3D.map Seq.length
    els, nFibs

  static member serializeFibersIntoEllipsoids (fibersDir, targetDir, origin, domSize, (boxes0, boxes1, boxes2 as boxes), ?acceptFiberFn) =
    if Directory.Exists targetDir = false then Directory.CreateDirectory targetDir |> ignore
    let fibersDir = DirectoryInfo fibersDir
    Utilities.Serializer.Binary.serializeObject (Path.Combine(targetDir, "_setup"), (origin, domSize, boxes))
    fibersDir.GetFiles () |> Array.Parallel.iter (fun file ->
      let out = Fibers.getEllipsoidsAndNFibers file.FullName (origin, domSize, (boxes0, boxes1, boxes2), acceptFiberFn)
      let targetFile = Path.Combine (targetDir, file.Name + ".ell")
      Utilities.Serializer.Binary.serializeObject (targetFile, out) )

  static member deserializeEllipsoids file: (float32 [] * float32 [,]) option [,,] * int [,,] =
    unbox (Utilities.Serializer.Binary.deSerializeObject file)