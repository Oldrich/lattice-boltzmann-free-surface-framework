﻿namespace LBM.BC.Shapes

open LBM
open LBM.Basic
open LBM.BC.Types
open LBM.BC.Types.Internal
open LBM.Naming

open Kit
open Kit.Math
open Kit.IO
open Kit.Collections

// Points on planes have incorrect positions -> + 0.5

module HelpingFns =

  let thickness = 0.6

  let getSymmetricCoordAndIndex (getNormal: _ -> float triple) (lt: Lattice) (lxyzFluid: (int * int * int)) (lx: int, ly, lz) i =
    let n = getNormal (triple lxyzFluid)
    let symCoord = lx + int n.x, ly + int n.y, lz + int n.z
    let i, _ =
      let ci = 
        let cf = lt.Consts.Cf.[i]
        match n.x, n.y, n.z with
        | _, 0., 0. -> triple(-cf.x, cf.y, cf.z)
        | 0., _, 0. -> triple(cf.x, -cf.y, cf.z)
        | 0., 0., _ -> triple(cf.x, cf.y, -cf.z)
        | _ ->
          let n = if Triple.dot(n, cf) < 0. then -n else n
          let cfS = cf - 2. * n * Triple.dot(cf, n)
          cfS |> Triple.map round
      lt.Consts.Cf
        |> Array.foldi (fun i (minId, minLen) cidx ->
          let len = Triple.norm2 (cidx - ci)
          if len < minLen then i, len else minId, minLen ) (-1, 1000.)
    symCoord, i

  let mapToLBM_GetF (lt: Lattice) (f: float []) rho (u: VelocityVector) =
    let ccZero = Array2D.create 3 3 0.
    let fEq = lt.Consts.getfEqA (rho, u)
    let PI   =
      let fi = fEq |> Array.mapi (fun i feq -> feq + f.[ lt.Consts.Opposite.[i]  ] - fEq.[lt.Consts.Opposite.[i]])
      fi |> Array.foldi (fun i sum f -> sum |> Array2D.mapi (fun x y s -> s + f * lt.Consts.CC.[i].[x,y])) ccZero
    let PIeq = fEq |> Array.foldi (fun i sum f -> sum |> Array2D.mapi (fun x y s -> s + f *  lt.Consts.CC.[i].[x,y])) ccZero
    fEq |> Array.mapi( fun i feq ->
      let QPI = lt.Consts.Q.[i] |> Array2D.sumByi (fun (x, y) q -> q * (PI.[x,y] - PIeq.[x,y]) )
      let df = lt.Consts.W.[i] / (2. * lt.Consts.Cs4) * QPI
      feq + df  )

  let mapToLBM_Vel (lt: LBM.Lattice) getNormal (uPres: float triple) (lxyz: int*int*int, f: float []) =
    let rho =
      let n = getNormal (triple lxyz)
      let known, _unknown, tan, id = lt.Consts.knownUnknownTan n
      let rhoOp = 2. * (known |> List.sumBy (fun i -> f.[i]))
      let rhoTan = tan |> List.sumBy (fun i -> f.[i])
      (rhoTan + rhoOp) / (1. - n.[id] * uPres.[id])
    let fnew = mapToLBM_GetF lt f rho uPres
    fnew, rho, uPres

  let mapToLBM_Vel_Rho (lt: LBM.Lattice) _ (uPres: float triple, rhoPres: float) _ =
    lt.Consts.getfEqA (rhoPres, uPres), rhoPres, uPres

  let mapToLBM_VelFn_Rho (lt: LBM.Lattice) _ (uPresFn, rhoPres: float) (xyz, _f: float []) =
    let uPres = uPresFn xyz
    lt.Consts.getfEqA (rhoPres, uPres), rhoPres, uPres

  let mapToLBM_VelFn (lt: LBM.Lattice) getNormal uPresFn (xyz:int*_*_, f: float []) =
    let uPres: float triple = uPresFn xyz
    let rho =
      let n = getNormal (triple xyz)
      let known, _unknown, tan, id = lt.Consts.knownUnknownTan n
      let rhoOp = 2. * (known |> List.sumBy (fun i -> f.[i]))
      let rhoTan = tan |> List.sumBy (fun i -> f.[i])
      (rhoTan + rhoOp) / (1. - n.[id] * uPres.[id])
    let fnew = mapToLBM_GetF lt f rho uPres
    fnew, rho, uPres

  let mapToLBM_Rho (lt: LBM.Lattice) getNormal rho (lxyz:int*int*int, f: float []) =
    let u =
      let n = getNormal (triple lxyz)
      let known, _unknown, tan, _id = lt.Consts.knownUnknownTan n
      let rhoOp = 2. * (known |> List.sumBy (fun i -> f.[i]))
      let rhoTan = tan |> List.sumBy (fun i -> f.[i])
      n * (1. - (rhoTan + rhoOp) / rho)
    let fnew = mapToLBM_GetF lt f rho u
    fnew, rho, u

  let mapToLBM_DoNothing (lt: LBM.Lattice) getNormal ((lx: int, ly, lz), _) =
    let xyz =
      let n: float triple = triple (lx, ly, lz) |> getNormal
      triple(lx + int n.x, ly + int n.y, lz + int n.z)
    lt.Par.FA.[xyz].Value, lt.Par.RhoA.[xyz].Value, triple(lt.Par.UxA.[xyz].Value, lt.Par.UyA.[xyz].Value, lt.Par.UzA.[xyz].Value)

  let getPurpose purpose (lt: LBM.Lattice) getNormal distanceAndNormal = 
    let modifyLBMMapper mapToLBM (lbmMapper: LBMMap) =
      lt.LengthA_Loc |> Array3.iterIndex (fun x y z ->
        let distance = triple(x, y, z) |> distanceAndNormal |> fst
        if distance <= 0. then lbmMapper.[(x,y,z)] <- mapToLBM )
    match purpose with
      | BcBounceBack (vel, slip) -> BcBounceBackI (vel, slip)
      | BcVelocity uPres -> BcMapLBMI (modifyLBMMapper (mapToLBM_Vel lt getNormal uPres))
      | BcVelocityFn uPresFn -> BcMapLBMI (modifyLBMMapper (mapToLBM_VelFn lt getNormal uPresFn))
      | BcDensity rho -> BcMapLBMI (modifyLBMMapper (mapToLBM_Rho lt getNormal rho))
      | BcDoNothing -> BcMapLBMI (modifyLBMMapper (mapToLBM_DoNothing lt getNormal))
      | BcInflow (uPres, rhoPres) -> BcMapLBMI (modifyLBMMapper (mapToLBM_Vel_Rho lt getNormal (uPres, rhoPres)))
      | BcInflowFn (uPresFn, rhoPres) -> BcMapLBMI (modifyLBMMapper (mapToLBM_VelFn_Rho lt getNormal (uPresFn, rhoPres)))

  let getPurposeZaml purpose = 
    match purpose with
      | BcBounceBack (vel, slip) ->
        ZamlSeq [
          yield Report.BC.purpose, ZamlS Report.BC.bounceBack
          yield match slip with SlipCoef slip -> Report.BC.slipCoef, ZamlF slip | SlipLength slip -> Report.BC.slipLength, ZamlF slip
          yield Report.BC.velocity, ZamlT vel ]
      | BcVelocity uPres -> ZamlSeq [ Report.BC.purpose, ZamlS Report.BC.presVel; Report.BC.velocity, ZamlT uPres ]
      | BcVelocityFn _ -> ZamlSeq [ Report.BC.purpose, ZamlS Report.BC.presVelFn ]
      | BcDensity rho -> ZamlSeq [ Report.BC.purpose, ZamlS Report.BC.bounceBack; Report.BC.density, ZamlF rho ]
      | BcDoNothing -> ZamlSeq [ Report.BC.purpose, ZamlS Report.BC.doNothing ]
      | BcInflow (uPres, rhoPres) -> ZamlSeq [ Report.BC.purpose, ZamlS Report.BC.inflowPresVelRho; Report.BC.velocity, ZamlT uPres; Report.BC.density, ZamlF rhoPres ]
      | BcInflowFn (_, rhoPres) -> ZamlSeq [ Report.BC.purpose, ZamlS Report.BC.inflowPresVelFnRho; Report.BC.density, ZamlF rhoPres ]

open HelpingFns

type Planes =
  static member infDepth (lt: Lattice, purpose: BcPurpose, pointOnPlane: float triple, normal: float triple, ?invisible) =
    let normal = Triple.normalize normal
    let fnN _ = normal
    let getDistance (localPoint: float triple) =
      let globPoint = lt.xyzLoc2Glob localPoint
      match normal.x, normal.y, normal.z with
        | nx, 0., 0. -> (globPoint.x - pointOnPlane.x - nx * 0.5) * nx
        | 0., ny, 0. -> (globPoint.y - pointOnPlane.y - ny * 0.5) * ny
        | 0., 0., nz -> (globPoint.z - pointOnPlane.z - nz * 0.5) * nz
        | _ -> (globPoint - pointOnPlane) * normal - 0.5
    let distanceAndNormal (localPoint: float triple) = getDistance localPoint, normal
    let zml = 
      Report.BC.planeOfInfDepth, ZamlSeq [
        Report.BC.purposeType, getPurposeZaml purpose
        Report.BC.pointOnPlane, ZamlT pointOnPlane
        Report.BC.normal, ZamlT normal]
    { Purpose = getPurpose purpose lt (fun _ -> normal) distanceAndNormal
      Shape = BcPlanes
      distanceAndNormal = distanceAndNormal
      getSymmetricCoordAndIndex = getSymmetricCoordAndIndex fnN lt
      Description = zml
      Invisible = defaultArg invisible false }

  static member unitDepth (lt: LBM.Lattice, purpose: BcPurpose, pointOnPlane: triple, normalNoSign: triple, ?invisible) =
    let getNormal =
      let normal = Triple.normalize normalNoSign
      fun (localPoint: float triple) ->
        let globPoint = lt.xyzLoc2Glob localPoint
        if (globPoint - pointOnPlane) * normal > 0. then normal else - normal
    let getDistance (localPoint: float triple) =
      let globPoint = lt.xyzLoc2Glob localPoint
      match normalNoSign.x, normalNoSign.y, normalNoSign.z with
        | _, 0., 0. -> abs (globPoint.x - pointOnPlane.x) - 0.5
        | 0., _, 0. -> abs (globPoint.y - pointOnPlane.y) - 0.5
        | 0., 0., _ -> abs (globPoint.z - pointOnPlane.z) - 0.5
        | _ -> abs ((globPoint - pointOnPlane) * normalNoSign) - 0.5
    let distanceAndNormal (localPoint: float triple) = getDistance localPoint, getNormal localPoint
    let zml = 
      Report.BC.planeOfUnitDepth, ZamlSeq [
        Report.BC.purposeType, getPurposeZaml purpose
        Report.BC.thickness, ZamlF thickness
        Report.BC.pointOnPlane, ZamlT pointOnPlane
        Report.BC.normal, ZamlT normalNoSign]
    { Purpose = getPurpose purpose lt getNormal distanceAndNormal
      Shape = BcPlanes
      distanceAndNormal = distanceAndNormal
      getSymmetricCoordAndIndex = getSymmetricCoordAndIndex getNormal lt
      Description = zml
      Invisible = defaultArg invisible false }

  static member minX (lt: LBM.Lattice, purpose, ?offset, ?invisible) = Planes.infDepth (lt, purpose, triple(defaultArg offset 0,0,0), triple(1,0,0), ?invisible = invisible)
  static member maxX (lt: LBM.Lattice, purpose, ?offset, ?invisible) = Planes.infDepth (lt, purpose, triple(lt.NodesX_Glob - 1 - defaultArg offset 0,0,0), triple(-1,0,0), ?invisible = invisible)

  static member minY (lt: LBM.Lattice, purpose, ?offset, ?invisible) = Planes.infDepth (lt, purpose, triple(0,defaultArg offset 0,0), triple(0,1,0), ?invisible = invisible)
  static member maxY (lt: LBM.Lattice, purpose, ?offset, ?invisible) = Planes.infDepth (lt, purpose, triple(0,lt.NodesY_Glob - 1 - defaultArg offset 0,0), triple(0,-1,0), ?invisible = invisible)

  static member minZ (lt: LBM.Lattice, purpose, ?offset, ?invisible) = Planes.infDepth (lt, purpose, triple(0,0,defaultArg offset 0), triple(0,0,1), ?invisible = invisible)
  static member maxZ (lt: LBM.Lattice, purpose, ?offset, ?invisible) = Planes.infDepth (lt, purpose, triple(0,0,lt.NodesZ_Glob - 1 - defaultArg offset 0), triple(0,0,-1), ?invisible = invisible)

// ---------------------------------------------------------------------------------------
type Polygons =

  static member unitDepth (lt: LBM.Lattice, purpose: BcPurpose, vertices: float triple [], ?invisible) =
    if vertices.Length < 3 then failwith "error in getPolygonNoDepth"
    let segments = vertices |> Array.mapi (fun i vertex -> vertex, vertices.[(i+1) % vertices.Length])
    let normalNoSign =
      let v1 = vertices.[1] - vertices.[0]
      let v2 = vertices.[2] - vertices.[0]
      Triple.normalize (Triple.cross(v1, v2))
    let getNormal (localPoint: float triple) =
      let globPoint = lt.xyzLoc2Glob localPoint
      if (globPoint - vertices.[0]) * normalNoSign > 0. then normalNoSign else - normalNoSign
    let distanceAndNormal (localPoint: float triple) =
      let globPoint = lt.xyzLoc2Glob localPoint
      let distFromPlane =
        match normalNoSign.x, normalNoSign.y, normalNoSign.z with
        | _, 0., 0. -> abs (globPoint.x - vertices.[0].x)
        | 0., _, 0. -> abs (globPoint.y - vertices.[0].y)
        | 0., 0., _ -> abs (globPoint.z - vertices.[0].z)
        | _ -> abs ((globPoint - vertices.[0]) * normalNoSign)
      let normal = getNormal localPoint
      let pointOnPlane = globPoint - normal * distFromPlane
      let isInPoly = pointOnPlane |> Geometry.pointInPoly3D vertices
      if isInPoly then distFromPlane - 0.75, normal
      else
        let ps =
          let distSegments = segments |> Array.map (fun segment ->
              Geometry.ShortestDistance.segmentPoint segment globPoint )
          distSegments |> Array.minBy (fun ps -> Triple.norm(globPoint - ps))
        Triple.norm(globPoint - ps) - 0.75, normal
    let zml =
      Report.BC.polygon, ZamlSeq [
        Report.BC.purposeType, getPurposeZaml purpose
        Report.BC.vertices, vertices |> Array.mapi (fun i t -> Report.BC.vertex i, ZamlT t) |> seq |> ZamlSeq
        Report.BC.normal, ZamlT normalNoSign ]
    { Purpose = getPurpose purpose lt getNormal distanceAndNormal
      Shape = BcPolygons
      distanceAndNormal = distanceAndNormal
      getSymmetricCoordAndIndex = getSymmetricCoordAndIndex getNormal lt
      Description = zml
      Invisible = defaultArg invisible false }

type Surfaces =

  static member CylinderNoDepthNoCap (lt: LBM.Lattice, purpose: BcPurpose, p1: float triple, p2: float triple, radius: float, ?invisible) =
    let distanceAndNormal (localPoint: float triple) = // max error in distance = 0.2
      let globPoint = lt.xyzLoc2Glob localPoint
      let anyTangent (ni: float triple) =
        let a = if ni.z = 0. then triple(0., -ni.z, ni.y) elif ni.y = 0. then triple(-ni.x, 0., ni.z) else triple(-ni.x, ni.y, 0.)
        Triple.normalize a
      let getDNCap pNear pFar =
        let pointOnCircle =
          let tangent =
            let normalCircle = Triple.normalize (pNear - pFar)
            let pointProjectedOnPlane =
              let projectionDist = Triple.dot(globPoint - pNear, normalCircle)
              globPoint - projectionDist * normalCircle
            if pointProjectedOnPlane <> pNear then Triple.normalize (pointProjectedOnPlane - pNear)
            else anyTangent normalCircle
          pNear + radius * tangent
        let distVect = globPoint - pointOnCircle
        let distNorm = Triple.norm distVect
        distNorm - 0.7, distVect / distNorm    
      let pointOnSegment = Geometry.ShortestDistance.segmentPoint (p1, p2) globPoint
      if pointOnSegment = p1 then getDNCap p1 p2          
      elif pointOnSegment = p2 then getDNCap p2 p1
      else 
        let dv = globPoint - pointOnSegment
        let dist = Triple.norm dv - radius
        let normal =
          if dist > 0. then Triple.normalize dv
          elif dist = - radius then anyTangent (p1 - p2)
          else - Triple.normalize dv
        abs dist - 0.7, normal
    let getNormal p = snd (distanceAndNormal p)
    let zml = 
      Report.BC.hollowCylinder, ZamlSeq [
        Report.BC.purposeType, getPurposeZaml purpose
        Report.BC.point 0, ZamlT p1
        Report.BC.point 1, ZamlT p2
        Report.BC.radius, ZamlF radius ]
    { Purpose = getPurpose purpose lt getNormal distanceAndNormal
      Shape = BcCircularPlanes
      distanceAndNormal = distanceAndNormal
      getSymmetricCoordAndIndex = getSymmetricCoordAndIndex getNormal lt
      Description = zml
      Invisible = defaultArg invisible false }

type Bodies =

  static member Cylinder (lt: Lattice, purpose: BcPurpose, p1: float triple, p2: float triple, radius: float, ?invisible) =
    let distanceAndNormal (localPoint: float triple) = // max error in distance = 0.2
      let globPoint = lt.xyzLoc2Glob localPoint
      let anyTangent (ni: float triple) =
        let a = if ni.x = 0. then triple(0., -ni.z, ni.y) elif ni.y = 0. then triple(-ni.x, 0., ni.z) else triple(-ni.x, ni.y, 0.)
        Triple.normalize a
      let getDNCap pNear pFar =
        let pointOnCircle =
          let tangent =
            let normalCircle = Triple.normalize (pNear - pFar)
            let pointProjectedOnPlane =
              let projectionDist = Triple.dot(globPoint - pNear, normalCircle)
              globPoint - projectionDist * normalCircle
            if pointProjectedOnPlane <> pNear then Triple.normalize (pointProjectedOnPlane - pNear)
            else anyTangent normalCircle
          pNear + radius * tangent
        let distVect = globPoint - pointOnCircle
        let distNorm = Triple.norm distVect
        distNorm, distVect / distNorm    
      let pointOnSegment = Geometry.ShortestDistance.segmentPoint (p1, p2) globPoint
      if pointOnSegment = p1 then getDNCap p1 p2          
      elif pointOnSegment = p2 then getDNCap p2 p1
      else 
        let dv = globPoint - pointOnSegment
        let dist = Triple.norm dv - radius
        let normal =
          if dist > 0. then Triple.normalize dv
          elif dist = 0. then anyTangent (p1 - p2)
          else - Triple.normalize dv
        dist, normal
    let getNormal p = snd (distanceAndNormal p)
    let zml = 
      Report.BC.cylinder, ZamlSeq [
        Report.BC.purposeType, getPurposeZaml purpose
        Report.BC.point 0, ZamlT p1
        Report.BC.point 1, ZamlT p2
        Report.BC.radius, ZamlF radius ]
    { Purpose = getPurpose purpose lt getNormal distanceAndNormal
      Shape = BcCircularPlanes
      distanceAndNormal = distanceAndNormal
      getSymmetricCoordAndIndex = getSymmetricCoordAndIndex getNormal lt
      Description = zml
      Invisible = defaultArg invisible false }