﻿namespace LBM.PreProcess

open System.IO

open Utilities.Math
open Utilities.IO

open LBM
open LBM.Basic
open LBM.LO.Types
open LBM.Naming

type Report =

  static member local (lt: Lattice, ?lPO: LO.Types.LagrangianProblem, ?additionalZaml) =
    let z = 
      ZamlSeq [
        yield lt.ToZaml true
        yield (match lPO with Some x -> x.ToZaml true | None -> Report.LP.LagrangianProblem, ZamlS Report.none)
        match additionalZaml with Some z -> yield z | _ -> ()
      ] |> Zaml.stringify
    File.tryWriteAllText (General.Files.report, z)

  static member generalGlobal (path, lt: Lattice, bCp: BC.BcProblem, lbmFluid: Materials.LbmFluid Lazy, unitsO: LBM.Units option, ?lPO: LO.Types.LagrangianProblem, ?loA: LO [], ?additionalZaml: (string * ZamlType)) =
    let str = 
      ZamlSeq [
        yield lt.ToZaml false
        yield
          (match unitsO with 
            | Some u -> u.ToZaml
            | None -> Report.Units.unitsConversion, ZamlS Report.none)
        yield (try lbmFluid.Value.ToZaml() with _ -> "", ZamlS "")
        yield bCp.ToZaml()
        yield (match lPO with Some x -> x.ToZaml false | None -> Report.LP.LagrangianProblem, ZamlS Report.none)
        yield (Report.LP.lagObjects, 
          match loA with 
            | Some loA ->
              let s, f, e = LBM.LO.Types.LagrangianProblem.groupShapes loA
              let sAmount = LagrangianProblem.amountsOfShape s
              let fAmount = LagrangianProblem.amountsOfShape f
              let eAmount = LagrangianProblem.amountsOfShape e
              ZamlSeq [
                if sAmount.Length > 0 then yield Report.LP.spheres, ZamlSeq sAmount
                if fAmount.Length > 0 then yield Report.LP.fibers, ZamlSeq fAmount
                if eAmount.Length > 0 then yield Report.LP.symEllipsoids, ZamlSeq eAmount ]
            | None -> ZamlS Report.none)
        match additionalZaml with Some z -> yield z | _ -> ()
       ] |> Zaml.stringify
    File.tryWriteAllText(path + "\\" + General.Files.report, str)