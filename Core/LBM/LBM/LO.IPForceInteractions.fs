﻿module internal LBM.LO.IPForceInteractions

open LBM
open LBM.LO.Types
open LBM.Basic

open Kit
open Kit.Collections
open Kit.Math

let sphereSphere ((loi: LO, loj: LO), (si, sj), iPbehavior: InteractionsPP, lt: Lattice) =
  let hij, nij, _, _ = LO.HelpingFunctions.DistanceNormalDxiDxj.sphereSphere loi loj si sj
  let FiTiFjTj =
    let getLubricationForce (hLub, hCut) =
      let h = max hij hCut
      let visc =
        let v = loi.CoG + (si.Radius + 0.5 * h) * nij
        let xyz = v |> Triple.map (fun x -> x |> round |> int)
        match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
      let u = Triple.dot (loi.U - loj.U, nij)
      -6.0 * visc * pi * u * (si.Radius**2.0 * sj.Radius**2.0) / (si.Radius + sj.Radius)**2.0 * (1./h - 1./hLub)
    iPbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut =
        if hij > 0. && hij < hLub then
          let fLub = getLubricationForce (hLub, hCut)
          match sumO with
            | Some (fiSum, tiSum, fjSum, tjSum) -> Some (fiSum + fLub * nij, tiSum, fjSum - fLub * nij, tjSum)
            | None -> Some (fLub * nij, Triple.zerof, -fLub * nij, Triple.zerof)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut
        | LubricationFn fn ->
          match fn (loi.Shape, loj.Shape) with Some (hLub, hCut) -> fnFT hLub hCut | None -> sumO
        | HookForce (hHook, f0h) ->
          if hij < hHook && hij > 0.0 then 
            let fHook = f0h - f0h/hHook * hij
            match sumO with
              | Some (fiSum, tiSum, fjSum, tjSum) -> Some (fiSum + fHook * nij, tiSum, fjSum - fHook * nij, tjSum)
              | None -> Some (fHook * nij, Triple.zerof, -fHook * nij, Triple.zerof)
          else sumO
        | _ -> sumO ) None
  FiTiFjTj

let fiberSphere ((loi: LO, loj: LO), (fi: FiberType, sj), iPbehavior: InteractionsPP, lt: Lattice, (omegaGlobi, _)) =
  let hij, nij, dxi, _ = LO.HelpingFunctions.DistanceNormalDxiDxj.fibreSphere loi loj fi sj
  let FiTiFjTj =
    let FijTij (hLub, hCut) =
      let h = max hij hCut
      let visc =
        let v = loi.CoG + dxi + nij * 0.5 * hij
        let xyz = v |> Triple.map (fun x -> x |> round |> int)
        match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
      let loiU = loi.U + Triple.cross(omegaGlobi, dxi)
      let du = Triple.dot(loiU - loj.U, nij)
      let fLub = -6.0 * visc * pi * du * (fi.R**2.0 * sj.Radius**2.0) / (fi.R + sj.Radius)**2.0 * (1./h - 1./hLub)      
      let Fij = nij * fLub
      Fij, Triple.cross(dxi, Fij)
    iPbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut =
        if hij > 0. && hij < hLub then
          let Fij, Tij = FijTij (hLub, hCut)
          match sumO with
            | Some (fiSum, tiSum, fjSum, tjSum) -> Some (fiSum + Fij, tiSum + Tij, fjSum - Fij, tjSum)
            | None -> Some (Fij, Tij, -Fij, Triple.zerof)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut
        | LubricationFn fn ->
          match fn (loi.Shape, loj.Shape) with Some (hLub, hCut) -> fnFT hLub hCut | None -> sumO
        | _ -> sumO ) None
  FiTiFjTj

let fiberFiber ((loi: LO, loj: LO), (fi: FiberType, fj: FiberType), iPbehavior: InteractionsPP, 
                 lt: Lattice, (omegaGlobi, omegaGlobj) ) = 
  let hij, nij, dxi, dxj = HelpingFunctions.DistanceNormalDxiDxj.fiberFiber loi loj fi fj
  let FiTiFjTj =
    let getLubricationFijTiTj (hLub, hCut) =
      let hij = max hij hCut
      let du = Triple.dot(nij, loi.U + Triple.cross(omegaGlobi, dxi) - loj.U - Triple.cross(omegaGlobj, dxj))
      let visc =
        let xyz =
          let t = 0.5 * (loi.CoG + dxi + loj.CoG + dxj)
          t |> Triple.map (round >> int)
        match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
      let Fij =
        let rij = fi.R**2. * fj.R**2. / (fi.R + fj.R)**2.
        let aixajNorm =
          let aixajNorm = Triple.norm(Triple.cross(Triple.normalize dxi, Triple.normalize dxj))
          if aixajNorm < 1. / fi.AspectRatio then 1. / fi.AspectRatio else aixajNorm
        let h' = (1./hij - 1./hLub)
        let fLub = - 12. * visc * pi * du * rij / aixajNorm * h'
        nij * fLub
      Fij, Triple.cross(dxi, Fij), Triple.cross(dxj, -Fij)
    iPbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut =
        if hij > 0. && hij < hLub then
          let Fij, Tij, Tji = getLubricationFijTiTj (hLub, hCut)
          match sumO with
            | Some (fiSum, tiSum, fjSum, tjSum) -> Some (fiSum + Fij, tiSum + Tij, fjSum - Fij, tjSum + Tji)
            | None -> Some (Fij, Tij, -Fij, Tji)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut
        | LubricationFn fn ->
          match fn (loi.Shape, loj.Shape) with Some (hLub, hCut) -> fnFT hLub hCut | None -> sumO
        | _ -> sumO ) None
  FiTiFjTj

let symEllipsoidSymEllipsoid ((loi: LO, loj: LO), (l1, r1), (l2,r2), iPbehavior: InteractionsPP, lt: Lattice, (omegaGlobi, omegaGlobj)) =
  let hReal, normal, ri, rj, ai, aj =
    let h, nij, dxi, dxj = LO.HelpingFunctions.DistanceNormalDxiDxj.ellEll loi loj
    let piL, pjL = dxi |> Versor.rotateG2L loi.Rot, dxj |> Versor.rotateG2L loj.Rot
    let curvature le re x = le**6.0 * re**8.0 / (le**4.0 * re**4.0 + re**4.0 * (re**2.0 - le**2.0) * x**2.0 )**2.0
    h, nij, 1. / sqrt (curvature l1 r1 piL.x), 1. / sqrt (curvature l2 r2 pjL.x), dxi, dxj
  let FiTiFjTj =
    let FijTiTj (hLub, hCut) =
      let h = max hReal hCut
      let visc =
        let v = 0.5 * (loi.CoG + loj.CoG)
        let xyz = v |> Triple.map (round >> int)
        match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
      let Fij =
        let du = Triple.dot(loi.U + Triple.cross(omegaGlobi, ai) - loj.U - Triple.cross(omegaGlobj, aj), normal)
        let fLub = -6.0 * visc * pi * du * (ri**2.0 * rj**2.0) / (ri + rj)**2.0 * (1./h - 1./hLub) 
        normal * fLub
      let Tij, Tji = Triple.cross(ai, Fij), Triple.cross(aj, -Fij)
      Fij, Tij, Tji
    iPbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut =
        if hReal > 0. && hReal < hLub then
          let Fij, Tij, Tji = FijTiTj (hLub, hCut)
          match sumO with
            | Some (fiSum, tiSum, fjSum, tjSum) -> Some (fiSum + Fij, tiSum + Tij, fjSum - Fij, tjSum + Tji)
            | None -> Some (Fij, Tij, -Fij, Tji)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut
        | LubricationFn fn ->
          match fn (loi.Shape, loj.Shape) with Some (hLub, hCut) -> fnFT hLub hCut | _ -> sumO
        | _ -> sumO ) None
  FiTiFjTj

let symEllipsoidSphere ((loi: LO, loj: LO), ((le, re), r), iPbehavior: InteractionsPP, lt: Lattice, (omegaGlobi, _)) =
  let hReal, normal, rEff, ai =
    let h, n, _, pL = LO.HelpingFunctions.Ellipsoid.distanceNormalPoint_PointEllipsoid loi loj.CoG
    let curvature = le**6.0 * re**8.0 / (le**4.0 * re**4.0 + re**4.0 * (re**2.0 - le**2.0) * pL.x**2.0 )**2.0
    h - r, n, 1. / sqrt curvature, pL |> Versor.rotateL2G loi.Rot
  let FiTiFjTj =
    let FiTij (hLub, hCut) =
      let h = max hReal hCut
      let visc =
        let v = loj.CoG - (r + 0.5*h) * normal
        let xyz = v |> Triple.map (round >> int)
        match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
      let Fij = 
        let du = Triple.dot(loi.U + Triple.cross(omegaGlobi, ai) - loj.U, normal)
        let fLub = -6.0 * visc * pi * du * (r**2.0 * rEff**2.0) / (r + rEff)**2.0 * (1./h - 1./hLub)
        normal * fLub
      Fij, Triple.cross(ai, Fij)
    iPbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut =
        if hReal > 0. && hReal < hLub then
          let Fij, Tij = FiTij (hLub, hCut)
          match sumO with
            | Some (fiSum, tiSum, fjSum, tjSum) -> Some (fiSum + Fij, tiSum + Tij, fjSum - Fij, tjSum )
            | None -> Some (Fij, Tij, -Fij, Triple.zerof)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut 
        | LubricationFn fn ->
          match fn (loi.Shape, loj.Shape) with Some (hLub, hCut) -> fnFT hLub hCut | _ -> sumO
        | _ -> sumO) None
  FiTiFjTj

// -------------------------------------------------------------------------------------------------------------------

let getForces (lt: Lattice, time, loA: LO [], initLP: LagrangianProblem) =
  
  let Fsum, Tsum = Array.create loA.Length Triple.zerof, Array.create loA.Length Triple.zerof

  let getijFiTiFjTj ((loi: LO, loj: LO) as loij) =
    let inter = initLP.InteractionsPP
    match loi.Shape, loj.Shape with
      | Sphere si, Sphere sj -> 
        true, sphereSphere (loij, (si, sj), inter, lt)
      | Fiber fi, Fiber fj -> 
        true, fiberFiber (loij, (fi, fj), inter, lt, (loi.OmegaGlob, loj.OmegaGlob))
      | SymmetricEllipsoid se1, SymmetricEllipsoid se2 ->
        true, symEllipsoidSymEllipsoid (loij, (se1.MainHalfAxis, se1.Radius), (se2.MainHalfAxis, se2.Radius), inter, lt, (loi.OmegaGlob, loj.OmegaGlob))
      | SymmetricEllipsoid se, Sphere s ->
        true, symEllipsoidSphere (loij, ((se.MainHalfAxis, se.Radius), s.Radius), inter, lt, (loi.OmegaGlob, loj.OmegaGlob))
      | Sphere s, SymmetricEllipsoid se -> 
        false, symEllipsoidSphere ((loj, loi), ((se.MainHalfAxis, se.Radius), s.Radius), inter, lt, (loj.OmegaGlob, loi.OmegaGlob))
      | Sphere si, Fiber fj -> 
        false, fiberSphere ((loj, loi), (fj, si), inter, lt, (loj.OmegaGlob, loi.OmegaGlob))
      | Fiber fi, Sphere sj ->
        true, fiberSphere ((loi, loj), (fi, sj), inter, lt, (loi.OmegaGlob, loj.OmegaGlob))
      | SymmetricEllipsoid _, Fiber _ | Fiber _, SymmetricEllipsoid _ -> failwith "non implemented"

  initLP.iter3D (fun x y z ->
    let xyz = (x, y, z)
    if xyz = (1,1,1) then
      initLP.getPairsPP xyz time false |> Array.iter (fun (i, j, _) ->
        let notRot, data = getijFiTiFjTj (loA.[i], loA.[j])
        let i, j = if notRot then i, j else j, i
        data |> Option.iter (fun (Fi, Ti, Fj, Tj) ->
          Fsum.[i] <- Fsum.[i] + Fi
          Fsum.[j] <- Fsum.[j] + Fj
          Tsum.[i] <- Tsum.[i] + Ti
          Tsum.[j] <- Tsum.[j] + Tj ) )
    else
      initLP.getPairsPG xyz time false |> Array.iter (fun (i, ghost, _)  ->
        let notRot, data = getijFiTiFjTj (loA.[i], ghost)
        data |> Option.iter (fun (Fi, Ti, Fj, Tj) ->
          let F, T = if notRot then Fi, Ti else Fj, Tj
          Fsum.[i] <- Fsum.[i] + F
          Tsum.[i] <- Tsum.[i] + T ) ) )
  Fsum, Tsum