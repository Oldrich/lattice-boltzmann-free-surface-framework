﻿namespace LBM.PostProcess

open LBM.Naming
open LBM.LO.Types

open Utilities.PostProcess
open Utilities.Math
open Utilities.PostProcess.U3D
open Utilities.IO
open Utilities.PostProcess.U3D.Types

open System
open System.Collections.Generic
open System.IO
open System.Diagnostics

type U3Di () =

  let colors =
    let lightRed, red, _darkRed = (1.,0.4,0.4), (1., 0., 0.), (0.6, 0., 0.)
    let lightBlue, blue, darkBlue = (0.2, 0.8, 1.), (0.2, 0.2, 1.), (0., 0., 0.8)
    let lightGreen, green, darkGreen = (0., 1., 0.2), (0., 0.8, 0.), (0., 0.4, 0.)
    let lightYellow, yellow, _darkYellow = (1., 1., 0.8), (1., 1., 0.), (1., 0.8, 0.)
    let _lightOrange, orange, gold = (1., 0.6, 0.), (1., 0.4, 0.), (1., 0.8, 0.2)
    let white, lightGray, _gray, _darkGray, _black = (1., 1., 1.), (0.8, 0.8, 0.8), (0.6, 0.6, 0.6), (0.4, 0.4, 0.4), (0., 0., 0.)
    let lightBrown, brown, darkBrown = (0.6, 0.4, 0.), (0.4, 0.2, 0.), (0.2, 0., 0.)
    //ambient, diffuse, specular, emissive, reflectivity, opacity
    [
      [ blue, lightBlue, darkBlue, blue, 0., 0.75 // fluid blue
      ]
      [ brown, lightBrown, gold, darkBrown, 0.5, 1. // BC
      ]
      [ lightBlue, gold, white, lightGray, 0., 1. // Fibers
      ]
      [ (1.,0.,0.), (0.,1.,0.), (0.,0.,1.), (1.,0.,0.), 0., 1. // Spheres
      ]
      [ (1.,0.,0.), (0.,1.,0.), (0.,0.,1.), (1.,0.,0.), 0., 1. // Ellipsoids
      ]
      [ lightGreen, darkBlue, darkGreen, green, 0., 0.75 // OrientationEllipsoids
        lightYellow, gold, orange, yellow, 0., 1.
        lightRed, gold, orange, red, 0., 1.
      ]
    ]

  let u3dDict, groups = Dictionary (), Dictionary ()

  let shaderRA, materialRA, markerToColor =
    let s, m, mc = List (), List (), Dictionary()
    let n = ref 0
    colors |> List.iteri (fun i colorL ->
      colorL |> List.iteri (fun j color ->
        let shader, material = sprintf "Shader %d" !n, sprintf "Material %d" !n
        s.Add (U3D.Resources.Shader (shader, material))
        m.Add (U3D.Resources.Material (material, color))
        mc.Add ((i,j),!n)
        incr n ) )
    s, m, mc
  
  member o.addGroup (name, parent) =
    let id = name + parent
    if groups.ContainsKey id = false then
      groups.Add (id, U3D.Nodes.Group(name, [parent, Array2D.eye 4]))

  member o.addGroup name = o.addGroup (name, "")

  member o.add (groupName, nodeName: string, surface: U3Dmesh, difColor, ?specColor, ?visibility) =
    let visibility = defaultArg visibility "FRONT"
    let faceDiffuseColorIdA, modelDiffuseColorA =
      let faceLength = (Tuple.fst surface).Length
      Array.create faceLength (0,0,0), [| difColor |]
    let faceSpecularColorIdA, modelSpecularColorA =
      match specColor with
        | Some color ->
          let faceLength = (Tuple.fst surface).Length
          Some (Array.create faceLength (0,0,0)), Some [| color |]
        | None -> None, None
    if u3dDict.ContainsKey nodeName = false then
      let resourceName = nodeName + " Resource"
      let node = U3D.Nodes.Mesh (nodeName, [groupName, Array2D.eye 4], resourceName, visibility)
      let mesh = U3D.Resources.MeshModel (resourceName, surface, faceDiffuseColorIdA, modelDiffuseColorA, ?faceSpecularColorIdA = faceSpecularColorIdA, ?modelSpecularColorA = modelSpecularColorA)
      u3dDict.Add (nodeName, (node, mesh, None))
    else
      let _, mesh, _ = u3dDict.[nodeName]
      mesh.Add (surface, faceDiffuseColorIdA, modelDiffuseColorA, ?faceSpecularColorIdA = faceSpecularColorIdA, ?modelSpecularColorA = modelSpecularColorA)

  member o.add (groupName, shapeName: string, marker, surface: U3Dmesh, ?visibility) =
    let marker = markerToColor.[marker]
    let visibility = defaultArg visibility "FRONT"
    let nodeName = String.Format ("{0}, Marker={1}", shapeName, marker)
    if u3dDict.ContainsKey nodeName = false then
      let resourceName = nodeName + " Resource"
      let shaderName = String.Format ("Shader {0}", marker)
      let node = U3D.Nodes.Mesh (nodeName, [groupName, Array2D.eye 4], resourceName, visibility)
      let mesh = U3D.Resources.MeshModel (resourceName, surface)
      let modifier = U3D.Modifiers.Shading (nodeName, shaderName)
      u3dDict.Add (nodeName, (node, mesh, Some modifier))
    else
      let _, mesh, _ = u3dDict.[nodeName]
      mesh.Add surface

  member o.write (outputFile: string) =
    use sw = new StreamWriter (outputFile, false, Text.Encoding.ASCII)
    let nodes, meshes, shadings = u3dDict |> Seq.map (fun v -> v.Value) |> Array.ofSeq |> Array.unzip3
    U3D.Main.u3d (sw, (groups.Values, nodes), (meshes, shaderRA, materialRA, []), ([], shadings |> Array.choose id))
    sw.Close()

type U3D (isBC, isFluid, isLoA, ?orientationEllipsoids, ?acceptTime, ?acceptBC, ?acceptLO, ?fluidMarker, ?bcMarker, ?refresh, ?isInvisibleBC, ?isInvisibleFluid, ?refreshScreenshot) =

  static member private transformMesh (scale: triple) (rot: quaternion) (shift: triple) ((facePositionIdA, modelPositionA, faceNormalIdA, modelNormalA): U3Dmesh): U3Dmesh =
    let rot = Utilities.Rotation.QuaternionRotation rot
    let newModelPositionA = modelPositionA |> Array.map (fun (x,y,z) ->
      let nxyz = triple (x * scale.X, y * scale.Y, z * scale.Z)
      let v = rot.rotateL2G nxyz
      (shift + v).toTuple )
    let newModelNormalA = modelNormalA |> Array.map (fun xyz -> (rot.rotateL2G (triple xyz)).toTuple)
    facePositionIdA, newModelPositionA, faceNormalIdA, newModelNormalA

  member private o.takeScreenShotU3D (outputPath, view, fileName: string) =
    let tex = (Utilities.IO.File.readResource "Screenshot.tex").Replace("{0}",view).Replace("{1}", fileName)
    File.WriteAllText (Path.Combine(outputPath, "trash.tex"), tex)
    let p1, outStr = Utilities.Shell.shellExecute ("pdflatex", "-interaction=nonstopmode -synctex=1 trash.tex", outputPath)
    if p1.ExitCode <> 0 then printfn "%s" outStr
    else
      let p2, _ =
        Utilities.Shell.shellExecute (@"c:\Program Files (x86)\Adobe\Reader 10.0\Reader\AcroRd32.exe", "/A \"navpanes=0\" trash.pdf", outputPath, waitForExit = false, maximized = true)
      Utilities.GUI.TopMostMessageBox.Show "Center the 3D and then close this window." |> ignore
      System.Threading.Thread.Sleep 500
      let bmp =
        let bmp = Utilities.Screen.capture ()
        let rect =
          let rec find isX fn i =
            let p = if isX then bmp.GetPixel(i, bmp.Height / 2) else bmp.GetPixel(bmp.Width / 2, i)
            if p.R = 26uy && p.G = 26uy && p.B = 51uy then i else find isX fn (fn i 1)
          let minX, minY = find true (+) 0, find false (+) 0
          let maxX, maxY = find true (-) (bmp.Width - 1), find false (-) (bmp.Height - 1)
          new System.Drawing.Rectangle(minX, minY, maxX - minX, maxY - minY)
        bmp.Clone(rect, bmp.PixelFormat)
      bmp.Save (Path.Combine (outputPath, "screenshot.png"))
      p2.Kill()
    for file in (DirectoryInfo outputPath).GetFiles "trash*" do
      try File.Delete file.FullName with _ -> ()

  member o.write (problemPath, outputPath) =

    let outputDir = Directory.CreateDirectory outputPath

    let acceptTime = defaultArg acceptTime (fun _ -> true)
    let acceptBC = defaultArg acceptBC (fun _ -> true)
    let acceptLO = defaultArg acceptLO (fun _ -> true)
    let fluidMarker = defaultArg fluidMarker 0
    let bcMarker = defaultArg bcMarker 0

    let globZaml = Zaml.parse (File.ReadAllText (Path.Combine(problemPath, General.Files.report)))
    let nodes = globZaml.find [Report.Lattice.lattice; Report.Lattice.globDim] |> Zaml.toTriple

    let realTimeToLbm =
      match globZaml.tryFind [Report.Units.unitsConversion; Report.Units.scalingTime] with
        | Some (ZamlS s) -> (Utilities.String.findAllFloats s).[0]
        | _ -> 1.0

    let getGroupName (lbmTime, full) =
      let timeStr = if full then "Time " else ""
      String.Format ("{0}{1:0.####}", timeStr, float lbmTime / realTimeToLbm)

    let u3dDir = Path.Combine (outputPath, "U3D")
    let getU3DFile (time: int) = String.Format ("u3d_{0}.u3d", (getGroupName (time, false)).Replace(".", "_"))
    let getU3DPath time = Path.Combine (u3dDir, getU3DFile time)

    let u3dDict = SortedDictionary ()
    let addU3D (lbmTime, fn: U3Di -> unit) =
      if u3dDict.ContainsKey lbmTime = false then u3dDict.Add (lbmTime, List [fn]) else u3dDict.[lbmTime].Add fn

    let bcU3D (u3d: U3Di) =
      match globZaml.find Report.BC.boundaryConditions with
        | ZamlSeq zs ->

          let planeToBrick (pointOnPlane: triple, normal: triple) =
            let meshShift =
              match normal.X, normal.Y, normal.Z with
              | _, 0., 0. -> Some (Shapes.brick (1., nodes.Y, nodes.Z), triple(pointOnPlane.X, 0.5 * nodes.Y - 0.5, 0.5 * nodes.Z - 0.5))
              | 0., _, 0. -> Some (Shapes.brick (nodes.X, 1., nodes.Z), triple(0.5 * nodes.X - 0.5, pointOnPlane.Y, 0.5 * nodes.Z - 0.5))
              | 0., 0., _ -> Some (Shapes.brick (nodes.X, nodes.Y, 1.), triple(0.5 * nodes.X - 0.5, 0.5 * nodes.Y - 0.5, pointOnPlane.Z))
              | _ -> None
            meshShift |> Option.map (fun (mesh, shift) -> U3D.transformMesh Triple.Ones Quaternion.Zero shift mesh)

          u3d.addGroup "Boundary Conditions"
          zs |> Seq.iteri (fun i (name, data) ->
            let checkPurpose =
              match data.tryFind Report.BC.purpose with
                | Some (ZamlS s) when s = Report.BC.bounceBack -> true
                | _ -> false
            let visibility =
              match isInvisibleBC with Some isInvisibleBC when isInvisibleBC i -> "NONE" | _ -> "FRONT" 
            if checkPurpose && acceptBC i then
              if (name = Report.BC.planeOfInfDepth || name = Report.BC.planeOfUnitDepth) && checkPurpose then
                let p = data.find Report.BC.pointOnPlane |> Zaml.toTriple
                let n = data.find Report.BC.normal |> Zaml.toTriple
                planeToBrick (p, n) |> Option.iter (fun brick -> u3d.add ("Boundary Conditions", sprintf "Plane %d" i, (1, bcMarker), brick, visibility))
              elif name = Report.BC.polygon && checkPurpose then
                let vertices =
                  match data.find Report.BC.vertices with
                    | ZamlSeq s -> s |> Seq.map (snd>>Zaml.toTriple) |> Seq.toArray
                    | _ -> failwith "not a sequence"
                u3d.add ("Boundary Conditions", sprintf "Polygon %d" i, (1, bcMarker), Shapes.planePolygon vertices, visibility) )
        | _ -> ()

    let rc = LBM.PostProcess.ReadAndCombine problemPath

    if isFluid then
      let visibility =
        match isInvisibleFluid with Some isInvisibleFluid when isInvisibleFluid -> "NONE" | _ -> "BOTH" 
      rc.readFluidShape |> Seq.iter (fun (KeyValue(time, data)) ->
        addU3D (time, fun u3d ->
          let fluidHeights = Array2D.zeroCreate (int nodes.X) (int nodes.Y)
          for KeyValue((x,y),z) in data.Value do fluidHeights.[x, y] <- float z
          u3d.add (getGroupName (time, true), "Fluid", (0, fluidMarker), U3D.Shapes.fluidMesh fluidHeights, visibility) ))

    if isLoA then
      let sphereSurfaces, fiberSurfaces = Dictionary (), Dictionary ()
      let loNames = ["Spheres"; "Cylinders"; "Symmetric Ellipsoids"]
      rc.readLoA |> Seq.iter (fun (KeyValue(time, data)) ->
        addU3D (time, fun u3d ->
          data.Value |> Array.iter (fun ((_, marker), (shift, rot), shape, _ as lo) ->
            if acceptLO (time, lo) then
              let id, surface, colorI = 
                match shape with
                  | Sphere s ->
                    let surface =
                      if sphereSurfaces.ContainsKey (s.Diameter) then sphereSurfaces.[s.Diameter] else U3D.Shapes.sphere (s.Diameter, 1)
                    0, surface, 3
                  | Fiber f ->
                    let surface =
                      if fiberSurfaces.ContainsKey (f.Length, f.D, f.D) then fiberSurfaces.[f.Length, f.D, f.D] else U3D.Shapes.fiber (f.Length, f.D)
                    1, surface, 2
                  | SymmetricEllipsoid _ ->
                    //2, fineSphereSurface, triple(2. * e.MainHalfAxis, e.Diameter, e.Diameter), 4
                    failwith "Not implemented"
              u3d.add (getGroupName (time, true), loNames.[id], (colorI, marker), U3D.transformMesh Triple.Ones rot shift surface, visibility = "BOTH") )))

    match orientationEllipsoids with
      | Some (dx: float, layers) ->
        let nx, ny, nz =
          let fn lx dx = int (ceil (lx / dx))
          fn nodes.X dx, fn nodes.Y dx, fn nodes.Z dx
        let dxT = nodes ./ triple (nx, ny, nz)
        let sphere = U3D.Shapes.sphere (dx, 1)
        rc.readLoA |> Seq.iter (fun (KeyValue(time, data)) ->
          addU3D (time, fun u3d ->
            let arr3D = Array3D.init nx ny nz (fun _ _ _ -> List ())
            data.Value |> Array.iter (fun (_, (shift, rot), shape, interaction as lo) ->
              if acceptLO (time, lo) then
                match shape, interaction with
                  | Fiber f, TwoWay | Fiber f, TrackingParticle ->
                    let i, j, k =
                      let fn i = min (int (shift.[i] / dxT.[i])) ((arr3D.GetLength i) - 1)
                      fn 0, fn 1, fn 2
                    let rot = Utilities.Rotation.QuaternionRotation rot
                    let px = triple(f.Length, 0., 0.)
                    arr3D.[i,j,k].Add (rot.rotateL2G px)
                  | _ -> () )
            arr3D |> Array3D.iteri (fun i j k pxA ->
              if pxA.Count > 0 then
                Utilities.Math.OrientationEllipse.pToEllipsoid pxA |> Option.iter (fun (vals, vecs) ->
                  let mesh =
                    let shift = dxT .* triple(float i + 0.5,float j + 0.5, float k + 0.5)
                    let scale =
                      let va =
                        let fn v = max (min v 0.95) 0.05
                        triple(fn vals.[0], fn vals.[1], fn vals.[2])
                      va
                    U3D.transformMesh scale (Quaternion.ofRotMatrix vecs) shift sphere
                  let difColor =
                    let minV, maxV = Array.min vals, Array.max vals
                    float (maxV - minV), 1. - float (maxV - minV), 0., 1.
                  let speColor = 1.,1.,1.,1.
                  match layers with
                    | Some ori ->
                      let index = let ijk = [i;j;k] in ijk.[ori]
                      u3d.add (getGroupName (time, true), sprintf "Orientation Ellipsoids L%d" index, mesh, difColor, speColor)
                    | None -> u3d.add (getGroupName (time, true), "Orientation Ellipsoids", (5,0), mesh) ))))
      | None -> ()

    if Directory.Exists u3dDir = false then Directory.CreateDirectory u3dDir |> ignore

    let acceptedTimeA =
      let lbmTimeA = u3dDict.Keys |> Array.ofSeq
      let isAcceptedTimeA = lbmTimeA |> Array.mapi (fun i time -> i = 0 || i = lbmTimeA.Length - 1 || acceptTime time)
      let firstTime = lbmTimeA.[isAcceptedTimeA |> Array.findIndex ((=) true)]
      u3dDict |> Array.ofSeq |> Array.mapi (fun i (KeyValue(lbmTime, u3dL)) ->
        let save () =
          let idtfFile = Path.Combine (u3dDir, String.Format ("u3d_{0}.idtf", lbmTime))
          let u3d = U3Di ()
          for fn in u3dL do fn u3d
          if isBC && lbmTime = firstTime then bcU3D u3d
          u3d.addGroup (getGroupName (lbmTime, true))
          u3d.write idtfFile
          let p, _ = Utilities.Shell.shellExecute (@"C:\Programs\U3D\IDTFConverter.exe", sprintf "-input \"%s\" -output \"%s\"" idtfFile (getU3DPath lbmTime))
          Console.WriteLine ("{0}) ExitCode = {1}", lbmTime, p.ExitCode)
          if p.ExitCode = 0 then File.Delete idtfFile
        if isAcceptedTimeA.[i] then
          if File.Exists (getU3DPath lbmTime) = false then save ()
          else match refresh with Some r when r -> save ()  | _ -> ()
          Some lbmTime
        else None ) |> Array.choose id

    let view = View ()
    view.addBasicViews (0.5 * nodes, 2. * nodes.norm)
    view.write (Path.Combine (outputPath, "u3d.vws"))

    let defaultView =
      let defaultRadius, defaultCog2Camera, defaultCog = 2. * nodes.norm, triple(-1,-1,1), 0.5 * nodes
      String.Format ("3Droo={0}, 3Dcoo= {1} {2} {3}, 3Dc2c={4} {5} {6}", defaultRadius, defaultCog.X, defaultCog.Y, defaultCog.Z, defaultCog2Camera.X, defaultCog2Camera.Y, defaultCog2Camera.Z)

    let tex =
      let firstFile = sprintf "%s/U3D/%s" outputDir.Name (getU3DFile acceptedTimeA.[0])
      let resourcesStr =
        let str = acceptedTimeA |> Array.choosei (fun i lbmTime ->
          if i = 0 then None
          else Some (sprintf "addresource = {%s/U3D/%s}" outputDir.Name (getU3DFile lbmTime)) ) |> String.concat ", "
        if str.Length > 0 then ",\n" + str else ""
      let str = LBM.PostProcess.Helping.find ("U3D", "Report.tex")
      str.Replace("{dir}", outputDir.Name).Replace("{resources}", resourcesStr).Replace("{defView}", defaultView).Replace("{file}", firstFile)
    File.WriteAllText (Path.Combine (outputPath, "u3d.tex"), tex)

    let js =
      let resourceIdA = acceptedTimeA |> Array.map (fun lbmTime -> getGroupName (lbmTime, false)) |> String.concat ", "
      (Utilities.IO.File.readResource @"PostProcess.U3D.js").Replace("/*0*/", resourceIdA).Replace("{1}", outputDir.Name)
    File.WriteAllText (Path.Combine (outputPath, "u3d.js"), js)

    let fileName = "U3D/" + (getU3DFile acceptedTimeA.[0])
    if File.Exists (Path.Combine(outputPath, "screenshot.png")) = false then o.takeScreenShotU3D (outputPath, defaultView, fileName)
    else match refreshScreenshot with Some true -> o.takeScreenShotU3D (outputPath, defaultView, fileName) | _ -> ()