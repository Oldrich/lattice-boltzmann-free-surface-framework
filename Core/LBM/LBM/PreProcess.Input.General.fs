﻿namespace LBM.PreProcess.Input

open System.IO
open System.Collections.Generic
open System.Threading

open Utilities.Basic
open Utilities.Math
open Utilities.IO

open LBM
open LBM.Basic
open LBM.BC.Types
open LBM.BC.Shapes
open LBM.LO.Types
open LBM.LO.Integration
open LBM.Naming.General
open LBM.Naming.Input

type Convergence =
  | FractionOfCellsAtMaxTau of float
  | TotalMass of float
  | MaxTime of int

type UserConverge =
  | Exit
  | Continue
  | DontCare

type _Solids internal (g: General) =

  let realPSD = lazy (g.getFTL Solids.Spheres.realSphereSizeDistribution |> List.map (fun (a,b) -> 1.<m> * a, b))
  let lbmPSD = lazy (
    if g.IsReal then realPSD.Value |> List.map (fun (a,b) -> a |> g.Units.toLbm.Length, b)
    else g.getFTL Solids.Spheres.lbmSphereSizeDistribution )

  // Common
  member o.CoeffOfRestitution = g.getF Solids.Other.coeffOfRestitution
  member o.Friction = g.getF Solids.Other.dynamicFrictionCoef, g.getF Solids.Other.staticFrictionCoef
  
  member o.LbmMaxSize =
    if g.IsReal then
      let fiberLength = 1.<m> * (defaultArg (g.tryGetF Solids.Fibers.realFiberLength) 0.) |> g.Units.toLbm.Length
      let sphereD = 1.<m> * (defaultArg (g.tryGetF Solids.Spheres.realMaxSphereD) 0.) |> g.Units.toLbm.Length
      let ellipsoidD = 1.<m> * (defaultArg (g.tryGetF Solids.Ellipsoids.realMaxEllipsoidD) 0.) * (defaultArg (g.tryGetF Solids.Ellipsoids.ellipsoidAspectRatio) 0.) |> g.Units.toLbm.Length
      max ellipsoidD (max fiberLength sphereD)
    else
      let fiberLength = defaultArg (g.tryGetF Solids.Fibers.lbmFiberLength) 0.
      let sphereD = defaultArg (g.tryGetF Solids.Spheres.lbmMaxSphereD) 0.
      let ellipsoidD = (defaultArg (g.tryGetF Solids.Ellipsoids.lbmMaxEllipsoidD) 0.) * (defaultArg (g.tryGetF Solids.Ellipsoids.ellipsoidAspectRatio) 0.)
      max ellipsoidD (max fiberLength sphereD)

  member o.LoOffsetCos = (1 + int o.LbmMaxSize) / 2 + 3

  // spheres
  member o.RealSphereDensity = 1.<kg/m^3> * (g.getF Solids.Spheres.realSphereDensity)
  member o.LbmSphereDensity =
    if g.IsReal then o.RealSphereDensity |> g.Units.toLbm.Density
    else g.getF Solids.Spheres.lbmSphereDensity

  member o.RealMinSphereD = 1.<m> * (g.getF Solids.Spheres.realMinSphereD)
  member o.LbmMinSphereD =
    if g.IsReal then g.Units.toLbm.Length o.RealMinSphereD
    else g.getF Solids.Spheres.lbmMinSphereD

  member o.RealMaxSphereD = 1.<m> * (g.getF Solids.Spheres.realMaxSphereD)
  member o.LbmMaxSphereD =
    if g.IsReal then g.Units.toLbm.Length o.RealMaxSphereD
    else g.getF Solids.Spheres.lbmMaxSphereD

  member o.RealPSD = realPSD.Value
  member o.LbmPSD = lbmPSD.Value

  member o.RelativeSphereGap = g.getF Solids.Spheres.relativeSphereGap

  member o.SphereVolumeFraction = g.getF Solids.Spheres.sphereVolumeFraction
  member o.MaxGeneratedSphereVolumeFraction = g.getF Solids.Spheres.maxGeneratedSphereVolumeFraction

  member o.FictiousAggregates = g.getB Solids.Spheres.fictiousAggregates

  member o.LbmDLength = g.getF Solids.Other.lbmDLength
  
  // Fibers
  member o.RealFiberLength = 1.<m> * (g.getF Solids.Fibers.realFiberLength)
  member o.LbmFiberLength =
    if g.IsReal then o.RealFiberLength |> g.Units.toLbm.Length
    else g.getF Solids.Fibers.lbmFiberLength

  member o.RealFiberDensity = 1.<kg/m^3> * (g.getF Solids.Fibers.realFiberDensity)
  member o.LbmFiberDensity =
    if g.IsReal then o.RealFiberDensity |> g.Units.toLbm.Density
    else g.getF Solids.Fibers.lbmFiberDensity

  member o.FiberVolumeFraction = g.getF Solids.Fibers.fiberVolumeFraction
  member o.MaxGeneratedFiberVolumeFraction = g.getF Solids.Fibers.maxGeneratedFiberVolumeFraction  

  member o.FiberAspectRatio = g.getF Solids.Fibers.fiberAspectRatio
  member o.RelativeFiberGap = g.getF Solids.Fibers.relativeFiberGap

  member o.lbmFiberDiameter = o.LbmFiberLength / o.FiberAspectRatio

  // ellipsoids
  member o.RealEllipsoidDensity = 1.<kg/m^3> * (g.getF Solids.Ellipsoids.realEllipsoidDensity)
  member o.LbmEllipsoidDensity =
    if g.IsReal then o.RealEllipsoidDensity |> g.Units.toLbm.Density
    else g.getF Solids.Ellipsoids.lbmEllipsoidDensity

  member o.RealMinEllipsoidD = 1.<m> * (g.getF Solids.Ellipsoids.realMinEllipsoidD)
  member o.LbmMinEllipsoidD =
    if g.IsReal then g.Units.toLbm.Length o.RealMinEllipsoidD
    else g.getF Solids.Ellipsoids.lbmMinEllipsoidD

  member o.RealMaxEllipsoidD = 1.<m> * (g.getF Solids.Ellipsoids.realMaxEllipsoidD)
  member o.LbmMaxEllipsoidD =
    if g.IsReal then g.Units.toLbm.Length o.RealMaxEllipsoidD
    else g.getF Solids.Ellipsoids.lbmMaxEllipsoidD

  member o.RelativeEllipsoidGap = g.getF Solids.Ellipsoids.relativeEllipsoidGap

  member o.EllipsoidVolumeFraction = g.getF Solids.Ellipsoids.ellipsoidVolumeFraction
  member o.MaxGeneratedEllipsoidVolumeFraction = g.getF Solids.Ellipsoids.maxGeneratedEllipsoidVolumeFraction

  member o.EllipsoidAspectRatio = g.getF Solids.Ellipsoids.ellipsoidAspectRatio

and _Runtime internal (g: General) as o =

  let unitsO = if g.IsReal then Some g.Units else None

  let lt, bcP, lp = ref None, ref None, ref None

  let w = lazy (new Runtime.Write (g.Args.[Args.dirPath], o.Lattice, g.MaxTauL, g.LbmFluidL, ?lPO = !lp))

  let get v str = match !v with Some v -> v | None -> failwith str

  let isRunning = new ManualResetEvent true

  let mutable isFractionOfCellsAboveTau = true
  let mutable isMinMaxMeanShearRate = true

  //let _publicWCF = if g.IsMainDomain then Some (PreProcess.PublicWCF(g.TaskName, isRunning)) else None
  
  interface System.IDisposable with
    member o.Dispose () =
      isRunning.Dispose()
      (w.Value :> System.IDisposable).Dispose()

  member o.IsFractionOfCellsAboveTau with get () = isFractionOfCellsAboveTau and set v = isFractionOfCellsAboveTau <- v
  member o.IsMinMaxMeanShearRate with get () = isMinMaxMeanShearRate and set v = isMinMaxMeanShearRate <- v

  member o.Writer = w.Value

  member o.Lattice with get () = get lt "_Runtime: Lattice does not exist" and set v = lt := Some v
  member o.BcProblem with get () = get bcP "_Runtime: BcProblem does not exist" and set v = bcP := Some v
  member o.LagrangianProblem with get () = get lp "_Runtime: LagrangianProblem does not exist" and set v = lp := Some v
  member o.LagrangianProblemO with get () = !lp and set v = lp := v

  member o.createLattice (nodes, latticeBoundary, loOffset, ?timer) =
    let nodesX, nodesY, nodesZ = nodes
    let index = Array2D.create nodesX nodesY nodesZ
    let parallelization = {curId = g.CurId; mainId = g.MainId; nDomains = g.NDomains; loOffsetD = loOffset}
    let timer = defaultArg timer (Timer [Levels.L0 1; Levels.All 50])
    o.Lattice <- Lattice (index, nodesZ, g.Consts, latticeBoundary, parallelization, g.Args, timer = timer)
    o.Lattice

  member o.setBcProblem (bcp: BC.BcProblem) = bcP := Some bcp
  
  member o.createLagrangianProblem (?frictionPP, ?forceInteractonPP, ?collisionDistancePP, ?fictitiousParticlesPP, ?frictionBC, ?forceInteractonBC, ?collisionDistanceBC, ?fictitiousParticlesBC, ?initLoA, ?fiberFit, ?deactivatePairsPP) =
    let maxRKstep = defaultArg (g.tryGetF Solids.Other.maxRKstep) 1.0
    let controls = {
      TwoWayIntegrationMethod = RungeKutta.fehlberg, (IntegrationControlsType maxRKstep )
      TwoWayDiracDelta3D = LBM.HelpingFunctions.DiracDelta.cosine
      TrackingDiracDelta3D = LBM.HelpingFunctions.DiracDelta.smoothenLinear }
    let interactionsPP = 
      let collisionDistance = defaultArg collisionDistancePP 0.
      { forceInteraction = defaultArg forceInteractonPP []
        friction = defaultArg frictionPP (FrictionLawType.CoulombFriction g.Solids.Friction) |> Some
        collisionFnP = fun _ _ dist -> dist < collisionDistance
        cR = g.Solids.CoeffOfRestitution
        interactingDistance = 2. * (g.LbmUmax + collisionDistance)
        fictitiousParticleFn = fictitiousParticlesPP } |> Some
    let interactionsBC = 
      let collisionDistance = defaultArg collisionDistanceBC 0.
      { forceInteraction = defaultArg forceInteractonBC []
        friction = defaultArg frictionBC (FrictionLawType.CoulombFriction g.Solids.Friction) |> Some
        collisionFn = fun _ _ dist -> dist < collisionDistance
        cR = g.Solids.CoeffOfRestitution
        interactingDistance = 2. * (g.LbmUmax + collisionDistance)
        fictitiousParticleFn = fictitiousParticlesBC }
    lp := Some(LagrangianProblem (o.Lattice, o.BcProblem.Objects, defaultArg initLoA [||], controls, interactionsPP, interactionsBC, 2. * g.LbmMaxPotentialSpeed, g.Solids.LbmMaxSize, g.LbmViscosity, ?fiberFit = fiberFit, ?deactivatePairsPP = deactivatePairsPP))
    o.LagrangianProblem
  
  member o.generateLoaInMain (?fluidVolume, ?gCogAngleSphere, ?gCogAngleFibers, ?gCogAngleEllipsoids, ?acceleration, ?uAngularU, ?initLoA, ?compacter, ?interaction, ?vtkIsCylinder) =
    if g.CurId = g.MainId then
      let generatorData = 
        let gravityAccel = defaultArg acceleration g.LbmGravityAccelZ
        let moveRotateInteraction = Triple.Ones, Triple.Ones, defaultArg interaction TwoWay
        let uAngularU = defaultArg uAngularU (Triple.Zero, Triple.Zero)
        {
        FluidVolume = defaultFn fluidVolume (fun () ->
          let sumV = o.BcProblem.bytePhaseG |> GArray3D.sumBy (fun v -> if v < 201uy then float v else 0.0)
          sumV / 200.0 )
        SphereData = gCogAngleSphere |> Option.map (fun gCogAngleSphere ->
          let generateLO =           
            match g.tryGetFTL Solids.Spheres.lbmSphereSizeDistribution with
              | Some _ ->
                let generatePSD = LO.ParticleDistributions.PSD.getAggregateDiameterFromPSD g.Solids.LbmPSD
                fun vf ->
                  let d = generatePSD g.Solids.SphereVolumeFraction vf
                  if d > g.Solids.LbmMinSphereD && vf < g.Solids.MaxGeneratedSphereVolumeFraction then
                    let shape = Sphere (SphereType(diameter = d))
                    let (gCog: triple), (angle: triple) = gCogAngleSphere()
                    let lCog = o.Lattice.xyzGlob2Loc gCog
                    LO.Spheres.sphereToLo shape (lCog, angle) g.Solids.LbmSphereDensity gravityAccel g.Solids.LbmDLength uAngularU moveRotateInteraction 1e-3 0 |> Some
                  else None
              | None ->
                let shape = Sphere (SphereType(diameter = g.Solids.LbmMaxSphereD))
                fun vf ->
                  if vf < g.Solids.MaxGeneratedSphereVolumeFraction then
                    let (gCog: triple), (angle: triple) = gCogAngleSphere()
                    let lCog = o.Lattice.xyzGlob2Loc gCog
                    LO.Spheres.sphereToLo shape (lCog, angle) g.Solids.LbmSphereDensity gravityAccel g.Solids.LbmDLength uAngularU moveRotateInteraction 1e-3 0 |> Some
                  else None
          g.Solids.SphereVolumeFraction, generateLO )
        FiberData = gCogAngleFibers |> Option.map (fun gCogAngleFibers ->
          let fiber = FiberType (g.Solids.LbmFiberLength, g.Solids.FiberAspectRatio, ?vtkIsCylinder = vtkIsCylinder, dLength = 1.0)
          let generateLO _volumeFraction =
            let (gCog: triple), (angle: triple) = gCogAngleFibers()
            let lCog = o.Lattice.xyzGlob2Loc gCog
            LO.Fibers.fiberToLO moveRotateInteraction ((lCog, angle), uAngularU) g.Solids.LbmFiberDensity gravityAccel fiber 0 |> Some
          g.Solids.FiberVolumeFraction, generateLO )
        EllipsoidData = gCogAngleEllipsoids |> Option.map (fun gCogAngleEllipsoids ->
          let generateLO vf =
            let ell = SymmetricEllipsoidType( dLength = g.Solids.LbmDLength, MainHalfAxis = 0.5 * g.Solids.LbmMaxEllipsoidD * g.Solids.EllipsoidAspectRatio, diameter = g.Solids.LbmMaxEllipsoidD)
            if vf < g.Solids.MaxGeneratedEllipsoidVolumeFraction then
              let (gCog: triple), (angle: triple) = gCogAngleEllipsoids()
              let lCog = o.Lattice.xyzGlob2Loc gCog
              LO.Ellipsoids.ellipsoidToLo ell (lCog, angle) g.Solids.LbmEllipsoidDensity gravityAccel uAngularU moveRotateInteraction 0 |> Some
            else None
          g.Solids.EllipsoidVolumeFraction, generateLO )
        gapFnP =
          let relFibGap = defaultArg (g.tryGetF Solids.Fibers.relativeFiberGap) 0.0
          fun s1 _ -> match s1 with Sphere s1 -> g.Solids.RelativeSphereGap * s1.Radius | Fiber f -> relFibGap * f.D | SymmetricEllipsoid se -> g.Solids.RelativeEllipsoidGap * se.Radius
        gapFnBc = fun s1 _ -> match s1 with Sphere s1 -> g.Solids.RelativeSphereGap * s1.Radius | Fiber _ -> 0. | SymmetricEllipsoid se -> g.Solids.RelativeEllipsoidGap * se.Radius
        Compacter =
          match compacter with
            | Some c when c ->
              if g.Solids.MaxGeneratedSphereVolumeFraction <= 0.25 then None 
              elif g.Solids.MaxGeneratedSphereVolumeFraction <= 0.35 then 
                let maxGap =
                  let sphereG = if g.Solids.MaxGeneratedSphereVolumeFraction > 0. then g.Solids.RelativeSphereGap * g.Solids.LbmMaxSphereD else 0.
                  let ellG = if g.Solids.MaxGeneratedEllipsoidVolumeFraction > 0. then g.Solids.RelativeEllipsoidGap * g.Solids.LbmMaxEllipsoidD else 0.
                  max sphereG ellG
                LargestDistance maxGap |> Some 
              else
                let lbmLx, lbmLy, lbmLz = g.getI General.lbmDomLx, g.getI General.lbmDomLy, g.getI General.lbmDomLz
                MoveToPoint (triple (lbmLx / 2, lbmLy / 2, lbmLz / 2), None, None, None) |> Some
            | _ -> None
        }
      let gen = LO.Generator (o.Lattice, o.BcProblem, generatorData, defaultArg initLoA [], ?checkIsInFluid = g.tryGetB (Solids.Other.checkIsInFluid), ?checkOverlapsPP = g.tryGetB (Solids.Other.checkOverlapsPP))
      gen.generate ()
    else [||]

  member o.initAndReport (?distributeLOA: LO [], ?rhoUTauInit, ?additionalZamlLoc, ?additionalZamlGlob) =
    let w = w.Value
    let rhoUTauInit = defaultArg rhoUTauInit (fun _ -> 1.0, Triple.Zero, 1.0)
    o.Lattice.initializeLBM (o.BcProblem.LocInitPhaseA, rhoUTauInit)     
    distributeLOA |> Option.iter (fun loA -> o.LagrangianProblem.distributeLoA (1, loA))
    if o.Lattice.DomId = g.MainId then
      PreProcess.Report.generalGlobal(g.Args.[Args.dirPath], o.Lattice, o.BcProblem, g.LbmFluidL, unitsO, ?lPO = lp.Value, ?loA = distributeLOA, ?additionalZaml = additionalZamlGlob)
    PreProcess.Report.local(o.Lattice, ?lPO = lp.Value, ?additionalZaml = additionalZamlLoc)
    w.VTK.saveBoth 0; w.binaryLOA; w.binaryFluidShape

  member o.propagate time =
    let lt = o.Lattice
    lt.Timer.setLbmTime time
    lt.Timer.doTimer 0 (fun timer ->
      if timer.IsRunning then lt.Timer.tockPrintTick 0 (System.String.Format("{0}: Step duration", time - 1)) else lt.Timer.tick 0)
    //lt.Timer.printString 0 (sprintf "               Time = %d" time)
    match !lp with
      | Some lp -> lp.propagateLBM_LO time
      | None -> lt.Par.propagateLBM ()

  member o.run (freeSurface, ?convergence: Convergence, ?forceFn, ?loopFn1, ?analyzeFn, ?terminateFn, ?quiet, ?dontAskToEnd, ?computeBcForces, ?lbmCallbacks) =
    
    let w = w.Value
    let lt, bcp = o.Lattice, o.BcProblem
    let nCol, wCol = ref 0., ref 0.
    let getForce = defaultArg forceFn (fun _ -> g.LbmGravityAccelZ)

    let convFnName = "defaultInternalConvergenceFunction"

    if lt.DomId = lt.MainID && File.Exists @"..\Delete me to exit" = false then File.Create(@"..\Delete me to exit").Dispose()

    let internalConvData =
      match convergence with
        | Some (FractionOfCellsAtMaxTau f) ->
          let convTau = 0.9 * g.MaxTau
          let convFr = o.Lattice.Par.WCF.Exchange1D(convFnName + "FractionOfCells", fun (l: (float*float) list) ->
            let sumMass = l |> List.sumBy fst
            let fractionOfCellsAboveTau = l |> List.sumBy (fun (mass, maxTauFr) ->
              mass * maxTauFr / sumMass )
            lt.Timer.printAny 1 (sprintf "Fraction above converge tau (%g)" convTau) fractionOfCellsAboveTau
            fractionOfCellsAboveTau > f )
          fun (time, w: Runtime.Write) -> convFr.sendToMain (time, (w.Evaluate.FluidMass, w.Evaluate.FractionOfCellsAboveTau convTau))
        | Some (TotalMass m) ->
          let convMass = o.Lattice.Par.WCF.Exchange1D (convFnName + "Mass", fun l -> List.sum l >= m)
          fun (time, w: Runtime.Write) -> convMass.sendToMain (time, w.Evaluate.FluidMass)
        | Some (MaxTime t) ->
          let convMaxTime = o.Lattice.Par.WCF.Exchange1D(convFnName + "Time", fun l -> l.[0] >= t)
          fun (time: int, _: Runtime.Write) -> convMaxTime.sendToMain (time, time)
        | None -> fun _ -> false

    let terminateFn = defaultArg terminateFn (fun time -> w.VTK.saveBoth time; w.binaryFluidShape; w.binaryLOA)

    let updateLbm = Lbm.update (lt, o.BcProblem, g.getTau, getForce, computeBcForces, lbmCallbacks)

    let endDateExch =
      let endDate = ref None
      let ex = o.Lattice.Par.WCF.Exchange1D("internalEndDateFn", fun (_: int list) ->
        let askUser () =
          let hours =
            match dontAskToEnd with
              | Some true -> 0.0
              | _ ->
                let title = "The end of the simulation"
                let description = g.Args.[Args.dirPath] + "\n\nIn how many hours would you like to exit? 0 = immediate exit."
                float (Microsoft.VisualBasic.Interaction.InputBox (description, title, "0.0"))
          endDate := Some (System.DateTime.Now.AddHours hours)
        match !endDate with | None -> askUser() | Some date when System.DateTime.Now > date -> askUser() | _ -> ()
        System.Threading.Thread.Sleep 100
        System.DateTime.Now > endDate.Value.Value )
      ex

    let globT = Utilities.Basic.Timer ()
    globT.tick

    let loup = LO.Updater (lt, bcp.Objects, ?lPO = !lp)
    
    let rec mainLoop time =

      isRunning.WaitOne() |> ignore

      loopFn1 |> Option.iter (fun fn -> fn time)
      
      o.propagate time
      
      let nC, wC = loup.update (time)
      nCol := !nCol + nC
      wCol := !wCol + wC

      updateLbm time

      if freeSurface then FreeSurface.rePhase time lt

      w._refresh time

      if time % g.SaveFluidShapeEvery = 0 then w.binaryFluidShape
      if time % g.SaveLOAEvery = 0 then w.binaryLOA
      if time % g.SaveVtkLBMEvery = 0 then w.VTK.saveLBM time
      if time % g.SaveVtkLOAEvery = 0 then w.VTK.saveLOA time

      if time % g.AnalyzeEvery = 0 then
        globT.print "Total Duration"
        match quiet with
          | Some true -> ()
          | _ ->
            w.fluidMass
            if isFractionOfCellsAboveTau then w.fractionOfCellsAboveTauA
            if isMinMaxMeanShearRate then w.minMaxMeanShearRate
            w.meanTau
            w.minMaxMeanLbmSpeed
            w.minMaxMeanRhoDiff
            !lp |> Option.iter (fun _ ->
              w.particleVolumeFraction
              w.kineticEnergyLO()
              w.numberOfParticles
              w.numberOfCollisionsDissipatedEnergy (!nCol, !wCol) )
        
        let internalConverged = internalConvData (time, w)
        let userConverged = match analyzeFn with Some fn -> fn (time, w, (!nCol, !wCol)) | None -> DontCare

        for w in g.UserWriters do w.Value.Flush()

        nCol := 0.; wCol := 0.
        match internalConverged, userConverged, File.Exists @"..\Delete me to exit" with
        | false, DontCare, true
        | _, Continue, true -> mainLoop (time + 1)
        | _, _, _ ->
          let isEnd = endDateExch.sendToMain (time, 0)
          if isEnd then terminateFn time else mainLoop (time + 1)
      else mainLoop (time + 1)

    mainLoop 1

and General (args: IDictionary<string,string>, curId: (int*int*int), mainId, ?inputDict) =
  
  let inputDict = defaultFn inputDict (fun () -> args.[Args.inputFile] |> File.ReadAllText |> ML.parse)
  let getSi, getIi, getFi, getBi, getFTLi =
    let inline fn f = ML.failGet f inputDict
    fn string, fn int, fn float, fn boolean, fn (list <| tuple float)
  let tryGetSi, tryGetIi, tryGetFi, tryGetBi, tryGetFTLi =
    let inline fn f = ML.tryGet f inputDict
    fn string, fn int, fn float, fn boolean, fn (list <| tuple float)

  let isReal = Option.isSome (tryGetFi General.real0_1Visc)
  let seed = lazy (getIi General.seed)
  let rnd = lazy (Utilities.Random seed.Value)

  let realYieldStress = lazy (1.<Pa> * (getFi Fluid.realYieldStress))
  let realViscosity = lazy (1.<Pa s> * (getFi Fluid.realViscosity))
  let realFluidDensity = lazy (1.<kg/m^3> * (getFi Fluid.realDensity))
  let lbm1cm = lazy (getFi General.lbm1cm)
  let real0_1Visc = lazy (1.<Pa s> * (getFi General.real0_1Visc))
  let units = lazy (Units(0.01<m>, real0_1Visc.Value, realFluidDensity.Value, lbm1cm.Value, 0.1))

  let lbmViscosity = lazy (
    if isReal then realViscosity.Value |> units.Value.toLbm.DynamicViscosity
    else getFi Fluid.lbmViscosity )

  let lbmYieldStress = lazy (
    if isReal then realYieldStress.Value |> units.Value.toLbm.Stress
    else getFi Fluid.lbmYieldStress )

  let lbmFluid = lazy (
    if isReal then Materials.BinghamPlastic(realYieldStress.Value, realViscosity.Value, units.Value) :> Materials.LbmFluid
    else Materials.BinghamPlastic(lbmYieldStress.Value, lbmViscosity.Value) :> Materials.LbmFluid )

  let maxTau = lazy (getFi Fluid.maxTau)

//  let convTau = lazy (
//    let tau =
//      if isReal then
//        let visc = 1.0<Pa s> * (getFi Fluid.realViscosity) |> units.Value.toLbm.DynamicViscosity
//        visc * 3.0 + 0.5
//      else (getFi Fluid.lbmViscosity) * 3.0 + 0.5
//    let convTau = 8. * tau
//    if convTau >= maxTau.Value then failwithf "convTau = %g > maxTau = %g" convTau maxTau.Value
//    convTau )

  let getTauP =
    ref (fun (_time, _xyz, shearRate, shearStress) ->
          match lbmFluid.Value.getApparentViscosity shearRate shearStress with
            | Some visc ->
              let nTau = 0.5 + 3. * visc
              if nTau < 0.501 then 0.501 elif nTau > maxTau.Value then maxTau.Value else nTau
            | None -> 1.0 )

  let tryGet (nLBM, nReal) =
    match tryGetIi nLBM, tryGetFi nReal with
      | Some _, Some _ -> failwith "Either LBM or REAL units should be set only"
      | Some i, None -> i
      | None, Some iReal -> max (int (units.Value.toLbm.Time (1.0<s> * iReal))) 1
      | _ -> System.Int32.MaxValue

  let analyzeEvery = lazy (tryGet (General.analyzeEvery, General.analyzeEveryReal))
  let saveFluidShapeEvery = lazy (tryGet (General.saveFluidShapeEvery, General.saveFluidShapeEveryReal))
  let saveLOAEvery = lazy (tryGet (General.saveLOAEvery, General.saveLOAEveryReal))
  let saveVtkLBMEvery = lazy (tryGet (General.saveVtkLBMEvery, General.saveVtkLBMEveryReal))
  let saveVtkLOAEvery = lazy (tryGet (General.saveVtkLOAEvery, General.saveVtkLOAEveryReal))

  let mutable solids = None
  let mutable runtime = None

  let userWriters = Dictionary ()

  member o.TaskName = o.getS General.taskName

  member o.GetUserWriter name =
    let fileName = sprintf "@%s.txt" name
    let fs = new FileStream (fileName, FileMode.Append, FileAccess.Write, FileShare.Read)
    let w = new StreamWriter (fs)
    userWriters.Add (name, w)
    w

  member o.UserWriters: Dictionary<string, StreamWriter> = userWriters

  member o.AnalyzeEvery = analyzeEvery.Value
  member o.SaveFluidShapeEvery = saveFluidShapeEvery.Value
  member o.SaveLOAEvery = saveLOAEvery.Value
  member o.SaveVtkLBMEvery = saveVtkLBMEvery.Value
  member o.SaveVtkLOAEvery = saveVtkLOAEvery.Value

  member o.CurId = curId
  member o.MainId = mainId

  member o.IsMainDomain = mainId = curId

  member o.Args: IDictionary<string,string> = args

  member o.Solids: _Solids =
    match solids with
      | Some s -> s
      | None ->
        let out = _Solids o
        solids <- Some out
        out

  member o.Runtime =
    match runtime with
      | Some r -> r
      | None ->
        let r = new _Runtime (o)
        runtime <- Some r
        r

  member o.getS = getSi
  member o.getI = getIi
  member o.getF = getFi
  member o.getB = getBi
  member o.getFTL = getFTLi

  member o.tryGetS = tryGetSi
  member o.tryGetI = tryGetIi
  member o.tryGetF = tryGetFi
  member o.tryGetB = tryGetBi
  member o.tryGetFTL = tryGetFTLi

  member o.NDomains =
    let nDomains = o.getS General.nDomains
    let out = nDomains.Split [||] |> Array.map int
    out.[0], out.[1], out.[2]

  member o.Consts =
    let incmpr = defaultArg (o.tryGetB Fluid.isIncompressible) false
    let ismrt = defaultArg (o.tryGetB Fluid.isMRT) false
    Constants.D3Q15 incmpr ismrt
  member o.Seed = seed.Value
  member o.Rnd = rnd.Value
  
  member o.Tolerance = o.getF General.tolerance

  member o.LbmGravityAccelZ = triple (0., 0., o.Units.toLbm.Acceleration -9.81<m/s^2>)

  member o.Units: Units = units.Value

  member o.MaxTau = maxTau.Value
  member o.MaxTauL = maxTau
  //member o.ConvTau = convTau.Value
  member o.LbmUmax = o.getF Fluid.lbmMaxPotentialSpeed
  member o.getTau with get () = !getTauP and set fn = getTauP := fn

  member o.LbmFluid = lbmFluid.Value
  member o.LbmFluidL = lbmFluid
  member o.LbmViscosity = lbmViscosity.Value
  member o.LbmYieldStress = lbmYieldStress
  
  member o.LbmMaxPotentialSpeed = o.getF Fluid.lbmMaxPotentialSpeed

  member o.SlipCoeff = o.getF Fluid.slipCoeff

  member o.IsReal = isReal

  member o.reset () =
    match runtime with
      | Some r ->
        (r :> System.IDisposable).Dispose()
        runtime <- None
      | _ -> ()

  interface System.IDisposable with
    member o.Dispose () =
      runtime |> Option.iter (fun v -> (v :> System.IDisposable).Dispose())
      for w in userWriters do w.Value.Close()
  