﻿namespace LBM.PostProcess.Matlab

//open System.IO
//open LBM.PostProcess
//open Utilities.Math
//
//type Plate =
//
//  static member getSimulationLines (fiberLength, path, ?filterFn: (triple -> bool), ?fiberSplitN) =
//    let n = defaultArg fiberSplitN 1
//    let filterFn = defaultArg filterFn (fun _ -> true)
//    let file = File.ReadAllLines path
//    let lst = System.Collections.Generic.Stack()
//    for line in file do
//      let data = line.Split [| '\t' |] |> Array.map float
//      let p1 = triple (data.[1], data.[2], data.[3])
//      let v = triple (data.[4], data.[5], data.[6])
//      if filterFn v then
//        let dx = fiberLength / float n
//        for i in 0 .. n - 1 do lst.Push (p1 + float i * dx * v, p1 + float (i + 1) * dx * v) // spatne
//    lst.ToArray()
//
//  static member private linesToArray3D (nX, nY, nZ) (minX, minY, minZ) (maxX, maxY, maxZ) (res: (triple * triple) seq) =
//
//    let dx, dy, dz = (maxX - minX) / float nX, (maxY - minY) / float nY, (maxZ - minZ) / float nZ
//
//    let out = Array3D.create nX nY nZ []
//    res |> Seq.iter (fun (p1, p2) ->
//      let idX, idY, idZ =
//        let cog = (p1 + p2) / 2.
//        min (max (min (int ((cog.X - minX) / dx)) (nX - 1)) 0) (nX - 1),
//        min (max (min (int ((cog.Y - minY) / dy)) (nY - 1)) 0) (nY - 1),
//        min (max (min (int ((cog.Z - minZ) / dz)) (nZ - 1)) 0) (nZ - 1)
//      out.[idX, idY, idZ] <- (p2 - p1) :: out.[idX, idY, idZ] )
//
//    out, dx, dy, dz
//
//  static member plotSimulationEllipses (refresh, problemDirPath, plateSizeX, plateSizeY, plateSizeZ, nX, nY, nZ, plane: Plane, displayPlanes, origin, fiberLength: float, ?lineSpec, ?doForEachView, ?m: Utilities.Matlab) =
//
//    let origin1, origin2 = origin
//
//    let lineSpec = defaultArg lineSpec ""
//    let m = defaultFn m (fun () -> Utilities.Matlab true)
//
//    m.holdOn
//
//    let (dir1, dir2, dir3), n3, (d1, d2, d3), (size1, size2), nXYZ12, getX12 =
//      let dX, dY, dZ = plateSizeX / float nX, plateSizeY / float nY, plateSizeZ / float nZ
//      match plane with
//        | XY -> (0,1,2), nZ, (dX, dY, dZ), (plateSizeX, plateSizeY), (nX, nY, 1), fun (i,j,_) -> (float i + 0.5) * dX, (float j + 0.5) * dY
//        | XZ -> (0,2,1), nY, (dX, dZ, dY), (plateSizeX, plateSizeZ), (nX, 1, nZ), fun (i,_,k) -> (float i + 0.5) * dX, (float k + 0.5) * dZ
//        | YZ -> (1,2,0), nX, (dY, dZ, dX), (plateSizeY, plateSizeZ), (1, nY, nZ), fun (_,j,k) -> (float j + 0.5) * dY, (float k + 0.5) * dZ
//
//    let lines =
//      let chooseFiles (fileA: FileInfo []) =
//        let m = fileA |> Array.maxBy (fun file -> file.CreationTime)
//        [| m |]
//      let loAfiles = (ReadAndCombine problemDirPath).oldWriteParticlesTypeCoGandAxis (chooseFiles, refresh)
//      ReadAndCombine.oldParticlesTypeCoGandAxis2 (loAfiles.[0].FullName, fun (_, v) -> abs v.X <> 1. && abs v.Y <> 1. && abs v.Z <> 1.0)
//
//    let i1 = ref -1
//    for i3 = 0 to n3 - 1 do
//
//      if displayPlanes |> Seq.exists (fun p -> p = i3) then
//        incr i1
//    
//        let lines = lines |> Array.choose (fun (cog, v) ->
//          let p1, p2 = cog - v * 0.5 * fiberLength, cog + v * 0.5 * fiberLength
//          let min3, max3 = float i3 * d3, float (i3 + 1) * d3
//          if p1.[dir3] > min3 && p2.[dir3] > min3 && p1.[dir3] < max3 && p2.[dir3] < max3 then Some (p1,p2) else None )
//   
//        if lines.Length > 0 then
//          let arr3D, _,_,_ = Plate.linesToArray3D nXYZ12 (0.,0.,0.) (plateSizeX, plateSizeY, plateSizeZ) lines
//
//          let origin1 = origin1 + 1.1 * float !i1 * size1
//          doForEachView |> Option.iter (fun fn -> fn (origin1, origin2) (size1, size2) (d1, d2))
//
//          m.plot ([origin1; origin1 + size1; origin1 + size1; origin1; origin1], [origin2; origin2; origin2 + size2; origin2 + size2; origin2], linespec = "k-", other = ",'LineWidth', 0.2")
//
//          let ellA = arr3D |> Array3D.map (fun fibers ->
//            let fibers2D = fibers |> List.map (fun fiber -> fiber.[dir1], fiber.[dir2])
//            Utilities.Math.OrientationEllipse.pToEllipse fibers2D )
//          
//          let scale =
//            let maxAB = ellA |> Array3D.maxIBy (function Some (_, a, b, _) -> max a b | None -> 0.)
//            0.5 * (min d1 d2) / maxAB
//
//          ellA |> Array3D.iteri (fun i j k ell ->
//            ell |> Option.iter (fun (phi, a, b, _len) ->
//              let x, y = getX12 (i,j,k)
//              //let scale = len * 1.
//              m.plotEllipse (origin1 + x, origin2 + y, scale * a, scale * b, phi, linespec = lineSpec, other = ",'LineWidth', 0.5") ))