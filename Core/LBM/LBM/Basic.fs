﻿module LBM.Basic

open LBM

open Kit
open Kit.IO
open Kit.Math
open Kit.Collections

open System.Collections.Generic

let trashData = Dictionary<string, obj>()
let trashFn = Dictionary<string, obj->unit>()

type LatticeBoundary =
  | LTPeriodic
  | LTFail

type NormalVector = triple<float>
type VelocityVector = triple<float>
type PositionVector = triple<float>
type ForceVector = triple<float>

type Phase = 
  | Fluid
  | Gas
  /// MASS !!!
  | Interface of float
  | BoundaryCondition of int
  static member toFloat (v: Phase) =
    match v with
      | Fluid -> 2.
      | Gas -> -1.
      | BoundaryCondition v -> float (-2 - v) //<_, -2>
      | Interface w when w > -0.9 && w < 1.9 -> w // <-0.1, 1.1>
      | Interface w -> failwithf "Phase.toFloat: Interface %g not supported" w 
  static member ofFloat v =
    if v <= -2. then BoundaryCondition (-2 - int v)
    elif v = -1. then Gas
    elif v > -0.9 && v < 1.9 then Interface v
    elif v = 2. then Fluid
    else failwithf "Phase.ofFloat: %g does not exist" v

type PhaseArray = Phase array3

type RhoArrayO = float option array3
type UArrayO = float option array3
type FArrayO = float [] option array3
type TauArrayO = float option array3

type DxQx (dim, aDim, c, weights: float [], opposite, knownUnknownTan: NormalVector -> (int list) * (int list) * (int list) * int, isIncompressible, relaxFn) = 
  let dimF = float dim
  let aDimA = [| 1 .. aDim - 1 |]
  let cF = c |> Array.map Triple.ofList
  let cL2 = cF |> Array.map (fun c -> Triple.dot(c, c))
  let cs, cs2, cs4 = sqrt(1./3.), 1./3., 1./9.
  let cc = Array.init aDim (fun i -> Triple.outer(cF.[i], cF.[i]))
  let q = cc |> Array.map (Array2D.mapi (fun x y m-> if x = y then m-1./3. else m))
  
  member o.Dim = dim
  member o.Dimf = dimF
  member o.ADim = aDim
  member o.ADimA = aDimA
  member o.C = c
  member o.Cf = cF
  member o.Cl2 = cL2
  member o.Opposite = opposite
  member o.W = weights
  member o.Cs = cs
  member o.Cs2 = cs2
  member o.Cs4 = cs4
  member o.CC = cc
  member o.Q = q
  member o.knownUnknownTan = knownUnknownTan
  member o.IsIncompressible = isIncompressible
  
  member o.getRhoU (f: float []) =
    let rho = Array.sum f
    let fe = f |> Array.sumByi (fun i fi -> fi * cF.[i] )
    if isIncompressible then rho, fe else rho, fe / rho
  
  member o.getForceTerm (F: triple<float>) i =
    weights.[i] * (F * cF.[i]) * 3. //1st order
    //consts.W.[i] * ((consts.Cf.[i] - u) * F + 3. * (consts.Cf.[i] * u) * (consts.Cf.[i] * F)) //2nd order
  
  member o.collide (f: float [], fNeq: float [], relaxationTime: float, F: ForceVector) =
    let (fRelaxed: float []) = relaxFn fNeq relaxationTime
    f |> Array.mapi (fun i f ->
      let forceTerm = o.getForceTerm F i
      f - fRelaxed.[i] + forceTerm)

  member o.getfEq (rho, u: VelocityVector, i) = 
    let ci_u = Triple.dot(cF.[i], u)
    if isIncompressible then weights.[i] * ( rho + 3. * ci_u + 4.5 * ci_u * ci_u - 1.5 * u * u ) //incompressible LBM + u = sum f / 1.0
    else weights.[i] * rho * ( 1. + 3. * ci_u + 4.5 * ci_u * ci_u - 1.5 * u * u ) //compressible LBM
  
  member o.getfEq (rho, u: VelocityVector, u_u, i) = 
    let ci_u = Triple.dot cF.[i] u
    if isIncompressible then weights.[i] * ( rho + 3. * ci_u + 4.5 * ci_u * ci_u - 1.5 * u_u ) //incompressible LBM + u = sum f / 1.0
    else weights.[i] * rho * ( 1. + 3. * ci_u + 4.5 * ci_u * ci_u - 1.5 * u_u ) //compressible LBM
  
  member o.getfEqA (rho, velocity: VelocityVector) = 
    let uu = velocity * velocity
    Array.init aDim (fun i -> o.getfEq (rho, velocity, uu, i))

  override o.ToString() = sprintf "D%dQ%d" o.Dim o.ADim

module Constants =
  let D3Q15 isIncompressible isMRT =
    let c =
      [|[0.;0.;0.];   //0
        [1.;0.;0.];    //1
        [-1.;0.;0.];   //2
        [0.;1.;0.];    //3
        [0.;-1.;0.];   //4
        [0.;0.;1.];    //5
        [0.;0.;-1.];   //6
        [1.;1.;1.];    //7
        [1.;1.;-1.];   //8
        [1.;-1.;1.];   //9
        [1.;-1.;-1.];  //10
        [-1.;1.;1.];   //11
        [-1.;1.;-1.];  //12
        [-1.;-1.;1.];  //13
        [-1.;-1.;-1.]|] //14
    let w = [| 2./9.; 1./9.;1./9.;1./9.;1./9.;1./9.;1./9.; 1./72.;1./72.;1./72.;1./72.;1./72.;1./72.;1./72.;1./72. |]
    let opposite = [|0; 2; 1; 4; 3; 6; 5; 14; 13; 12; 11; 10; 9; 8; 7|]
    let knownUnknownTan (n: NormalVector) =
        match n.x, n.y, n.z with
          | -1.,  0.,  0. -> [1;7;8;9;10], [2;11;12;13;14], [0;3;4;5;6], 0// East
          |  1.,  0.,  0. -> [2;11;12;13;14], [1;7;8;9;10], [0;3;4;5;6], 0// West
          |  0.,  1.,  0. -> [3;7;8;11;12], [4;9;10;13;14], [0;1;2;5;6], 1// North
          |  0., -1.,  0. -> [4;9;10;13;14], [3;7;8;11;12], [0;1;2;5;6], 1// South
          |  0.,  0.,  1. -> [6;8;10;12;14], [5;7;9;11;13], [0;1;2;3;4], 2// Rear
          |  0.,  0., -1. -> [5;7;9;11;13], [6;8;10;12;14], [0;1;2;3;4], 2// Front
          | _ -> failwith "function 'known' in Basic / D2Q9: unsupported normal triple"
    
    let relaxFn =
      if isMRT then   
        let m, mInv =
          let m =
            let cF = c |> Array.map Triple.ofList
            [ cF |> Array.map (fun _ -> 1.) // 0
              cF |> Array.map (fun t -> Triple.norm2 t - 2.) // 1
              cF |> Array.map (fun t -> 0.5 * (15. * (Triple.norm2 t)**2.0 - 55. * Triple.norm2 t + 32.)) // 2
              cF |> Array.map (fun t -> t.x) // 3
              cF |> Array.map (fun t -> 0.5 * (5. * Triple.norm2 t - 13.) * t.x) // 4
              cF |> Array.map (fun t -> t.y) // 5
              cF |> Array.map (fun t -> 0.5 * (5. * Triple.norm2 t - 13.) * t.y) // 6
              cF |> Array.map (fun t -> t.z) // 7
              cF |> Array.map (fun t -> 0.5 * (5. * Triple.norm2 t - 13.) * t.z) // 8
              cF |> Array.map (fun t -> 3. * t.x**2.0 - Triple.norm2 t ) // 9
              cF |> Array.map (fun t -> t.y**2.0 - t.z**2.0) // 10
              cF |> Array.map (fun t -> t.x * t.y) // 11
              cF |> Array.map (fun t -> t.y * t.z) // 12
              cF |> Array.map (fun t -> t.x * t.z) // 13
              cF |> Array.map (fun t -> t.x * t.y * t.z) ] |> array2D // 14
          let invM = Array2D.invert3x3 m
          ArrayOfArrays.init 15 15 (fun i j -> m.[i, j]) , ArrayOfArrays.init 15 15 (fun i j -> invM.[i, j])
        let s = [| Some 0.; Some 1.6; Some 1.2; Some 0.; Some 1.6; Some 0.; Some 1.6; Some 0.; Some 1.6; None; None; None; None; None; Some 1.2 |]
        (fun fNeq relaxationTime ->
          let SmNeq =
            let mNeq: float [] = m * fNeq
            s |> Array.mapi (fun i s ->
              let s = defaultArg s (1./relaxationTime)
              s * mNeq.[i])
          mInv * SmNeq )
      else (fun fNeq relaxationTime -> fNeq |> Array.map (fun f -> f / relaxationTime))
    DxQx(3, 15, c, w, opposite, knownUnknownTan, isIncompressible, relaxFn)