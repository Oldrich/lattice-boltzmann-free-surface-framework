﻿module LBM.Materials

open Utilities.IO
open LBM
open LBM.Basic
open LBM.Naming

let checkMinMaxTau tauMin tauMax =
  (fun tau -> if tau < tauMin then tauMin elif tau > tauMax then tauMax else tau)

let limitTauChange limitFrom limitTo limitPar maxChange =
  (fun tauPrev tau ->
    let dTau = tau - tauPrev
    if dTau < limitFrom then tau
    elif dTau < limitTo then tauPrev + limitPar * dTau
    else tauPrev - maxChange * float (sign dTau) )

[<AbstractClass>]
type LbmFluid() =
  abstract member getApparentViscosity : float -> float -> float option
  abstract member getShearRateAndStressFromTau : float -> (float * float)
  abstract member ToZaml : unit -> string * ZamlType
  member o.Density = 1.0

type BinghamPlastic(lbmYieldStress: float, lbmPlasticViscosity: float, ?dontUse: LBM.Units) =
  inherit LbmFluid()
  override o.getApparentViscosity shearRate shearStress =
    if lbmYieldStress = 0.0 then Some lbmPlasticViscosity
    elif shearRate = 0. then None
    elif shearStress < lbmYieldStress then Some 1e15
    else Some (lbmYieldStress / shearRate + lbmPlasticViscosity)
  
  override o.getShearRateAndStressFromTau tau =
    let nu = (tau - 0.5) / 3.
    let shearRate = lbmYieldStress / (nu - lbmPlasticViscosity)
    let shearStress = nu * shearRate
    shearRate, shearStress

  override o.ToZaml() =
    match dontUse with
      | Some u ->
        Report.Fluid.fluidProperties, ZamlSeq [
          Report.Fluid.realFluidProp, ZamlSeq [
            Report.Fluid.realYieldStress, ZamlS <| sprintf "%A Pa" (lbmYieldStress |> u.toReal.Stress )
            Report.Fluid.realViscosity, ZamlS <| sprintf "%A Pa.s" (lbmPlasticViscosity |> u.toReal.DynamicViscosity )
            Report.Fluid.realFluidDensity, ZamlS <| sprintf "%A kgm$^{-3}$" (o.Density |> u.toReal.Density ) ]
          Report.Fluid.lbmFluidProp, ZamlSeq [
            Report.Fluid.lbmYieldStress, ZamlS <| sprintf "%g LU" lbmYieldStress
            Report.Fluid.lbmViscosity, ZamlS <| sprintf "%g LU" lbmPlasticViscosity
            Report.Fluid.lbmFluidDensity, ZamlS <| sprintf "%g LU" o.Density ] ]
      | None -> 
        Report.Fluid.lbmFluidProp, ZamlSeq [
          Report.Fluid.lbmYieldStress, ZamlS <| sprintf "%g LU" lbmYieldStress
          Report.Fluid.lbmViscosity, ZamlS <| sprintf "%g LU" lbmPlasticViscosity
          Report.Fluid.lbmFluidDensity, ZamlS <| sprintf "%g LU" o.Density ]

  new (realYieldStress: float<Pa>, realPlasticViscosity: float<Pa s>, units: LBM.Units) =
    BinghamPlastic(realYieldStress |> units.toLbm.Stress, realPlasticViscosity |> units.toLbm.DynamicViscosity, units)

type NewtonianFluid (viscosity: float, ?units: LBM.Units) =
  inherit LbmFluid ()
  override o.getApparentViscosity _shearRate _shearStress = Some viscosity
   
  override o.getShearRateAndStressFromTau _tau = 0., 0.

  override o.ToZaml() =
    match units with
      | Some u ->
        Report.Fluid.fluidProperties, ZamlSeq [
          Report.Fluid.realFluidProp, ZamlSeq [
            Report.Fluid.realViscosity, ZamlS <| sprintf "%A Pa.s" (viscosity |> u.toReal.DynamicViscosity )
            Report.Fluid.realFluidDensity, ZamlS <| sprintf "%A kgm$^{-3}$" (o.Density |> u.toReal.Density ) ]
          Report.Fluid.lbmFluidProp, ZamlSeq [
            Report.Fluid.lbmViscosity, ZamlS <| sprintf "%g LU" viscosity
            Report.Fluid.lbmFluidDensity, ZamlS <| sprintf "%g LU" o.Density ] ]
      | None -> 
        Report.Fluid.lbmFluidProp, ZamlSeq [
          Report.Fluid.lbmViscosity, ZamlS <| sprintf "%g LU" viscosity
          Report.Fluid.lbmFluidDensity, ZamlS <| sprintf "%g LU" o.Density ]
  new (viscosity: float<Pa s>, units: LBM.Units) =
    NewtonianFluid( viscosity |> units.toLbm.DynamicViscosity, units)

type General( lbmShearRateStressA: (float * float) list, ?units: LBM.Units) =
  inherit LbmFluid()

  let lbmShearRateA, lbmShearStressA = lbmShearRateStressA |> List.sortBy fst |> List.unzip
  let viscosities = lbmShearRateA |> List.mapi (fun i shearRate ->
    if i = 0 then
      (lbmShearStressA.[i+1] - lbmShearStressA.[i]) / (lbmShearRateA.[i+1] - shearRate)
    elif i = lbmShearRateA.Length - 1 then
      (lbmShearStressA.[i] - lbmShearStressA.[i-1]) / (shearRate - lbmShearRateA.[i-1])
    else
      let visc1 = (lbmShearStressA.[i+1] - lbmShearStressA.[i]) / (lbmShearRateA.[i+1] - shearRate)
      let visc2 = (lbmShearStressA.[i] - lbmShearStressA.[i-1]) / (shearRate - lbmShearRateA.[i-1])
      0.5 * (visc1 + visc2) )
  let rateDiff = Array.init (lbmShearRateA.Length - 1) (fun i -> lbmShearRateA.[i+1] - lbmShearRateA.[i] )
  let rateFrac = rateDiff |> Array.mapi (fun i rateD -> lbmShearRateA.[i] / rateD)
  
  override o.getApparentViscosity shearRate _ =
    match lbmShearRateA |> List.tryFindIndex (fun s -> shearRate < s) with
      | Some i ->
        let frac = shearRate / rateDiff.[i-1] - rateFrac.[i-1]
        Some (if frac > 0.5 then viscosities.[i] else viscosities.[i-1])
      | None -> Some viscosities.[viscosities.Length - 1]
  
  override o.getShearRateAndStressFromTau tau =
    let nu = (tau - 0.5) / 3.
    match viscosities |> List.tryFindIndex (fun visc -> visc < nu) with
      | Some i ->
        let shearRate =
          if nu - viscosities.[i-1] = 0. then lbmShearRateA.[i-1]
          else (lbmShearStressA.[i-1] - viscosities.[i-1] * lbmShearRateA.[i-1]) / (nu - viscosities.[i-1])
        let shearStress = nu * shearRate
        shearRate, shearStress
      | None ->
        lbmShearRateA.[lbmShearRateA.Length - 1], lbmShearStressA.[lbmShearStressA.Length - 1]

  override o.ToZaml() =
    match units with
      | Some u ->
        Report.Fluid.fluidProperties, ZamlSeq [
          Report.Fluid.realFluidProp, ZamlSeq [
            Report.Fluid.realShearRateVsStressList, ZamlS <| sprintf "%A s$^{-1}$ vs. Pa" (List.init lbmShearRateStressA.Length (fun _ -> u.toReal.ShearRate, u.toReal.Stress ))
            Report.Fluid.realFluidDensity, ZamlS <| sprintf "%A kgm$^{-3}$" (o.Density |> u.toReal.Density ) ]
          Report.Fluid.lbmFluidProp, ZamlSeq [
            Report.Fluid.lbmShearRateVsStressList, ZamlS <| sprintf "%A LU" lbmShearRateStressA
            Report.Fluid.lbmFluidDensity, ZamlS <| sprintf "%g LU" o.Density ] ]
      | None -> 
        Report.Fluid.lbmFluidProp, ZamlSeq [
          Report.Fluid.lbmShearRateVsStressList, ZamlS <| sprintf "%A LU" lbmShearRateStressA
          Report.Fluid.lbmFluidDensity, ZamlS <| sprintf "%g LU" o.Density ]

  new (realShearRateStressA: (float<s^-1> * float<Pa>) list, units: LBM.Units) =
    General( (realShearRateStressA |> List.map(fun (rate, stress) -> units.toLbm.ShearRate rate, units.toLbm.Stress stress )), units)
 