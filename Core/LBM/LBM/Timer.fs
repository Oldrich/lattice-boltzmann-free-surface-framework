﻿namespace LBM

type Levels =
  | L0 of int
  | L1 of int
  | L2 of int
  | L3 of int
  | L4 of int
  | All of int
  static member getEvery (o: Levels) = match o with All t -> t | L0 t -> t | L1 t -> t | L2 t -> t | L3 t -> t | L4 t -> t
  static member getId (o: Levels) = match o with All _ -> -1 | L0 _ -> 0 | L1 _ -> 1 | L2 _ -> 2 | L3 _ -> 3 | L4 _ -> 4
  member o.Id = Levels.getId o
  member o.Every = Levels.getEvery o

type Timer (levels: Levels seq) =

  let n = 6

  let leftPad = List.init n (fun i -> i * 2)

  let all, everyA =
    if Seq.length levels > 0 then
      let everyA = Array.create n None
      let mutable all = None
      for level in levels do
        if level.Id = -1 then all <- Some level.Every
        else everyA.[level.Id] <- Some level.Every
      all, everyA
    else None, [||]

  let isActiveA = Array.zeroCreate everyA.Length
  let _setLbmTime lbmTime =
    everyA |> Array.iteri (fun i every ->
      isActiveA.[i] <-
        match all, every with
          | Some every0, Some everyX -> lbmTime % every0 = 0 || lbmTime % everyX = 0
          | None, Some everyX -> lbmTime % everyX = 0
          | Some every0, None -> lbmTime % every0 = 0
          | None, None -> false )
  do _setLbmTime -1

  let timerA = Array.init everyA.Length (fun _ -> System.Diagnostics.Stopwatch())

  let _print level (v: string) time =
    System.Console.WriteLine ("{0}{1} = {2} ms", "".PadRight leftPad.[level], v.PadRight (50 - leftPad.[level], '.'), time)

  member o.isActive level = level >= 0 && level < isActiveA.Length && isActiveA.[level]

  member internal o.tick level =
    if o.isActive level then
      timerA.[level].Reset()
      timerA.[level].Start()

  member private o.tock level =
    timerA.[level].Stop()
    let out = timerA.[level].ElapsedMilliseconds
    timerA.[level].Reset()
    out

  member internal o.doTimer level fn = if o.isActive level then fn timerA.[0]

  member internal o.setLbmTime lbmTime = _setLbmTime lbmTime

  member internal o.tickAll = for timer in timerA do timer.Reset (); timer.Start ()

  member internal o.printString level str =
    if o.isActive level then
      System.Console.WriteLine ("{0}{1}", "".PadRight leftPad.[level], str)
  
  member o.printAny level (str: string) v =
    if o.isActive level then
      System.Console.WriteLine ("{0}{1} = {2}", "".PadRight leftPad.[level], str.PadRight (50 - leftPad.[level], '.'), v)

  member internal o.printTime level v =
    if o.isActive level then
      let time = timerA.[level].ElapsedMilliseconds
      if time < 0L then _print level v time

  member internal o.tockPrint level v =
    if o.isActive level then
      let time = o.tock level
      if time > 0L then _print level v time

  member internal o.tockPrintTick level v =
    if o.isActive level then
      let time = o.tock level
      if time > 0L then _print level v time
      o.tick level

  member internal o.printNow level str =
    if o.isActive level then o.printAny level str (System.DateTime.Now.ToString "m:ss:fffffff")


