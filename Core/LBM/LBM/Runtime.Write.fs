﻿namespace LBM.Runtime

open System.Collections.Generic
open System.IO

open Utilities.Math
open Utilities.PostProcess.U3D.Types
open Utilities.IO

open LBM
open LBM.Naming.General
open LBM.Basic
open LBM.LO.Types
open LBM.Materials

type Write (problemDir, lt: Lattice, maxTau, lbmFluid, ?lPO: LagrangianProblem) =

  let mutable lbmTime, e = 0, Evaluate (lt, maxTau, lbmFluid, ?lPO = lPO)

  let vtk = VTK (problemDir, lt, ?lPO = lPO)

  let files =
    let fn str = lazy (
      let fs = new FileStream (str, FileMode.Append, FileAccess.Write, FileShare.Read);
      new StreamWriter (fs) )
    [ fn Files.minMaxMeanRhoDiff; fn Files.fluidMass; fn Files.fractionOfCellsAboveTau; fn Files.kineticEnergyOfLO
      fn Files.minMaxMeanShearRate; fn Files.meanTau; fn Files.minMaxMeanLbmSpeed; fn Files.numberOfCollisionsDissipatedEnergy
      fn Files.numberOfParticles; fn Files.particleVolumeFractionFiberSphereEllipsoid; fn Files.shape; fn Files.spread
      fn Files.velocityProfile ]

  member o.Evaluate = e
  member o.VTK = vtk

  member o._refresh time =
    e <- Evaluate (lt, maxTau, lbmFluid, ?lPO = lPO)
    lbmTime <- time
    for file in files do
      if file.IsValueCreated then file.Value.Flush()

  member o.binaryFluidShape =
    Directory.CreateDirectory "fluidShape" |> ignore
    use stream = File.OpenWrite (sprintf @"fluidShape\fluidShape_%d.zip" lbmTime)
    use zipStream = new Compression.GZipStream (stream, Compression.CompressionMode.Compress)
    use bw = new BinaryWriter (zipStream)
    let pA = lt.Par.PA
    for x in 0 .. pA.LengthX - 1 do
      for y in 0 .. pA.LengthY - 1 do
        let rec find z =
          if z > 0 then
            match pA.[x,y,z] with
              | Interface _ | Fluid -> bw.Write (uint16 z)
              | _ -> find (z - 1)
          else bw.Write 65535us
        find (pA.lengthZ x y - 1)
    bw.Close()

  member o.binaryLOA = lPO |> Option.iter (fun lp ->
    Directory.CreateDirectory "loA" |> ignore
    use stream = File.OpenWrite (sprintf @"loA\loA_%d.zip" lbmTime)
    use zipStream = new Compression.GZipStream (stream, Compression.CompressionMode.Compress)
    use bw = new BinaryWriter (zipStream)
    LO.HelpingFunctions.Binary.toBinary (lt.OriginsA.[1,1,1], lp.Particles.Values, bw)
    bw.Close () )

  member o.minMaxMeanRhoDiff =
    let minR, maxR, meanR = e.MinMaxMeanRhoDiff
    files.[0].Value.WriteLine ("{0}\t{1:e}\t{2:e}\t{3:e}", lbmTime, minR, maxR, meanR)

  member o.fluidMass = files.[1].Value.WriteLine("{0}\t{1:e}", lbmTime, e.FluidMass)

  member o.fractionOfCellsAboveTauA =
    let str = e.FractionOfCellsAboveTauA |> Array.map (fun f -> sprintf "%e" f) |> String.concat "\t"
    files.[2].Value.WriteLine ("{0}\t{1}", lbmTime, str)

  member o.kineticEnergyLO (?ek) = lPO |> Option.iter (fun _ ->
    let ek = defaultArg ek e.KineticEnergyLO
    files.[3].Value.WriteLine("{0}\t{1:e}", lbmTime, ek) )

  member o.minMaxMeanShearRate =
    let minS, maxS, meanS = e.MinMaxMeanShearRate
    files.[4].Value.WriteLine ("{0}\t{1:e}\t{2:e}\t{3:e}", lbmTime, minS, maxS, meanS)

  member o.meanTau = files.[5].Value.WriteLine ("{0}\t{1:e}", lbmTime, e.MeanTau)

  member o.minMaxMeanLbmSpeed =
    let minU, maxU, meanU = e.MinMaxMeanLbmSpeed
    files.[6].Value.WriteLine ("{0}\t{1:e}\t{2:e}\t{3:e}", lbmTime, minU, maxU, meanU)

  member o.numberOfCollisionsDissipatedEnergy (nCol: float, wCol: float) =
    files.[7].Value.WriteLine ("{0}\t{1:g}\t{2:e}", lbmTime, nCol, wCol)

  member o.numberOfParticles =
    let fiberN, sphereN, ellN = e.NumberOfParticles
    files.[8].Value.WriteLine ("{0}\t{1}\t{2}\t{3}", lbmTime, fiberN, sphereN, ellN)

  member o.particleVolumeFraction =
    let fiber, sphere, ellipsoid = e.ParticleVolumeFraction
    files.[9].Value.WriteLine ("{0}\t{1:e}\t{2:e}\t{3:e}", lbmTime, fiber, sphere, ellipsoid)

  member o.fluidShapeYZ x =
    let shape = e.FluidShapeYZ x
    let shapeS = shape |> Array.map string |> String.concat "\t"
    files.[10].Value.WriteLine ("{0}\t{1}", lbmTime, shapeS)

  member o.fluidSpread sFn  =
    let s = e.FluidSpread sFn
    files.[11].Value.WriteLine ("{0}\t{1:e}", lbmTime, s)
  
  member o.fluidVelocityProfile (p1, p2, uDirection) =
    let profileS = e.FluidVelocityProfile1D (p1, p2, uDirection) |> Array.map (fun u -> System.String.Format ("{0:e}", u)) |> String.concat "\t"
    files.[12].Value.WriteLine ("{0}\t{1}", lbmTime, profileS)

  interface System.IDisposable with
    member o.Dispose () =
      for file in files do if file.IsValueCreated then file.Value.Dispose()