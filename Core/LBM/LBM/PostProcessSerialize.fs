﻿namespace LBM.PostProcess

open LBM.Basic
open LBM.LO.Types
open LBM.Units

open System.IO
open System.IO.Compression
open System.Runtime.Serialization.Formatters.Binary

type Serialize =

  static member binaryDeSerialize path =
    use fs = new FileStream(path, FileMode.Open) 
    use decompressor = new GZipStream(fs, CompressionMode.Decompress) 
    let bf = new BinaryFormatter() 
    bf.Deserialize decompressor :?> ( (MacroResult option) * (PhaseArray option) * (LagrangianProblem option) * (Conversion option) )
  
  static member binarySerialize (folderPath, time, mRO: MacroResult option, pAO: PhaseArray option, lPO: LagrangianProblem option, units: Conversion option) =
    Directory.CreateDirectory folderPath |> ignore
    let lbm = mRO |> Option.map (fun mr -> mr.RhoA.LengthA, mr.RhoA.Values, mr.TauA.Values, mr.UxA.Values, mr.UyA.Values, mr.UzA.Values )
    let data = mRO
    let path = sprintf @"%s\%d" folderPath time
    use fs = new FileStream(path, FileMode.Create) 
    use compressor = new GZipStream(fs, CompressionMode.Compress) 
    let bf = new BinaryFormatter() 
    bf.Serialize(compressor, box data) 

