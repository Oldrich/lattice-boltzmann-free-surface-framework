﻿module LO.HelpingFunctions.Ellipsoid

open System.Collections.Generic

open LBM.LO.Types

open Kit
open Kit.Math

let private sumSqr a = a |> Array.sumBy (fun a -> a * a)

let private bisector (e: float []) y =
  let n = e.Length - 1
  let esqr = e |> Array.map (fun e -> e * e)
  let ey = (e, y) ||> Array.map2 (fun e y -> e * y)
  let t0 = ref <| -esqr.[n] + ey.[n]
  let t1 = ref <| -esqr.[n] + sqrt (sumSqr ey)
  let t = ref !t1
  let i = ref 0
  let limit = 256
  while !i < limit do
    incr i
    t := 0.5 * (!t0 + !t1)
    if !t = !t0 || !t = !t1 then i := limit
    else
      let f = 
        let r = (ey, esqr) ||> Array.map2 (fun ey es -> ey / (!t + es)) |> sumSqr
        r - 1.
      if f > 0. then t0 := !t
      elif f < 0. then t1 := !t
      else i := limit

  let p = (esqr, y) ||> Array.map2 (fun es y -> es * y / (!t + es))
  p

let private distancePointEllipsoidSpecial (e: float []) (y: float []) =
  let ePos, yPos = 
    let ePos, yPos = List (), List ()
    (e, y) ||> Array.iter2 (fun e y ->
      if y > 0. then
        ePos.Add e
        yPos.Add y )
    ePos.ToArray(), yPos.ToArray()
  
  let xPos =  
    if y.[2] > 0. then bisector ePos yPos
    else
      let e2Sqr = e.[2] * e.[2]
      let denom = Array.init ePos.Length (fun i -> ePos.[i] * ePos.[i] - e2Sqr)
      let ey = Array.init ePos.Length (fun i -> ePos.[i] * yPos.[i])
      let inAABBSubEllipse = 
        if (ey, denom) ||> Array.exists2 (fun ey denom -> ey  >= denom) then false else true

      if inAABBSubEllipse then
        // yPos[] is inside the axis-aligned bounding box of the
        // sub ellipse.  This intermediate test is designed to guard
        // against the division by zero when ePos[i] == e[N-1] for some i.
        let xde = (ey, denom) ||> Array.map2 (fun ey denom -> ey / denom)
        let discr = 1. - sumSqr xde
        if discr > 0. then
          // yPos[] is inside the sub ellipse.  The closest ellipsoid
          // point has x[2] > 0.
          let xPos = (ePos, xde) ||> Array.map2 (fun e x -> e * x)
          let xPos2 = e.[2] * sqrt discr
          Array.append xPos [| xPos2 |]
        else
          let xLoc = bisector ePos yPos
          Array.append xLoc [|0.|]
      else
        let pLoc = bisector ePos yPos
        Array.append pLoc [|0.|]

  // Fill in those x[] values that were not zeroed out initially.
  let n = ref 0
  let p = Array.create 3 0.
  for i = 0 to 2 do
    if y.[i] > 0. then
      p.[i] <- xPos.[!n]
      incr n

  p
        
//----------------------------------------------------------------------------
// The ellipsoid is (x0/e0)^2 + (x1/e1)^2 + (x2/e2)^2 = 1.  The query point is
// (y0,y1,y2).  The function returns the distance from the query point to the
// ellipsoid.   It also computes the ellipsoid point (x0,x1,x2) that is
// closest to (y0,y1,y2).
//----------------------------------------------------------------------------
let internal _distancePointEllipsoid (e: float []) (y: float [] ) =
  
  // Determine reflections for y to the first octant.
  let reflect = y |> Array.map (fun y -> y < 0.)

  // Determine the axis order for decreasing extents.
  let permute =
    if e.[0] < e.[1] then
      if e.[2] < e.[0] then [| 1; 0; 2 |]
      elif e.[2] < e.[1] then [| 1; 2; 0 |]
      else [| 2; 1; 0 |]     
    else
      if e.[2] < e.[1] then [| 0; 1; 2 |]
      elif e.[2] < e.[0] then [| 0; 2; 1 |]
      else [| 2; 0; 1 |] 
    
  let invPermute = 
    let a = Array.zeroCreate 3
    for i = 0 to 2 do a.[permute.[i]] <- i
    a
    
  let locE = permute |> Array.map (fun j -> e.[j])
  let locY = permute |> Array.map (fun j ->
    let locY = y.[j]
    if reflect.[j] then -locY else locY)

  let locX = distancePointEllipsoidSpecial locE locY

  // Restore the axis order and reflections.
  let globX = invPermute |> Array.map (fun j -> locX.[j]) |> Array.mapi (fun i x -> if reflect.[i] then -x else x)
  globX |> Triple.ofArray

let distanceNormalPoint_PointEllipsoid (lo: LO) (y: float triple) =
  match lo.Shape with
    | SymmetricEllipsoid el ->
      let e = [| el.MainHalfAxis; el.Radius; el.Radius |]
      let yL = (y - lo.CoG) |> Versor.rotateG2L lo.Rot
      let pL = _distancePointEllipsoid e (Triple.toArray yL)
      let p = (pL |> Versor.rotateL2G lo.Rot) + lo.CoG
      let n = y - p
      let h = Triple.norm n
      h, n / h, p, pL
    | _ -> failwith "unsupported shape"