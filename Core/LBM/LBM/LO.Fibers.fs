﻿namespace LBM.LO

open LBM.Basic
open LBM.LO.Types

open Kit
open Kit.Collections
open Kit.Math

type Fibers =

  static member fiberToLO (move, rotate, interaction) ((cog, angle: float triple), (u, omegaYPR)) density (gravityAcceleration: float triple) (f: FiberType) marker = 
    let rot = Versor.ofYawPitchRoll(angle.x, angle.y, angle.y)
    let r = f.Length / f.AspectRatio / 2.
    let volume = pi * r * r * f.Length
    let mass = density * volume
    let DX, V =
      let dl = f.Length / ceil (f.Length / f.DLength)
      let DX = [| dl / 2. .. dl .. f.Length + 0.001 * dl |] |> Array.map (fun lx -> triple (lx - f.Length / 2., 0., 0.))
      let V = Array.create DX.Length (pi * r * r * dl)
      DX, V
    let I =
      let I = mass * (3. * r * r + f.Length * f.Length) / 12.
      let Ismall = 0.5 * mass * r * r
      triple(Ismall, I, I)
    let nodes = { LocalDX = DX; V = V}
    let omegaGlob =
      let a = Versor.ofYawPitchRoll omegaYPR
      let b = Versor.toAxisAngle a
      let c = Versor.angularVelocityVectorFromAxisAngle b
      c
    LO (Shape = Fiber f, CoG = cog, Mass = mass, Density = density,
                      U = u, OmegaGlob = omegaGlob, I = I, Rot = rot,
                      GAcc = gravityAcceleration, Nodes = nodes, Favg = Triple.zerof, Tavg = Triple.zerof, _Move = move,
                      _Rotate = rotate, _Interaction = interaction, Marker = marker, Guid = System.Guid.NewGuid() )

  static member cylinderToLO (move, rotate, interaction) ((cog, angle: float triple), (u, omegaYPR))
                              density (gravityAcceleration: float triple) (f: FiberType) marker = 
    let rot = Versor.ofYawPitchRoll(angle.x, angle.y, angle.y)
    let r = f.Length / f.AspectRatio / 2.
    let volume = pi * r * r * f.Length
    let mass = density * volume

    let dlL = f.Length / ceil (f.Length / f.DLength)
  
    let circleDXV = 
      let dlR = r / ceil (r / f.DLength)
      [| r .. - dlR .. 0. |] |> Array.collect (fun ri ->
        let perimeter = 2. * pi * ri
        let pointsC = (perimeter / f.DLength + 1e-10) |> ceil
        let dV =
          if ri = r then pi * (ri**2. - (ri - 0.5 * dlR)**2.) * dlL / pointsC
          elif ri = 0.0 then pi * (0.5 * dlR)**2. * dlL
          else perimeter * dlR * dlL / pointsC
        let angleStep = 2. * pi / pointsC
        [| angleStep .. angleStep .. 2. * pi + 0.000001 |] |> Array.map (fun alfa ->
          let DX = triple (0., ri * cos alfa, ri * sin alfa)
          DX, dV ) )

    let DX, V =
      [| dlL / 2. .. dlL .. f.Length + 0.001 * dlL|] |> Array.collect (fun lx ->
        circleDXV |> Array.map (fun (dx, dV) ->
          dx + triple (lx - f.Length/2., 0., 0.), dV
        ) ) |> Array.unzip

    let error = abs (volume - Array.sum V) / volume
    if error > 1e-10 then failwith "cylinderToLO: The volume does not match"

//    let V =
//      let error = (volume - Array.sum V) / float DX.Length
//      V |> Array.map (fun Vi -> Vi + error)

    let I =
      let I = mass * (3. * r * r + f.Length * f.Length) / 12.
      let Ismall = 0.5 * mass * r * r
      triple(Ismall, I, I)
    let nodes = { LocalDX = DX; V = V}
    let omegaGlob = omegaYPR |> Versor.ofYawPitchRoll |> Versor.toAxisAngle |> Versor.angularVelocityVectorFromAxisAngle
    LO (Shape = Fiber f, CoG = cog, Mass = mass, Density = density,
                      U = u, OmegaGlob = omegaGlob, I = I,Rot = rot,
                      GAcc = gravityAcceleration, Nodes = nodes, Favg = Triple.zerof, Tavg = Triple.zerof, _Move = move,
                      _Rotate = rotate, _Interaction = interaction, Marker = marker, Guid = System.Guid.NewGuid())
