﻿namespace LBM.PostProcess.Latex

open System.IO
open System.Collections.Generic
open System.Text.RegularExpressions

open LBM.Naming
open LBM.PostProcess
open Utilities.IO
open System.Collections.Generic

type Report (problemDirPath, reportPath) =

  do if Directory.Exists reportPath = false then Directory.CreateDirectory reportPath |> ignore

  let preamble = Utilities.IO.File.readResource "Preamble.tex"
  let sections = List()

  let titlePageFileName = "titlePage.tex"

  member o.addSection (text: string) = sections.Add text

  member o.addGlobReport () =
    let path = (Path.Combine(problemDirPath, General.Files.report)).Replace(@"\", "/")
    o.addSection (sprintf @"\embedfile[filespec=Report.rtf]{%s}" path)

  member o.addTitlePage (title, client, ?author) =
    let titlePageStr =
      let s = Utilities.IO.File.readResource "TitlePage.tex"
      let author = defaultArg author "Oldrich Svec"
      s.Replace("{title}", title).Replace("{author}", author).Replace("{client}", client)
    File.WriteAllText (Path.Combine(reportPath, titlePageFileName), titlePageStr)
    o.addSection (sprintf @"\input{%s}" titlePageFileName)

  member o.addU3D (u3d: LBM.PostProcess.U3D) =
    let u3dPath = Path.Combine (reportPath, "U3D_Model")
    u3d.write (problemDirPath, u3dPath)
    //o.addSection @"\input{U3D_Model/u3d.tex}"

  member o.addGraphs () =
    let dirName = "AsyGraphs"
    let graphs =
      let asyPath = Path.Combine (reportPath, dirName)
      LBM.PostProcess.Asymptote.Graphs (problemDirPath, asyPath)
    graphs.addAllGraphs
    graphs.save ()
    let str = LBM.PostProcess.Helping.find ("Header", "Graphs.tex")
    o.addSection (str.Replace("{0}", dirName))

  member o.addAssignment () =
    let files = ["Task.pdf"; "task.pdf"; "Assignment.pdf"; "assignment.pdf"]
    let pdfFile =  files |> List.tryPick (fun file ->
      let path = Path.Combine(reportPath, @"..\..\..\Contract", file)
      if File.Exists path then Some (path.Replace(@"\","/")) else None )
    match pdfFile with
      | Some path ->
        let str = LBM.PostProcess.Helping.find ("Task", "Report.tex")
        o.addSection (str.Replace("{pdfFile}", path))
      | None -> System.Console.WriteLine "!! Assignment file not found !!"

  member o.addInput () =
    let inputFileName = "Input.txt"
    let inputPageStr =
      let str = LBM.PostProcess.Helping.find ("Input", "Report.tex")
      let text =
        let textA = File.ReadAllLines(Path.Combine(problemDirPath, General.Files.input))
        textA |> Array.map (fun t -> t.Replace(" ", @"\ ") + @"\\") |> String.concat "\n"
      str.Replace("{input.txt}", text)
    File.WriteAllText (Path.Combine(reportPath, inputFileName), inputPageStr)
    o.addSection (sprintf @"\input{%s}" inputFileName)

  member o.addAll (title, client, ?author, ?u3d: LBM.PostProcess.U3D) =
    o.addTitlePage (title, client, ?author = author)
    o.addGlobReport ()
    o.addAssignment ()
    o.addInput ()
    u3d |> Option.iter o.addU3D
    o.addGraphs ()

  member o.save (?fileName, ?dontExec) =
    let fileName = defaultArg fileName "report.tex"
    System.Console.WriteLine "Creating final PDF"
    let text = sections |> String.concat "\n%\n"
    File.WriteAllText (Path.Combine(reportPath, fileName), preamble + text + "\n\n\\end{document}")
    match dontExec with
      | Some true -> ()
      | _ ->
        let pars = sprintf "-interaction=batchmode -halt-on-error -file-line-error %s" fileName
        let p1, outStr1 = Utilities.Shell.shellExecute ("pdflatex", pars, reportPath)
        if p1.ExitCode <> 0 then printfn "%s" outStr1
        else
          let p2, outStr2 = Utilities.Shell.shellExecute ("pdflatex", pars, reportPath)
          if p2.ExitCode <> 0 then printfn "%s" outStr2
    