﻿namespace LBM.PreProcess.Latex

open System.IO
open Utilities.IO
open LBM.Naming

type Report =
  static member save path =
    let reportPath =    
      let dir0, dir1 = 
        let dirI = DirectoryInfo path
        dirI.Parent.Name, dirI.Name
      let reportDir = path + sprintf @"\..\..\..\Report\%s\%s" dir0 dir1
      if Directory.Exists reportDir then try Directory.Delete (reportDir, true) with _ -> ()
      Utilities.IO.Dir.createDir reportDir
    ()
    //File.WriteAllText(reportPath + "\\bc.tex", BC.makeTable path)