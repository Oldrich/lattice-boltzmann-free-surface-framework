﻿namespace LBM.Naming.Input

open System.Text.RegularExpressions
open System.Collections.Generic

open Kit
open Kit.Math

module General =
  let taskName = "General | Task name [[-]]"
  let mainId = "General | Domain main ID [[-]]"
  let nDomains = "General | Number of domains [[-]]" 
  let lbm1cm = "General | Length conversion - 1 [cm] [[lbm]]"
  let real0_1Visc = "General | Viscosity conversion - 0.1 [lbm] [[Pa s]]"

  let lbmDomLx = "General | Domain length X [[lbm]]"
  let lbmDomLy = "General | Domain length Y [[lbm]]"
  let lbmDomLz = "General | Domain length Z [[lbm]]"

  let realDomLx = "General | Domain length X [[m]]"
  let realDomLy = "General | Domain length Y [[m]]"
  let realDomLz = "General | Domain length Z [[m]]"

  //Controls
  let seed = "General | Seed [[-]]"

  let saveVtkLBMEvery = "General | Save VTK LBM every [[lbm]]"
  let saveVtkLOAEvery = "General | Save VTK Lagrangian objects every [[lbm]]"
  let saveLOAEvery = "General | Save Lagrangian objects every [[lbm]]"
  let saveFluidShapeEvery = "General | Save fluid shape every [[lbm]]"
  let saveVtkLBMEveryReal = "General | Save VTK LBM every [[s]]"
  let saveVtkLOAEveryReal = "General | Save VTK Lagrangian objects every [[s]]"
  let saveLOAEveryReal = "General | Save Lagrangian objects every [[s]]"
  let saveFluidShapeEveryReal = "General | Save fluid shape every [[s]]"

  let analyzeEvery = "General | Analyze Every [[lbm]]"
  let analyzeEveryReal = "General | Analyze Every [[s]]"
  let optimizeEvery = "General | Optimize the domain every [[lbm]]"
  let tolerance = "General | Tolerance [[-]]"

module Fluid =
  let lbmDensity = "Fluid | Density [[lbm]]"
  let realDensity = "Fluid | Density [[kg/m3]]"
  let lbmViscosity = "Fluid | Viscosity [[lbm]]"
  let realViscosity = "Fluid | Viscosity [[Pa s]]"
  let lbmYieldStress = "Fluid | Yield stress [[lbm]]"
  let realYieldStress = "Fluid | Yield stress [[Pa]]"
  let maxTau = "Fluid | Maximum tau [[lbm]]"
  let lbmMaxPotentialSpeed = "Fluid | Maximum potential speed [[lbm]]"
  let isMRT = "Fluid | Is multi-relaxation-time collision operator [[-]]"
  let isIncompressible = "Fluid | Is incompressible [[-]]"
  let slipCoeff = "Fluid | BC - Slip coefficient [[-]]"
  let lbmSlipLength = "Fluid | BC - LBM slip length [[-]]"
  let realShearRateVsStressList = "Fluid | Shear rate versus shear stress list [1/s, Pa]]"

/// Names used by PreProcess | Solids - do not add, do not change
module Solids =

  module Other =

    let lbmCutOffDistance = "Solids | Other | Cut-off distance [[lbm]]"
    let coeffOfRestitution = "Solids | Other | Coefficient of restitution [[-]]"
    let lbmCollisionDistance = "Solids | Other | Collision distance [[lbm]]"

    let dynamicFrictionCoef = "Solids | Other | Dynamic friction coefficient [[-]]"
    let staticFrictionCoef = "Solids | Other | Static friction coefficient [[-]]"

    let lbmDLength = "Solids | Other | Spacing of Lagrangian nodes [[lbm]]"
    let maxRKstep = "Solids | Other | Max length of step in RK integration [[lbm]]"
    let checkIsInFluid = "Solids | Other | Check is in fluid [[-]]"
    let checkOverlapsPP = "Solids | Other | Check overlaps of Lagrangian objects [[-]]"

    let realRebarDiameter = "Solids | Other | Rebar diameter [[m]]"
    let lbmRebarDiameter = "Solids | Other | Rebar diameter [[lbm]]"

  module Spheres =

    let lbmSphereDensity = "Solids | Spheres | Density [[lbm]]"
    let realSphereDensity = "Solids | Spheres | Density [[kg/m3]]"

    let lbmMinSphereD = "Solids | Spheres | Minimum diameter [[lbm]]"
    let realMinSphereD = "Solids | Spheres | Minimum diameter [[m]]"
    
    let lbmMaxSphereD = "Solids | Spheres | Diameter [[lbm]]"
    let realMaxSphereD = "Solids | Spheres | Diameter [[m]]"

    let lbmSphereSizeDistribution = "Solids | Spheres | Size distribution [[-, -]]"
    let realSphereSizeDistribution = "Solids | Spheres | Size distribution [[mm, %]]"

    let relativeSphereGap = "Solids | Spheres | Relative gap [[-]]"

    let sphereVolumeFraction = "Solids | Spheres | Volume fraction [[-]]"
    let maxGeneratedSphereVolumeFraction = "Solids | Spheres | Maximal generated volume fraction [[-]]"
    
    let fictiousAggregates = "Solids | Spheres | Fictitious aggregates [[-]]"

  module Fibers =

    let lbmFiberLength = "Solids | Fibers | Length [[lbm]]"
    let realFiberLength = "Solids | Fibers | Length [[m]]"

    let lbmFiberDensity = "Solids | Fibers | Density [[lbm]]"
    let realFiberDensity = "Solids | Fibers | Density [[kg/m3]]"

    let lbmFiberDiameter = "Solids | Fibers | Diameter [[lbm]]"
    let realFiberDiameter = "Solids | Fibers | Diameter [[m]]"

    let fiberVolumeFraction = "Solids | Fibers | Volume fraction [[-]]"
    let maxGeneratedFiberVolumeFraction = "Solids | Fibers | Maximal generated volume fraction [[-]]"

    let fiberAspectRatio = "Solids | Fibers | Aspect ratio [[-]]"
    let relativeFiberGap = "Solids | Fibers | Relative gap [[-]]"

  module Ellipsoids =

    let lbmEllipsoidDensity = "Solids | Ellipsoids | Density [[lbm]]"
    let realEllipsoidDensity = "Solids | Ellipsoids | Density [[kg/m3]]"

    let lbmMinEllipsoidD = "Solids | Ellipsoids | Minimum diameter [[lbm]]"
    let realMinEllipsoidD = "Solids | Ellipsoids | Minimum diameter [[m]]"
    
    let lbmMaxEllipsoidD = "Solids | Ellipsoids | Diameter [[lbm]]"
    let realMaxEllipsoidD = "Solids | Ellipsoids | Diameter [[m]]"

    let ellipsoidAspectRatio = "Solids | Ellipsoids | Aspect ratio [[-]]"

    let lbmEllipsoidSizeDistribution = "Solids | Ellipsoids | Size distribution [[-, -]]"
    let realEllipsoidSizeDistribution = "Solids | Ellipsoids | Size distribution [[mm, %]]"

    let relativeEllipsoidGap = "Solids | Ellipsoids | Relative gap [[-]]"

    let ellipsoidVolumeFraction = "Solids | Ellipsoids | Volume fraction [[-]]"
    let maxGeneratedEllipsoidVolumeFraction = "Solids | Ellipsoids | Maximal generated volume fraction [[-]]"

module UserDefined =
  let scale = "UserDefined | Scale [[-]]"

  let isTrue = "UserDefined | Is True [[-]]"

  let other1 = "UserDefined | Other 1 [[-]]"
  let other2 = "UserDefined | Other 2 [[-]]"
  let other3 = "UserDefined | Other 3 [[-]]"

  let lbmL1 = "UserDefined | Length 1 [[lbm]]"
  let lbmL2 = "UserDefined | Length 2 [[lbm]]"
  let lbmL3 = "UserDefined | Length 3 [[lbm]]"
  let lbmLx1 = "UserDefined | Length X1 [[lbm]]"
  let lbmLx2 = "UserDefined | Length X2 [[lbm]]"
  let lbmLx3 = "UserDefined | Length X3 [[lbm]]"
  let lbmLy1 = "UserDefined | Length Y1 [[lbm]]"
  let lbmLy2 = "UserDefined | Length Y2 [[lbm]]"
  let lbmLy3 = "UserDefined | Length Y3 [[lbm]]"
  let lbmLz1 = "UserDefined | Length Z1 [[lbm]]"
  let lbmLz2 = "UserDefined | Length Z2 [[lbm]]"
  let lbmLz3 = "UserDefined | Length Z3 [[lbm]]"
  let lbmSpeed1 = "UserDefined | Speed 1 [[lbm]]"
  let lbmSpeed2 = "UserDefined | Speed 2 [[lbm]]"
  let lbmSpeed3 = "UserDefined | Speed 3 [[lbm]]"
  let lbmAcceleration1 = "UserDefined | Acceleration 1 [[lbm]]"
  let lbmAcceleration2 = "UserDefined | Acceleration 2 [[lbm]]"
  let lbmAcceleration3 = "UserDefined | Acceleration 3 [[lbm]]"
  let lbmTime1 = "UserDefined | Time 1 [[lbm]]"
  let lbmTime2 = "UserDefined | Time 2 [[lbm]]"
  let lbmTime3 = "UserDefined | Time 3 [[lbm]]"
  let lbmShearRate1 = "UserDefined | ShearRate 1 [[lbm]]"
  let lbmDiameter1 = "UserDefined | Diameter 1 [[lbm]]"

  let realL1 = "UserDefined | Length 1 [[m]]"
  let realL2 = "UserDefined | Length 2 [[m]]"
  let realL3 = "UserDefined | Length 3 [[m]]"
  let realLx1 = "UserDefined | Length X1 [[m]]"
  let realLx2 = "UserDefined | Length X2 [[m]]"
  let realLx3 = "UserDefined | Length X3 [[m]]"
  let realLy1 = "UserDefined | Length Y1 [[m]]"
  let realLy2 = "UserDefined | Length Y2 [[m]]"
  let realLy3 = "UserDefined | Length Y3 [[m]]"
  let realLz1 = "UserDefined | Length Z1 [[m]]"
  let realLz2 = "UserDefined | Length Z2 [[m]]"
  let realLz3 = "UserDefined | Length Z3 [[m]]"
  let realLz4 = "UserDefined | Length Z4 [[m]]"
  let realSpeed1 = "UserDefined | Speed 1 [[m/s]]"
  let realSpeed2 = "UserDefined | Speed 2 [[m/s]]"
  let realSpeed3 = "UserDefined | Speed 3 [[m/s]]"
  let realAcceleration1 = "UserDefined | Acceleration 1 [[m/s2]]"
  let realAcceleration2 = "UserDefined | Acceleration 2 [[m/s2]]"
  let realAcceleration3 = "UserDefined | Acceleration 3 [[m/s2]]"
  let realTime1 = "UserDefined | Time 1 [[s]]"
  let realTime2 = "UserDefined | Time 2 [[s]]"
  let realTime3 = "UserDefined | Time 3 [[s]]"
  let realShearRate1 = "UserDefined | ShearRate 1 [[1/s]]"
  let realDiameter1 = "UserDefined | Diameter 1 [[m]]"

  let any1 = "UserDefined | Any 1 [[-]]"
  let any2 = "UserDefined | Any 2 [[-]]"
  let any3 = "UserDefined | Any 3 [[-]]"
  
module UserDefinedI =
  open UserDefined

  let private fn str info = sprintf "%s --%s--" str info

  let scale = fn scale

  let isTrue = fn isTrue

  let any1 = fn any1
  let any2 = fn any2
  let any3 = fn any3

  let lbmL1 = fn lbmL1
  let lbmL2 = fn lbmL2
  let lbmL3 = fn lbmL3
  let lbmLx1 = fn lbmLx1
  let lbmLx2 = fn lbmLx2
  let lbmLx3 = fn lbmLx3
  let lbmLy1 = fn lbmLy1
  let lbmLy2 = fn lbmLy2
  let lbmLy3 = fn lbmLy3
  let lbmLz1 = fn lbmLz1
  let lbmLz2 = fn lbmLz2
  let lbmLz3 = fn lbmLz3
  let lbmSpeed1 = fn lbmSpeed1
  let lbmSpeed2 = fn lbmSpeed2
  let lbmSpeed3 = fn lbmSpeed3
  let lbmAcceleration1 = fn lbmAcceleration1
  let lbmAcceleration2 = fn lbmAcceleration2
  let lbmAcceleration3 = fn lbmAcceleration3
  let lbmTime1 = fn lbmTime1
  let lbmTime2 = fn lbmTime2
  let lbmTime3 = fn lbmTime3
  let lbmShearRate1 = fn lbmShearRate1
  let lbmDiameter1 = fn lbmDiameter1

  let other1 = fn other1
  let other2 = fn other2
  let other3 = fn other3

  let realL1 = fn realL1
  let realL2 = fn realL2
  let realL3 = fn realL3
  let realLx1 = fn realLx1
  let realLx2 = fn realLx2
  let realLx3 = fn realLx3
  let realLy1 = fn realLy1
  let realLy2 = fn realLy2
  let realLy3 = fn realLy3
  let realLz1 = fn realLz1
  let realLz2 = fn realLz2
  let realLz3 = fn realLz3
  let realSpeed1 = fn realSpeed1
  let realSpeed2 = fn realSpeed2
  let realSpeed3 = fn realSpeed3
  let realAcceleration1 = fn realAcceleration1
  let realAcceleration2 = fn realAcceleration2
  let realAcceleration3 = fn realAcceleration3
  let realTime1 = fn realTime1
  let realTime2 = fn realTime2
  let realTime3 = fn realTime3
  let realShearRate1 = fn realShearRate1
  let realDiameter1 = fn realDiameter1

type ML =
  
  static member stringify (v: (string * string) seq) =
    let names, values = Array.ofSeq v |> Array.unzip
    let names, units =
      names |> Array.map (fun name ->
        let units = ref ""
        let name = Regex.Replace(name, @" \[\[.+\]\]", fun (m: Match) -> units := m.Value.Replace("[[", "[").Replace("]]", "]"); "")
        name, !units ) |> Array.unzip
    let maxNames = names |> Array.maxBy String.length |> String.length
    let maxVals = values |> Array.maxBy String.length |> String.length
    Array.zip3 names values units
      |> Array.sortBy Tuple.fst
      |> Array.map (fun (name, value, units) ->
        (name + " ").PadRight(maxNames + 6, '.') + " " + value.PadRight(maxVals, ' ') + units )
      |> String.concat "\n"
    
  static member parse (s: string) =
    let sA = s.Split([|'\n'|], System.StringSplitOptions.RemoveEmptyEntries)
    sA |> Array.map (fun si ->
      let siA = Regex.Split(si, " \\.{5,} ")
      if siA.Length <> 2 then failwithf "ML: Error in parse: %s" si
      else
        let units = ref ""
        let s1 = Regex.Replace(siA.[1], @" \[.+\]$", fun (m: Match) -> units := m.Value.Replace("[", "[[").Replace("]", "]]"); "").Trim()
        let s0 = Regex.Replace (siA.[0], " --.+--", "") + !units
        s0, s1) |> dict

  static member tryGet fn (dic: IDictionary<string,string>) name =
    let isFound, v = dic.TryGetValue name
    if isFound then
      if v.ToLowerInvariant() <> "null" then Some(fn v)
      else None
    else None

  static member get fn (dic: IDictionary<string,string>) defaultValue name =
    let v = ML.tryGet fn dic name
    defaultArg v defaultValue

  static member failGet fn (dic: IDictionary<string,string>) name =
    match ML.tryGet fn dic name with Some v -> v | None -> failwithf "ML.failGet: '%s' does not exist" name

