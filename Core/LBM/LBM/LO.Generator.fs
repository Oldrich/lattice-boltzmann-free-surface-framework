﻿namespace LBM.LO

open LBM
open LBM.LO.Types
open LBM.BC
open LBM.BC.Types.Internal
open LBM.Basic

open Kit
open Kit.Collections
open Kit.Math

type Generator (lt: Lattice, bcP: BcProblem, pG: ParticleGenerator, oldLoA: LO seq, ?checkIsInFluid: bool, ?checkOverlapsPP: bool) =

  let nS, nF, nE = ref 0, ref 0, ref 0
  let vS, vF, vE = ref 0., ref 0., ref 0.
  let checkFluid = defaultArg checkIsInFluid true
  let checkOverlapPP = defaultArg checkOverlapsPP true

  member private o.isInBC (lo: LO) =
    bcP.Objects |> Array.fold (fun overlaps bc ->
      match bc.Purpose with
        | BcBounceBackI _ when bc.Invisible = false ->
          let dist = LO.HelpingFunctions.getDistance_BC lo bc
          dist < pG.gapFnBc lo.Shape bc.Shape || overlaps
        | _ -> overlaps) false

  member private o.isOverlapping (los: LO list, loi: LO, compacter) =
    if checkOverlapPP then
      let minDistance = if los.Length = 0 && Seq.length oldLoA = 0 then ref 0. else ref System.Double.MaxValue
      let isInLO (loA: LO seq) = loA |> Seq.exists (fun loj ->
        let gap = pG.gapFnP loi.Shape loj.Shape
        let dist, _ = HelpingFunctions.getPeriodicDistanceNormal lt loi loj
        if dist < !minDistance then minDistance := dist
        dist < gap )
      match compacter with 
        | None -> o.isInBC loi || isInLO los || isInLO oldLoA
        | Some c ->
          match c with 
            | MoveToPoint _ -> o.isInBC loi || isInLO los || isInLO oldLoA
            | LargestDistance d -> 
              o.isInBC loi || isInLO los || isInLO oldLoA || !minDistance > d
    else o.isInBC loi

  member private o.isNotInFluid (lo: LO) =
    lo.Nodes.LocalDX |> Array.exists (fun localDX ->
      let gx = lt.xyzLoc2Glob (lo.CoG + localDX |> Versor.rotateL2G lo.Rot)
      let gxi = gx |> Triple.map (round >> int) |> lt.checkGlobalPeriodic
      bcP.bytePhaseG.[gxi] = 0uy )

  member private o.moveObject (los, loIn: LO, moveData) =
    let (cog: float triple), itMax, (seed: int option), moveForRel = moveData
    let itMax = defaultArg itMax 10000
    let moveForRel = defaultArg moveForRel 0.1
    let seed = defaultArg seed 1
    let rnd = Random seed
    let distanceToCog (lo: LO) =
      let gap = pG.gapFnP loIn.Shape lo.Shape
      let dist =
        match lo.Shape with
          | Sphere s -> Triple.norm(lo.CoG - cog) - s.Radius
          | Fiber f ->
            let point = Geometry.Distance.segmentPoint(f.P1P2, cog) // Not periodic
            Triple.norm (point - lo.CoG) - f.R
          | SymmetricEllipsoid se ->
            let h, _, _, _ = LO.HelpingFunctions.Ellipsoid.distanceNormalPoint_PointEllipsoid lo cog
            h - se.Radius
      dist - gap
    let rec move (lo: LO) hMin it =
      if it < itMax then
        let dCog = 
          let n = rnd.nextTripleFloat() |> Triple.normalize
          let d =
            match lo.Shape with
              | Sphere s -> moveForRel * s.Radius * n
              | Fiber f -> moveForRel * f.Length * n
              | SymmetricEllipsoid se -> moveForRel * se.Radius * n
          if Triple.dot (cog - lo.CoG, d) > 0. then d else -d
        let nLo = lo.copy(coG = lo.CoG + dCog)
        let nH = distanceToCog nLo
        if nH < hMin then
          let isNotInFluid = o.isNotInFluid nLo
          if isNotInFluid || o.isOverlapping (los, nLo, None) then 
            move lo hMin (it + 1)
          else move nLo nH (it + 1)
        else move lo hMin (it + 1)
      else Some lo
    move loIn (distanceToCog loIn) 0

  member private o.newLO (los, generateLO, pVolFrac) =
    let mutable search, foundLO = true, None
    while search do
      match generateLO pVolFrac with
        | Some lo ->
          let isNotInFluid = if checkFluid then o.isNotInFluid lo else false
          let isOverlapping = o.isOverlapping (los, lo, pG.Compacter)
          if isNotInFluid = false && isOverlapping = false then
            search <- false
            match pG.Compacter with
              | None -> foundLO <- Some lo
              | Some c ->
                match c with 
                  | MoveToPoint data -> foundLO <- o.moveObject (los, lo, data)
                  | LargestDistance _-> foundLO <- Some lo
        | None -> search <- false
    foundLO |> Option.map (fun lo ->
      let loVolume = lo.Mass / lo.Density
      lo, loVolume )

  member private o.generate (los: LO list, fiberVolFrac, sphereVolFrac, ellipsoidVolFrac) =

    match pG.SphereData, pG.FiberData, pG.EllipsoidData with
      | Some (volumeFr, generateLO), _, _ when sphereVolFrac < volumeFr ->
        match o.newLO (los, generateLO, sphereVolFrac) with
          | Some (lo, loVolume) ->
            if !nS = 0 then 
              vS := volumeFr
              lt.Timer.printString 0 "Generating spheres: "
            if lt.Timer.isActive 0 then printf "%.3f %%; " (100. * sphereVolFrac / volumeFr)
            incr nS
            o.generate (lo :: los, fiberVolFrac, loVolume / pG.FluidVolume + sphereVolFrac, ellipsoidVolFrac)
          | None ->
            vS := sphereVolFrac
            o.generate (los, fiberVolFrac, 1e3, ellipsoidVolFrac)
      | _, Some (volumeFr, generateLO), _ when fiberVolFrac < volumeFr ->
        match o.newLO (los, generateLO, fiberVolFrac) with
          | Some (lo, loVolume) ->
            if !nF = 0 then 
              vF := volumeFr
              lt.Timer.printString 0 "Generating fibers: "
            if lt.Timer.isActive 0 then printf "%.3f %%; " (100. * fiberVolFrac / volumeFr)
            incr nF
            o.generate (lo :: los, loVolume / pG.FluidVolume + fiberVolFrac, sphereVolFrac, ellipsoidVolFrac)
          | None ->
            vF := fiberVolFrac
            o.generate (los, 1e3, sphereVolFrac, ellipsoidVolFrac)
      | _, _, Some (volumeFr, generateLO) when ellipsoidVolFrac < volumeFr ->
        match o.newLO (los, generateLO, ellipsoidVolFrac) with
          | Some (lo, loVolume) ->
            if !nE = 0 then 
              vE := volumeFr
              lt.Timer.printString 0 "Generating ellipsoids: "
            if lt.Timer.isActive 0 then printf "%.3f %%; " (100. * ellipsoidVolFrac / volumeFr)
            incr nE
            o.generate (lo :: los, fiberVolFrac, sphereVolFrac, loVolume / pG.FluidVolume + ellipsoidVolFrac)
          | None ->
            vE := ellipsoidVolFrac
            o.generate (los, fiberVolFrac, sphereVolFrac, 1e3)
      | _ -> 
        lt.Timer.printString 0 <| sprintf "\n%d spheres with volume fraction %g generated" !nS !vS
        lt.Timer.printString 0 <| sprintf "%d fibers with volume fraction %g generated" !nF !vF
        lt.Timer.printString 0 <| sprintf "%d ellipsoids with volume fraction %g generated" !nE !vE
        lt.Timer.tockPrint 0 "loA generated"
        los

  member o.generate () =
    lt.Timer.tick 0
    o.generate ([], 0., 0., 0.) |> Array.ofList
