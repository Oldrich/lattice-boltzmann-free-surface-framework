﻿module LBM.FreeSurface

open LBM.Basic
open Utilities.Math
open LBM.BC.Types

open System.Collections.Generic

let internal replaceStream (x, y, z) i (lt: Lattice) =
  let fx, ux, uy, uz = lt.Par.FA.[x, y, z].Value, lt.Par.UxA.[x, y, z].Value, lt.Par.UyA.[x, y, z].Value, lt.Par.UzA.[x, y, z].Value
  let u = triple(ux, uy, uz)
  let uu = u * u
  let fiEq = lt.Consts.getfEq (1.0, u, uu, i)
  let fi'Eq = lt.Consts.getfEq (1.0, u, uu, lt.Consts.Opposite.[i])
  let fi' = fx.[lt.Consts.Opposite.[i]]
  let newf = fiEq + fi'Eq - fi'
  newf

let private normalFromInterfaceToFluid (x, y, z) (lt: Lattice) =
  
  let rec getWaterRatio xdx =
    match lt.Par.getPhase xdx, lt.Par.getRho xdx with
      | Gas, None -> 0.
      | BoundaryCondition _, None -> 0.
      | Interface m, Some rho -> m / rho
      | Fluid, Some _  -> 1.
      | _ -> failwithf "error in normalFromInterfaceToFluid:\nphase = %A\nrhoM = %A" (lt.Par.getPhase xdx) (lt.Par.getRho xdx)
  let xdxW, x_dxW = (x + 1, y, z) |> getWaterRatio, (x - 1, y, z) |> getWaterRatio
  let ydyW, y_dyW = (x, y + 1, z) |> getWaterRatio, (x, y - 1, z) |> getWaterRatio
  let zdzW, z_dzW = (x, y, z + 1) |> getWaterRatio, (x, y, z - 1) |> getWaterRatio
  triple(0.5 * (xdxW - x_dxW), 0.5 * (ydyW - y_dyW), 0.5 * (zdzW - z_dzW))

let internal fFromAir xyz (lt: Lattice) c =
  let n = normalFromInterfaceToFluid xyz lt
  Triple.dot c n > 0.

/// MUTATES ALL LBM !!
let leftMass = ref 0.
let massExch = ref None
let rePhase time (lt: Lattice) =

  lt.Timer.tick 1
  lt.Timer.tick 2

  match !massExch with
    | None ->
      let o = lt.Par.WCF.Exchange1D ("____massFS____", fun (data: (float*int) list) ->
        let mTot, nTot = data |> List.fold (fun (mTot, nTot) (leftMass, n) -> mTot + leftMass, nTot + n) (0., 0)
        mTot / float nTot )
      massExch := Some o
    | Some _ -> ()

  let correctGases xyz = lt.Consts.ADimA |> Array.iter (fun i ->
    let xdx = lt.xdx (xyz, i)
    if lt.Par.PA.exists xdx && lt.Par.PA.[xdx] = Gas then lt.Par.setLBM7 (xdx, (Interface 0., lt.Par.FA.[xyz], lt.Par.RhoA.[xyz], lt.Par.UxA.[xyz], lt.Par.UyA.[xyz], lt.Par.UzA.[xyz], lt.Par.TauA.[xyz])))

  let correctFluids xyz = lt.Consts.ADimA |> Array.iter (fun i ->
    let xdx = lt.xdx (xyz, i)
    if lt.Par.PA.exists xdx && lt.Par.PA.[xdx] = Fluid then lt.Par.PA.[xdx] <- Interface lt.Par.RhoA.[xdx].Value )

  let xyzM = 
    lt.MassExchange |> Seq.toArray |> Array.choose (fun (KeyValue(xyz, dMass)) ->
      if lt.Par.PA.exists xyz then
        let mass = match lt.Par.PA.[xyz] with Interface m -> m + dMass | _ as p -> failwithf "phase %A in massExchange" p
        let noFluid = lt.Consts.ADimA |> Array.forall (fun i -> 
          let xdx = lt.xdx (xyz, i)
          match lt.Par.tryGetPhase xdx with Some Fluid -> lt.Par.TauA.[xyz] <- lt.Par.getTau xdx; false | _ -> true )
        if noFluid then
          leftMass := !leftMass + mass;
          lt.Par.setLBM7 (xyz, (Gas, None, None, None, None, None, None))
          None
        else Some (xyz, mass)
      else None)

  lt.Timer.tockPrintTick 2 "Artifacts"

  let mAvg = massExch.Value.Value.sendToMain (time, (!leftMass, xyzM.Length))

  lt.Timer.tockPrintTick 2 "sendAndReceive_FSmass"

  let change = Array3D.init 3 3 3 (fun _ _ _ -> List ())
  let set (x,y,z) toFluid =
    let isNotThere = Array3D.create 3 3 3 true
    isNotThere |> Array3D.iteri (fun i j k _ ->
    let (ni, nj, nk) as nijk, xyz = 
      let fn x xG nx = if x < 0 then 0, System.Int32.MaxValue elif x > nx - 1 then 2, System.Int32.MinValue else 1, xG
      let (ni, x), (nj, y), (nk, z) = fn (x + i - 1) x lt.NodesX_Loc, fn (y + j - 1) y lt.NodesY_Loc, fn (z + k - 1) z lt.NodesZ_Loc
      (ni, nj, nk), (x, y, z)
    if nijk <> (1,1,1) && isNotThere.[ni, nj, nk] then
      isNotThere.[ni, nj, nk] <- false
      change.[ni,nj,nk].Add (xyz, toFluid) )

  leftMass := 
    xyzM |> Array.sumBy (fun (xyz, mass) ->
      let rho = lt.Par.RhoA.[xyz].Value
      let newMass = mass + mAvg
      if newMass < -0.01 then
        lt.Par.setLBM7 (xyz, (Gas, None, None, None, None, None, None))
        set xyz false; correctFluids xyz; newMass
      elif newMass > rho + 0.01 then
        lt.Par.PA.[xyz] <- Fluid
        set xyz true; correctGases xyz; newMass - rho
      else lt.Par.PA.[xyz] <- Interface newMass; 0. )

  lt.Timer.tockPrintTick 2 "to Gas or Fluid "

  let incom = lt.Par.WCF.sendAndReceive_Exchange3D ("-FreeSurface-", change |> Array3D.map (fun v -> v.ToArray()))
  //sendAndReceive_ChangeFS3D (time, change)

  lt.Timer.tockPrintTick 2 "sendAndReceive_Change"

  incom |> Array3D.iter (fun v ->
    v |> Option.iter (fun data ->
      data |> Array.iter (fun ((x,y,z), toFluid) ->
        let xyz =
          let fn x nx = if x = System.Int32.MaxValue then nx elif x = System.Int32.MinValue then -1 else x
          fn x lt.NodesX_Loc, fn y lt.NodesY_Loc, fn z lt.NodesZ_Loc
        if toFluid then
          lt.Consts.ADimA |> Array.iter (fun i ->
            let xdx = lt.xdx (xyz, i)
            if lt.Par.PA.exists xdx && lt.Par.PA.[xdx] = Gas then
              lt.Par.setLBM7 (xdx, (Interface 0., lt.Par.getfA xyz, lt.Par.getRho xyz, lt.Par.getUx xyz, lt.Par.getUy xyz, lt.Par.getUz xyz, lt.Par.getTau xyz)))
        else correctFluids xyz )))

  lt.Timer.tockPrint 2 "rephase"
  lt.Timer.tockPrint 1 "FreeSurface.rePhase"
