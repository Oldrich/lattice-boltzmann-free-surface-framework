﻿namespace LBM.PostProcess

open System.IO
open System.Collections.Generic
open System.Text.RegularExpressions

open Utilities.Math
open Utilities.IO
open Utilities.IO
open Utilities.PostProcess.U3D.Types

open LBM
open LBM.Basic
open LBM.LO.Types
open LBM.Materials
open LBM.PreProcess
open LBM.Naming
open LBM.Naming.General

type Helping =
  static member tryFind (what: string, resource) =
    let where = Utilities.IO.File.readResource resource
    let m = Regex.Match (where, System.String.Format("% Begin = {0}(.+)% End = {0}", what), RegexOptions.Singleline)
    if m.Groups.Count = 2 then Some m.Groups.[1].Value else None

  static member find (what, resource) =
    match Helping.tryFind (what, resource) with Some s -> s | None -> failwithf "Cannot find %s in %s" what resource

type U3DataLO = Dictionary<int,Lazy<((System.Guid * int) * (triple * quaternion) * Shape * Interaction) []>>

type ReadAndCombine (problemDirPath) as o =

  let units = lazy (
    let rp = Zaml.parse (File.ReadAllText (Path.Combine(problemDirPath, Files.report)))
    LBM.Units.ofZaml rp )

  let problemDir = DirectoryInfo problemDirPath

  let readLOA = lazy (
    o.readLmbLoaFiles "loA" |> Dictionary.mapB (fun key value ->
      let key = int (Utilities.String.findAllFloats key).[0]
      let value = lazy (
        let loA = List()
        for file: FileInfo in value do
          use stream = file.OpenRead()
          use zipStream = new Compression.GZipStream (stream, Compression.CompressionMode.Decompress)
          use br = new BinaryReader (zipStream)
          LO.HelpingFunctions.Binary.ofBinary (br, loA)
          br.Close()
        loA.ToArray() )
      key, value ) )

  member private o.readFile (fileNames, combineFn) =
    try
      let dirs = problemDir.GetDirectories "*Domain*"
      let out =
        dirs |> Array.fold (fun (oldLines: float [][]) dir ->
          let getVals (line: string) = line.Split [|'\t'|] |> Array.map float
          let newLinesA = fileNames |> Array.map (fun fileName ->
            let filePath = Path.Combine (dir.FullName, fileName)
            use fs = new FileStream (filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            use r = new StreamReader(fs)
            r.ReadToEnd().Split ([| System.Environment.NewLine |], System.StringSplitOptions.RemoveEmptyEntries) )
          let minLinesLength =
            let newL = newLinesA |> Array.minBy Array.length |> Array.length
            if oldLines.Length = 0 then newL else min newL oldLines.Length
          Array.init minLinesLength (fun linei ->
            let newLineA = newLinesA |> Array.map (fun newLines -> getVals newLines.[linei])
            let oldLine = if oldLines.Length = 0 then None else Some oldLines.[linei]
            combineFn newLineA oldLine )) [||]
      Some out
    with _ -> None

  member private o.combineFirstTimeFnAdd fileName =
    o.readFile ([| fileName |], fun newVA oldVA ->
      match oldVA with
        | Some oldVA ->
          newVA.[0] |> Array.mapi (fun i newV ->
            if i = 0 then
              if newV = oldVA.[0] then newV else failwithf "combineFirstTimeFnAdd: The new time %g does not match the old time %g." newV oldVA.[0]
            else newV + oldVA.[i] )
        | None -> newVA.[0] )
    |> Option.map (Array.map (fun v -> v.[0], Array.init (v.Length - 1) (fun i -> v.[i + 1] )))

  member private o.combineFirstTimeFnWeightedMean (fileName, weightFileName) =
    let sumWeight = Dictionary()
    let out = o.readFile ([| fileName; weightFileName |], fun newVA oldVA ->
      sumWeight.[newVA.[0].[0]] <- if sumWeight.ContainsKey newVA.[0].[0] = false then newVA.[1].[1] else sumWeight.[newVA.[0].[0]] + newVA.[1].[1]
      newVA.[0] |> Array.mapi (fun i _ ->
        if i = 0 then newVA.[0].[0]
        else
          let oldV = match oldVA with Some oldVA -> oldVA.[i] | None -> 0.
          if i = 0 then oldV else oldV + newVA.[1].[1] * newVA.[0].[i]) )
    out |> Option.map (Array.map (fun v -> v.[0], Array.init (v.Length - 1) (fun i -> v.[i + 1] / sumWeight.[v.[0]]) ))

  member private o.combineFirstTimeFnMinMaxWeightedMean (fileName, weightFileName) =
    let sumWeight = Dictionary()
    let out = o.readFile ([| fileName; weightFileName |], fun newVA oldVA ->
      let time, vMin, vMax, vMean, weight = newVA.[0].[0], newVA.[0].[1], newVA.[0].[2], newVA.[0].[3], newVA.[1].[1]
      sumWeight.[newVA.[0].[0]] <- 
        if sumWeight.ContainsKey newVA.[0].[0] = false then newVA.[1].[1] 
        else sumWeight.[newVA.[0].[0]] + weight
      match oldVA with Some oV -> [| time; min oV.[1] vMin; max oV.[2] vMax; oV.[3] + vMean * weight |] | None -> [| time; vMin; vMax; vMean * weight |] )
    out |> Option.map (Array.map (fun v -> v.[0], [| v.[1]; v.[2]; v.[3] / sumWeight.[v.[0]] |]))

  member o.Units = units.Value

  member o.fluidMass = o.combineFirstTimeFnAdd Files.fluidMass
  member o.fractionOfCellsAboveTau = o.combineFirstTimeFnWeightedMean (Files.fractionOfCellsAboveTau, Files.fluidMass)
  member o.meanTau = o.combineFirstTimeFnWeightedMean (Files.meanTau, Files.fluidMass)
  member o.minMaxMeanLbmSpeed = o.combineFirstTimeFnMinMaxWeightedMean (Files.minMaxMeanLbmSpeed, Files.fluidMass)
  member o.minMaxMeanRhoDiff = o.combineFirstTimeFnMinMaxWeightedMean (Files.minMaxMeanRhoDiff, Files.fluidMass)
  member o.minMaxMeanPressureDiff = o.minMaxMeanRhoDiff |> Option.map (Array.map (fun (time, rhoA) -> time, rhoA |> Array.map (fun v -> v / 3.0)))
  member o.minMaxMeanShearRate = o.combineFirstTimeFnMinMaxWeightedMean (Files.minMaxMeanShearRate, Files.fluidMass)
  
  member o.kineticEnergyLO = o.combineFirstTimeFnAdd Files.kineticEnergyOfLO
  member o.numberOfCollisionsDissipatedEnergy = o.combineFirstTimeFnAdd Files.numberOfCollisionsDissipatedEnergy
  member o.numberOfParticles = o.combineFirstTimeFnAdd Files.numberOfParticles
  member o.particleVolumeFractionFiberSphereEllipsoid = o.combineFirstTimeFnWeightedMean (Files.particleVolumeFractionFiberSphereEllipsoid, Files.fluidMass)
  

  member o.numberOfCollisionsDissipatedEnergyPerLO =
    o.numberOfParticles |> Option.bind (fun numP ->
      o.numberOfCollisionsDissipatedEnergy |> Option.map (Array.mapi (fun i (time1, colA) ->
        time1, [| colA.[0] / (snd numP.[i]).[0]; colA.[1] / (snd numP.[i]).[0] |] )))

  member private o.toReal (vA, fn: float -> float<'a>) =
    vA |> Option.map (Array.map (fun (time, vA) -> time |> o.Units.toReal.Time |> float, vA |> Array.map (fun a -> fn a |> float)))

  member private o.toReal (vA, fnA: (float -> float<'a>) list) =
    vA |> Option.map (Array.map (fun (time, vA) -> time |> o.Units.toReal.Time |> float, vA |> Array.mapi (fun i a -> fnA.[i] a |> float)))
  
  member private o.toRealTime vA = vA |> Option.map (Array.map (fun (time, v) -> time |> o.Units.toReal.Time |> float, v ))

  
  member o.fluidMassReal = o.toReal (o.fluidMass, o.Units.toReal.Mass)
  member o.fractionOfCellsAboveTauReal = o.toReal (o.fractionOfCellsAboveTau, id)
  member o.minMaxMeanRhoDiffReal = o.toReal (o.minMaxMeanRhoDiff, o.Units.toReal.Density)
  member o.minMaxMeanPressureDiffReal = o.toReal (o.minMaxMeanPressureDiff, o.Units.toReal.Stress)
  member o.minMaxMeanSpeedReal = o.toReal (o.minMaxMeanLbmSpeed, o.Units.toReal.Speed)
  member o.minMaxMeanShearRateReal = o.toReal (o.minMaxMeanShearRate, o.Units.toReal.ShearRate)
  member o.meanViscosityReal = o.toReal (o.meanTau, fun tau -> (tau - 0.5) / 3. |> o.Units.toReal.DynamicViscosity)

  member o.kineticEnergyLOReal = o.toReal (o.kineticEnergyLO, o.Units.toReal.Energy)
  member o.numberOfCollisionsDissipatedEnergyPerLOReal = o.toReal (o.numberOfCollisionsDissipatedEnergyPerLO, [id; fun v -> v |> o.Units.toReal.Energy |> float])
  member o.numberOfCollisionsDissipatedEnergyReal = o.toReal (o.numberOfCollisionsDissipatedEnergy, [id; fun v -> v |> o.Units.toReal.Energy |> float])
  member o.numberOfParticlesReal = o.toReal (o.numberOfParticles, id)
  member o.particleVolumeFractionFiberSphereEllipsoidReal = o.toReal (o.particleVolumeFractionFiberSphereEllipsoid, id)
  

  member o.readLmbLoaFiles name =
    let out = Dictionary ()
    for domDir in problemDir.GetDirectories "Domain_*" do
      let loAdir = DirectoryInfo (Path.Combine (domDir.FullName, name))
      if loAdir.Exists then
        let loAfiles = loAdir.GetFiles (sprintf "%s_*" name)
        loAfiles |> Array.iter (fun (losFile: FileInfo) ->
          if out.ContainsKey losFile.Name = false then out.Add (losFile.Name, List [losFile]) else out.[losFile.Name].Add losFile )
      else printfn "Did not find %s" loAdir.FullName
    out

  member o.readLoA: U3DataLO = readLOA.Value

  member o.readFluidShape =
    o.readLmbLoaFiles "fluidShape" |> Dictionary.mapB (fun key value ->
      let time = int (Utilities.String.findAllFloats key).[0]
      let value = lazy (
        let lbmA = Dictionary()
        for file in value do
          use stream = file.OpenRead()
          use zipStream = new Compression.GZipStream (stream, Compression.CompressionMode.Decompress)
          use br = new BinaryReader (zipStream)
          let rp = Zaml.parse (File.ReadAllText (Path.Combine(file.Directory.Parent.FullName, Files.report)))
          let nodes = rp.find [Report.Lattice.lattice; Report.Lattice.locDim] |> Zaml.toTriple
          let origin = rp.find [Report.Lattice.lattice; Report.Lattice.origin] |> Zaml.toTriple
          for x in 0 .. int nodes.X - 1 do
            for y in 0 .. int nodes.Y - 1 do
              let z = br.ReadUInt16()
              if z <> 65535us then
                let key = int origin.X + x, int origin.Y + y
                let value = int origin.Z + int z
                lbmA.[key] <- if lbmA.ContainsKey key then max lbmA.[key] value else value
          br.Close()
        lbmA )
      time, value )

  member o.oldParticlesTypeCoGandAxis (?chooseFiles) =
    let chooseFiles = defaultArg chooseFiles id

    [|for domDir in problemDir.GetDirectories "Domain_*_LbmTime-*" do

        let locReportFile = (domDir.GetFiles "_report_*.txt").[0]
        if locReportFile.Exists then
          let globReportFile =
            let files = domDir.GetFiles "_report.txt"
            if files.Length = 0 then locReportFile else files.[0]
          let (ox, oy, oz), lbm2real =
            let locZaml = Zaml.parse (File.ReadAllText locReportFile.FullName)
            let globZaml = Zaml.parse (File.ReadAllText globReportFile.FullName)
            let origin =
              [| "Origin X"; "Origin Y"; "Origin Z"|] |> Array.map (fun name -> 
                match locZaml.find name with
                  | ZamlS a -> float a
                  | _ -> failwith "particlesTypeCoGandAxis: cannot parse" )
            let lbm2real =
              match globZaml.find Report.Units.scalingLength with
                | ZamlS s -> 1. / (Utilities.String.findAllFloats s).[0]
                | _ -> failwith "particlesTypeCoGandAxis: cannot parse 2"
            (origin.[0], origin.[1], origin.[2]), lbm2real

          let losDir = (domDir.GetDirectories "los-cog-axis_*").[0]
          let losFiles = losDir.GetFiles "los*.txt"
          let ox, oy, oz =
            if losFiles.[0].Name.Contains "Real" then
              ox * lbm2real, oy * lbm2real, oz * lbm2real
            else ox, oy, oz
          let rA = chooseFiles losFiles |> Array.map (fun (losFile: FileInfo) ->
            losFile, lazy
              ( let newLines = File.ReadAllLines losFile.FullName |> Array.map (fun line ->
                  let vals = line.Split [||]
                  let cog = [| ox + float vals.[1]; oy + float vals.[2]; oz + float vals.[3] |]
                  let p = [| float vals.[4]; float vals.[5]; float vals.[6] |]
                  int vals.[0], cog, p )
                newLines ))
          yield domDir, rA
        else printfn "Did not find %s" locReportFile.FullName |]

  /// Takes los-cog-axis for individual domains and combines them into one
  member o.oldWriteParticlesTypeCoGandAxis (?chooseFiles, ?refresh) =
    
    let loADir = Path.Combine (problemDirPath, "los-cog-axis")
    match refresh with Some true when Directory.Exists loADir -> Directory.Delete (loADir, true) | _ -> ()
    let targetDir = Directory.CreateDirectory loADir

    let rA = o.oldParticlesTypeCoGandAxis (?chooseFiles = chooseFiles)

    (snd rA.[0]) |> Array.Parallel.iteri (fun i (timeFile, _) ->
      let outFile = Path.Combine (targetDir.FullName, timeFile.Name)
      if File.Exists outFile = false then
        use stream = File.Open (outFile, FileMode.Create)
        use w = new StreamWriter (stream)
        for _, data in rA do
          let newTimeFile, data = data.[i]
          if newTimeFile.Name <> timeFile.Name then failwithf "%s <> %s" newTimeFile.Name timeFile.Name
          for kind,cog,p in data.Value do
            w.WriteLine ("{0}\t{1:e}\t{2:e}\t{3:e}\t{4:e}\t{5:e}\t{6:e}", kind, cog.[0], cog.[1], cog.[2], p.[0], p.[1], p.[2]) )
    targetDir.GetFiles "los*"

  /// file: is produced by ReadAndCombine.oldWriteParticlesTypeCoGandAxis
  /// acceptFiber: cog * orientation -> accept?
  static member oldParticlesTypeCoGandAxis2 (filePath, acceptFiber) =
    let file = File.ReadAllLines filePath
    let lst = List()
    for line in file do
      let data = line.Split [| '\t' |] |> Array.map float
      let cog = triple (data.[1], data.[2], data.[3])
      let v = triple (data.[4], data.[5], data.[6])
      if acceptFiber (cog, v) then lst.Add (cog, v)
    lst.ToArray()