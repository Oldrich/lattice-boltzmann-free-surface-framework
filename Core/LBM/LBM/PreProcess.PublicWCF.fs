﻿namespace LBM.PreProcess

open LBM

open System
open System.ServiceModel
open System.ServiceModel.Web
open System.ServiceModel.Description
open System.Threading
open System.IO

[<ServiceContract>]
type internal IPublicWCF =

  [<OperationContract>]
  [<WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)>]
  abstract Ping: Nothing:unit -> unit

  [<OperationContract>]
  [<WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)>]
  abstract Pause: Nothing:unit -> unit

  [<OperationContract>]
  [<WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)>]
  abstract Play: Nothing: unit -> unit

  [<OperationContract>]
  [<WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json)>]
  abstract Exit: Nothing: unit -> unit

[<ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)>]
type LocalWCF (isRunning: ManualResetEvent) =

  interface IPublicWCF with

    member o.Ping () = ()

    member o.Pause () = if isRunning.Reset() = false then failwith "Cannot Pause the simulation..."

    member o.Play () = if isRunning.Set() = false then failwith "Cannot Play the simulation..."

    member o.Exit () = File.Delete @"..\Delete me to exit"

type PublicWCF (taskName, isRunning) =

  let locWcf = LocalWCF (isRunning)

  do
    let svh = new ServiceHost (locWcf, new Uri("http://localhost/" + taskName))
    let binding = new WebHttpBinding()
    let endPoint = svh.AddServiceEndpoint(typeof<IPublicWCF>, binding, "")
    let behavior = new WebHttpBehavior()
    endPoint.Behaviors.Add behavior
    svh.Open ()

