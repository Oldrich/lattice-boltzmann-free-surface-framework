﻿namespace LBM.BC.Types

open LBM.Basic
open Kit

type BCPhase = BCGas | BCFluid

type BcVolume =
  | BcAnalytical of totalVolume: float
  | BcFixedI of volumeInCell: float

type Slip = SlipCoef of float | SlipLength of float

type BcPurpose =
  /// Velocity triple * LBM slip length
  | BcBounceBack of (float triple * Slip) 
  /// Velocity triple
  | BcVelocity of velocity: float triple
  | BcVelocityFn of (int*int*int -> VelocityVector)
  | BcInflow of velocity: float triple * rho:float
  | BcInflowFn of (int*int*int -> VelocityVector) * float
  | BcDensity of float 
  | BcDoNothing

namespace LBM.BC.Types.Internal

open System.Collections.Generic
open Kit

/// ((loc x*y*z) * particle population) -> (new F * new Rho * new U)
type MapToLBMfn = ((int*int*int) * float []) -> (float [] * float * float triple)
type LBMMap = Dictionary<int*int*int, MapToLBMfn>

type BcPurposeI = 
  /// velocity * slip length
  | BcBounceBackI of (float triple * LBM.BC.Types.Slip)
  ///Used to modify LBM
  | BcMapLBMI of (LBMMap -> unit)

type BcShape =
  | BcPlanes
  | BcPolygons
  | BcCircularPlanes

type BcObject = {
  Purpose: BcPurposeI
  Shape: BcShape
  /// local point -> distance * normal (bc to point)
  distanceAndNormal: float triple -> float * float triple
  /// (x*y*z) -> (x*y*z)
  getSymmetricCoordAndIndex: (int triple -> int triple -> int -> (int triple * int))
  Description: string * Utilities.IO.ZamlType
  Invisible: bool
}