﻿namespace LBM.LO

open LBM
open LBM.LO.Types
open LBM.Basic

open Kit
open Kit.Collections
open Kit.Math

open System.Collections.Generic

type Updater (lt: Lattice, bcA, ?lPO: LagrangianProblem) =
  
  member o.evaluateData (lp: LagrangianProblem, duration) = 
 
    let loA = lp.MyParticles

    let loDataRhoV, loDataU, nbrsX, nbrsY, nbrsZ, nbrsD =
      let loDataRhoV, loDataU, nX, nY, nZ, nD =
        let fn () = loA |> Array.map (fun lo -> Array.zeroCreate lo.Nodes.LocalDX.Length)
        fn (), fn (), fn (), fn (), fn (), fn ()
      loA |> Array.iteri (fun i lo ->
        lo.Nodes.LocalDX |> Array.iteri (fun j locDxi ->
          let xNode, yNode, zNode = Triple.toTuple (lo.CoG + lo.Rot.rotateL2G locDxi)
          let xx = int (round xNode), int (round yNode), int (round zNode)
          match lt.Par.tryGetUx xx with
            | Some a when Option.isSome a ->
              let hD, dirac = match lo._Interaction with TwoWay -> lp.Controls.TwoWayDiracDelta3D | TrackingParticle -> lp.Controls.TrackingDiracDelta3D                      
              let nbrsX, nbrsY, nbrsZ, nbrsD, globU, globRho, sumD =
                let (lx, hx), (ly, hy), (lz, hz) = 
                  let fn x = (x - hD) |> ceil |> int, (x + hD) |> floor |> int
                  fn xNode, fn yNode, fn zNode
                let nbrsD, nbrsX, nbrsY, nbrsZ, dUx, dUy, dUz, nbrsDRho =
                  let length = (hx - lx + 1) * (hy - ly + 1) * (hz - lz + 1)  
                  List length, List length, List length, List length, List length, List length, List length, List length
                let mutable sumD = 0.
                for x2 = lx to hx do
                  for y2 = ly to hy do
                    for z2 = lz to hz do
                      match lt.Par.tryGetRhoUxUyUz (x2, y2, z2) with
                        | Some (rho, ux, uy, uz) ->
                            let nbrsDi = dirac (float32 x2 - float32 xNode, float32 y2 - float32 yNode, float32 z2 - float32 zNode) |> float
                            if nbrsDi > 0. then
                              nbrsX.Add x2; nbrsY.Add y2; nbrsZ.Add z2; nbrsD.Add nbrsDi; dUx.Add (nbrsDi * ux); dUy.Add (nbrsDi * uy); dUz.Add (nbrsDi * uz); nbrsDRho.Add (nbrsDi * rho)
                              sumD <- sumD + nbrsDi
                        | None -> ()
                nbrsX.ToArray(), nbrsY.ToArray(), nbrsZ.ToArray(), nbrsD.ToArray(), triple (dUx.ToArray() |> Array.sum, dUy.ToArray() |> Array.sum, dUz.ToArray() |> Array.sum) , nbrsDRho.ToArray() |> Array.sum, sumD
              if sumD > 0. then
                nX.[i].[j] <- nbrsX; nY.[i].[j] <- nbrsY; nZ.[i].[j] <- nbrsZ; nD.[i].[j] <- nbrsD |> Array.map (fun nbrsDi -> nbrsDi / sumD)
                loDataRhoV.[i].[j] <- globRho / sumD * lo.Nodes.V.[j] / duration; loDataU.[i].[j] <- globU / sumD
              else
                nX.[i].[j] <- [||]; nY.[i].[j] <- [||]; nZ.[i].[j] <- [||]; nD.[i].[j] <- [||]
                loDataRhoV.[i].[j] <- 0.; loDataU.[i].[j] <- Triple.zerof
            | _ ->
              nX.[i].[j] <- [||]; nY.[i].[j] <- [||]; nZ.[i].[j] <- [||]; nD.[i].[j] <- [||]
              loDataRhoV.[i].[j] <- 0.; loDataU.[i].[j] <- Triple.zerof ))
      loDataRhoV, loDataU, nX, nY, nZ, nD
    
    let ponorA = Array.init loA.Length (fun i -> (loDataRhoV.[i] |> Array.sumBy (fun a -> if a = 0. then 0. else 1.)) / float loDataRhoV.[i].Length * duration)

    loA, loDataRhoV, loDataU, nbrsX, nbrsY, nbrsZ, nbrsD, ponorA

  member private o.getMean (v0G: _ array3, v1G) =
    v1G |> Array3.mapi (fun x y z v1 ->
      match v0G.[x,y,z], v1 with
        | Some a, Some b -> Some (0.5 * (a + b))
        | None, Some b -> Some b
        | Some _, None | None, None -> None )

  member o.update (curTime) = o.update (curTime - 1, curTime, curTime + 1)

  member o.update (lastTime, curTime, nextTime) =

    lt.Timer.tick 1

    match lPO with
      | Some lp ->

        let duration = float (curTime - lastTime)
        
        lt.Timer.tick 2
        let loA, loDataRhoV, loDataU, nbrsX, nbrsY, nbrsZ, nbrsD, ponorA = o.evaluateData (lp, duration)
        lt.Timer.tockPrintTick 2 "loFluidData_Ponor"

        lt.Timer.tick 3
        lp.propagateLO (loA, true)
        lt.Timer.tockPrintTick 3 "Update: propagateLO"
        
        let n0, w0 = LO.Collisions.modifyLPByInitialCollisions (curTime, duration, lp, loA, bcA)
        lt.Timer.tockPrintTick 3 "Update: initial collision"
        
        let loA = LO.Integration.TrackingParticles.run (duration, loA, loDataU)
        lt.Timer.tockPrintTick 3 "Update: tracking particles"

        lt.Timer.tick 2
        let activeLoA, ids = lp.removeTrackingParticles loA
        let loDataRhoV, loDataU, nbrsX, nbrsY, nbrsZ, nbrsD, ponorA =
          Array.init7 ids.Length (fun i ->
            let i = ids.[i]
            loDataRhoV.[i], loDataU.[i], nbrsX.[i], nbrsY.[i], nbrsZ.[i], nbrsD.[i], ponorA.[i])
        let newActiveLoA, newFfsiA, (nI, wI) =
          Integration.RungeKutta.run (lp.Controls.TwoWayIntegrationMethod, duration, lp, activeLoA, (loDataRhoV, loDataU, lt, curTime, ponorA), bcA)
        
        if LBM.Basic.trashFn.ContainsKey "collisionForce" then LBM.Basic.trashFn.["collisionForce"] LBM.Basic.trashData.["collisionForce"]
        
        lt.Timer.tockPrintTick 2 "Integration"  

        lp.SetMyParticles (loA, newActiveLoA)
        lt.Timer.tockPrintTick 2 "NewParticlesOfLP"
        
        lp.moveToTheFuture (curTime, nextTime)
        lt.Timer.tockPrintTick 2 "PairsUpdate"

        lt.Par.resetForce_LO()
        newActiveLoA |> Array.iteri (fun i _ ->
          nbrsX.[i] |> Array.iteri (fun j xA -> 
            xA |> Array.iteri (fun k x ->
              let force = newFfsiA.[i].[j] * nbrsD.[i].[j].[k]
              lt.Par.addForce_LO (x, nbrsY.[i].[j].[k], nbrsZ.[i].[j].[k], force) ) ) )
        lt.Par.propagateLoFutureForces ()
        
        lt.Timer.tockPrintTick 2 "ForceField"
        lt.Timer.tockPrint 1 "LO.Main.update"

        (n0 + nI, w0 + wI)
      | _ -> 0., 0.