﻿namespace LBM.Parallel

open LBM
open LBM.Basic

open Kit
open Kit.Collections
open Kit.Math

open System.IO
open System.Collections.Generic

module GA =

  let getSlice ((xMin, xMax), (yMin, yMax), (zMin, zMax)) fnW (v: _ array3)  =
    let fn v lx d =
      match v with
       | Some v -> if v < 0 then 0 elif v > lx - 1 then lx - 1 else v
       | None -> d
    let xMin, xMax = fn xMin v.lx 0, fn xMax v.lx (v.lx - 1)
    let yMin, yMax = fn yMin (v.ly 0) 0, fn yMax (v.ly 0) ((v.ly 0) - 1)

    let zMinMax = Array.init (xMax - xMin + 1) (fun lx ->
      Array.init (yMax - yMin + 1) (fun ly ->
        let lengthZ = v.lz(xMin + lx, yMin + ly)
        fn zMin lengthZ 0, fn zMax lengthZ (lengthZ - 1) ))

    let lengthA = zMinMax |> Array.map (Array.map (fun (zMin, zMax) -> zMax - zMin + 1))

    use m = new MemoryStream ()
    use w = new BinaryWriter (m)
    for x in xMin .. xMax do
      for y in yMin .. yMax do
        let zMin, zMax = zMinMax.[x - xMin].[y - yMin]
        for z in zMin .. zMax do
          fnW w v.[x, y, z]

    lengthA, m.ToArray()

  let ofArr (lengthA: int [,]) (arr: byte []) fnR =
    use m = new MemoryStream (arr)
    use r = new BinaryReader (m)
    let out = Array.init (lengthA.GetLength 0) (fun x ->
      Array.init (lengthA.GetLength 1) (fun y ->
        Array.init lengthA.[x,y] (fun _ ->
          fnR r )))
    array3 (lengthA, out)

type LbmMain (domainID: (int*int*int) option [,,], loOffsetD, nodesXYZl, isSerial, mainId, args, timer) =

  let nXl, nYl, nZl = nodesXYZl
  let lbmOffset, loOffsetD = 1, max loOffsetD 1

  let wcf = WCF (domainID, isSerial, mainId, args, timer)

  let initNone () = Array3D.init 3 3 3 (fun _ _ _ -> ref None)
  let paA: PhaseArray option ref [,,] = initNone ()
  let uxaA: UArrayO option ref [,,] = initNone ()
  let uyaA: UArrayO option ref [,,] = initNone ()
  let uzaA: UArrayO option ref [,,] = initNone ()
  let rhoaA: RhoArrayO option ref [,,] = initNone ()
  let tauaA: TauArrayO option ref [,,] = initNone ()
  let faA: FArrayO option ref [,,] = initNone ()

  let initNoneF =
    let lengthAOA = domainID |> Array3D.mapi (fun i j k idO -> idO |> Option.map (fun _ ->
      let fn i lx = match i with 0 | 2 -> loOffsetD | _ -> lx
      let lx, ly, lz = (fn i nXl), (fn j nYl), (fn k nZl)
      Array2D.create lx ly lz ))
    fun () -> lengthAOA |> Array3D.map (Option.map (fun lengthA -> Array3.create lengthA None))

  let forceLO: float triple option array3 option [,,] ref = ref (initNoneF ())

  let mapIndexG2L ofs (xyz: int triple) =
    let fn x nXl = if x < 0 then 0, x + ofs elif x < nXl then 1, x else 2, x - nXl
    let (i, xloc), (j, yloc), (k, zloc) = fn xyz.x nXl, fn xyz.y nYl, fn xyz.z nZl
    (i,j,k), triple(xloc, yloc, zloc)

  let getVal def offset xyz (v: _ array3 option ref [,,]) =
    let (i,j,k), lxyz = mapIndexG2L offset xyz
    match !v.[i,j,k] with
      | Some vv -> vv.[lxyz]
      | None when Option.isSome domainID.[i, j, k] -> def
      | _ -> failwithf "out of range in ijk = (%d,%d,%d), domainID.[i,j,k] = %A at xyz = %A" i j k domainID.[i, j, k] xyz
  
  let tryGetVal offset xyz (v: _ array3 option ref [,,]) =
    let (i,j,k), lxyz = mapIndexG2L offset xyz
    match !v.[i,j,k] with Some vi -> vi.tryGet lxyz | None -> None

  let apply (fn1, fn2, fn3) (pA, fA, rhoA, uxA, uyA, uzA, tauA) =
    fn1 paA pA; fn2 faA fA; fn3 rhoaA rhoA; fn3 uxaA uxA; fn3 uyaA uyA; fn3 uzaA uzA; fn3 tauaA tauA

  member o.UxA = uxaA.[1,1,1].Value.Value
  member o.UyA = uyaA.[1,1,1].Value.Value
  member o.UzA = uzaA.[1,1,1].Value.Value
  member o.RhoA = rhoaA.[1,1,1].Value.Value
  member o.TauA = tauaA.[1,1,1].Value.Value
  member o.FA = faA.[1,1,1].Value.Value
  member o.PA = paA.[1,1,1].Value.Value
  member o.ForceLoA = forceLO.Value.[1,1,1].Value

  member o.PAA = paA

  member o.TauAA = tauaA

  member o.getPhase xyz = getVal Gas lbmOffset xyz paA
  member o.getfA xyz = getVal None lbmOffset xyz faA
  member o.getRho xyz = getVal None loOffsetD xyz rhoaA
  member o.getUx xyz = getVal None loOffsetD xyz uxaA
  member o.getUy xyz = getVal None loOffsetD xyz uyaA
  member o.getUz xyz = getVal None loOffsetD xyz uzaA
  member o.getTau xyz = getVal None loOffsetD xyz tauaA

  member o.tryGetPhase xyz = tryGetVal lbmOffset xyz paA
  member o.tryGetfA xyz = tryGetVal lbmOffset xyz faA
  member o.tryGetRho xyz = tryGetVal loOffsetD xyz rhoaA
  member o.tryGetUx xyz = tryGetVal loOffsetD xyz uxaA
  member o.tryGetUy xyz = tryGetVal loOffsetD xyz uyaA
  member o.tryGetUz xyz = tryGetVal loOffsetD xyz uzaA
  member o.tryGetTau xyz = tryGetVal loOffsetD xyz tauaA

  member o.tryGetRhoUxUyUz xyz =
    let (i,j,k), lxyz = mapIndexG2L loOffsetD xyz
    match !rhoaA.[i,j,k] with
      | Some rhoA when rhoA |> Array3.existsIndex lxyz ->
        match rhoA.[lxyz] with
          | Some rho -> 
            Some (rho, (!uxaA.[i,j,k]).Value.[lxyz].Value, (!uyaA.[i,j,k]).Value.[lxyz].Value, (!uzaA.[i,j,k]).Value.[lxyz].Value)
          | None -> None
      | _ -> None
      
  member o.setLBMA7 (pA, fA, rho, ux, uy, uz, tau) =
    let inline set (v: _ array3 option ref [,,]) vi = v.[1,1,1] := Some vi
    apply (set, set, set) (pA, fA, rho, ux, uy, uz, tau)

  member o.setLBMA6 (fA, rhoA, uxA, uyA, uzA, tauA) =
    let inline set (v: _ array3 option ref [,,]) vi = v.[1,1,1] := Some vi
    set faA fA; set rhoaA rhoA; set uxaA uxA; set uyaA uyA; set uzaA uzA; set tauaA tauA

  member o.setfA v = faA.[1,1,1] := Some v

  member internal o.propagateLoFutureForces () =
    timer.tick 3

    let lo2LbmData = Array3D.init 3 3 3 (fun i j k ->
      match forceLO.Value.[i,j,k] with
        | Some forceA when (i,j,k) <> (1,1,1) && Array3.exists Option.isSome forceA ->      
          forceA |> GA.getSlice ((None,None), (None,None), (None,None)) (fun w v -> 
            match v with 
              | Some v -> w.Write v.x; w.Write v.y; w.Write v.z
              | None -> w.Write System.Double.NaN ) |> Some
        | _ -> None )

    let inc = wcf.sendAndReceive_Exchange3D ("-propagateForces-", lo2LbmData)
    
    inc |> Array3D.iteri (fun i j k di ->
      di |> Option.iter (Option.iter (fun (lengthA, force) ->
        let lengthA = Array2D.init lengthA.Length lengthA.[0].Length (fun x y -> lengthA.[x].[y])
        GA.ofArr lengthA force (fun r ->
          let x = r.ReadDouble()
          if System.Double.IsNaN x then None else
            let y, z = r.ReadDouble(), r.ReadDouble()
            triple (x, y, z) |> Some)
        |> Array3.iteri (fun xyz fO -> fO |> Option.iter (fun force ->
          let fn i x lx = match i with 0 | 1 -> x | _ -> x + lx - loOffsetD
          o.addForce_LO (fn i xyz.x nXl, fn j xyz.y nYl, fn k xyz.z nZl, force) )))))

    timer.tockPrint 3 "LbmMain.propagateLoForces"

  member internal o.propagate (loData) =

    timer.tick 3

    let getRanges (i,j,k) ofs (g: _ array3) =
      let fn i lengthX =
        if i = 0 then None, Some (ofs - 1)
        elif i = 1 then None, None
        else Some (lengthX - ofs), None
      fn i g.lx, fn j (g.ly 0), fn k g.MaxZ

    let fn1 fn ijk ofs (v: _ array3 option ref [,,]) =
      let g = v.[1,1,1].Value.Value
      g.Values |> JArray3D.getSliceBy (getRanges ijk ofs g) fn

    let fn2 ijk ofs (v: float option array3 option ref [,,]) =
      let g = v.[1,1,1].Value.Value
      g |> GA.getSlice (getRanges ijk ofs g) (fun w v -> w.Write (match v with | Some v -> v | None -> System.Double.NaN) )

    let lbmData = Array3D.init 3 3 3 (fun i j k ->
      if (i,j,k) <> (1,1,1) && Option.isSome domainID.[i,j,k] then
        let ijk = i, j, k
        let npA = fn1 Phase.toFloat ijk lbmOffset paA
        let nfA = fn1 id ijk lbmOffset faA
        let fn3 v = fn2 ijk loOffsetD v |> snd
        let lengthA, rho = fn2 ijk loOffsetD rhoaA
        Some (npA, nfA, lengthA, rho, fn3 uxaA, fn3 uyaA, fn3 uzaA, fn3 tauaA)
      else None )

    let inc = wcf.sendAndReceive_Exchange3D ("-propagateLBMLO-", Array3D.zip(lbmData, loData))
    
    let out = inc |> Array3D.mapi (fun i j k di ->
      match di with
      | None -> None
      | Some(fluid, loData) ->
        fluid |> Option.iter (fun (pa, fa, lengthA, rho, ux, uy, uz, tau) ->
          let lengthA = Array2D.init lengthA.Length lengthA.[0].Length (fun x y -> lengthA.[x].[y])
          paA.[i,j,k] := Some (array3 pa |> Array3.map Phase.ofFloat)
          faA.[i,j,k] := Some (array3 fa)
          let fn3 (vA: _ [,,]) (vi: byte []) =
            vA.[i,j,k] := Some (GA.ofArr lengthA vi (fun r ->
              let v = r.ReadDouble()
              if System.Double.IsNaN v then None else Some v ) )
          fn3 rhoaA rho; fn3 uxaA ux; fn3 uyaA uy; fn3 uzaA uz; fn3 tauaA tau )

        loData )

    timer.tockPrint 3 "LbmMain.propagate"

    out

  member o.propagateLBM () =
    let loData = Array3D.create 3 3 3 None
    o.propagate (loData) |> ignore

  // -------------- Free Surface -------------------------

  /// pA, fA, rhoA, uxA, uyA, uzA, tauA
  member o.setLBM7 (lxyz, data) =
    let inline fn (v: _ array3 option ref [,,]) vi = v.[1,1,1].Value.Value.[lxyz] <- vi
    apply (fn, fn, fn) data
  
  member o.trySetLBM7 (xyzl: int triple, data) =
    let ijk, _ = mapIndexG2L 0 xyzl
    if ijk = (1,1,1) then
      let inline fn (v: _ array3 option ref [,,]) vi = v.[1,1,1].Value.Value.[xyzl] <- vi
      apply (fn, fn, fn) data

  // -------------- Lagrangian Objects -------------------------

  member o.resetForce_LO () = forceLO := initNoneF ()

  member o.addForce_LO (x, y, z, t: float triple) =
    let (i,j,k), lx = mapIndexG2L loOffsetD (triple(x, y, z))
    forceLO.Value.[i,j,k].Value.[lx] <- Some (match forceLO.Value.[i,j,k].Value.[lx] with Some v -> v + t | None -> t )

  member o.getForce_LO x = match forceLO.Value.[1,1,1] with Some f -> f.[x] | None -> None

  //member o.ForceLO = forceLO

  member o.WCF = wcf