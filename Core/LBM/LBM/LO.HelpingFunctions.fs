﻿module LBM.LO.HelpingFunctions

open LBM
open LBM.Basic
open LBM.LO.Types
open LBM.BC.Types.Internal

open Kit
open Kit.Math
open Kit.Collections

open System.IO

type Binary =

  static member toBinary (origin: float triple, ((uid: System.Guid, marker), (cog, rot: quaternion), shape, interaction), bw: BinaryWriter) =
    let w (v: float) = bw.Write (float32 v)
    let wt (t: float triple) = w t.x; w t.y; w t.z
    bw.Write (uid.ToByteArray()); bw.Write (byte marker); wt (origin + cog); w rot.w; wt rot.t
    match shape with
      | Fiber f -> bw.Write 0uy; w f.Length; w f.AspectRatio
      | Sphere s -> bw.Write 1uy; w s.Radius
      | SymmetricEllipsoid e -> bw.Write 2uy; w e.MainHalfAxis; w e.Radius
    bw.Write (match interaction with TwoWay -> 0uy | TrackingParticle -> 1uy)

  static member toBinary (origin, lo: LBM.LO.Types.LO, bw) =
    Binary.toBinary (origin, ((lo.Guid, lo.Marker), (lo.CoG, lo.Rot), lo.Shape, lo._Interaction), bw)

  static member toBinary (origin, data: (_*_*_*_) [] seq, s) =
    for dA in data do for d in dA do Binary.toBinary (origin, d, s)
  
  static member toBinary (origin, loA: LBM.LO.Types.LO seq, s) =
    for lo in loA do Binary.toBinary (origin, lo, s)

  static member ofBinary (br: BinaryReader, out: _ System.Collections.Generic.List) =
    let r () = float (br.ReadSingle())
    let rt () = triple(r(), r(), r())
    let guid = ref [||]
    let readGui () =
      guid := br.ReadBytes 16
      guid.Value.Length = 16
    while readGui() do
      let ids = (System.Guid (), int (br.ReadByte()))
      let pos = (rt(), quaternion(r(), rt()))
      let shape =
        match br.ReadByte() with
          | 0uy -> Fiber (FiberType (r(), r()))
          | 1uy -> Sphere (SphereType (r()))
          | 2uy -> SymmetricEllipsoid (SymmetricEllipsoidType (1., r(), r()))
          | _ -> failwith "Not supported"
      let interaction = match br.ReadByte() with 0uy -> TwoWay | 1uy -> TrackingParticle | _ -> failwith "not supported"
      out.Add (ids, pos, shape, interaction)

module private DistanceNormalDxi_Plane =
  let ellPlane (lo: LO) (bc: LBM.BC.Types.Internal.BcObject) =
    match lo.Shape, bc.Shape with
      | SymmetricEllipsoid el, LBM.BC.Types.Internal.BcPlanes ->      
        let p1, p2 = 
          let planeNormal =
            let _, n = bc.distanceAndNormal lo.CoG
            n |> Versor.rotateG2L lo.Rot
          let k1, k2 =
            let planeNormal2 = planeNormal |> Triple.map (fun n -> n * n)
            let disc = sqrt <| (el.L2 |> Array.sumByi (fun i l2 -> l2 * planeNormal2.[i]))
            1. / disc, -1. / disc
          let p1, p2 = Triple.init (fun i -> k1 * planeNormal.[i] * el.L2.[i]), Triple.init (fun i -> k2 * planeNormal.[i] * el.L2.[i])
          (p1 |> Versor.rotateL2G lo.Rot) + lo.CoG, (p2 |> Versor.rotateL2G lo.Rot) + lo.CoG
        let h1, n1 = bc.distanceAndNormal p1
        let h2, n2 = bc.distanceAndNormal p2
        if h1 < h2 then h1, n1, p1 - lo.CoG else h2, n2, p2 - lo.CoG
      | _ -> failwith "not implemented"    

module DistanceNormalDxiDxj =
  let inline internal sphereSphere (loi: LO) (loj: LO) (si: SphereType) (sj: SphereType) =
    let normal = loj.CoG - loi.CoG
    let l = Triple.norm normal
    let normal = normal / l
    l - si.Radius - sj.Radius, normal / l, si.Radius * normal, -sj.Radius * normal

  let inline internal sphereSpherePeriodic (lt: Lattice) (loi: LO) (loj: LO) (si: SphereType) (sj: SphereType) =
    let normal = lt.shortestGlobalNormal loi.CoG loj.CoG
    let l = lt.shortestGlobalDistance loi.CoG loj.CoG
    let normal = normal / l
    l - si.Radius - sj.Radius, normal / l, si.Radius * normal, -sj.Radius * normal

  let inline internal fibreSphere (loi: LO) (loj: LO) (fi: FiberType) (sj: SphereType) =
    let point = Geometry.Distance.segmentPoint(fi.P1P2, loj.CoG) // Not periodic
    let dp = loj.CoG - point
    let l = Triple.norm dp
    let norm = dp / l
    l- sj.Radius - fi.R, norm, point - loi.CoG, -sj.Radius * norm

  let inline internal fiberFiber  (loi: LO) (loj: LO) (fi: FiberType) (fj: FiberType) =
    let poi, poj = Geometry.Distance.segmentSegment(fi.P1P2, fj.P1P2) // Not periodic
    let dp = poj - poi
    let l = Triple.norm dp
    l - fi.R - fj.R, dp / l, poi - loi.CoG, poj - loj.CoG
  
  let inline private boundingSpheres (fnDist: float triple -> float triple -> float) (fnNormal: float triple -> float triple -> float triple) (cogi: float triple [], ri: float []) (cogj: float triple [], rj: float []) =
    let overlaps = ref false
    let dSep, dPen = ref System.Double.MaxValue, ref 0.
    let iSep, jSep, iPen, jPen = ref -1, ref -1, ref -1, ref -1

    for i = 0 to cogi.Length - 1 do
      for j = 0 to cogj.Length - 1 do
        let h = fnDist cogi.[i] cogj.[j] - (ri.[i] + rj.[j])
        if (h < 0. || (!overlaps)) && h < !dPen then
          dPen := h; overlaps := true; iPen := i; jPen := j
        elif h < !dSep then dSep := h; iSep := i; jSep := j
    
    let i, j = if !overlaps then !iPen, !jPen else !iSep, !jSep
    let h = fnDist cogi.[i] cogj.[j]
    let normal = fnNormal cogi.[i] cogj.[j]
    h - (ri.[i] + rj.[j]), normal, cogi.[i] + normal * ri.[i], cogj.[j] - normal * rj.[j]

  let inline private cogArA (lo: LO) =
      match lo.Shape with 
        | SymmetricEllipsoid e ->
          e.BoundingSpheresCog |> Array.map (fun sCog -> (sCog |> Versor.rotateL2G lo.Rot) + lo.CoG), e.BoundingSpheresR
        | _ -> failwith "unsupported shape in ellEll"

  let ellEll (loi: LO) (loj: LO) =   
    let h, n, pi, pj = 
      boundingSpheres (fun ci cj -> Triple.norm (cj - ci)) (fun ci cj -> Triple.normalize (cj - ci)) (cogArA loi) (cogArA loj)
    h, n, pi - loi.CoG, pj - loj.CoG

  let ellEllPeriodic (lt:Lattice) (loi: LO) (loj: LO) =   
    let h, n, pi, pj = boundingSpheres lt.shortestGlobalDistance lt.shortestGlobalNormal (cogArA loi) (cogArA loj)
    h, n, pi - loi.CoG, pj - loj.CoG

let getDistanceNormalDxiDxj (loi: LO) (loj: LO) =
  match loi.Shape, loj.Shape with
    | Sphere ri, Sphere rj ->  DistanceNormalDxiDxj.sphereSphere loi loj ri rj
    | Sphere ri, Fiber fj -> DistanceNormalDxiDxj.fibreSphere loj loi fj ri
    | Fiber fi, Sphere rj -> DistanceNormalDxiDxj.fibreSphere loi loj fi rj
    | Fiber fi, Fiber fj -> DistanceNormalDxiDxj.fiberFiber loi loj fi fj
    | SymmetricEllipsoid _, SymmetricEllipsoid _ -> DistanceNormalDxiDxj.ellEll loi loj
    | _ -> failwith "not implemented"

let getDistanceNormal (loi: LO) (loj: LO) =
  match loi.Shape, loj.Shape with
    | Sphere ri, Sphere rj -> 
      let h, n, _, _ = DistanceNormalDxiDxj.sphereSphere loi loj ri rj
      h, n
    | Sphere ri, Fiber fj -> 
      let h, n, _, _ = DistanceNormalDxiDxj.fibreSphere loj loi fj ri
      h, n
    | Fiber fi, Sphere rj ->
      let h, n, _, _ = DistanceNormalDxiDxj.fibreSphere loi loj fi rj
      h, -n
    | Fiber fi, Fiber fj ->
      let h, n, _, _ = DistanceNormalDxiDxj.fiberFiber loi loj fi fj
      h, n
    | SymmetricEllipsoid _, SymmetricEllipsoid _ -> 
      let h, n, _, _ = DistanceNormalDxiDxj.ellEll loi loj
      h, n
    | _ -> failwith "not implemented"

let getPeriodicDistanceNormal (lt: Lattice) (loi: LO) (loj: LO) =
  match loi.Shape, loj.Shape with
    | Sphere ri, Sphere rj -> 
      let h, n, _, _ = DistanceNormalDxiDxj.sphereSpherePeriodic lt loi loj ri rj
      h, n
    | Sphere ri, Fiber fj -> 
      let h, n, _, _ = DistanceNormalDxiDxj.fibreSphere loj loi fj ri
      h, n
    | Fiber fi, Sphere rj ->
      let h, n, _, _ = DistanceNormalDxiDxj.fibreSphere loi loj fi rj
      h, -n
    | Fiber fi, Fiber fj ->
      let h, n, _, _ = DistanceNormalDxiDxj.fiberFiber loi loj fi fj
      h, n
    | SymmetricEllipsoid _, SymmetricEllipsoid _ -> 
      let h, n, _, _ = DistanceNormalDxiDxj.ellEllPeriodic lt loi loj
      h, n
    | _ -> failwith "not implemented"

let getRoughDistance (loi: LO) (loj: LO) =
  match loi.Shape, loj.Shape with
    | Sphere ri, Sphere rj -> Triple.norm (loj.CoG - loi.CoG) - (ri.Radius + rj.Radius)
    | Sphere ri, Fiber fi
    | Fiber fi, Sphere ri ->
      let point = Geometry.Distance.segmentPoint(fi.P1P2, loj.CoG)
      Triple.norm (loj.CoG - point) - (ri.Radius + fi.R)
    | Fiber fi, Fiber fj ->
      let poi, poj = Geometry.Distance.segmentSegment(fi.P1P2, fj.P1P2) // Not periodic
      Triple.norm (poi - poj) - (fi.R + fi.R)
    | SymmetricEllipsoid e1, SymmetricEllipsoid e2 -> 
      Triple.norm (loj.CoG - loi.CoG) - (e1.MainHalfAxis + e2.MainHalfAxis)
    | _ -> failwith "not implemented"

let getDistance_BC (lo: LO) (bc: BcObject) =
  
  match lo.Shape with
    | Sphere s -> 
      let d, _ = bc.distanceAndNormal lo.CoG
      d - s.Radius
    | Fiber f ->
      let p1, p2 = f.P1P2
      let d1, _ = bc.distanceAndNormal p1
      let d2, _ =  bc.distanceAndNormal p2
      (min d1 d2) - f.R
    | SymmetricEllipsoid _ ->
      let h, _, _ = DistanceNormalDxi_Plane.ellPlane lo bc
      h

let getDistanceNormalDxi_BC (lo: LO) (bc: BcObject) =
  
  match lo.Shape with
    | Sphere s -> 
      let d, n = bc.distanceAndNormal lo.CoG
      d - s.Radius, n, -s.Radius*n
    | Fiber f ->
      let p1, p2 = f.P1P2
      let d1, n1 = bc.distanceAndNormal p1
      let d2, n2 =  bc.distanceAndNormal p2
      if d1 < d2 then d1 - f.R, n1, p1 - lo.CoG else d2 - f.R, n2, p2 - lo.CoG
    | SymmetricEllipsoid _ -> DistanceNormalDxi_Plane.ellPlane lo bc

let getTimePP (_: LO) (_: LO) (h: float) (maxSpeed: float) = h / maxSpeed |> int
let getTimeBC (_: LO) (h: float) (maxSpeed: float) = h / maxSpeed |> int 