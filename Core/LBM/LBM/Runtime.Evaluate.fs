﻿namespace LBM.Runtime

open System.IO
open System.Collections.Generic

open Utilities.Math
open Utilities.IO
open Utilities.IO
open Utilities.PostProcess.U3D.Types

open LBM
open LBM.Basic
open LBM.LO.Types
open LBM.Materials

type Evaluate (lt: Lattice, maxTau: float Lazy, lbmFluid: LbmFluid Lazy, ?lPO: LagrangianProblem) =

  let minMaxMeanRhoDiff = lazy (
    let minRhoDiff, maxRhoDiff, sumRhoDiff, sumRho =
      lt.Par.RhoA |> GArray3D.fold (fun (minRhoDiff, maxRhoDiff, sumRhoDiff, sumRho as old) rho ->
        match rho with
          | Some rho -> min minRhoDiff (rho - 1.), max maxRhoDiff (rho - 1.), sumRhoDiff + abs (rho - 1.), sumRho + 1.
          | None -> old ) (System.Double.MaxValue, System.Double.MinValue, 0., 0.)
    let meanRhoDiff = if sumRho > 0.0 then sumRhoDiff / sumRho else 0.0
    minRhoDiff, maxRhoDiff, meanRhoDiff )

  let fluidMass = lazy (
    let m = lt.Par.PA |> GArray3D.sumByi (fun x y z p ->
      match p with Interface m -> m | Fluid -> lt.Par.RhoA.[x,y,z].Value | _ -> 0. )
    m + !LBM.FreeSurface.leftMass )

  let fractionOfCellsAboveTau (tauA: _ []) =
    let sumMass = Array.zeroCreate tauA.Length
    if fluidMass.Value = 0. then Array.zeroCreate sumMass.Length
    else 
      lt.Par.TauA |> GArray3D.iteri (fun x y z tauO ->
        match tauO  with
          | Some tau ->
            let mass = match lt.Par.PA.[x,y,z] with Interface m -> m | _ -> lt.Par.RhoA.[x,y,z].Value
            for i = 0 to tauA.Length - 1 do
              if tau > tauA.[i] then sumMass.[i] <- sumMass.[i] + mass
          | _ -> () )
      sumMass |> Array.map (fun mass -> mass / fluidMass.Value)

  let fractionOfCellsAboveTauA = lazy (
    let tauA = 
      let tauMin = 0.5
      let n = 4
      Array.init n (fun i -> tauMin + float (i + 1) / float n * (maxTau.Value - tauMin))
    fractionOfCellsAboveTau tauA )

  let kineticEnergyLO = lazy (
    match lPO with
      | Some lP -> 
          lP.Particles.Values |> Seq.fold (fun (ek: float) lo ->
            let omegaLoc = lo.OmegaGlob |> lo.Rot.rotateG2L 
            ek + 0.5 * (lo.Mass * lo.U * lo.U + lo.I * (omegaLoc .* omegaLoc) ) ) 0.
      | None -> 0. )

  let minMaxMeanShearRate = lazy (
    let shearRateSum, minS, maxS, nNodes = 
      lt.Par.TauA |> GArray3D.fold (fun (shearRateSum, minS, maxS, n) tauO ->
        match tauO with
          | Some tau ->
              let shearRate, _ = lbmFluid.Value.getShearRateAndStressFromTau tau
              if shearRate > 0. then 
                shearRateSum + shearRate, (min shearRate minS), (max shearRate maxS), n + 1
              else shearRateSum, minS, maxS, n
          | _ -> shearRateSum, minS, maxS, n
        ) (0.0, System.Double.MaxValue, System.Double.MinValue, 0)
    if nNodes > 0 then minS, maxS, shearRateSum / float nNodes else 0.0, 0.0, 0.0 )

  let meanTau = lazy (
    let sum, i =
      lt.Par.TauA |> GArray3D.fold (fun (sum, i as old) t ->
        match t with
          | Some t -> sum + t, i + 1
          | None -> old) (0., 0)
    if i = 0 then 0. else sum / float i )

  let minMaxMeanLbmSpeed = lazy (
    let minU, maxU, accU, i =
      lt.Par.UxA |> GArray3D.foldi (fun x y z (minU, maxU, meanU, i as old) ux ->
        let uy, uz = lt.Par.UyA.[x,y,z], lt.Par.UzA.[x,y,z]
        match ux, uy, uz with
          | Some ux, Some uy, Some uz ->
            let speed = sqrt(ux*ux + uy*uy + uz*uz)
            min speed minU, max speed maxU, meanU + speed, i + 1
          | _ -> old ) (1e5, -1e5, 0., 0 )
    minU, maxU, if i = 0 then 0. else accU / float i )

  let numberOfParticles = lazy (
    match lPO with
      | Some lp ->
        lp.Particles.Values |> Seq.fold (fun (fiberN, sphereN, ellipsoidN) lo ->
          match lo.Shape with
            | Fiber _ -> fiberN + 1, sphereN, ellipsoidN
            | Sphere _ -> fiberN, sphereN + 1, ellipsoidN
            | SymmetricEllipsoid _ -> fiberN, sphereN, ellipsoidN + 1
        ) (0,0,0)
      | None -> 0, 0, 0 )

  let particleVolumeFraction = lazy (
    let fiberV, sphereV, ellipsoidV =
      match lPO with
        | Some lp ->
          lp.Particles.Values |> Seq.fold (fun (fiberV, sphereV, ellipsoidV) lo ->
            match lo.Shape with
              | Fiber f -> fiberV + f.volume, sphereV, ellipsoidV
              | Sphere s -> fiberV, sphereV + s.Volume, ellipsoidV
              | SymmetricEllipsoid e -> fiberV, sphereV, ellipsoidV + e.Volume
          ) (0.,0.,0.)
        | None -> 0., 0., 0.
    let lbmFluidMass = fluidMass.Value
    if lbmFluidMass = 0.0 then 0.,0.,0.
    else fiberV / lbmFluidMass, sphereV / lbmFluidMass, ellipsoidV / lbmFluidMass )

  let particlesTypeCoGandAxis = lazy (
    match lPO with
      | Some lp ->
        lp.Particles.Values |> Seq.map (fun lo ->
          let shape = match lo.Shape with Fiber _ -> 1 | Sphere _ -> 2 | SymmetricEllipsoid _ -> 3
          let v = lo.Rot.rotateL2G (triple (1,0,0))
          shape, lo.CoG, v ) |> Seq.toArray
      | None -> [||] )

  member o.FractionOfCellsAboveTau tauA = (fractionOfCellsAboveTau [| tauA |]).[0]
  
  member o.MinMaxMeanRhoDiff = minMaxMeanRhoDiff.Value
  member o.FractionOfCellsAboveTauA = fractionOfCellsAboveTauA.Value
  member o.KineticEnergyLO = kineticEnergyLO.Value
  member o.MinMaxMeanShearRate = minMaxMeanShearRate.Value
  member o.MeanTau = meanTau.Value
  member o.MinMaxMeanLbmSpeed = minMaxMeanLbmSpeed.Value
  member o.ParticlesTypeCoGandAxis = particlesTypeCoGandAxis.Value
  member o.ParticleVolumeFraction = particleVolumeFraction.Value
  member o.NumberOfParticles = numberOfParticles.Value
  member o.FluidMass = fluidMass.Value
  
  member o.FluidShapeYZ x =
    let phasesYZ = lt.Par.PA |> GArray3D.trySliceYZ x |> Array2D.removeOption Gas
    let ly, lz = phasesYZ.GetLength 0, phasesYZ.GetLength 1
    let z = [lz - 1 .. -1 .. 0]
    Array.init ly (fun y ->
      z |> List.tryPick (fun z ->
        match phasesYZ.[y,z] with Fluid -> Some (z, 1.) | Interface m -> Some(z, m) | _ -> None ))
    |> Array.map (function
      | Some(z, m) -> (float z - 0.5 + m)
      | None -> 0.0 )

  member o.FluidSpread (start, fn) =
    lt.Par.PA |> GArray3D.foldi (fun x y z (maxX: float) p -> 
      match p with Interface m -> fn (x,y,z) maxX m | _ -> maxX ) start

  member o.FluidVelocityProfile1D (p1: triple, p2: triple, uDirection: triple) =
    let dx = (p2 - p1)
    let length, dxn = dx.norm, dx.normalize
    let posA = Array.linspace (0., length, int(length * 2.5))
    let iA = posA |> Array.map (fun pos ->
      let p = p1 + pos * dxn
      int (round p.X), int (round p.Y), int (round p.Z) )
    let iAd = Array.distinct iA
    iAd |> Array.map (fun xyz ->
      match lt.Par.UxA.tryGet xyz, lt.Par.UyA.tryGet xyz, lt.Par.UzA.tryGet xyz with
        | Some (Some ux), Some (Some uy), Some (Some uz) -> triple (ux, uy, uz) * uDirection.normalize
        | _ -> 0.0 )

  

  
