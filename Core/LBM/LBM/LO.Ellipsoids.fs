﻿module LBM.LO.Ellipsoids

open Microsoft.FSharp.Math
open Utilities.Math
open LBM.Basic
open LBM.LO.Types

let internal nodesEllipsoidDict = System.Collections.Generic.Dictionary()

let ellipsoidToLo (ell: SymmetricEllipsoidType) (cog, angle) (density: float) (gravityAcceleration: triple) (u, omegaYPR) (move, rotate, interaction) marker =
  
  let rec layersCircle nodes xi ri r rEffVol dl =       
    let N, dTheta =
      let circ = 2. * pi * ri
      let N = circ / dl |> ceil |> int
      if N > 2 then N, 2. * pi / float N else 3, 2. * pi / 3.        
    let dV = 
      if ri = r then pi * (rEffVol**2.0 - (ri - 0.5*dl)**2.0) * dl / float N
      else pi * ((ri + 0.5*dl)**2.0 - (ri - 0.5*dl)**2.0) * dl / float N
    let dx = Array.init N (fun i -> triple(xi, ri * sin (float i * dTheta), ri * cos ( float i * dTheta)) )
    let newNodes =
      { LocalDX = Array.append nodes.LocalDX dx
        V = Array.append nodes.V <| Array.create N dV
      }
    if ri - dl > 0.5 * dl then layersCircle newNodes xi (ri - dl) r rEffVol dl
    else
      let dV = pi * (ri - 0.5 * dl)**2.0 * dl
      {newNodes with LocalDX = Array.append newNodes.LocalDX [| triple(xi, 0., 0.) |]; V = Array.append newNodes.V [|dV|] }

  let rec addCircles nodes xi (l, r) dl =
    let bi, rEffVol =
      let m2 = 1. - xi**2.0 / l**2.0
      let m2r1, m2r2 =
        if xi = 0. then
          1., 1. - (0.5 * dl)**2.0 / l**2.0
        else 1. - (xi - 0.5 * dl)**2.0 / l**2.0, 1. - (xi + 0.5 * dl)**2.0 / l**2.0
      sqrt(m2) * r, sqrt( 0.5 * (m2r1 + m2r2)) * r
    if xi = 0. then
      let newNodes = layersCircle {LocalDX = [||]; V = [||]} xi bi bi rEffVol dl
      let nodes = 
        { LocalDX = Array.append nodes.LocalDX newNodes.LocalDX
          V = Array.append nodes.V newNodes.V
        }
      if xi + dl < l then addCircles nodes (xi + dl) (l, r) dl
      else
        let dV =
          let vGen = newNodes.V |> Array.sum
          let vEl = 4./ 3. * pi * r**2.0 * l
          let dV = 0.5 * (vEl - vGen)
          if dV > 0. then dV else failwith "negative dV when generating ellipsoid"
        {newNodes with LocalDX = Array.append nodes.LocalDX [| triple(l, 0., 0.); triple(-l, 0., 0.) |]; V = Array.append nodes.V [|dV; dV|] }
    else
      let addedNodes = layersCircle {LocalDX = [||]; V = [||]} xi bi bi rEffVol dl
      let dx2 = addedNodes.LocalDX |> Array.map (fun dx -> triple (-dx.X, dx.Y, dx.Z))
      let nodes = 
        { LocalDX = Array.append nodes.LocalDX (Array.append addedNodes.LocalDX dx2)
          V = Array.append nodes.V (Array.append addedNodes.V addedNodes.V)
        }
      if xi + dl < l then addCircles nodes (xi + dl) (l, r) dl
      else
        let dV =
          let vGen = nodes.V |> Array.sum
          let vEl = 4./ 3. * pi * r**2.0 * l
          let dV = 0.5 * (vEl - vGen)
          if dV > 0. then dV else failwith "negative dV when generating ellipsoid"
        {nodes with LocalDX = Array.append nodes.LocalDX [| triple(l, 0., 0.); triple(-l, 0., 0.) |]; V = Array.append nodes.V [|dV; dV|] }
  
  let l, r = ell.MainHalfAxis, ell.Radius

  let nodes =
    if l < ell.DLength then failwith "too large spacing of Lagrangian nodes, make dL smaller or ellipsoid larger"
    else
      if nodesEllipsoidDict.ContainsKey (l,r) then nodesEllipsoidDict.[(l,r)]
      else
        let nodes = addCircles {LocalDX = [||]; V = [||]} 0. (l, r) ell.DLength
        nodesEllipsoidDict.Add((l, r), nodes)
        nodes
  let V = 4./ 3. * pi * r**2.0 * l
  let mass = density * V
  let rot = Utilities.Rotation.QuaternionRotation(angle |> Quaternion.ofYawPitchRoll)
  let omegaGlob = omegaYPR |> Quaternion.ofYawPitchRoll |> Quaternion.toAxisAngle |> Quaternion.angularVelocityVectorFromAxisAngle
  LO (Shape = SymmetricEllipsoid ell, CoG = cog, Mass = mass, Density = density,
                    U = u, Favg = Triple.Zero, Tavg = Triple.Zero, OmegaGlob = omegaGlob,
                    I = mass / 5. * triple(r**2. + r**2.0, l**2.0 + r**2.0, l**2.0 + r**2.0), 
                    Rot =rot, GAcc = gravityAcceleration, Nodes = nodes, _Rotate = rotate, _Move = move, 
                    _Interaction = interaction, Marker = marker, Guid = System.Guid.NewGuid())