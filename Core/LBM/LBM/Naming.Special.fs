﻿module LBM.Naming.Special

module ConvergenceFiles =
  
  let shearRate = "##convergenceShearRate.txt"
  let shearStress = "##convergenceShearStress.txt"
  let kineticEnergyLO = "##convergenceKineticEnergyLO.txt"
  let shearStressBC0 = "##convergenceShearStressBC0.txt"
  let shearStressBC1 = "##convergenceShearStressBC1.txt"
  let spreadSpeed = "##convergenceSpreadingSpeed.txt"
  let dragForce = "##convergenceDragForce.txt"

module Honza =
   
  let evaluatedStrain = "Honza | Evaluated strain"
  let evaluatedDistance = "Honza | Evaluated distance"
  let evaluatedTime = "Honza | Evaluated time"
  let initialStrain = "Honza | Initial strain"

  let minD = "Honza | Minimum diameter"
  let maxD = "Honza | Maximum diameter"

  let realDiamater = "Honza | Real diameter"

  module Files =
    let shearRateAndStress = "#shearRateAndStress.txt"
    let averageStressBC0 = "#averageStressBC0.txt"
    let averageStressBC1 = "#averageStressBC1.txt"
    let averageShearRate = "#averageShearRate.txt"
    let shearStressBC0 = "#shearStressBC0.txt"
    let shearStressBC1 = "#shearStressBC1.txt"
    let kineticEnergyLO = "#kineticEnergyLO.txt"
    let forceAndTorqueOnParticle = "#forceAndTorqueOnParicle.txt"
    let dragForce = "#dragForce.txt"
    let lubricationForce = "#lubricationForce.txt"

