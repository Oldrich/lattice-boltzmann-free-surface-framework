﻿module internal LBM.LO.BCPForceInteractions

open LBM
open LBM.Basic
open LBM.LO.Types
open LBM.BC.Types.Internal

open Kit
open Kit.Math
open Kit.Collections

let fiberBC (lt: Lattice, lo: LO, pBCbehavior, bc: BcObject) (f: FiberType, omegaGlob, bcU: triple) =
  let p1, p2 = f.P1P2
  let (h1, normal1), (h2, normal2) =
    let getHN p =
      let h, n = bc.distanceAndNormal p
      h - f.R, n
    getHN p1, getHN p2
  let rp1, rp2 = p1 - lo.CoG, p2 - lo.CoG
  let FiTi =
    let getFT hLub (h, normal: float triple, ai) (p: float triple) =
      if h < hLub then
        let dhij = Triple.dot(normal, (lo.U + (Triple.cross(omegaGlob, ai)) - bcU))
        if dhij < 0. then
          let visc =
            let xyz = p |> Triple.map int
            match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
          let Fij =
            let v = Triple.normalize (ai - (Triple.dot(ai, normal)) * normal)
            let aixajNorm =
              let aixajNorm = Triple.norm (Triple.cross(v, Triple.normalize ai))
              if aixajNorm < 1. / f.AspectRatio then 1. / f.AspectRatio else aixajNorm
            let h' = (1./h - 1./hLub)
            - 12. * visc * pi * dhij * f.R * f.R / aixajNorm * h' * normal
          let Tij = Triple.cross(ai, Fij)
          Fij, Tij
        else Triple.zerof, Triple.zerof
      else Triple.zerof, Triple.zerof
    pBCbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut =
        if (h1 > 0. && h1 < hLub) || (h2 > 0. && h2 < hLub) then 
          let (F1, T1), (F2, T2) =
            getFT hLub (max h1 hCut, normal1, rp1) p1, getFT hLub (max h2 hCut, normal2, rp2) p2
          match sumO with
            | Some (fiSum, tiSum) -> Some (fiSum + F1 + F2, tiSum + T1 + T2)
            | None -> Some (F1 + F2, T1 + T2)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut
        | LubricationFnBc fn ->
          match fn (lo.Shape, bc) with Some (hLub, hCut) -> fnFT hLub hCut | None -> sumO
        | _ -> sumO ) None
  FiTi

let sphereBC (lt: Lattice, lo: LO, pBCbehavior, bc: BcObject) (r, bcU: float triple) = 
  let h, normal, _ = HelpingFunctions.getDistanceNormalDxi_BC lo bc
  let FiTi =
    let fLub (hLub, hCut) = 
      let h1 = max h hCut
      let visc =
        let xyz = lo.CoG + (r + 0.5 * h) * normal |> Triple.map int
        match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
      -6.0 * visc * pi * (Triple.dot(bcU - lo.U, normal)) * (r**2.0 * (10. * r)**2.0) / (r + 10. * r)**2.0 * (1./h1 - 1./hLub)
    pBCbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut=
        if h > 0. && h < hLub then
          let fLub = fLub (hLub, hCut)
          let F, T = -normal * fLub, Triple.zerof
          match sumO with
            | Some (fiSum, tiSum) -> Some (fiSum + F, tiSum + T)
            | None -> Some (F, T)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut
        | LubricationFnBc fn ->
          match fn (lo.Shape, bc) with Some (hLub, hCut) -> fnFT hLub hCut | None -> sumO
        | _ -> sumO ) None
  FiTi

let ellipsoidBC (lt: Lattice, lo: LO, pBCbehavior, bc: BcObject) (l, r, bcU) =
  let hReal, normal, rEff, ai =
    let h, n, dxG = LBM.LO.HelpingFunctions.getDistanceNormalDxi_BC lo bc
    let curvature =
      let pL = dxG |> Versor.rotateG2L lo.Rot
      l**6.0 * r**8.0 / (l**4.0 * r**4.0 + r**4.0 * (r**2.0 - l**2.0) * pL.x**2.0 )**2.0
    h, n, 1. / sqrt curvature, dxG
  let FiTi =
    let fLub (hLub, hCut) =
      let h1 = max hReal hCut
      let visc =
        let xyz = lo.CoG + (r + 0.5 * hReal) * normal |> Triple.map int
        match lt.Par.TauA |> Array3.tryGet xyz with Some (Some tau) -> (tau - 0.5) / 3. | _ -> 0.
      -6.0 * visc * pi * (Triple.dot (bcU - lo.U - Triple.cross(lo.OmegaGlob, ai)) normal) * (rEff**2.0 * (10. * r)**2.0) / (rEff + 10. * r)**2.0 * (1./h1 - 1./hLub)      
    pBCbehavior.forceInteraction |> Seq.fold (fun sumO rule ->
      let fnFT hLub hCut =
        if hReal > 0. && hReal < hLub then
          let F, T = 
            let f = -normal * fLub (hLub, hCut)
            f, Triple.cross(ai, f)
          match sumO with
            | Some (fiSum, tiSum) -> Some (fiSum + F, tiSum + T)
            | None -> Some (F, T)
        else sumO
      match rule with
        | Lubrication (hLub, hCut) -> fnFT hLub hCut
        | LubricationFnBc fn ->
          match fn (lo.Shape, bc) with Some (hLub, hCut) -> fnFT hLub hCut | None -> sumO
        | _ -> sumO ) None
  FiTi

let getForcesIds (lt: Lattice, time, loA: LO [], initLP: LagrangianProblem, bcA: BcObject []) =
  
  let Fsum, Tsum = Array.create loA.Length Triple.zerof, Array.create loA.Length Triple.zerof
  initLP.getPairsBC time false |> Array.iter (fun (i, j, _) ->
    if loA.[i]._Interaction = TwoWay then
      let lo, bc = loA.[i], bcA.[j]
      let data =
        let common = lt, lo, initLP.InteractionsBC, bc
        match lo.Shape, bc.Purpose with
          | Fiber f, BcBounceBackI (u, _) ->
            fiberBC common (f, lo.OmegaGlob, u)
          | Sphere s, BcBounceBackI (u, _) ->
            sphereBC common (s.Radius, u)
          | SymmetricEllipsoid se, BcBounceBackI (u, _) ->
            ellipsoidBC common (se.MainHalfAxis, se.Radius, u)
          | _ -> failwith "Not implemented"
      data |> Option.iter (fun (Fi, Ti) ->
        Fsum.[i] <- Fsum.[i] + Fi
        Tsum.[i] <- Tsum.[i] + Ti ))

  Fsum, Tsum
