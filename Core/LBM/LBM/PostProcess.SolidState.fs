﻿namespace LBM.PostProcess

open System.IO
open Utilities.Math

type SolidState =

  static member getCmodCurve (file: FileInfo) =
    File.ReadAllLines file.FullName
      |> Array.map (fun str -> str.Replace (",", "."))
      |> Array.fold (fun lst line ->
        if line.Length > 0 && System.Char.IsDigit line.[0] then
          let data = line.Split [| ';'; '\t'; ' ' |]
          (0.1 * abs (float data.[2]), float data.[3]) :: lst
        else lst ) []
      |> List.rev
      |> List.foldi (fun i (minX, lst) (x,y) ->
        if i % 1 <> 0 then minX, lst
        elif y > 200. then minX, (x - minX, y) :: lst
        else x, lst ) (0., [])
      |> snd |> List.rev

  static member getCmodPoint (cmodCurve, cmodPoint) =
    let i = cmodCurve |> List.findIndex (fun (x, _) -> x > cmodPoint)
    cmodCurve.[i]
