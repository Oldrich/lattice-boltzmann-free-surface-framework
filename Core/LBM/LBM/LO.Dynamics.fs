﻿module internal LBM.LO.Dynamics

exception Explode

open LBM
open LBM.Basic
open LBM.LO.Types

open Kit
open Kit.Collections
open Kit.Math

open System.Collections.Generic

type internal Derivatives = {
  dCog: float triple []
  dRot: float triple []
  F: ForceVector []
  T: ForceVector []
  dUGlob: float triple []
  dOmegaGlob: float triple [] }

let fluidParticleForces (i, lo: LO, rhoV: _[][], uNbrs: float triple[][]) =

  let fit =
    match lo.Shape with
    | Fiber f when f.D < 0.1 -> 75.7
    | _ -> 1.0

  let dxA = lo.Nodes.LocalDX |> Array.map (fun locDx -> locDx|> Versor.rotateL2G lo.Rot)
  let FfsiA = dxA |> Array.mapi (fun j dx ->
    if rhoV.[i].[j] > 0.0 then
      let uf, uj = uNbrs.[i].[j], lo.U + Triple.cross(lo.OmegaGlob, dx)
      rhoV.[i].[j] * (uj - uf) * fit
    else Triple.zerof )
  let Ffs, Tfs =
    (FfsiA, dxA) ||> Array.fold2 (fun (sumF, sumT) ffsi dx ->
      sumF - ffsi, sumT + Triple.cross(dx, -ffsi)
    ) (Triple.zerof, Triple.zerof)
  Ffs, Tfs, FfsiA

let getForces (loA: LO [], ponorA: float [], lt, time, LP, bcA, rhoV, uNbrs) =
  let Fgravity = loA |> Array.mapi (fun i lo -> (1. - ponorA.[i] / lo.Density ) * lo.GAcc * lo.Mass)
  let FsumFluid, TsumFluid, FfsiA = loA |> Array.mapi (fun i lo -> fluidParticleForces (i, lo, rhoV, uNbrs)) |> Array.unzip3
  let FsumIP, TsumIP = IPForceInteractions.getForces (lt, time, loA, LP)
  let FsumBCP, TsumBCP = BCPForceInteractions.getForcesIds (lt, time, loA, LP, bcA)
  let Fsum = FsumFluid |> Array.mapi (fun i fluidF -> FsumIP.[i] + FsumBCP.[i] + fluidF + Fgravity.[i])
  let Tsum = TsumFluid |> Array.mapi (fun i fluidT -> TsumIP.[i] + TsumBCP.[i] + fluidT)
  (Fsum, Tsum), FfsiA

let getDU (globF: float triple) (lo: LO) = (globF / lo.Mass) .* lo._Move

let getDOmega (globT: float triple) (lo: LO) = 
  let domegaGlob =
    let locT = globT |> Versor.rotateG2L  lo.Rot
    let omegaLocX, omegaLocY, omegaLocZ = lo.OmegaGlob |> Versor.rotateG2L lo.Rot |> Triple.toTuple
    let dx = (lo.I.y - lo.I.z) * omegaLocY * omegaLocZ + locT.x
    let dy = (lo.I.z - lo.I.x) * omegaLocZ * omegaLocX + locT.y
    let dz = (lo.I.x - lo.I.y) * omegaLocX * omegaLocY + locT.z
    triple( dx / lo.I.x, dy / lo.I.y, dz / lo.I.z) |> Versor.rotateL2G lo.Rot
  domegaGlob .* lo._Rotate

let getDerivatives (loA: LO [], ponorA, lt, time, LP, bcA, rhoV, uNbrs) =
  let (F, T), FfsiA = getForces (loA, ponorA, lt, time, LP, bcA, rhoV, uNbrs)
  let dUGlob = loA |> Array.mapi (fun i lo -> getDU F.[i] lo )
  let dOmegaGlob = loA |> Array.mapi (fun i lo -> getDOmega T.[i] lo )
  let dCog, dRot = loA |> Array.map (fun lo -> lo.U), loA |> Array.map (fun lo -> lo.OmegaGlob)
  let derivatives = {dCog = dCog; dRot = dRot; F = F; T = T; dUGlob = dUGlob; dOmegaGlob = dOmegaGlob}
  derivatives, FfsiA

let updateState (dt: float, constA: _ list, derA: Derivatives List, clearFT, initLOA: LO []) =
  if dt > 0. then
    let cdt = constA |> List.map (fun c -> c * dt)
    let fn fn1 (fn2: _->_-> float triple) = initLOA |> Array.mapi (fun i lo -> fn1 lo + List.sumByi (fun j cdt -> cdt * fn2 j i) cdt )
    let newCog = fn (fun lo -> lo.CoG) (fun i j -> derA.[i].dCog.[j])
    let newRot = initLOA |> Array.mapi (fun i lo ->
      let drotQuat = cdt |> List.mapi (fun j cdt -> (derA.[j].dRot.[i] * cdt) |> Versor.ofAngularVelocityVector |> Quaternion.normalize )
      let rot = drotQuat |> List.fold (fun state drotQuat -> drotQuat * state) lo.Rot
      rot )
    let newU = fn (fun lo -> lo.U) (fun i j -> derA.[i].dUGlob.[j])
    let newOmega = fn (fun lo -> lo.OmegaGlob) (fun i j -> derA.[i].dOmegaGlob.[j])
    let newObjects = initLOA |> Array.mapi (fun i lo ->
      let dF = List.sumByi (fun j cdt -> cdt * derA.[j].F.[i]) cdt
      let dT = List.sumByi (fun j cdt -> cdt * derA.[j].T.[i]) cdt
      let F = if clearFT then dF else lo.Favg + dF
      let T = if clearFT then dT else lo.Tavg + dT
      lo.copy (coG = newCog.[i], rot = newRot.[i], u = newU.[i], omegaGlob = newOmega.[i], f = F, t = T) )
    newObjects
  else initLOA