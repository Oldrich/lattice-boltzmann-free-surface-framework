﻿namespace LBM.BC

open System.Collections.Generic

open LBM
open LBM.Basic
open LBM.BC.Types
open LBM.BC.Types.Internal

open Kit
open Kit.IO
open Kit.Math
open Kit.Collections

module Internal =

  let gasToInt(bytePhase: byte array3, lt: Lattice) =    
    bytePhase |> Array3.mapi (fun xyz phase ->
      if phase = 0uy then
        let existsFluid = lt.Consts.ADimA |> Array.exists (fun i ->
          let xdx = lt.xdx (xyz, i)
          if bytePhase |> Array3.existsIndex xdx then bytePhase.[xdx] = 200uy else false )
        if existsFluid then 1uy else 0uy
      else phase )

  let fluidToInt(bytePhase: byte array3, lt: Lattice) =
    bytePhase |> Array3.mapi (fun xyz phase ->
      if phase = 200uy then
        let existsGas = lt.Consts.ADimA |> Array.exists (fun i ->
          let xdx = lt.xdx (xyz, i)
          if bytePhase |> Array3.existsIndex xdx then bytePhase.[xdx] = 0uy else false )
        if existsGas then 199uy else 200uy
      else phase )

  let rec completeFSI (bytePhase: byte array3, dM: float, mass: float, lt: Lattice) =
    if dM < -0.1 then
      let npA = fluidToInt(bytePhase, lt)
      let nI = npA |> Array3.sumBy (fun p -> if p = 199uy then 1 else 0) |> float
      if dM + nI < 0.0 then
        let newIsFluid = npA |> Array3.map (fun npa -> if npa = 199uy then 0uy else npa)
        completeFSI(newIsFluid, dM + nI, mass, lt)
      else
        let newMass = byte (200.0 + 200.0 * dM / float nI)
        npA |> Array3.map (fun npa -> if npa = 199uy then newMass else npa)
    elif dM > 0.1 then
      let npA = gasToInt(bytePhase, lt)
      let nI = npA |> Array3.sumBy (fun p -> if p = 1uy then 1 else 0) |> float
      if dM - nI > 0.0 then
        let newIsFluid = npA |> Array3.map (fun npa -> if npa = 1uy then 200uy else npa)
        completeFSI(newIsFluid, dM - nI, mass, lt)
      else
        let newMass = byte (200.0 * dM / float nI)
        npA |> Array3.map (fun npa -> if npa = 1uy then newMass else npa)
    else
      let newMass = max (byte (200.0 * mass)) 1uy
      bytePhase |> Array3.mapi (fun xyz phase ->
        if phase = 0uy then
          let existsFluid = lt.Consts.ADimA |> Array.exists (fun i ->
            let xdx = lt.xdx (xyz, i)
            if bytePhase |> Array3.existsIndex xdx then bytePhase.[xdx] = 200uy else false )
          if existsFluid then newMass else 0uy
        else phase )

  let completeFS (volume, bytePhase, lt: Lattice) =
    let dm, mass =
      let volume = defaultArg volume (BcFixedI 0.)
      match volume with
      | BcAnalytical analyticalVolume ->
        let totalGenerated =
          let sum = bytePhase |> Array3.sumBy (fun v -> if v < 201uy then float v else 0.0)
          float sum / 200.
        analyticalVolume - totalGenerated, 0.
      | BcFixedI mass -> 0., mass
    completeFSI(bytePhase, dm, mass, lt)

  let addInterfaceToWallsFn(addInterfaceToWalls, extPA: Phase array3, lt: Lattice) =
    match addInterfaceToWalls with
    | Some true ->
      extPA |> Array3.mapi (fun xyz phase ->
        let isAroundWall (xyz: int triple) =
          lt.Consts.ADimA |> Array.exists (fun i ->
            let xdx = lt.xdx (xyz, i)
            match extPA |> Array3.tryGet xdx with
              | Some (BoundaryCondition _) -> true
              | _ -> false
          )
        match phase with
          | Fluid when isAroundWall(xyz) -> Interface 0.999
          | _ -> phase
      )
    | _ -> extPA

  let bytePhaseAddBC(bytePhase: byte array3, bcObjects: BcObject []) =
    bytePhase |> Array3.mapi (fun xyz phase ->
      let ido = bcObjects |> Array.foldi (fun id old bc ->
        match bc.Purpose with
        | BcBounceBackI _ ->
          let dist, _ = bc.distanceAndNormal(xyz |> Triple.map float)
          if dist < 0.0 then Some id else old
        | _ -> old ) None
      match ido with
      | Some id -> 201uy + byte id
      | None -> phase
    )

type BcProblem (lt: LBM.Lattice, bcObjects: BcObject seq, ?getPhaseGlob, ?volume: BcVolume, ?addInterfaceToWalls) =

  let getPhase = defaultArg getPhaseGlob (fun _ -> BCFluid)

  let bcObjects = bcObjects |> Seq.toArray

  let bcForces = Array.init bcObjects.Length (fun _ -> triple(0.,0.,0.))

  let bytePhase =
    let bytePhase = Array3.init lt.LengthA_Glob (fun gxyz ->
      match getPhase gxyz with BCFluid -> 200uy | BCGas -> 0uy )
    let bytePhaseBC = Internal.bytePhaseAddBC(bytePhase, bcObjects)
    Internal.completeFS(volume, bytePhaseBC, lt)

  let extLocLengthA = Array2D.init (lt.NodesX_Loc + 2) (lt.NodesY_Loc + 2) (fun x y ->
    let x = if x = 0 then 0 elif x = lt.NodesX_Loc + 1 then lt.NodesX_Loc - 1 else x - 1
    let y = if y = 0 then 0 elif y = lt.NodesY_Loc + 1 then lt.NodesY_Loc - 1 else y - 1
    lt.LengthA_Loc.[x,y] + 2 )

  let extPM' = Array3.init extLocLengthA (fun xyz ->
    let gxyz =
      let gxyz = lt.xyzLoc2Glob (xyz - 1)
      match lt.tryCheckGlobalPeriodic gxyz with Some v -> v | _ -> gxyz
    if bytePhase |> Array3.existsIndex gxyz then
      if bytePhase.[gxyz] = 0uy then Gas
      elif bytePhase.[gxyz] = 200uy then Fluid
      elif bytePhase.[gxyz] < 200uy then Interface (float bytePhase.[gxyz] / 200.0)
      else BoundaryCondition (int bytePhase.[gxyz] - 201)
    else Gas
  )

  let extPM = Internal.addInterfaceToWallsFn(addInterfaceToWalls, extPM', lt)

  let pMinit = Array3.init lt.LengthA_Loc (fun xyz -> extPM.[xyz.x + 1, xyz.y + 1, xyz.z + 1] )
  
  let symmetryD =
    let symD = Dictionary()
    extPM |> Array3.iteri (fun xyze phase ->
      match phase with
        | Fluid | Gas | Interface _ ->
          let xyz = xyze - 1
          for i = 1 to lt.Consts.ADim - 1 do
            if symD.ContainsKey (xyz, i) = false then
              let xyz_de = lt.x_dx (xyze, i)
              match extPM |> Array3.tryGet xyz_de with
                | Some (BoundaryCondition id) ->
                  match bcObjects.[id].Purpose with
                    | BcBounceBackI _ ->
                      let xyzs, is = bcObjects.[id].getSymmetricCoordAndIndex xyz (xyz_de - 1) i
                      match extPM |> Array3.tryGet (xyzs + 1) with
                        | Some (BoundaryCondition _) -> ()
                        | Some _ when xyzs <> xyz ->
                          let xyzdxes = lt.xdx (xyzs + 1, is)
                          match extPM |> Array3.tryGet xyzdxes with
                            | Some (BoundaryCondition id2) when id = id2 ->
                              let xyzss, iss = bcObjects.[id].getSymmetricCoordAndIndex xyzs (xyzdxes-1) lt.Consts.Opposite.[is]
                              if xyz = xyzss && i = lt.Consts.Opposite.[iss] then
                                symD.Add ((xyz, i),(xyzs, is))
                                symD.Add ((xyzs, lt.Consts.Opposite.[is]),(xyz, lt.Consts.Opposite.[i]))
                            | _ -> ()   
                        | _ -> ()                  
                    | _ -> ()
                | _ -> ()
        | _ -> () )
    symD

  let fModMapList =
    let fModMaps = List.init bcObjects.Length (fun _ -> Dictionary())
    extPM |> Array3.iteri (fun xyz phase ->
      match phase with
        | BoundaryCondition id ->
          let u =
            match bcObjects.[id].Purpose with
              | BcBounceBackI (vel, _) -> vel
              | _ -> failwith "Error in fModMapList"
          for i = 0 to lt.Consts.ADim - 1 do
            let xdx = lt.xdx (xyz - 1, i)
            let fMod = 6. * lt.Consts.W.[i] * (Triple.dot(u, lt.Consts.Cf.[i]))
            fModMaps.[id].[(xdx,i)] <- fMod
        | _ -> () )
    fModMaps

  let lbmMapper = LBMMap()
  let _modifyLbmMapper = bcObjects |> Array.iter (fun bc ->
    match bc.Purpose with BcMapLBMI fn -> fn lbmMapper | _ -> () )

  member o.BcForces: float triple [] = bcForces

  member o.Objects = bcObjects
 
  member o.LocInitPhaseA = pMinit

  member internal o.getFmodXsIs (id, (xyz, i as key)) =
    let fMod = if fModMapList.[id].ContainsKey key then fModMapList.[id].[key] else 0.
    let xs,is = 
      if symmetryD.ContainsKey key then symmetryD.[key]
      else xyz, lt.Consts.Opposite.[i]
    fMod, xs, is

  member internal o.mapLBM lxyz (fs, rho, u) =
    let b, fn = lbmMapper.TryGetValue lxyz
    if b then fn (lxyz, fs) else fs, rho, u
  
  /// 0 = Gas | 1 - 199 = Interface | 200 = Fluid | 201 - 255 = BC
  member o.bytePhaseG = bytePhase

  member o.ToZaml() = Naming.Report.BC.boundaryConditions, bcObjects |> Array.map (fun bc -> bc.Description ) |> seq |> ZamlSeq
      