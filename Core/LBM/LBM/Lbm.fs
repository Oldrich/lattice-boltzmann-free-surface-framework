﻿module internal LBM.Lbm

open Basic
open Utilities.Math
open LBM.BC.Types.Internal
open LBM.BC.Types
open System.Collections.Generic

module L = 
  let inline deviatoricStressTensorInvariantAndShearRate (fNeq, tau, lt: Lattice) =
    let fcxcx_D, fcycy_D, fczcz_D, fcxcy, fcxcz, fcycz =
      fNeq |> Utilities.Math.Array.foldi (fun i (fcxcx_D, fcycy_D, fczcz_D, fcxcy, fcxcz, fcycz) fi ->
        let cc, cl2_dim = lt.Consts.CC.[i], lt.Consts.Cl2.[i] / lt.Consts.Dimf
        fcxcx_D + fi * (cc.[0, 0] - cl2_dim),
        fcycy_D + fi * (cc.[1, 1] - cl2_dim),
        fczcz_D + fi * (cc.[2, 2] - cl2_dim),
        fcxcy + fi * cc.[0, 1],
        fcxcz + fi * cc.[0, 2],
        fcycz + fi * cc.[1, 2]
      ) (0.,0.,0.,0.,0.,0.)
    let stressInv =  (1. - 0.5 / tau) * sqrt (0.5 * (fcxcx_D**2. + fcycy_D**2. + fczcz_D**2. + 2.*fcxcy**2. + 2.*fcxcz**2. + 2.*fcycz**2. ))
    let shearRate = - 1.5 / tau * triple(fcycz, fcxcz, fcxcy)
    stressInv, shearRate

  let inline collide (phase, f, rho, u, tau, g: triple, xyz, time, lt: Lattice, stressShearCallback, getNewTau, getForce) =
    let fEq = lt.Consts.getfEqA (rho, u)
    let fNeq = f |> Array.mapi (fun i f-> f - fEq.[i])
    let devStressInv, shearRate = deviatoricStressTensorInvariantAndShearRate (fNeq, tau, lt)
    stressShearCallback (time, xyz, devStressInv, shearRate)
    let tauN =
      match phase with
        | Interface _ -> tau
        | _ -> getNewTau (time, xyz, (2. * sqrt (shearRate * shearRate)), devStressInv)
    lt.Consts.collide (f, fNeq, tauN, getForce (shearRate, tau) + g), tauN

  let inline add xyz dMass (lt: Lattice) =
    if lt.MassExchange.ContainsKey xyz then lt.MassExchange.[xyz] <- lt.MassExchange.[xyz] + dMass
    else lt.MassExchange.Add (xyz, dMass)

  let inline bounceBack (i, id, interfaceMO, xyz, f: _ [], computeBcForces, lt: Lattice, bcP: BC.BcProblem) =
    match bcP.Objects.[id].Purpose with
      | BcBounceBackI (_, slip) ->            
        let fMod, xs, is = bcP.getFmodXsIs (id, (xyz, i))
        let fop = lt.Par.RhoA.[xyz].Value * fMod + f.[lt.Consts.Opposite.[i]]
        let slipCoef =
          match slip with
            | SlipCoef slipCoef -> slipCoef
            | SlipLength slipLength ->
              let meanVisc =
                let meanTau =
                  let tau1 = lt.Par.TauA.[xyz].Value
                  match lt.Par.tryGetTau xs with Some (Some tau2) -> 0.5 * (tau1 + tau2) | _ -> tau1
                (meanTau - 0.5) / 3.0
              slipLength / (3.0 * meanVisc + slipLength)
        if computeBcForces then
          let rho = lt.Par.RhoA.[xyz].Value
          let F = - f.[lt.Consts.Opposite.[i]] * lt.Consts.Cf.[i] - (f.[lt.Consts.Opposite.[i]] + fMod * rho) * ((1. - slipCoef) * lt.Consts.Cf.[i] - slipCoef * lt.Consts.Cf.[is])
          let oldF = bcP.BcForces.[id]
          bcP.BcForces.[id] <- oldF + F
        match lt.Par.getPhase xs, lt.Par.getfA xs with
          | Fluid, Some fxs -> 
            interfaceMO |> Option.iter (fun _ -> 
              add xyz (fxs.[is] * slipCoef) lt
              if lt.Par.PA.exists xs = false then add xyz (-fop * slipCoef) lt )
            fxs.[is] * slipCoef + fop * (1. - slipCoef)
          | Interface m1, Some fxs ->
            match interfaceMO with
              | Some m0 ->
                let mf = 0.5 * (m0 + m1) * fxs.[is] * slipCoef
                add xyz mf lt; add xs -mf lt
                if lt.Par.PA.exists xs = false then add xyz (-0.5 * (m0 + m1) * fop * slipCoef) lt
              | None -> add xs -(fxs.[is] * slipCoef) lt
            fxs.[is] * slipCoef + fop * (1. - slipCoef)
          | Gas, None -> fop
          | BoundaryCondition _, None -> fop
          | _ as phase, fxs-> failwithf "error in symmetry: phase %A with fxs %A at xs = %A" phase fxs xs
      | _ -> failwith "unsupported BC in streaming"

  let inline stream (xyz, f: float [], computeBcForces, lt: Lattice, bcP: BC.BcProblem) =
    let newF = Array.zeroCreate f.Length
    newF.[0] <- f.[0]
    for i = 1 to f.Length - 1 do
      let x_dx = lt.x_dx (xyz,i)
      newF.[i] <-
        match lt.Par.PA.[xyz], lt.Par.getPhase x_dx, lt.Par.getfA x_dx with
          | Interface _, Fluid, Some fx_dx -> 
            add xyz fx_dx.[i] lt
            if lt.Par.PA.exists x_dx = false then add xyz -lt.Par.FA.[xyz].Value.[lt.Consts.Opposite.[i]] lt
            fx_dx.[i]
          | Fluid, Interface _, Some fx_dx -> add x_dx -fx_dx.[i] lt; fx_dx.[i]
          | Fluid, Fluid, Some fx_dx -> fx_dx.[i]
        
          | Interface m0, Interface m1, Some fx_dx ->
            let m = 0.5 * (m0 + m1)
            let mf = m * fx_dx.[i]
            add xyz mf lt; add x_dx -mf lt
            if lt.Par.PA.exists x_dx = false then add xyz (-m * lt.Par.FA.[xyz].Value.[lt.Consts.Opposite.[i]]) lt
            if FreeSurface.fFromAir xyz lt lt.Consts.Cf.[i] = false then fx_dx.[i]
            else FreeSurface.replaceStream xyz i lt 

          | Interface _, Gas, None -> FreeSurface.replaceStream xyz i lt

          | Interface m0, BoundaryCondition id, None -> bounceBack (i, id, Some m0, xyz, f, computeBcForces, lt, bcP)
          | Fluid, BoundaryCondition id, None -> bounceBack (i, id, None, xyz, f, computeBcForces, lt, bcP)
          
          | _ as pA, pAx_dx, _ -> 
            failwithf "Streaming to phase %A at pos xyz = %A from phase %A at pos x_dx = %A at direction = %d" pA xyz pAx_dx x_dx i
    newF

let inline update(lt: Lattice, bcP: BC.BcProblem, getNewTau, getForce, computeBcForces, callbacks: IDictionary<string, obj> option) (time: int) =

  let callbacks = defaultArg callbacks (Dictionary () :> IDictionary<_,_>)
  let stressShearCallback = if callbacks.ContainsKey("stressShear") then unbox callbacks.["stressShear"] else fun _ -> ()

  let computeBcForces = match computeBcForces with Some computeBcForces -> computeBcForces time | None -> false
  for i in 0 .. bcP.BcForces.Length - 1 do bcP.BcForces.[i] <- Triple.Zero

  lt.Timer.tick 1

  lt.Timer.tick 2

  lt.MassExchange.Clear()

  let newTauA =
    let newfAc, newTauA = // after collision
      GArray3D.init2 lt.LengthA_Loc (fun x y z ->
        let xyz = x, y, z
        if Option.isSome lt.Par.RhoA.[xyz] then
          match lt.Par.FA.[xyz], lt.Par.RhoA.[xyz], lt.Par.UxA.[xyz], lt.Par.UyA.[xyz], lt.Par.UzA.[xyz], lt.Par.TauA.[xyz] with
            | Some f, Some rho, Some ux, Some uy, Some uz, Some tau ->
              let g = (defaultArg (lt.Par.getForce_LO xyz) Triple.Zero)
              let newf, newTau = L.collide (lt.Par.PA.[xyz], f, rho, triple(ux,uy,uz), tau, g, xyz, time, lt, stressShearCallback, getNewTau, getForce)
              Some newf, Some newTau
            | _ -> None, None
        else None, None )
    lt.Par.setfA newfAc
    lt.Par.propagateLBM ()
    newTauA

  lt.Timer.tockPrintTick 2 "Lbm.collision"

  let newfA, newRho, newUx, newUy, newUz = // after streaming
    GArray3D.init5 lt.LengthA_Loc (fun x y z ->
      let xyz = x, y, z
      match lt.Par.FA.[xyz] with
        | Some newfc ->
          let fs, rho, u =
            let fs = L.stream (xyz, newfc, computeBcForces, lt, bcP)
            let rho, u = lt.Consts.getRhoU fs
            (fs, rho, u) |> bcP.mapLBM xyz
          Some fs, Some rho, Some u.[0], Some u.[1], Some u.[2]
        | None -> None, None, None, None, None )

  lt.Timer.tockPrintTick 2 "Lbm.streaming"
            
  lt.Par.setLBMA6 (newfA, newRho, newUx, newUy, newUz, newTauA)

  lt.Timer.tockPrint 2 "Lbm.setLBMA6"
  lt.Timer.tockPrint 1 "Lbm.update"