﻿namespace LBM

open LBM
open LBM.Naming
open LBM.Basic
open Utilities.IO

type UnitsToReal = {
  Time: float -> float<s>
  Energy: float -> float<J>
  Length: float -> float<m>
  Speed: float -> float<m/s>
  AngularSpeed: float -> float</s>
  Force: float -> float<N>
  ForcePerUnitLength: float -> float<N/m>
  Density: float -> float<kg/m^3>
  Volume: float -> float<m^3>
  Stress: float ->float<Pa>
  KinematicViscosity: float -> float<m^2/s>
  DynamicViscosity: float -> float<Pa s>
  Mass: float -> float<kg>
  Acceleration: float -> float<m/s^2>
  ShearRate: float -> float<1/s>
}

type UnitsToLBM = {
  Time: float<s> -> float
  Energy: float<J> -> float
  Length: float<m> -> float
  Speed: float<m/s> -> float
  AngularSpeed: float</s> -> float
  Force: float<N> -> float
  ForcePerUnitLength: float<N/m> -> float
  Density: float<kg/m^3> -> float 
  Volume: float<m^3> -> float
  Stress: float<Pa> -> float
  KinematicViscosity: float<m^2/s> -> float
  DynamicViscosity: float<Pa s> -> float
  Mass: float<kg> -> float
  Acceleration: float<m/s^2> -> float
  ShearRate: float<1/s> -> float
}

type Units (realLength: float<m>, realDynamicViscosity: float<Pa*s>, realDensity: float<kg/m^3> , lbmLength: float, lbmViscosity: float ) =  
   
  member o.scalingL = lbmLength/realLength
  member o.scalingL2 = o.scalingL*o.scalingL
  member o.scalingL3 = o.scalingL2*o.scalingL
  member o.scalingKg = o.scalingL3 / realDensity
  /// to LBM
  member o.scalingT: float</s> = realDynamicViscosity / realDensity / lbmViscosity * o.scalingL2
  member o.scalingT2 = o.scalingT*o.scalingT

  member o.toLbm : UnitsToLBM = {
    Time = (fun y -> y*o.scalingT)
    Energy = (fun y -> y/o.scalingT2*(o.scalingKg*o.scalingL2))
    Length = (fun y -> y*o.scalingL)
    Speed = (fun y -> y*o.scalingL/o.scalingT)
    AngularSpeed = (fun y -> y/o.scalingT)
    Force = (fun y -> y * (o.scalingKg * o.scalingL) / o.scalingT2)
    ForcePerUnitLength = (fun y -> y * o.scalingKg / o.scalingT2)
    Density = (fun y -> y/realDensity)
    Volume = (fun y -> y * o.scalingL3)
    Stress = (fun y -> y*o.scalingKg/(o.scalingT2*o.scalingL))
    KinematicViscosity = (fun y -> y / o.scalingT * o.scalingL2)
    DynamicViscosity = (fun y -> y / o.scalingT / o.scalingL * o.scalingKg)
    Mass = (fun y -> y * o.scalingKg)
    Acceleration = (fun y -> y*o.scalingL/o.scalingT2)
    ShearRate = (fun y -> y/o.scalingT)
  }
  member o.toReal : UnitsToReal = {
    Time = (fun y -> y/o.scalingT)
    Energy = (fun y -> y*o.scalingT2/(o.scalingKg*o.scalingL2))
    Length = (fun y -> y/o.scalingL)
    Speed = (fun y -> y/o.scalingL*o.scalingT)
    AngularSpeed = (fun y -> y * o.scalingT)
    Force = (fun y -> y/ (o.scalingKg * o.scalingL) * o.scalingT2)
    ForcePerUnitLength = (fun y -> y / o.scalingKg * o.scalingT2)
    Density = (fun y -> y/o.scalingKg*o.scalingL3)
    Volume = (fun y -> y / o.scalingL3)
    Stress = (fun y -> y * o.scalingT2 * o.scalingL / o.scalingKg)
    KinematicViscosity = (fun y -> y *o.scalingT / o.scalingL2)
    DynamicViscosity = (fun y -> y * o.scalingT * o.scalingL / o.scalingKg)
    Mass = (fun y -> y/o.scalingKg)
    Acceleration = (fun y -> y / o.scalingL * o.scalingT2)
    ShearRate = (fun y -> y * o.scalingT)
  }

  static member ofZaml (z: ZamlType) =
    let getF str =
      match z.find [Report.Units.unitsConversion; str] with
        | ZamlS s -> 1. / (Utilities.String.findAllFloats s).[0]
        | _ -> failwith "Not a string"
    Units (1.<m> * getF Report.Units.scalingLength, 1.<Pa s> * getF Report.Units.scalingDynamicViscosity, 1.<kg/m^3> * getF Report.Units.scalingDensity, 1., 1.)

  member o.ToZaml =
    Report.Units.unitsConversion, ZamlSeq [
      Report.Units.realRefLength, ZamlS <| sprintf "%g m" (float realLength)
      Report.Units.realRefDynamicViscosity, ZamlS <| sprintf "%g Pa.s" (float realDynamicViscosity)
      Report.Units.realRefDensity, ZamlS <| sprintf "%g kg/m$^3$" (float realDensity)
      Report.Units.lbmRefLength, ZamlS <| sprintf "%g LU" lbmLength
      Report.Units.lbmRefViscosity, ZamlS <| sprintf "%g LU" lbmViscosity
      Report.Units.lbmRefDensity,  ZamlS "1.0 LU"
      Report.Units.scalingLength, ZamlS <| sprintf "%g LU / m" (float o.scalingL)
      Report.Units.scalingTime, ZamlS <| sprintf "%g LU / s" (float o.scalingT)
      Report.Units.scalingSpeed, ZamlS <| sprintf "%g LU / (m / s)" (1.<m/s> |> o.toLbm.Speed)
      Report.Units.scalingMass, ZamlS <| sprintf "%g LU / kg" (float o.scalingKg)
      Report.Units.scalingForce, ZamlS <| sprintf "%g LU / N" (1.<N> |> o.toLbm.Force)
      Report.Units.scalingStress, ZamlS <| sprintf "%g LU / Pa" (1.<Pa> |> o.toLbm.Stress)
      Report.Units.scalingEnergy, ZamlS <| sprintf "%g LU / J" (1.<J> |> o.toLbm.Energy)
      Report.Units.scalingAngularVelocity, ZamlS <| sprintf "%g LU / (rad / s)" (1.<1/s> |> o.toLbm.AngularSpeed)
      Report.Units.scalingVolume, ZamlS <| sprintf "%g LU / m$^3$" (1.<m^3> |> o.toLbm.Volume) 
      Report.Units.scalingDensity, ZamlS <| sprintf "%g LU / (kg / m$^3$)" (1.<kg/m^3> |> o.toLbm.Density)
      Report.Units.scalingKinematicViscosity, ZamlS <| sprintf "%g LU / (m$^2$/s)" (1.<m^2/s> |> o.toLbm.KinematicViscosity)
      Report.Units.scalingDynamicViscosity, ZamlS <| sprintf "%g LU / (Pa.s)" (1.<Pa s> |> o.toLbm.DynamicViscosity)
      Report.Units.scalingAcceleration, ZamlS <| sprintf "%g LU / (m / s$^2$)" (1.<m/s^2> |> o.toLbm.Acceleration)
      Report.Units.scalingShearRate, ZamlS <| sprintf "%g LU / (1 / s)" (1.<1/s> |> o.toLbm.ShearRate) ]
