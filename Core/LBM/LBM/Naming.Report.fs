﻿module LBM.Naming.Report

let none = "None"

module Lattice =
  let lattice = "Lattice"
  let globDim = "Global dimensions (number of nodes)"
  let locDim = "Local dimensions (number of nodes)"
  let origin = "Origin"
  let domainIndex = "Domain index"
  let latticeType = "Lattice type"
  let isInCompressible = "Is incompressible"

module BC =
  let boundaryConditions = "Boundary conditions"
  let planeOfInfDepth = "Plane of infinite depth"
  let planeOfUnitDepth = "Plane of unit depth"
  let polygon = "Polygon"
  let hollowCylinder = "Hollow cylinder"
  let cylinder = "Cylinder"
  let purpose = "Purpose"
  let purposeType = "Purpose type"
  let pointOnPlane = "Point on plane"
  let normal = "Normal"
  let thickness = "Thickness"

  let bounceBack = "Bounce-back"
  let presVel = "Prescribed velocity"
  let presVelFn = "Prescribed velocity by a function"
  let presRho = "Prescribed density"
  let doNothing = "Do nothing"
  let inflowPresVelRho = "Inflow with prescribed velocity and density"
  let inflowPresVelFnRho = "Inflow with prescribed velocity by a function and density"
  let velocity = "Velocity"
  let density = "Density"
  let slipCoef = "Slip coefficient"
  let slipLength = "Slip length"
  
  let vertices = "Vertices"
  let vertex i = sprintf "Vertex %d" i
  let point i = sprintf "Point %d" i
  let radius = "Radius"

module LO =
  let shape = "Shape"

  let sphere = "Sphere"
  let radius = "Radius"
  let diameter = "Diameter"

  let fiber = "Fiber"
  let length = "Length"
  let dLength = "Length discretization (dl)"
  let fiberAspectRatio = "Fiber aspect ratio"

  let symEllipsoid = "Symmetric ellipsoid"
  let mainHalfAxis = "Main half-axis"

module LP =
  let LagrangianProblem = "Lagrangian problem"
  let lagObjects = "Lagrangian objects"

  let interparticleInteraction = "Inter-particle interactions"
  let partilceBoundaryInteraction = "Particle-boundary interactions"

  let forceInteraction = "Force interaction"
  let lubrication = "Lubrication"
  let lubricationDistance = "Lubrication distance"
  let lbmCutOffDistance = "LBM Cut-off distance"

  let coulombFriction = "Coulomb friction"
  let dynamicFrictionCoef = "Dynamic friction coefficient"
  let staticFrictionCoef = "Static friction coefficient"

  let coeffOfRestitution = "Coefficient of restitution"

  let controls = "Controls"

  let twoWayIntegrationType = "Integration type of two way coupled interaction"

  let integrationControls = "Integration controls"
  let maxStepSize = "Maximum size of step"
  let maxErrors = "Maximum errors"
  let CoG = "Center of gravity"
  let rotation = "Rotation"
  let speed = "Speed"
  let rotationSpeed = "Rotation speed"

  let twoWayDiracDelta = "Dirac delta function type for two way coupled interaction"
  let tractkingDiracDelta = "Dirac delta function type for tracking particles"

  let kind i = sprintf "Kind %d" i
  let totalNumOfObjs = "Total number of objects"
  let spheres = "Spheres"
  let fibers = "Fibers"
  let symEllipsoids = "Symmetric ellipsoids"

module Units =
  let unitsConversion = "Units conversion"
  let realRefLength = "Real reference length" 
  let realRefDynamicViscosity = "Real reference dynamic viscosity"
  let realRefDensity = "Real reference density"
  let lbmRefLength = "LBM reference length"
  let lbmRefDensity = "LBM reference density"
  let lbmRefViscosity = "LBM reference viscosity"
  let scalingLength = "Scaling of length (to Lbm)"
  let scalingTime = "Scaling of time (to Lbm)"
  let scalingSpeed = "Scaling of speed (to Lbm)"
  let scalingMass = "Scaling of mass (to Lbm)"
  let scalingForce = "Scaling of force (to Lbm)"
  let scalingStress = "Scaling of stress (to Lbm)"
  let scalingEnergy = "Scaling of energy (to Lbm)"
  let scalingAngularVelocity = "Scaling of angular velocity (to Lbm)"
  let scalingVolume = "Scaling of volume (to Lbm)"
  let scalingDensity = "Scaling of density (to Lbm)"
  let scalingKinematicViscosity = "Scaling of kinematic viscosity (to Lbm)"
  let scalingDynamicViscosity = "Scaling of dynamic viscosity (to Lbm)"
  let scalingAcceleration = "Scaling of acceleration (to Lbm)"
  let scalingShearRate = "Scaling of shear rate (to Lbm)"

module Fluid =
  let fluidProperties = "Fluid properties"
  let realFluidProp = "Real fluid properties"
  let realYieldStress = "Real yield stress"
  let realViscosity = "Real viscosity"
  let realFluidDensity = "Real fluid density"

  let lbmFluidProp = "LBM fluid properties"
  let lbmYieldStress = "LBM yield stress"
  let lbmViscosity = "LBM viscosity"
  let lbmFluidDensity = "LBM fluid density"
  let realShearRateVsStressList = "Real shear rate versus shear stress list"
  let lbmShearRateVsStressList = "LBM shear rate versus shear stress list"
//
//let flowCharacteristics = "Flow characteristics"
//let particles = "Particles"
//let homog = "Homogeneous"
//let Re = "Reynolds number"
//let Bi = "Bingham number"
//
//let volumeFraction = "Volume Fraction"
//let loading = "Loading"

//let dlNum = "Discretize into number of lengths"
//let dVolume = "Volume discretization (dV)"
//let trackingParticles = "Tracking particles"
//let numberOfSteps = "Number of steps"