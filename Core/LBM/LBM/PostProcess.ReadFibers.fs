﻿namespace LBM.PostProcess

open System.IO
open System.Collections.Generic
open Utilities.Math

type ReadFibers =

  static member getFijiLines filterLength path =
    let file = File.ReadAllLines path
    let lst = List()
    for i = 1 to file.Length - 1 do
      let data = file.[i].Split [| '\t' |] |> Array.map float
      if float data.[2] > filterLength then
        let p1 = triple (data.[3], data.[4], data.[5])
        let p2 = triple (data.[6], data.[7], data.[8])
        lst.Add (p1, p2)
    lst.ToArray()

  static member getAvizoLines path =
    File.ReadAllLines path |> Array.fold (fun (lst, lastPoint) line ->
      if line.Length > 0 && System.Char.IsDigit line.[0] then
        let data = line.Split [| ' ' |] |> Array.map (fun v -> float v)
        let newPoint = triple (data.[0], data.[1], data.[2])
        match lastPoint with
          | Some lastPoint -> (lastPoint, newPoint) :: lst, Some newPoint
          | None -> lst, (Some newPoint)
      else lst, None ) ([], None) |> fst |> List.toArray

  static member getAvizoLinesStraight path =
    let out, _, _ =
      File.ReadAllLines path |> Array.fold (fun (lst, firstPoint, lastPoint) line ->
        if line.Length > 0 && System.Char.IsDigit line.[0] then
          let data = line.Split [| ' ' |] |> Array.map (fun v -> float v)
          let newPoint = triple (data.[0], data.[1], data.[2])
          if Option.isNone firstPoint then lst, Some newPoint, None else lst, firstPoint, Some newPoint
        else
          match firstPoint, lastPoint with
              | Some p1, Some p2 -> (p1, p2) :: lst, None, None
              | _ -> lst, None, None
      ) ([], None, None)
    out