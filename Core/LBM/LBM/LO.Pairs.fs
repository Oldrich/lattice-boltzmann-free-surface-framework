﻿namespace LBM.LO.Types

open LBM
open LBM.LO
open LBM.BC.Types.Internal

open System.Collections.Generic

[<AbstractClass>]
type internal _Pairs (nBoxes) =

  let iQ = Array.init nBoxes (fun _ -> List<uint16> ())
  let jQ = Array.init nBoxes (fun _ -> List<uint16> ())

  member o.IA = iQ
  member o.JA = jQ

  member o.getPairs time =
    let id = time % iQ.Length
    iQ.[id], jQ.[id]

  member o.replacePairs (curTime, ijDurA) =
    let whatId = curTime % iQ.Length
    iQ.[whatId] <- List (); jQ.[whatId] <- List ()
    ijDurA |> Array.iter (fun ((i, j), duration) ->
      let newId = (curTime + min duration (nBoxes - 1)) % iQ.Length
      iQ.[newId].Add i; jQ.[newId].Add j )
        
  member o.moveToTheFuture (currentTime, futureTime) =
    let nextId = futureTime % iQ.Length
    for time in currentTime .. futureTime - 1 do
      let curId = time % iQ.Length
      if nextId <> curId then
        iQ.[nextId].AddRange iQ.[curId]
        jQ.[nextId].AddRange jQ.[curId]
        iQ.[curId].Clear(); jQ.[curId].Clear()
  
type internal PairsP (maxSpeed: float, nBoxes, interactionDistance, deactivatePairs) as o =
  inherit _Pairs (nBoxes)

  let pushSym time (loD: IDictionary<int, LO>) =
    let keys, loA = Seq.toArray loD.Keys, Seq.toArray loD.Values
    for i in 0 .. keys.Length - 1 do
      for j = i + 1 to keys.Length - 1 do
        let id =
          let h = HelpingFunctions.getRoughDistance loA.[i] loA.[j] - interactionDistance
          (time + o.getDuration loA.[i] loA.[j] h) % nBoxes
        o.IA.[id].Add (uint16 keys.[i]); o.JA.[id].Add (uint16 keys.[j])

  let push time (oldLo: IDictionary<int, LO>) (newLo: IDictionary<int, LO>) =
    for vi in oldLo do
      for vj in newLo do
        let id =
          let h = HelpingFunctions.getRoughDistance vi.Value vj.Value - interactionDistance
          (time + o.getDuration vi.Value vj.Value h) % nBoxes
        o.IA.[id].Add (uint16 vi.Key); o.JA.[id].Add (uint16 vj.Key)

  member o.getDuration loi loj h = max (min (HelpingFunctions.getTimePP loi loj h maxSpeed) (nBoxes - 1)) 0

  member o.addNewPairs_LoLo (curTime, oldLoD, newLoD) =
    if deactivatePairs = false then
      push curTime oldLoD newLoD
      pushSym curTime newLoD
  member o.addNewPairs_LoGh (curTime, newLoD, newGD) =
    if deactivatePairs = false then
      push curTime newLoD newGD

type internal PairsBC (bcAInit: BcObject [], maxSpeed: float, nBoxes, interactionDistance) as o =
  inherit _Pairs (nBoxes)

  let push time (loD: IDictionary<int, LO>) (bcA: BcObject []) =
    for vi in loD do
      for j = 0 to bcA.Length - 1 do
        match bcA.[j].Purpose with
          | BcBounceBackI _ when bcA.[j].Invisible = false -> 
            let id =
              let h = HelpingFunctions.getDistance_BC vi.Value bcA.[j] - interactionDistance
              (time + o.getDuration vi.Value h) % nBoxes
            o.IA.[id].Add (uint16 vi.Key); o.JA.[id].Add (uint16 j)
          | _ -> ()

  member o.getDuration loi h = max (min (HelpingFunctions.getTimeBC loi h maxSpeed) (nBoxes - 1)) 0
  member o.addNewLOs (curTime: int, newLoD: IDictionary<int, LO>) = push curTime newLoD bcAInit  

  