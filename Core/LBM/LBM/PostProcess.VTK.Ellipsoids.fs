﻿namespace LBM.PostProcess.VTK

open Kitware.VTK
open System.IO
open System.Collections.Generic
open System.Text.RegularExpressions
open Utilities.Math
open LBM.PostProcess

type AnimateEllipsoids (serializedEllsDirPath, toggleDim, ?scale, ?record) =

  let _origin, domSize, boxes: (triple * triple * (int*int*int)) =
    unbox ( Utilities.Serializer.Binary.deSerializeObject (serializedEllsDirPath + @"\_setup") )

  let ellipsoids =
    let files = (DirectoryInfo serializedEllsDirPath).GetFiles "*.ell"
    let sortedFiles = files |> Array.sortBy (fun file -> (Utilities.String.findAllFloats file.Name).[0])
    sortedFiles |> Array.map (fun file -> AnimateEllipsoids.oldDeserializeEllipsoids file.FullName)

  let dimId = ref ((fst ellipsoids.[0]).GetLength toggleDim)
  let boxes0, boxes1, boxes2 = boxes
  let dx = domSize ./ triple (boxes0, boxes1, boxes2)
  let minDx = min (min dx.X dx.Y) dx.Z

  let renderer = vtkRenderer.New()
  let renderWindow = vtkRenderWindow.New()
  let renderWindowInteractor = vtkRenderWindowInteractor.New()
  let widget = vtkOrientationMarkerWidget.New()
  let axes = vtkAxesActor.New()

  let ellsVTK = Array3D.init boxes0 boxes1 boxes2 (fun _ _ _ ->
    let sphere = vtkSphereSource.New()
    sphere.SetPhiResolution 20
    sphere.SetThetaResolution 20
    sphere.SetRadius 0.0

    use fil = new vtkTransformPolyDataFilter ()
    let tr = new vtkTransform ()

    fil.SetTransform tr
    fil.SetInputConnection (sphere.GetOutputPort())
    
    use mapper = vtkPolyDataMapper.New()
    let actor = vtkActor.New()
    mapper.SetInputConnection (fil.GetOutputPort())
    actor.SetMapper mapper

    renderer.AddActor actor
    sphere, tr, actor )

  let resetView vs =
    let v, n =
      match vs with
        | "x"  -> triple (1,0,0), triple (0,0,1)
        | "y"  -> triple (0,1,0), triple (0,0,1)
        | "z"  -> triple (0,0,-1), triple (0,1,0)
        | _ -> failwithf "resetView: not supported = %s" vs
    let fp = triple (renderer.GetActiveCamera().GetFocalPoint())
    let pos = fp - fp .* v
    renderer.GetActiveCamera().SetPosition (pos.X, pos.Y, pos.Z)
    renderer.GetActiveCamera().SetViewUp (n.X, n.Y, n.Z)
    renderer.ResetCamera ()
    renderWindow.Render()

  let update i = 
    let ells, _ = ellipsoids.[i]
    ells |> Array3D.iteri (fun i j k ell ->
      let sphere, tr, actor = ellsVTK.[i,j,k]
      let fn () =
        match ell with
          | Some (evals, evecs) ->
            sphere.SetRadius (0.5 * minDx)
            use mat = vtkMatrix4x4.New ()
            evecs |> Array2D.iteri (fun m n v -> mat.SetElement(m,n,float v) )
            mat.SetElement (3,3,1.)
            if mat.Determinant () < 0. then for i in 0 .. 2 do mat.SetElement (i, 2, - mat.GetElement(i,2))
            tr.SetMatrix mat
            let sc = defaultArg scale 2.
            tr.Scale (sc * float evals.[0], sc * float evals.[1], sc * float evals.[2])
            let minV, maxV = Array.min evals, Array.max evals
            let c0, c1 = 0.5 + 1.5 * float (maxV - minV), 1.5 - 0.7 * float (maxV - minV)
            actor.GetProperty().SetColor (c0,c1,0.)
          | None -> sphere.SetRadius 0.0
      let idi = match toggleDim with 0 -> i | 1 -> j | 2 -> k | _ -> failwithf "toggleDim: not supported dimension = %d" toggleDim
      if !dimId = ells.GetLength toggleDim || idi = !dimId then fn () else sphere.SetRadius 0. )
    renderWindow.Render()

  do
    renderWindowInteractor.SetInteractorStyle (vtkInteractorStyleTrackballCamera.New())
    renderWindow.AddRenderer renderer
    //renderWindow.SetAAFrames 1
    //renderWindow.LineSmoothingOn()
    renderWindow.FullScreenOn()
    renderWindowInteractor.SetRenderWindow renderWindow

    widget.SetOrientationMarker axes
    widget.SetInteractor renderWindowInteractor 
    widget.SetViewport (0.0, 0.0, 0.4, 0.4)
    widget.SetEnabled 1
    widget.InteractiveOff()

    use lightKit = vtkLightKit.New()
    lightKit.SetKeyLightIntensity 1.0
    lightKit.AddLightsToRenderer renderer

    use textActor = vtkTextActor.New ()
    textActor.GetTextProperty().SetFontFamilyToCourier()
    textActor.GetTextProperty().SetFontSize 14
    textActor.SetPosition (0., 20.)
    textActor.SetInput "
Left | Page Up =    Previous time
Right | Page Down = Next time
Up | Home =         Zero time
Down | End =        End time
Space =             Play / pause time
t =                 Toggle visible plane
r =                 Reset view to see all
x | y | z =         Reset rotation to plane ...
q | e | esc =       Exit"
    renderer.AddActor2D textActor

    let recordingText = vtkTextActor.New ()
    record |> Option.iter (fun _ ->
      recordingText.GetTextProperty().SetFontFamilyToCourier()
      recordingText.GetTextProperty().SetFontSize 14
      recordingText.GetTextProperty().SetColor(1.,0.,0.)
      recordingText.SetPosition (0., 0.)
      recordingText.SetInput "Recording"
      recordingText.VisibilityOff()
      renderer.AddActor2D recordingText )

    renderWindowInteractor.Initialize()

    let imageFilter = vtkWindowToImageFilter.New()
    let w = vtkAVIWriter.New()
    imageFilter.SetInput renderWindow
    record |> Option.iter (fun (fileName, frameRate) ->
      w.SetFileName fileName
      w.SetRate frameRate )
    w.SetInputConnection(imageFilter.GetOutputPort())

    let isPlaying, index = ref false, ref (ellipsoids.Length - 1)
    let isRecording = ref false
    renderWindowInteractor.add_TimerEvt (vtkObject.vtkObjectEventHandler (fun a b ->
      if !index = ellipsoids.Length - 1 then
        isRecording := false; recordingText.VisibilityOff(); w.End()
        isPlaying := false; renderWindowInteractor.DestroyTimer () |> ignore
        renderWindow.Render()
      else
        if !isRecording then imageFilter.Modified(); w.Write()
        incr index; update !index ))

    renderWindowInteractor.add_ExitEvt (vtkObject.vtkObjectEventHandler (fun a b -> System.Environment.Exit 0))

    renderWindowInteractor.add_KeyReleaseEvt (vtkObject.vtkObjectEventHandler (fun a b ->
      let key = renderWindowInteractor.GetKeySym()
      //printfn "add_KeyReleaseEvt = %s" key
      
      match key with
        | "c" ->
          if !isRecording then isRecording := false; recordingText.VisibilityOff(); w.End()
          else isRecording := true; recordingText.VisibilityOn(); w.Start()
          renderWindow.Render()
        | "space" ->
          if !isPlaying then isPlaying := false; renderWindowInteractor.DestroyTimer () |> ignore
          else isPlaying := true; renderWindowInteractor.CreateRepeatingTimer (uint32 10) |> ignore
        | "r" -> renderer.ResetCamera()
        | "x" | "y" | "z" -> resetView key
        | "t" ->
          dimId := (!dimId + 1) % (ellsVTK.GetLength toggleDim + 1)
          update !index
        | "Escape" -> System.Environment.Exit 0
        | _ -> () ))

    renderWindowInteractor.add_KeyPressEvt (vtkObject.vtkObjectEventHandler (fun a b ->
      if !isPlaying = false then
        let key = renderWindowInteractor.GetKeySym()
        //printfn "add_KeyPressEvt = %s" key
        match key with
          | "Left" | "Prior" -> index := max 0 (!index - 1); update !index
          | "Right" | "Next" -> index := min (ellipsoids.Length - 1) (!index + 1); update !index
          | "Up" | "Home" -> index := 0; update !index
          | "Down" | "End" -> index := ellipsoids.Length - 1; update !index
          | _ -> () ))

    update !index
    renderer.ResetCamera()
    resetView "y"
    renderWindowInteractor.Start ()

  interface System.IDisposable with
    member o.Dispose () =
      ellsVTK |> Array3D.iter (fun (a,b,c) -> a.Dispose(); b.Dispose(); c.Dispose() )
      axes.Dispose ()
      widget.Dispose ()
      renderer.Dispose ()
      renderWindow.Dispose ()
      renderWindowInteractor.Dispose ()
  
  static member oldGetEllipsoidsAndNFibers (problemDirPath, origin: triple, domSize: triple, (boxes0: int, boxes1, boxes2), ?acceptFiberFn, ?chooseTimeFiles, ?refresh) =
    let acceptFiberFn = defaultArg acceptFiberFn (fun _ -> true)
    let boxesT = triple (boxes0, boxes1, boxes2)

    let chooseFiles = defaultArg chooseTimeFiles id
    let loAfiles = (ReadAndCombine problemDirPath).oldWriteParticlesTypeCoGandAxis (chooseFiles, ?refresh = refresh)
    let acceptFiber (_, v: triple) = abs v.X <> 1. && abs v.Y <> 1. && abs v.Z <> 1.0
    chooseFiles loAfiles |> Array.map (fun file ->
      file, lazy
        ( let boxes = Array3D.init boxes0 boxes1 boxes2 (fun _ _ _ -> List ())
          let lines = ReadAndCombine.oldParticlesTypeCoGandAxis2 (file.FullName, acceptFiber)
          lines |> Array.iter (fun (cog, p) ->
            if acceptFiberFn (file, cog, p) then
              let i, j, k =
                let i, j, k = (boxesT .* ((cog - origin) ./ domSize)).toTuple
                int i, int j, int k
              if i >= 0 && j >= 0 && k >= 0 && i < boxes.GetLength 0 && j < boxes.GetLength 1 && k < boxes.GetLength 2 then
                boxes.[int i, int j, int k].Add p )
          let els = boxes |> Array3D.map (fun fibers ->
            Utilities.Math.OrientationEllipse.pToEllipsoid fibers |> Option.map (fun (vals, vecs) ->
              vals |> Array.map float32, vecs |> Array2D.map float32 ))
          let nFibs = boxes |> Array3D.map Seq.length
          els, nFibs ))

  static member oldSerializeProblemIntoEllipsoids (problemDirPath, targetDir, origin, domSize, boxes, ?acceptFiberFn, ?chooseTimeFiles, ?refresh) =
    if Directory.Exists targetDir = false then Directory.CreateDirectory targetDir |> ignore
    let problemDirPath = DirectoryInfo problemDirPath
    Utilities.Serializer.Binary.serializeObject (Path.Combine(targetDir, "_setup"), (origin, domSize, boxes))
    let ellA = AnimateEllipsoids.oldGetEllipsoidsAndNFibers (problemDirPath.FullName, origin, domSize, boxes, ?acceptFiberFn = acceptFiberFn, ?chooseTimeFiles = chooseTimeFiles, ?refresh = refresh)
    ellA |> Array.iter (fun (file, out) ->
      let targetFile = Path.Combine (targetDir, file.Name + ".ell")
      let data = out.Value
      Utilities.Serializer.Binary.serializeObject (targetFile, data) )

  static member oldDeserializeEllipsoids file: (float32 [] * float32 [,]) option [,,] * int [,,] =
    unbox (Utilities.Serializer.Binary.deSerializeObject file)


type AnimateFibers (fibs: (triple * triple) seq) =

  let renderer = vtkRenderer.New()
  let append = vtkAppendPolyData.New ()
  let mapper = vtkPolyDataMapper.New()
  let actor = vtkActor.New()
  do
    mapper.SetInput (append.GetOutput())
    actor.SetMapper mapper
    renderer.AddActor actor
    fibs |> Seq.iter (fun (p1, p2) ->
      use line = vtkLineSource.New()
      line.SetPoint1 (p1.X, p1.Y, p1.Z)
      line.SetPoint2 (p2.X, p2.Y, p2.Z)
      append.AddInputConnection (line.GetOutputPort()) )

  let renderWindow = vtkRenderWindow.New()
  let renderWindowInteractor = vtkRenderWindowInteractor.New()
  let widget = vtkOrientationMarkerWidget.New()
  let axes = vtkAxesActor.New()
  
  do
    renderWindowInteractor.SetInteractorStyle (vtkInteractorStyleTrackballCamera.New())
    renderWindow.AddRenderer renderer
    renderWindowInteractor.SetRenderWindow renderWindow

    widget.SetOrientationMarker axes
    widget.SetInteractor renderWindowInteractor 
    widget.SetViewport (0.0, 0.0, 0.4, 0.4)
    widget.SetEnabled 1
    widget.InteractiveOff()
    renderer.ResetCamera()

    renderer.GetActiveCamera().ParallelProjectionOn()

    renderWindow.Render()
    renderWindowInteractor.Initialize()
    renderWindowInteractor.Start ()

    renderWindowInteractor.add_TimerEvt (vtkObject.vtkObjectEventHandler (fun a b ->
      renderWindow.Render()
    ))

  interface System.IDisposable with
    member o.Dispose () = 
      axes.Dispose ()
      widget.Dispose ()
      renderer.Dispose ()
      renderWindow.Dispose ()
      renderWindowInteractor.Dispose ()

