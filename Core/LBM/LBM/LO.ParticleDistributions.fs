﻿module LBM.LO.ParticleDistributions

open LBM
open LBM.LO.Types
open Utilities.Math

type PSD =
  static member internal splitPSD (psdDiameter_Percentage: (float*float) seq) =
    let psdDiameter_Percentage = psdDiameter_Percentage |> Seq.toArray
    let psdDiameter, psdPercentage =
      let psdDiameter_Percentage = psdDiameter_Percentage |> Array.sortBy (fun (psdD, psdP) -> psdP * 100000. + psdD) |> Array.rev
      psdDiameter_Percentage |> Array.filteri (fun i (_, psdP) -> i + 1 >= psdDiameter_Percentage.Length - 1 || psdP <> snd psdDiameter_Percentage.[i+1] ) |> Array.unzip
    let psdPercentage =
      let max = psdPercentage |> Array.max
      psdPercentage |> Array.map (fun psd -> psd / max)
    psdDiameter, psdPercentage
  
  static member getAggregateDiameterFromPSD (psdDiameter_Percentage: (float*float) seq) =
    let psdDiameter, psdPercentage = PSD.splitPSD psdDiameter_Percentage
    let rec findIndex i v = if v >= psdPercentage.[i + 1] && v <= psdPercentage.[i] then i else findIndex (i + 1) v
    fun totalVolumeFraction volumeFraction ->
      let percentage = 1. - volumeFraction / totalVolumeFraction
      let i = findIndex 0 percentage
      psdDiameter.[i] + (psdDiameter.[i+1] - psdDiameter.[i]) * (percentage - psdPercentage.[i]) / (psdPercentage.[i+1] - psdPercentage.[i])

  static member getRandomAggregateDiameterFromPSD (seed: int, psdDiameter_Percentage: (float*float) seq) =
    let xA, pmf =
      psdDiameter_Percentage
        |> Seq.toArray
        |> Utilities.Functions.differentiate
        |> Utilities.Functions.normalize
        |> Array.unzip
    let rnd = Utilities.RandomRejection (seed, pmf)
    fun () -> rnd.NextFloat xA

type FictitousAggregates =
  static member distanceProbability (lt: Lattice, totalVolumeFraction: float, psdDiameter_Percentage: (float*float) seq, minGeneratedD: float, ?seed: int, ?domainScale: float, ?nIterations: int, ?dimLessArcLength: float) =
    lt.Timer.tick 0
    let domainScale = defaultArg domainScale 5.
    let nIterations = defaultArg nIterations 5000
    let dimLessArcLength = defaultArg dimLessArcLength 0.15
    
    let rnd = Utilities.Random(defaultArg seed 13846)
    let domainSize = domainScale * minGeneratedD

    let cogA, r2A, cog2A =     
      let domainVolume = domainSize**3.0
      let psdDiaPer, pMax = 
        let psdDiameter, psdPercentage = PSD.splitPSD psdDiameter_Percentage
        let pMax = ref 0.
        let diaPer =
          psdDiameter |> Array.foldi (fun i cum di ->
            if di > minGeneratedD then cum
            elif di <= minGeneratedD && psdDiameter.[i - 1] >= minGeneratedD then
              let pi = psdPercentage.[i]
              let dj, pj = psdDiameter.[i - 1], psdPercentage.[i - 1]
              pMax := pi + (pj - pi) / (dj - di) * (minGeneratedD - di)
              [ minGeneratedD, !pMax; di, pi]
            else (di, psdPercentage.[i] ) :: cum) List.empty       
        diaPer, !pMax
      let getDiameter = PSD.getAggregateDiameterFromPSD psdDiaPer
      let remainingVF = totalVolumeFraction * pMax / (1. - totalVolumeFraction * (1. - pMax))

      let rec addSphere (sL: (triple * float) list) vf =
        if sL.Length < 100000 && vf < remainingVF then
          let r =
            let dia = getDiameter remainingVF vf
            dia / 2.
          let lowerBound, upperBound = triple(r, r, r), triple(domainSize - r, domainSize - r, domainSize - r)
          let cog =
            let rec newPosition() =
              let cog = rnd.NextTriple (lowerBound, upperBound)      
              if sL |> List.exists (fun (cogi, ri) -> (cog - cogi).norm < ri + r) then newPosition()
              else cog
            newPosition()
          addSphere ((cog, r) :: sL) (vf + 4. / 3. * System.Math.PI * r**3.0 / domainVolume)
        else sL
      
      addSphere [] 0.
      |> List.map (fun (cog, r) -> cog, r * r, cog * cog) |> List.toArray |> Array.unzip3

    let distances, probability =
      let upperBound = triple(domainSize, domainSize, domainSize)
      let rec findP() =
        let p = rnd.NextTriple (Triple.Zero, upperBound)
        let inLO = 
          cogA |> Array.existsi (fun i cog -> (cog - p) |> Triple.norm2 < r2A.[i])
        if inLO then findP() else p

      List.init nIterations (fun _ ->
        let p = findP()
        let n = (rnd.NextTriple() - p) |> Triple.normalize
        let p2, n2 = p * p, n * n
        cogA |> Array.choosei (fun i cog ->     
          let pnMnc = p * n - n * cog
          let pc = p * cog
          let discrHalf2 = pnMnc * pnMnc - n2 * (cog2A.[i] + p2 - 2. * pc - r2A.[i])
          if discrHalf2 > 0. then
            let d1, d2 = 
              let disc = sqrt discrHalf2
              -pnMnc + disc / n2, -pnMnc - disc / n2
            let d = abs (d1 - d2)
            if d < minGeneratedD && abs d1 < minGeneratedD && abs d2 < minGeneratedD then Some d else None
          else None ) )
        |> List.map (fun dA -> if dA.Length = 0 then 0. else Array.max dA)
        |> List.sort
        |> List.fold (fun (dL: float list) d -> if dL.IsEmpty || d <> dL.[0] then d :: dL else dL) []
        |> List.mapi (fun i d -> d, float (nIterations - i) / float nIterations) |> List.unzip
    
    let distApprox, probApprox = Utilities.Approximation.Interpolation.constantArcLength distances probability dimLessArcLength 0.01

    lt.Timer.tockPrintTick 0 "FictitiousParticles: distanceProbability"   
    distApprox, probApprox

  static member fictitiousDistanceFunction (rnd: Utilities.Random, dist: float [], prob : float [])=
    let minP, maxD = prob.[prob.Length - 1], 2. * dist.[0]
    let rec findDistance i p = 
      if p >= prob.[i + 1] && p <= prob.[i] then
        dist.[i+1] + (dist.[i] - dist.[i + 1]) / (prob.[i] - prob.[i+1]) * (p - prob.[i+1]) 
      else findDistance (i + 1) p
    let rndFn = (fun gap ->
      let p = rnd.NextFloat()
      if p <= minP || gap > maxD then 0.
      else
        let d = findDistance 0 p
        if d < gap then d else 0.)
    let detFn = (fun gap (i: uint16, j: uint16) ->
      let p =
        let id = max i j % (min i j + 1us)
        if id < 10us then float id / 10.
        elif id < 100us then float id / 100.
        elif id < 1000us then float id / 1000.
        elif id < 10000us then float id / 10000.
        else 0.5
      if p <= minP || gap > maxD then 0.
      else
        let d = findDistance 0 p
        if d < gap then d else 0.)
    rndFn, detFn