﻿namespace LBM.Starter

open Utilities.Math
open Utilities.IO

open LBM.Naming.General
open LBM.Naming.Input

open System
open System.Diagnostics
open System.IO
open System.Collections.Generic
open System.Text.RegularExpressions
open System.Windows.Forms

type Callee =

  static member private manage () =

    let parameters, maxProcesses = 
      match File.ReadAllLines @"!--- LBM_Caller ---\_execMany.txt" |> List.ofArray with
        | [] -> failwith "_execMany.txt is empty"
        | head :: tail ->
          let maxProcesses =
            let a = head.Split([| "\t" |], StringSplitOptions.None)
            int a.[0]
          let parameters = tail |> List.map CMD.toDict
          parameters, maxProcesses

    let limit = min maxProcesses parameters.Length
    let procA = Array.init limit (fun i -> Some (Caller.execOne parameters.[i]) )
    let position = ref limit

    let exitFn (p: Process option) = match p with Some p when not p.HasExited -> p.Kill() | _ -> ()

    Console.CancelKeyPress.Add (fun _ -> for d in procA do exitFn d)
    AppDomain.CurrentDomain.ProcessExit.Add (fun _ -> for d in procA do exitFn d)

    let procAlive () = procA |> Array.sumByi (fun i p ->
      match p with
        | Some p when not p.HasExited -> 1
        | Some p ->
          System.Console.WriteLine ("--Ended: {0}", (DirectoryInfo p.StartInfo.WorkingDirectory).Name)
          p.Dispose(); procA.[i] <- None
          if !position < parameters.Length then 
            procA.[i] <- Some (Caller.execOne parameters.[!position])
            incr position; 1
          else 0
        | None -> 0 )

    let thread = Threading.Thread(Threading.ThreadStart(fun () ->
      let s = Diagnostics.Stopwatch()
      s.Start()
      let mutable tempProcAlive = procAlive()
      while tempProcAlive <> 0 do
        let text = System.String.Format("Finished: {0}/{1} | Running: {2} | Duration: {3}", !position - tempProcAlive, parameters.Length, tempProcAlive, s.Elapsed)
        Console.Write text
        Threading.Thread.Sleep 500
        Console.CursorLeft <- 0; Console.Write (String(' ', text.Length)); Console.CursorLeft <- 0
        tempProcAlive <- procAlive()
      Console.WriteLine ("\n\n\nTime elapsed: {0}", s.Elapsed)
      Console.WriteLine "The computations have completed..."
      Console.ReadKey() |> ignore
    ))
    thread.Start()

  static member postProcess (fns: (string * (unit->unit)) list) =
    if fns.Length = 1 then (snd fns.[0])()
    else
      printfn "Choose a postProcess to execute:"
      fns |> Seq.iteri (fun i (name, _) -> printfn "%d) %s" i name)
      printf "Number = "
      let res = Console.ReadLine()
      (snd fns.[int res])()
    0

  static member main (args: string []) (fns: (string * ((Dictionary<string,string> -> unit) * ((string -> unit) option))) seq) =
    let fileExists = File.Exists @"!--- LBM_Caller ---\_execMany.txt"
    if args.Length = 0 && fileExists then Callee.manage ()
    elif args.Length = 0 then
      let fns = fns |> Array.ofSeq |> Array.choose (fun (name,(_,debug)) -> debug |> Option.map (fun debug -> name, debug))
      if fns.Length = 0 then printfn "No debugger exists"
      else
        let v = 
          if fns.Length = 1 then 0
          else
            printfn "Choose a debugger to execute:"
            fns |> Array.iteri (fun i (name, _) -> printfn "%d) %s" i name)
            printf "Number = "
            let res = Console.ReadLine()
            int res
        let name, fnDebug = fns.[v]
        printfn "%d) %s - Running ..." v name
        fnDebug name
    else
      let argsDict = CMD.toDict args
      let name = argsDict.[Args.name]
      let _, (fnReal, _) =
        match fns |> Seq.tryFind (fun (namei, _) -> namei = name) with
          | None -> failwithf "Starter.main: Cannot find: %s" name
          | Some v-> v
      fnReal argsDict
      File.WriteAllText ("! ---- Finished ----", "")
    0

  static member innerMain (args: IDictionary<string,string>, fn, ?toConsole) =

    let toConsole = defaultArg toConsole false

    #if DEBUG
    let tryWith (fn: unit -> unit)  = fn ()
    let isDebug = true
    #else
    let tryWith fn =
      if toConsole = false then
        try
          fn ()
        with ex ->
          File.AppendAllText ("-Error.txt", sprintf "##### %s ######\n%A\n\n" (DateTime.Now.ToString "dd.MM.yy - hh:mm:ss:fffffff") ex)
          File.WriteAllText ("! ---- Error ----", "")
      else fn ()
    #endif

    let inputDict = args.[Args.inputFile] |> File.ReadAllText |> ML.parse
    let mainId =
      let parse (str: string) =
        let out = str.Split [||] |> Array.map int
        out.[0], out.[1], out.[2]
      ML.failGet parse inputDict General.mainId

    tryWith (fun () ->

      let path = if args.ContainsKey Args.dirPath = false then Directory.GetCurrentDirectory () else args.[Args.dirPath]

      let globI, globJ, globK as globID =
        if args.ContainsKey Args.paralel then
          let m1 = (Regex("^globID-(\\d+)-(\\d+)-(\\d+)$")).Match args.[Args.paralel]
          if m1.Success then
            let fn (i: int) (v: Match) = int v.Groups.[i].Value
            let globID = fn 1 m1, fn 2 m1, fn 3 m1
            globID
          else mainId
        else mainId

      let dir = sprintf @"%s\Domain_%d-%d-%d" path globI globJ globK

      if Directory.Exists dir = false then
        let wdir = Directory.CreateDirectory dir
        Directory.SetCurrentDirectory wdir.FullName
        if toConsole = false then Utilities.IO.File.redirectStdOutToFile "-Out.txt" |> ignore
        let t = Diagnostics.Stopwatch()
        t.Start()
        use g = new LBM.PreProcess.Input.General (args, globID, mainId, inputDict)
        fn g
        File.WriteAllText ("! ---- Finished ----", sprintf "Duration\n %A" t.Elapsed) )

and Caller =

  static member private fileAccept (file: FileInfo) = file.Name <> "Kitware.VTK.xml"

  static member private getExe targetApp =
    let files = Directory.GetFiles (targetApp + @"\bin\Release", "*.exe")
    if files.Length <> 1 then failwithf "Folder contains %d exe files. Should be 1." files.Length
    files.[0]

  static member execMany (maxProcesses: int, problemName, _SOURCE_DIRECTORY_, targetDir, execOneDataA: _ seq) =
    let targetApp = targetDir + @"\!--- LBM_Caller ---"
    let path =
      if Directory.Exists targetApp = false then
        Utilities.IO.Dir.copy (_SOURCE_DIRECTORY_, targetApp, Caller.fileAccept, fun d -> d.Name <> "Debug")
      else
        let str = "'!--- LBM_Caller ---' already exists and will NOT be overwritten!\nDo you want to continue?"
        let res = Utilities.GUI.TopMostMessageBox.Show (str, "Warning!", MessageBoxButtons.YesNo)
        if (res = DialogResult.Yes) = false then Environment.Exit 0
      Caller.getExe targetApp

    let fsxFile = Path.Combine (targetDir, "getRunningSims.fsx")
    if File.Exists fsxFile = false then
      let text = Utilities.IO.File.readResource "getRunningSims.fsx"
      File.WriteAllText (fsxFile, text)

    let body = seq {
      yield sprintf "%d\t%s" maxProcesses _SOURCE_DIRECTORY_
      for dirName, arguments in execOneDataA do
        let targetProb = sprintf @"%s\%s" targetDir dirName
        targetProb |> Directory.CreateDirectory |> ignore
        let argsString = arguments |> LBM.Naming.Input.ML.stringify
        let inputPath = targetProb + @"\_input.txt"
        File.WriteAllText (inputPath, argsString)
        yield CMD.ofDict (dict [Args.name, problemName; Args.inputFile, inputPath; Args.dirPath, targetProb]) }
    File.WriteAllLines (targetApp + @"\_execMany.txt", body)

    let proc = new Process()
    proc.StartInfo.FileName <- path
    proc.StartInfo.WorkingDirectory <- targetDir
    if proc.Start() = false then failwith "execMany: Cannot start process"

  static member execOne (args: IDictionary<string,string>) =
    let targetDir = args.[Args.dirPath]
    let path = Caller.getExe (targetDir + @"\..\!--- LBM_Caller ---")
    Console.WriteLine ("Started: {0}", (DirectoryInfo targetDir).Name)
    let proc = new Process()
    proc.StartInfo.FileName <- path
    proc.StartInfo.Arguments <- CMD.ofDict args
    proc.StartInfo.WorkingDirectory <- targetDir
    proc.StartInfo.UseShellExecute <- false
    if proc.Start() = false then failwith "execOne: Cannot start process"
    proc

  static member getRunningProcesses =
    let runningProcesses =
      let wmiQuery =
        let queryProperties = [| "Name"; "ProcessId"; "Caption"; "ExecutablePath"; "CommandLine" |]
        Management.SelectQuery ("Win32_Process", "", queryProperties)
      let scope = Management.ManagementScope(@"\\.\root\CIMV2")
      (new Management.ManagementObjectSearcher(scope, wmiQuery)).Get()

    [ for obj in runningProcesses do
        let cmd =
          let cmd = string obj.["CommandLine"]
          cmd.Substring (1, cmd.Length - 2)
        if cmd.Contains "-LBM_Application" || (cmd.Contains "!--- LBM_Caller ---" && cmd.Contains "Name=") then
          let dict = Utilities.IO.CMD.toDict cmd
          yield Convert.ToInt32 obj.["ProcessId"], (string obj.["Name"]).Replace("\"",""), dict ]

  static member kill fn =
    let num = Caller.getRunningProcesses |> List.sumBy (fun (id, name, argsDict) ->
      let p = Process.GetProcessById id
      if fn (name, argsDict) then p.Kill(); 1
      else 0 )
    printfn "%d processes have been killed." num

  static member killAll = Caller.kill (fun _ -> true)

  static member killPath s = Caller.kill (fun (_, argsDict) -> argsDict.[Args.dirPath].Contains s)

  static member info =
    let pA = Caller.getRunningProcesses
    printfn "----------- Total = %d processes -------------" pA.Length
    for id, name, argsDict in pA do
        printfn "%d)\t%s\t%s\t%s" id name argsDict.[Args.dirPath] argsDict.[Args.inputFile]
    printfn "--------------------------"

  static member viewer (?path) =
    
    let file =
      let files = [
        @"..\..\LBM_Viewer\LBM_Viewer\bin\Release\LBM_Viewer.exe"
        @"d:\Olda\Project\LBM_Viewer\LBM_Viewer\bin\Release\LBM_Viewer.exe"
        @"d:\LBM\LBM_Viewer\LBM_Viewer\bin\Release\LBM_Viewer.exe" ]
      files |> List.find (fun file -> File.Exists file)

    let proc = new Process()
    proc.StartInfo.FileName <- file
    path |> Option.iter (fun path -> proc.StartInfo.Arguments <- path)
    if proc.Start() = false then failwith "viewer: Cannot start process"
