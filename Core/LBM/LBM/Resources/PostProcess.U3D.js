﻿var resourceIdA = [/*0*/];
// glob var resourceIdA

function dump(object) {
  var output = '';
  for (property in object) {
    output += property + ': ' + object[property]+'\n';
  }
  console.println(output);
}

var lastMat = new Matrix4x4();
var lastX = 0; var lastY = 0; var lastZ = 0;
var lasttX = 0; var lasttY = 0; var lastttZ = 0;
var lastuX = 0; var lastuY = 0; var lastuZ = 0;
var roll = 0;
var pType = '';
var freeze = false;

cameraManager = new RenderEventHandler();
cameraManager.onEvent = function(e) {
  if (freeze) {
    var cam = e.canvas.getCamera();
    cam.transform.set(lastMat);
    cam.roll = roll;
    cam.up.set(lastuX, lastuY, lastuZ);
    cam.position.set(lastX, lastY, lastZ);
    cam.targetPosition.set(lasttX, lasttY, lasttZ);
    cam.projectionType = pType;
    scene.lightScheme = 'cad';
    freeze = false;
  } else {
    cam = e.canvas.getCamera();
    lastMat.set(cam.transform);
    roll = cam.roll;
    lastuX = cam.up.x; lastuY = cam.up.y; lastuZ = cam.up.z;
    lastX = cam.position.x; lastY = cam.position.y; lastZ = cam.position.z;
    lasttX = cam.targetPosition.x; lasttY = cam.targetPosition.y; lasttZ = cam.targetPosition.z;
    pType = cam.projectionType;
  }
 }
cameraManager.PreventNextChange = function () { freeze = true; }

function isId(node, realTime) {
  if (!node || !node.name) { return false; }
  else if (node.name.indexOf('Time ' + realTime) != -1) { return true; }
  else { return isId(node.parent, realTime); }
}

var animatedNodes = [];
function addNodes(id) {
  animatedNodes[id] = {};
  for (var i = 0; i < scene.nodes.count; i++){
    var node = scene.nodes.getByIndex(i);
    if (isId(node, resourceIdA[id])) {
      node.visible = false;
      animatedNodes[id][node.name] = node; }
  }
}

function setVisible(oldId, id) {
  for (var name in animatedNodes[id]) {
    if (animatedNodes[oldId] && animatedNodes[oldId][name]) {
      animatedNodes[id][name].visible = animatedNodes[oldId][name].visible;
    } else { animatedNodes[id][name].visible = true; }
  }
  if (animatedNodes[oldId]) {
    for (var name2 in animatedNodes[oldId]) { animatedNodes[oldId][name2].visible = false; }
  }
}

var currId = -1;
function setCurId(newId) {
  if (newId < 0) { newId = 0; }
  else if (newId >= resourceIdA.length) { newId = resourceIdA.length - 1; }
  if (currId != newId) {
    if (!animatedNodes[newId]) {
      if (newId > 0) {
        cameraManager.PreventNextChange();
        scene.addModel(new Resource('pdf://{1}/U3D/u3d_' + resourceIdA[newId].toString().replace('.', '_') + '.u3d'));
      }
      addNodes(newId);
    }
    setVisible(currId, newId);
    currId = newId;
    scene.selectedNode = scene.meshes.getByIndex(scene.meshes.count-1);
    scene.selectedNode = undefined;
  }
}

setCurId(0);

var deltaTime = 0;
var busy = false;
myTimer = new TimeEventHandler();
myTimer.onEvent = function(e) {
  deltaTime = deltaTime + e.deltaTime;
  if (deltaTime > 0.1 && !busy) {
    busy = true;
    var newId = (currId === resourceIdA.length - 1) ? 0 : currId + 1;
    deltaTime = 0;
    setCurId(newId);
    busy = false;
  }
};

runtime.addCustomToolButton('home', '<|', 'push button');
runtime.addCustomToolButton('prev', '<<', 'push button');
runtime.addCustomToolButton('play', '> ||', 'push button');
runtime.addCustomToolButton('next', '>>', 'push button');
runtime.addCustomToolButton('end', '|>', 'push button');

var isRunning = false;
buttonEventHandler = new ToolEventHandler()
buttonEventHandler.onEvent = function(e){
  switch(e.toolName) {
    case 'home':
      setCurId(0);
      break;
    case 'prev':
      setCurId(currId - 1);
      break;
    case 'next':
      setCurId(currId + 1);
      break;
    case 'end':
      setCurId(resourceIdA.length - 1);
      break;
    case 'play':
      if (isRunning) {
        runtime.removeEventHandler(myTimer);
        isRunning = false;
      } else {
        runtime.addEventHandler(myTimer);
        isRunning = true;
      }
      break;
  }
}

keyEventHandler = new KeyEventHandler();
keyEventHandler.onKeyDown = true;
keyEventHandler.onEvent = function(e) {
  switch(e.characterCode) {
    case 4: //key home
    case 30: //key up
      setCurId(0);
      break;
    case 1: //key end
    case 31: //key down
      setCurId(resourceIdA.length - 1);
      break;
    case 28: // left
      if (e.ctrlKeyDown || e.shiftKeyDown) { setCurId(currId - 10); }
      else { setCurId(currId - 1); }
      break;
    case 29: // right
      if (e.ctrlKeyDown || e.shiftKeyDown) { setCurId(currId + 10); }
      else { setCurId(currId + 1); }
      break;
  }
};

renderEventHandler = new RenderEventHandler(); 
renderEventHandler.onEvent = function(e) { 
  runtime.removeEventHandler(this); 
  e.canvas.background.setColor(new Color(0.1,0.1,0.2));
};

runtime.addCustomMenuItem("loadAll", "Load all the time steps at once", "default", 0);

loadAllTimer = new TimeEventHandler();
var loadAlli = 1;
var background = null;
loadAllTimer.onEvent = function(e) {
  if (loadAlli < resourceIdA.length) { 
    if (!animatedNodes[loadAlli]) {
      cameraManager.PreventNextChange();
      scene.addModel(new Resource('pdf://{1}/U3D/u3d_' + resourceIdA[loadAlli].toString().replace('.', '_') + '.u3d'));
      addNodes(loadAlli);
    }
    var g = (loadAlli + 1.0) / resourceIdA.length
    background.setColor(new Color(1.0 - g, g, 0.0));
    loadAlli = loadAlli + 1;
  } else {
    runtime.removeEventHandler(loadAllTimer);
    scene.selectedNode = scene.meshes.getByIndex(scene.meshes.count - 1);
    scene.selectedNode = undefined;
    background.setColor(new Color(0.0, 0.0, 0.0));
  }
};

menuEventHandler = new MenuEventHandler();
menuEventHandler.onEvent = function (e) {
  if (e.menuItemName === "loadAll") {
    e.canvas.background.setColor(new Color(1.0, 0.0, 0.0));
    background = e.canvas.background;
    runtime.addEventHandler(loadAllTimer);
  }
};

runtime.addEventHandler(menuEventHandler);
runtime.addEventHandler(cameraManager);
runtime.addEventHandler(renderEventHandler); 
runtime.addEventHandler(buttonEventHandler);
runtime.addEventHandler(keyEventHandler);
