﻿open System.IO

let data =
  (DirectoryInfo __SOURCE_DIRECTORY__).GetDirectories() |> Array.map (fun root ->
    let dirA = Directory.GetDirectories (root.FullName, "Domain_*")
    if dirA.Length = 0 then 0, root.Name
    else
      let root2 = dirA.[0]
      let out = FileInfo (Path.Combine (root2, "-Out.txt"))
      let span = System.DateTime.Now - out.LastWriteTime
      (if span < System.TimeSpan(0,0,60) then 1 else 2), root.Name )

printfn "============ Not Started ============";;
for i, root in data do if i = 0 then printfn "%s" root

printfn "============ Running ============";;
for i, root in data do if i = 1 then printfn "%s" root

printfn "============ Finished ============";;
for i, root in data do if i = 2 then printfn "%s" root