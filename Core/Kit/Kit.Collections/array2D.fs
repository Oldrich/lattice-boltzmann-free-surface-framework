﻿[<RequireQualifiedAccess>]
module Kit.Collections.Array2D

let inline transpose (input : _ [,]) =
  Array2D.init (input.GetLength 1) (input.GetLength 0) (fun x y -> input.[y,x])

let inline invert3x3 (m: _ [,]) = 
  let one = LanguagePrimitives.GenericOne<_>
  let determinantOfMinor theRowHeightY theColumnWidthX =
    let x1 = if theColumnWidthX = 0 then 1 else 0
    let x2 = if theColumnWidthX = 2 then 1 else 2
    let y1 = if theRowHeightY = 0 then 1 else 0
    let y2 = if theRowHeightY = 2 then 1 else 2
    (m.[y1, x1] * m.[y2, x2] ) - (m.[y1, x2] * m.[y2, x1] )  
  let det = (m.[0, 0] * determinantOfMinor 0 0 ) - (m.[0, 1] * determinantOfMinor 0 1 ) +  (m.[0, 2] * determinantOfMinor 0 2)
  let detInv = one / det
  m |> Array2D.mapi (fun x y _ ->
    let d = detInv * (determinantOfMinor x y)
    if (x + y ) % 2 = 1 then -d
    else d ) |> transpose

let inline max (input: _ [,]) =
  let mutable maxV = input.[0,0]
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      if input.[i,j] > maxV then maxV <- input.[i,j]
  maxV

let inline maxBy func (input: _ [,]) =
  let mutable maxV = None
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      match maxV with
      | None -> maxV <- Some (func input.[i,j])
      | Some v ->
        let res = func input.[i,j]
        if res > v then maxV <- Some res
  maxV.Value

let inline maxByi func (input: _ [,]) =
  let mutable maxV = None
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      match maxV with
      | None -> maxV <- Some (func (0,0) input.[i,j])
      | Some v ->
        let res = func (i,j) input.[i,j]
        if res > v then maxV <- Some res
  maxV.Value

let inline min (input: _ [,]) =
  let mutable minV = input.[0,0]
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      if input.[i,j] < minV then minV <- input.[i,j]
  minV

let inline minBy func (input: _ [,]) =
  let mutable minV = None
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      match minV with
      | None -> minV <- Some (func input.[i,j])
      | Some v ->
        let res = func input.[i,j]
        if res < v then minV <- Some res
  minV.Value

let inline minByi func (input: _ [,]) =
  let mutable minV = None
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      match minV with
      | None -> minV <- Some (func (0,0) input.[i,j])
      | Some v ->
        let res = func (i,j) input.[i,j]
        if res < v then minV <- Some res
  minV.Value

let inline mult (input1: _ [,], input2: _ [,]) =
  Array2D.init (input1.GetLength 0) (input2.GetLength 1) (fun i j ->
    let mutable s = LanguagePrimitives.GenericZero<_>
    for pos in 0 .. input1.GetLength(1) - 1 do
      s <- s + input1.[i,pos] * input2.[pos,j]
    s )

let inline removeOption defaultValue (input: _ [,]) =
  input |> Array2D.map (fun v -> defaultArg v defaultValue)

let inline sum (input: _ [,]) =
  let mutable sumV = LanguagePrimitives.GenericZero<_>
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      sumV <- sumV + input.[i,j]
  sumV

let inline sumBy func (input: _ [,]) =
  let mutable sumV = LanguagePrimitives.GenericZero<_>
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      sumV <- sumV + func input.[i,j]
  sumV

let inline sumByi func (input: _ [,]) =
  let mutable sumV = LanguagePrimitives.GenericZero<_>
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      sumV <- sumV + func (i,j) input.[i,j]
  sumV

let inline rotateL2G (rotationMatrix: _ [,]) (matrix: _ [,]) = 
  mult(mult(rotationMatrix, matrix), transpose rotationMatrix)

let inline rotateG2L (rotationMatrix: _ [,]) (matrix: _ [,]) = 
  rotateL2G (invert3x3 rotationMatrix) matrix