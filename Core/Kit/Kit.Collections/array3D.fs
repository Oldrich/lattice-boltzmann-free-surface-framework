﻿[<RequireQualifiedAccess>]
module Kit.Collections.Array3D

open Kit

let inline sumBy (func : 'T -> ^U) (input: 'T [,,]) : ^U =
  let mutable acc = LanguagePrimitives.GenericZero< (^U) >
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      for k in 0 .. input.GetLength 2 - 1 do
        acc <- Checked.(+) acc (func input.[i,j,k])
  acc

let inline sumByi (func : int triple -> _ -> ^U) (input: _ [,,]) : ^U =
  let mutable acc = LanguagePrimitives.GenericZero< (^U) >
  for i in 0 .. input.GetLength 0 - 1 do
    for j in 0 .. input.GetLength 1 - 1 do
      for k in 0 .. input.GetLength 2 - 1 do
        acc <- Checked.(+) acc (func (triple(i,j,k)) input.[i,j,k])
  acc

let inline zip (el1: _ [,,], el2: _ [,,]) =
  el1 |> Array3D.mapi (fun i j k e1 -> e1, el2.[i,j,k])