﻿[<RequireQualifiedAccess>]
module Kit.Collections.List

let inline collecti func input =
  input |> List.mapi func |> List.concat

let inline filteri (func: int->_->bool) (input: _ list) =
  let result = new ResizeArray<_>()
  for i in 0 .. input.Length - 1 do 
    if func i input.[i] then
      result.Add input.[i] 
  result |> List.ofSeq

let inline foldi (func: int->_->_->_) initialValue (input: _ list) = 
  let mutable result = initialValue
  for i in 0 .. input.Length - 1 do
    result <- func i result input.[i]
  result

let inline sumBy (func: 'T -> ^U) (input: 'T list): ^U =
  let mutable acc = LanguagePrimitives.GenericZero< (^U) >
  for i in 0 .. input.Length - 1 do
    acc <- Checked.(+) acc (func input.[i])
  acc

let inline sumByi (func: int -> 'T -> ^U) (input: 'T list): ^U =
  let mutable acc = LanguagePrimitives.GenericZero< (^U) >
  for i in 0 .. input.Length - 1 do
    acc <- Checked.(+) acc (func i input.[i])
  acc