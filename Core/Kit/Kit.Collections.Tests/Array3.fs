﻿namespace Kit.Tests.Collections

open NUnit.Framework
open Kit
open Kit.Collections

type Array3_Tests() =

  let input =
    let lengthsZ = array2D [ [1;2;2]; [2;1;2] ]
    Array3.init lengthsZ (fun ijk -> 100 * ijk.x + 10 * ijk.y + ijk.z )

  let indexChecker() =
    let m, n = ref 1, ref 1
    let runner (t: _ triple) value =
      incr n
      m := !m + !n * (1000000 * t.x + 10000 * t.y + 100 * t.z + value)
    let result() = !m
    runner, result

  let pathChecker() =
    let m, n = ref 1, ref 1
    let runner value =
      incr n
      m := !m + !n * value
    runner, (fun () -> !m)

  let fullPath = 5330
  let fullIndex = 45808230

  [<Test>]
  member o.GetSlice() =
    let output = input.[0..0,0..,0..]
    let shouldBe = [| [| [|0|];[|10;11|];[|20;21|] |] |]
    Assert.That(output.array, Is.EqualTo shouldBe)

  [<Test>]
  member o.copy() =
    let output = Array3.copy input
    Assert.That(input.array, Is.EqualTo output.array)

  [<Test>]
  member o.create() =
    let old = array3 [| [|[|1|];[||];[||]|]; [|[||];[||];[|1|]|] |]
    let output = Array3.create old 5.0
    let values = [| [| [|5.0|];[||];[||] |]; [| [||];[||];[|5.0|] |] |]
    Assert.That(output.array, Is.EqualTo values)

  [<Test>]
  member o.exists() =
    let runner, ended = pathChecker()
    let output = input |> Array3.exists (fun value ->
      runner value
      value = 121 )
    printfn "%d" input.[1,2,1]
    Assert.That(output)
    Assert.That(ended(), Is.EqualTo fullPath)

  [<Test>]
  member o.existsi() =
    let runner, result = indexChecker()
    let output = input |> Array3.existsi (fun t value ->
      runner t value
      value = 121 )
    Assert.That(output, Is.True)
    Assert.That(result(), Is.EqualTo fullIndex)

  [<Test>]
  member o.fold() =
    let runner, result = pathChecker()
    let output =
      input |> Array3.fold (fun sum value ->
        runner value
        sum + value ) 5
    Assert.That(result(), Is.EqualTo fullPath)
    Assert.That(output, Is.EqualTo 619)

  [<Test>]
  member o.foldi() =
    let runner, result = indexChecker()
    let output =
      input |> Array3.foldi (fun t sum value ->
        runner t value
        sum + value ) 5
    Assert.That(result(), Is.EqualTo fullIndex)
    Assert.That(output, Is.EqualTo 619)

  [<Test>]
  member o.forall() =
    let runner, result = pathChecker()
    let output = input |> Array3.forall (fun value ->
      runner value
      value <> 121 )
    Assert.That(output, Is.False)
    Assert.That(result(), Is.EqualTo fullPath)

  [<Test>]
  member o.foralli() =
    let runner, result = indexChecker()
    let output = input |> Array3.foralli (fun t value ->
      runner t value
      value <> 121 )
    Assert.That(output, Is.False)
    Assert.That(result(), Is.EqualTo fullIndex)

  [<Test>]
  member o.init() =
    let output =
      let n = ref 0
      let lengthsZ = array2D [ [1;2;2]; [2;1;2] ]
      Array3.init lengthsZ (fun ijk ->
        incr n
        ijk.x  +  !n * ijk.y  +  !n * !n * ijk.z
      )
    let values = [| [| [|0|];[|2;12|];[|8;35|] |]; [| [|1;50|];[|9|];[|19;121|] |] |]
    Assert.That(output.array, Is.EqualTo values)

  [<Test>]
  member o.iter() =
    let runner, result = pathChecker()
    input |> Array3.iter (fun value -> runner value)
    Assert.That(result(), Is.EqualTo fullPath)

  [<Test>]
  member o.iteri() =
    let runner, result = indexChecker()
    input |> Array3.iteri (fun t value -> runner t value)
    Assert.That(result(), Is.EqualTo fullIndex)

  [<Test>]
  member o.map() =
    let runner, result = pathChecker()
    let output = input |> Array3.map (fun value ->
      runner value
      string value )
    let shouldBe = [| [| [|"0"|];[|"10";"11"|];[|"20";"21"|] |]; [| [|"100";"101"|];[|"110"|];[|"120";"121"|] |] |]
    Assert.That(output.array, Is.EqualTo shouldBe)
    Assert.That(result(), Is.EqualTo fullPath)

  [<Test>]
  member o.mapi() =
    let runner, result = indexChecker()
    let output = input |> Array3.mapi (fun t value ->
      runner t value
      string value
    )
    let shouldBe = [| [| [|"0"|];[|"10";"11"|];[|"20";"21"|] |]; [| [|"100";"101"|];[|"110"|];[|"120";"121"|] |] |]
    Assert.That(output.array, Is.EqualTo shouldBe)
    Assert.That(result(), Is.EqualTo fullIndex)

  [<Test>]
  member o.max() =
    input |> Array3.iteri (fun t value ->
      let temp = input |> Array3.map id
      temp.array.[t.x].[t.y].[t.z] <- 1000
      let output = temp |> Array3.max
      Assert.That(output, Is.EqualTo 1000)
    )

  [<Test>]
  member o.maxBy() =
    let runner, result = pathChecker()
    let output = input |> Array3.maxBy (fun value ->
      runner value
      float value )
    Assert.That(output, Is.EqualTo 121)
    Assert.That(result(), Is.EqualTo fullPath)

  [<Test>]
  member o.maxByi() =
    let runner, result = indexChecker()
    let output = input |> Array3.maxByi (fun t value ->
      runner t value
      float value
    )
    Assert.That(output, Is.EqualTo 121)
    Assert.That(result(), Is.EqualTo fullIndex)

  [<Test>]
  member o.removeOption() =
    let input =
      let lengthsZ = array2D [ [1;2;2]; [2;1;2] ]
      Array3.init lengthsZ (fun ijk ->
        Some (100 * ijk.x + 10 * ijk.y + ijk.z) )
    input.array.[1].[1].[0] <- None
    let output = input |> Array3.removeOption -1
    let shouldBe = [| [| [|0|];[|10;11|];[|20;21|] |]; [| [|100;101|];[|-1|];[|120;121|] |] |]
    Assert.That(output.array, Is.EqualTo shouldBe)

  [<Test>]
  member o.sumBy() =
    let runner, result = pathChecker()
    let output = input |> Array3.sumBy (fun value ->
      runner value
      float value )
    Assert.That(output, Is.EqualTo 614)
    Assert.That(result(), Is.EqualTo fullPath)

  [<Test>]
  member o.sumByi() =
    let runner, result = indexChecker()
    let output = input |> Array3.sumByi (fun xyz value ->
      runner xyz value
      float value )
    Assert.That(output, Is.EqualTo 614)
    Assert.That(result(), Is.EqualTo fullIndex)

  [<Test>]
  member o.tryGet() =
    input |> Array3.iteri (fun xyz v ->
      let output = input |> Array3.tryGet xyz
      Assert.That(output, Is.EqualTo(Some v)) )

  [<Test>]
  member o.tryGet2() =
    let output = input |> Array3.tryGet (triple(-1,1,1))
    Assert.That(output, Is.EqualTo None)
      

//  [<Test>]
//  member o.maxBy() =
//    let runner, result = pathChecker()
//    let output = input |> Array3.maxBy (fun value ->
//      runner value
//      value
//    )
//    Assert.That(result(), Is.EqualTo 30)
//    Assert.That(output, Is.EqualTo 4)
//
//  [<Test>]
//  member o.zeroCreate() =
//    let lengthsZ = array2D [ [1;0;0]; [0;0;1] ]
//    let output: float Array3 = Array3.zeroCreate lengthsZ
//    let values = [| [| [|0.0|];[||];[||] |]; [| [||];[||];[|0.0|] |] |]
//    Assert.That(output.Values, Is.EqualTo values)