﻿namespace Kit.Tests.Collections

open NUnit.Framework
open Kit.Collections

[<TestFixture>]
type List_Tests() =

  [<Test>]
  member o.collecti() =
    let arrayIn = [ 1; 2; 3 ]
    let result =
      arrayIn |> List.collecti (fun i value ->
        [ i; value ]
      )
    Assert.That(result, Is.EquivalentTo [ 0; 1; 1; 2; 2; 3 ])

  [<Test>]
  member o.filteri() =
    let arrayIn = [ 1; 2; 3 ]
    let filtered = arrayIn |> List.filteri (fun i text -> i > 1)
    Assert.That(filtered.Length, Is.EqualTo 1)
    Assert.That(filtered.[0], Is.EqualTo 3)

  [<Test>]
  member o.foldi() =
    let arrayIn = [ 1; 2; 3 ]
    let result =
      arrayIn |> List.foldi (fun i folded value ->
        i * value + folded ) 5
    Assert.That(result, Is.EqualTo 13)

  [<Test>]
  member o.sumBy() =
    let arrayIn = [ "3"; "1"; "6" ]
    let result = arrayIn |> List.sumBy (fun value ->
      int value )
    Assert.That(result, Is.EqualTo 10)

  [<Test>]
  member o.sumByi() =
    let arrayIn = [ "3"; "1"; "0" ]
    let result = arrayIn |> List.sumByi (fun i value ->
      int value + i
    )
    Assert.That(result, Is.EqualTo 7)