﻿namespace Kit.Tests.Collections

open NUnit.Framework
open Kit
open Kit.Collections

type Array4_Tests() =

  

  let input =
    let offset = 2
    let nodes = triple(3,4,5)
    let a2 = Array2D.create 3 3 3
    let arr = Array3.init a2 (fun i ->
      let nx = match i.x with 0 | 2 -> offset | _ -> nodes.x
      let ny = match i.y with 0 | 2 -> offset | _ -> nodes.y
      let nz = match i.z with 0 | 2 -> offset | _ -> nodes.z
      let arr = Array.init nx (fun x ->
        Array.init ny (fun y ->
          Array.init nz (fun z ->
            i.x * 100000 + i.y * 10000 + i.z * 1000 + x * 100 + y * 10 + z
        )))
      array3 arr
    )
    let origin = triple (5,6,7)
    array4 (arr, origin, nodes, 2)

  [<Test>]
  member o.GetX() =
    Assert.That(input.[triple(3,6,7)], Is.EqualTo 11000)
    Assert.That(input.[triple(4,6,7)], Is.EqualTo 11100)
    Assert.That(input.[triple(5,6,7)], Is.EqualTo 111000)
    Assert.That(input.[triple(6,6,7)], Is.EqualTo 111100)
    Assert.That(input.[triple(7,6,7)], Is.EqualTo 111200)
    Assert.That(input.[triple(8,6,7)], Is.EqualTo 211000)
    Assert.That(input.[triple(9,6,7)], Is.EqualTo 211100)

  [<Test>]
  member o.GetY() =
    Assert.That(input.[triple(4,4,7)], Is.EqualTo 1100)
    Assert.That(input.[triple(4,5,7)], Is.EqualTo 1110)
    Assert.That(input.[triple(4,6,7)], Is.EqualTo 11100)
    Assert.That(input.[triple(4,7,7)], Is.EqualTo 11110)
    Assert.That(input.[triple(4,8,7)], Is.EqualTo 11120)
    Assert.That(input.[triple(4,9,7)], Is.EqualTo 11130)
    Assert.That(input.[triple(4,10,7)], Is.EqualTo 21100)
    Assert.That(input.[triple(4,11,7)], Is.EqualTo 21110)

  [<Test>]
  member o.GetZ() =
    Assert.That(input.[triple(9,11,5)], Is.EqualTo 220110)
    Assert.That(input.[triple(9,11,6)], Is.EqualTo 220111)
    Assert.That(input.[triple(9,11,7)], Is.EqualTo 221110)
    Assert.That(input.[triple(9,11,8)], Is.EqualTo 221111)
    Assert.That(input.[triple(9,11,9)], Is.EqualTo 221112)
    Assert.That(input.[triple(9,11,10)], Is.EqualTo 221113)
    Assert.That(input.[triple(9,11,11)], Is.EqualTo 221114)
    Assert.That(input.[triple(9,11,12)], Is.EqualTo 222110)
    Assert.That(input.[triple(9,11,13)], Is.EqualTo 222111)


