﻿module Kit.SVG.Svg2Png

open System.IO

let run (path) =
  let jsPath = path + ".js"
  let fileInfo = FileInfo path
  let str =
    sprintf "var page = require('webpage').create();\npage.open('%s', function () { page.render('%s'); phantom.exit();});"
      fileInfo.Name (fileInfo.Name + ".png")
  File.WriteAllText (jsPath, str)
  let p = new System.Diagnostics.Process()
  p.StartInfo.WorkingDirectory <- fileInfo.Directory.FullName
  p.StartInfo.Arguments <- jsPath
  p.StartInfo.FileName <- @"c:\Users\osv\Documents\Programs\phantomjs-1.9.1-windows\phantomjs.exe"
  p.StartInfo.WindowStyle <- System.Diagnostics.ProcessWindowStyle.Hidden
  p.Start() |> ignore
  p.WaitForExit()
  File.Delete jsPath