﻿namespace Kit.Tests.Math

open NUnit.Framework
open Kit
open Kit.Math

[<TestFixture>]
type Quaternionm_Tests() =

  let input = quaternion(1., triple(2.,3.,4.))

  [<Test>]
  member o.zero() =
    Assert.That(Quaternion.zero, Is.EqualTo(quaternion(1.0, triple(0.,0.,0.))))

  [<Test>]
  member o.conjugate() =
    let output = Quaternion.conjugate input
    let shouldBe = quaternion(1.0, triple(-2.,-3.,-4.))
    Assert.That(output, Is.EqualTo shouldBe)

  [<Test>]
  member o.norm2() =
    let output = Quaternion.norm2 input
    let shouldBe = 30.
    Assert.That(output, Is.EqualTo shouldBe)

  [<Test>]
  member o.norm() =
    let output = Quaternion.norm input
    let shouldBe = sqrt 30.
    Assert.That(output, Is.EqualTo shouldBe)

  [<Test>]
  member o.inverse() =
    let output = Quaternion.inverse input
    let shouldBe = quaternion(1./30., triple(-1./15.,-1./10.,-2./15.))
    Assert.That(output, Is.EqualTo shouldBe)

  [<Test>]
  member o.normalize() =
    let output = Quaternion.normalize input
    let shouldBe = quaternion(1./sqrt 30., triple(2./sqrt 30.,3./sqrt 30.,4./sqrt 30.))
    Assert.That(output, Is.EqualTo shouldBe)