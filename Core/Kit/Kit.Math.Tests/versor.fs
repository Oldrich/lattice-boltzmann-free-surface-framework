﻿namespace Kit.Tests.Math

open NUnit.Framework
open Kit
open Kit.Math

[<TestFixture>]
type Versor_Tests() =

  [<Test>]
  member o.ofYawPitchRoll() =
    let output = Versor.ofYawPitchRoll (0.7854, 0.1, 0.)
    Assert.That(output.w, Is.InRange(0.9227, 0.9228))
    Assert.That(output.t.x, Is.InRange(-0.0192, -0.0191))
    Assert.That(output.t.y, Is.InRange(0.0461, 0.0462))
    Assert.That(output.t.z, Is.InRange(0.3822, 0.3823))

  [<Test>]
  member o.ofAxisAngle() =
    let output = Versor.ofAxisAngle (triple(1.,2.,3.), System.Math.PI / 6.0)
    Assert.That(output.w, Is.InRange(0.965925, 0.965926))
    Assert.That(output.t.x, Is.InRange(0.069172, 0.069173))
    Assert.That(output.t.y, Is.InRange(0.138344, 0.138345))
    Assert.That(output.t.z, Is.InRange(0.207516, 0.207517))

  [<Test>]
  member o.ofAngularVelocityVector() =
    let output = Versor.ofAngularVelocityVector (triple(1.,2.,3.))
    Assert.That(output.w, Is.InRange(-0.295552, -0.295551))
    Assert.That(output.t.x, Is.InRange(0.255321, 0.255322))
    Assert.That(output.t.y, Is.InRange(0.510643, 0.510644))
    Assert.That(output.t.z, Is.InRange(0.765965, 0.765966))

  [<Test>]
  member o.toAxisAngle() =
    let vs = quaternion(0.5, triple(1.,2.,3.)) |> Quaternion.normalize
    let t, angle = Versor.toAxisAngle vs
    Assert.That(angle, Is.InRange(2.875905422, 2.875905423))
    Assert.That(t.x, Is.InRange(0.267261, 0.267262))
    Assert.That(t.y, Is.InRange(0.534522, 0.534523))
    Assert.That(t.z, Is.InRange(0.801783, 0.801784))

  [<Test>]
  member o.toAngularVelocityVector() =
    let vs = quaternion(0.5, triple(1.,2.,3.)) |> Quaternion.normalize
    let t = Versor.toAngularVelocityVector vs
    Assert.That(t.x, Is.EqualTo(2.875905422 * 0.267261).Within(1e-5))
    Assert.That(t.y, Is.EqualTo(2.875905422 * 0.534522).Within(1e-5))
    Assert.That(t.z, Is.EqualTo(2.875905422 * 0.801783).Within(1e-5))

  [<Test>]
  member o.toMatrix() =
    let vs = quaternion(0.5, triple(1.,2.,3.)) |> Quaternion.normalize
    let output = Versor.toMatrix vs
    let shouldBe =
      array2D [
        [ -0.824561; 0.0701754; 0.561404 ]
        [ 0.491228; -0.403509; 0.77193 ]
        [ 0.280702; 0.912281; 0.298246 ]
      ]
    Assert.That(output, Is.EqualTo(shouldBe).Within(1e-6))

  [<Test>]
  member o.rotateG2L() =
    let t = triple(1.,0.,0.)
    let vs = Versor.ofYawPitchRoll(System.Math.PI/2.0, 0., 0.)
    let newt = t |> Versor.rotateG2L vs
    Assert.That(newt, Is.EqualTo(triple(0., -1., 0.)))

  [<Test>]
  member o.rotateL2G() =
    let t = triple(1.,0.,0.)
    let vs = Versor.ofYawPitchRoll(System.Math.PI/2.0, 0., 0.)
    let newt = t |> Versor.rotateL2G vs
    Assert.That(newt, Is.EqualTo(triple(0., 1., 0.)))
