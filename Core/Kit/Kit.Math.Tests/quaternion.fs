﻿namespace Kit.Tests.Math

open NUnit.Framework
open Kit
open Kit.Math

[<TestFixture>]
type Quaternion_Tests() =

  let input = quaternion(1., triple(2.,3.,4.))

  [<Test>] member o.w() = Assert.That(input.w, Is.EqualTo 1.)
  [<Test>] member o.tx() = Assert.That(input.t.x, Is.EqualTo 2.)
  [<Test>] member o.ty() = Assert.That(input.t.y, Is.EqualTo 3.)
  [<Test>] member o.tz() = Assert.That(input.t.z, Is.EqualTo 4.)

  [<Test>]
  member o.ToString() =
    Assert.That(input.ToString(), Is.EqualTo "[1; [2; 3; 4]]")

  [<Test>]
  member o.ToString2() =
    Assert.That(input.ToString(), Is.Not.EqualTo "[1; [3; 3; 4]]")

  [<Test>]
  member o.Equals() =
    Assert.That(input, Is.EqualTo(quaternion(1., triple(2.,3.,4.))))

  [<Test>]
  member o.Equals2() =
    Assert.That(input, Is.Not.EqualTo(quaternion(2., triple(2.,3.,4.))))

  [<Test>]
  member o.Equals3() =
    Assert.That(input, Is.Not.EqualTo(quaternion(1., triple(3.,3.,4.))))

  [<Test>]
  member o.Equals4() =
    Assert.That(input, Is.Not.EqualTo(quaternion(1., triple(2.,4.,4.))))

  [<Test>]
  member o.Equals5() =
    Assert.That(input, Is.Not.EqualTo(quaternion(1., triple(2.,3.,5.))))

  [<Test>]
  member o.GetHashCode() =
    let h1 = quaternion(0.56, triple(1.152,-2.235,3.889)).GetHashCode()
    let h2 = quaternion(0.56, triple(1.152,-2.235,3.889)).GetHashCode()
    Assert.That(h1, Is.EqualTo h2)

  [<Test>]
  member o.GetHashCode2() =
    let numberOfValues = 100000
    let values =
      let get =
        let rnd = System.Random()
        fun () -> 20.0 * rnd.NextDouble() - 10.0
      Array.init numberOfValues (fun _ -> quaternion(get(), triple(get(),get(),get())).GetHashCode() )
    let numberOfUniqueValues = values |> Seq.distinct |> Seq.length
    let fraction = float (numberOfValues - numberOfUniqueValues) / float numberOfValues
    Assert.That(fraction, Is.LessThan 0.01)

  [<Test>]
  member o.op_Multiply1() =
    Assert.That(4.0 * input, Is.EqualTo(quaternion(4.0, triple(8.,12.,16.))))

  [<Test>]
  member o.op_Multiply2() =
    let output = quaternion(1., triple(-2.,3.,2.)) * quaternion(11., triple(-2.,0.,-2.))
    Assert.That(output, Is.EqualTo(quaternion(11.0, triple(-30.,25.,26.))))

  [<Test>]
  member o.op_Division1() =
    Assert.That(input / 0.1, Is.EqualTo(quaternion(10., triple(20.,30.,40.))))

  [<Test>]
  member o.op_Addition1() =
    let output = quaternion(1., triple(-2.,3.,2.)) + quaternion(11., triple(-2.,0.,-2.))
    Assert.That(output, Is.EqualTo(quaternion(12.0, triple(-4.,3.,0.))))

  [<Test>]
  member o.op_Subtraction1() =
    let output = quaternion(1., triple(-2.,3.,2.)) - quaternion(11., triple(-2.,0.,-2.))
    Assert.That(output, Is.EqualTo(quaternion(-10., triple(0.,3.,4.))))