rmdir /s /q "_report"
md "_report"
cd /d ..\Collections_Tests\bin\Debug

"..\..\..\toolkit\opencover\OpenCover.Console.exe" -filter:"+[Collections]*" -target:"..\..\..\toolkit\nunit\bin\nunit-console-x86.exe" -register:user -targetargs:"/nologo Collections_Tests.dll" -output:"..\..\..\toolkit\_report\coverage.xml"

"..\..\..\toolkit\reportgenerator\bin\ReportGenerator.exe" -reports:"..\..\..\toolkit\_report\coverage.xml" -targetdir:"..\..\..\toolkit\_report"

"..\..\..\toolkit\_report\index.htm"