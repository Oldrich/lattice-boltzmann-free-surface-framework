﻿[<RequireQualifiedAccess>]
module Kit.Math.Geometry.Sample

open Kit

let rec private relax (N, prec, points: _ triple []) =
  let nPoints, dMax =
    points |> Array.Parallel.map ( fun p0 ->
      let force =
        points |> Array.fold ( fun fSum p1 ->
          let force = 
            let f = p0 - p1
            let l = Triple.norm f
            if l = 0. then Triple.zerof else f/ l**0.9
          fSum + force
        ) Triple.zerof
      let v = p0 +  force / float N
      let np =  Triple.normalize v
      np, Triple.norm (np - p0)
    ) |> Array.unzip
  if Array.max dMax > prec then relax (N, prec, nPoints) else nPoints

/// Uniformly sampled unit sphere by N points with precision prec
let sphere(N, prec) = 
  let rnd = Random 10
  let points = Array.init N (fun _ ->
    rnd.nextTripleFloat(-1., 1.)) |> Array.map (fun v -> v / Triple.norm v)
  relax (N, prec, points)