﻿[<RequireQualifiedAccess>]
module Kit.Math.Geometry.Distance

open Kit

/// return: point on segment closest to the given point
let segmentPoint (segment: (float triple * _ triple), point: _ triple) =
  let s1, s2 = segment
  let c = point - s1
  let v =  s2 - s1
  let norm = Triple.norm v
  let vn = 1. / norm * v
  let t = Triple.dot(c, vn)
  if t < 0. then s1
  elif t > norm then s2
  else s1 + vn * t

/// return: point on segment1 and segment2
let segmentSegment (segment1: _ triple * _ triple, segment2: _ triple * _ triple) =
  let p1A, p1B = segment1
  let p2A, p2B = segment2
  let SMALL_NUM = 1e-10
  let u = p1B - p1A
  let v = p2B - p2A
  let w = p1A - p2A
  let a = Triple.dot(u, u)
  let b = Triple.dot(u, v)
  let c = Triple.dot(v, v)
  let d = Triple.dot(u, w)
  let e = Triple.dot(v, w)
  let D = a * c - b * b
  let sN, sD, tN, tD =
    if D < SMALL_NUM then 0.0, 1.0, e, c
    else
      let sN = b * e - c * d
      let tN = a * e - b * d
      if sN < 0.0 then 0.0, D, e, c
      elif sN > D then D, D, e + b, c
      else sN, D, tN, D
  let tN, sN, sD =
    if tN < 0.0 then
      let sN, sD = if -d < 0.0 then 0.0, sD elif -d > a then sD, sD else -d, a
      0.0, sN, sD
    elif tN > tD then
      let sN, sD = if -d + b < 0.0 then 0., sD elif -d + b > a then sD, sD else -d + b, a
      tD, sN, sD
    else tN, sN, sD
  let sc = if abs sN < SMALL_NUM then 0.0 else sN / sD
  let tc = if abs tN < SMALL_NUM then 0.0 else tN / tD
  p1A + (sc * u), p2A + (tc * v)