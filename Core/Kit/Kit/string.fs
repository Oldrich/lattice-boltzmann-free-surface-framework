﻿module Kit.String

let toBase64 (input: string) =
  let bytes = System.Text.Encoding.UTF8.GetBytes input
  let base64String = System.Convert.ToBase64String bytes
  base64String

let ofBase64 (input: string) =
 let bytes = System.Convert.FromBase64String input
 let value = System.Text.Encoding.UTF8.GetString bytes
 value