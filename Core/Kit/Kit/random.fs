﻿namespace Kit

type Random(seed: int) =

  let random = new System.Random(seed)
  
  member val seed = seed

  member o.nextInt () =
    random.Next()

  member o.nextInt (max: int) =
    random.Next(max)

  member o.nextInt (min: int, max: int) =
    random.Next(int min, int max) * (max / int max)

  member o.nextFloat () =
    random.NextDouble()

  member o.nextFloat (max: float<'a>) =
    random.NextDouble() * max

  member o.nextFloat (min: float<'a>, max: float<'a>) =
    min + random.NextDouble() * (max - min)

  member o.nextTripleInt () =
    triple (random.Next(), random.Next(), random.Next())

  member o.nextTripleInt (max: int triple) =
    triple (
      random.Next(max.x),
      random.Next(max.y),
      random.Next(max.z) )

  member o.nextTripleInt (min: _ triple, max: _ triple) =
    triple (
      random.Next(min.x, max.x),
      random.Next(min.y, max.y),
      random.Next(min.z, max.z) )

  member o.nextTripleInt max =
    triple (
      random.Next(max),
      random.Next(max),
      random.Next(max) )

  member o.nextTripleInt (min, max) =
    triple (
      random.Next(min, max),
      random.Next(min, max),
      random.Next(min, max) )

  member o.nextTripleFloat () =
    triple (random.NextDouble(), random.NextDouble(), random.NextDouble())

  member o.nextTripleFloat (max: _ triple) =
    triple (
      random.NextDouble() * max.x,
      random.NextDouble() * max.y,
      random.NextDouble() * max.z )

  member o.nextTripleFloat (min: _ triple, max: _ triple) =
    triple (
      min.x + random.NextDouble() * (max.x - min.x), 
      min.y + random.NextDouble() * (max.y - min.y), 
      min.z + random.NextDouble() * (max.z - min.z) )

  member o.nextTripleFloat (max) =
    triple (random.NextDouble() * max, random.NextDouble() * max, random.NextDouble() * max)

  member o.nextTripleFloat (min, max) =
    triple (  min + random.NextDouble() * (max - min), 
              min + random.NextDouble() * (max - min), 
              min + random.NextDouble() * (max - min) )

