﻿[<AutoOpen>]
module AutoOpen

let inline defaultFn xo fn =
  match xo with Some x -> x | None -> fn ()

let pi = System.Math.PI

[<Measure>] type m
[<Measure>] type kg
[<Measure>] type s
[<Measure>] type N = kg m / s^2
[<Measure>] type Pa = N / m^2
[<Measure>] type J = N m
[<Measure>] type rad


let string v =
  match box v with
  | :? string as v -> v
  | :? char as v -> System.Char.ToString v
  | :? float32 as v -> v.ToString "G"
  | :? float as v -> v.ToString "G"
  | _ -> (sprintf "%A" v).Replace("\n","")