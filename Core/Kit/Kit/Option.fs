﻿[<RequireQualifiedAccess>]
module Kit.Option

let inline map func input =
  match input with Some v -> Some (func v) | None -> None