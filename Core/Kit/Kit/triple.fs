﻿namespace Kit

[<Struct>]
[<CustomEquality>]
[<NoComparison>]
type triple<'t when 't: equality and 't: struct> =
  val x: 't; val y: 't; val z: 't
  new (x,y,z) = { x = x; y = y; z = z}
  new ((x,y,z)) = { x = x; y = y; z = z}

  member o.Item (idx: int) =
    match idx with
      | 0 -> o.x
      | 1 -> o.y
      | 2 -> o.z
      | _ -> failwithf "invalid index %d in triple" idx

  override o.ToString() =
    System.String.Format ("[{0:G}; {1:G}; {2:G}]", o.x, o.y, o.z)

  override o.Equals(ob : obj) =
    match box o, ob with
    | (:? triple<float> as v1), (:? triple<float> as v2) ->
      abs (v2.x - v1.x) < 1e-10 && abs (v2.y - v1.y) < 1e-10 && abs (v2.z - v1.z) < 1e-10
    | (:? triple<float32> as v1), (:? triple<float32> as v2) ->
      abs (v2.x - v1.x) < 1e-10f && abs (v2.y - v1.y) < 1e-10f && abs (v2.z - v1.z) < 1e-10f
    | _, (:? triple<'t> as v) ->
      o.x.Equals v.x && o.y.Equals v.y && o.z.Equals v.z
    | _ -> false

  override o.GetHashCode() = 
    let hash = 31 + o.x.GetHashCode()
    let hash = hash * 31 + o.y.GetHashCode()
    hash * 31 + o.z.GetHashCode()

  static member inline get_Zero() =
    let mutable z = LanguagePrimitives.GenericZero< (^U) >
    triple (z,z,z)

  static member inline ( * ) (a: 'a when 'a: comparison, b: 'a triple) =
    triple(a * b.x, a * b.y, a * b.z)

  static member inline ( * ) (a: 'a triple, b: 'a when 'a: comparison) =
    triple(b * a.x, b * a.y, b * a.z)

  static member inline ( * ) (a: _ [,], b: _ triple) =
    let x = a.[0,0] * b.x + a.[0,1] * b.y + a.[0,2] * b.z
    let y = a.[1,0] * b.x + a.[1,1] * b.y + a.[1,2] * b.z
    let z = a.[2,0] * b.x + a.[2,1] * b.y + a.[2,2] * b.z
    triple(x,y,z)

  static member inline ( .* ) (a: _ triple, b: _ triple) =
    triple(a.x * b.x, a.y * b.y, a.z * b.z)

  static member inline ( / ) (a: 'a triple, b: 'a when 'a: comparison) =
    triple(a.x / b, a.y / b, a.z / b)

  static member inline ( ./ ) (a: _ triple, b: _ triple) =
    triple(a.x / b.x, a.y / b.y, a.z / b.z)

  static member inline (+) (a: 'a when 'a: comparison, b: 'a triple) =
    triple(a + b.x, a + b.y, a + b.z)

  static member inline (+) (a: 'a triple, b: 'a when 'a: comparison) =
    triple(b + a.x, b + a.y, b + a.z)

  static member inline (+) (a: 'a triple, b: 'a triple) =
    triple(a.x + b.x, a.y + b.y, a.z + b.z)

  static member inline (+) (a: 'a triple, (x,y,z)) =
      triple(a.x + x, a.y + y, a.z + z)

  static member inline (+) ((x,y,z), a: 'a triple) =
      triple(a.x + x, a.y + y, a.z + z)

  static member inline (-) (a: 'a when 'a: comparison, b: 'a triple) =
    triple(a - b.x, a - b.y, a - b.z)

  static member inline (-) (a: 'a triple, b: 'a when 'a: comparison) =
    triple(a.x - b, a.y - b, a.z - b)

  static member inline (-) (a: 'a triple, b: 'a triple) =
    triple(a.x - b.x, a.y - b.y, a.z - b.z)

  static member inline (-) (a: 'a triple, (x,y,z)) =
      triple(a.x - x, a.y - y, a.z - z)

  static member inline (-) ((x,y,z), a: 'a triple) =
      triple(x - a.x, y - a.y, z - a.z)

  static member inline (~-) (a: 'a triple) = triple(-a.x, -a.y, -a.z)

  static member inline ( .** ) (a: 'a triple, b: 'a when 'a: comparison) =
      triple(a.x**b, a.y**b, a.z**b)

[<AutoOpen>]
module TripleAutoOpen =
  let (|Triple|) (t: _ triple) = t.x, t.y, t.z