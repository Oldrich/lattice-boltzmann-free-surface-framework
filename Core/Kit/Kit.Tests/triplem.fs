﻿namespace Kit.Tests.Math

open NUnit.Framework
open Kit

[<TestFixture>]
type Triplem_Tests() =

  [<Test>]
  member o.Triple() =
    let (Triple (x,y,z)) = triple(1,0,4)
    Assert.That(x, Is.EqualTo 1)
    Assert.That(y, Is.EqualTo 0)
    Assert.That(z, Is.EqualTo 4)

  [<Test>]
  member o.ones() =
    Assert.That(Triple.ones, Is.EqualTo(triple(1,1,1)))

  [<Test>]
  member o.onesf() =
    Assert.That(Triple.onesf, Is.EqualTo(triple(1.,1.,1.)))

  [<Test>]
  member o.zero() =
    Assert.That(Triple.zero, Is.EqualTo(triple(0,0,0)))

  [<Test>]
  member o.zerof() =
    Assert.That(Triple.zerof, Is.EqualTo(triple(0.,0.,0.)))

  [<Test>]
  member o.create() =
    Assert.That(Triple.create 4, Is.EqualTo(triple(4,4,4)))

  [<Test>]
  member o.cross() =
    let result = Triple.cross(triple(1.,0.1,0.5), triple(2.,1.,0.2))
    Assert.That(result, Is.EqualTo(triple(-0.48,0.8,0.8)))

  [<Test>]
  member o.dot() =
    let result = Triple.dot(triple(1.,0.1,0.5), triple(2.,1.,0.2))
    Assert.That(result, Is.EqualTo 2.2)

  [<Test>]
  member o.exists() =
    let result = triple(1.,0.1,0.5) |> Triple.exists (fun v -> v = 0.5)
    Assert.That(result, Is.True)

  [<Test>]
  member o.init() =
    let result = Triple.init (fun i -> float i)
    Assert.That(result, Is.EqualTo (triple(0.,1.,2.)))

  [<Test>]
  member o.iteri() =
    let out = ref 0.0
    let result = triple(1.,0.1,0.5) |> Triple.iteri (fun i v ->
      out := !out + float (i + 1) * v
    )
    Assert.That(!out, Is.EqualTo 2.7)

  [<Test>]
  member o.map() =
    let result = triple(1.,0.1,0.5) |> Triple.map (fun v ->
      v + 3.0 )
    Assert.That(result, Is.EqualTo (triple(4.,3.1,3.5)))

  [<Test>]
  member o.mapi() =
    let result = triple(1.,0.1,0.5) |> Triple.mapi (fun i v ->
      float (i + 1) * v
    )
    Assert.That(result, Is.EqualTo (triple(1.,0.2,1.5)))

  [<Test>]
  member o.norm() =
    let result = triple(-1.,-0.1,-0.5) |> Triple.norm
    Assert.That(result, Is.InRange(1.1224, 1.1225))

  [<Test>]
  member o.norm2() =
    let result = triple(-1.,-0.1,-0.5) |> Triple.norm2
    Assert.That(result, Is.EqualTo 1.26)

  [<Test>]
  [<ExpectedException(typeof<System.DivideByZeroException>)>]
  member o.normalize() =
    triple(0.,0.,-0.) |> Triple.normalize |> ignore
    
  [<Test>]
  member o.normalize2() =
    let result = triple(1.,-2.,3.) |> Triple.normalize
    Assert.That(result, Is.EqualTo (triple(1./sqrt 14., -sqrt(2./7.), 3. / sqrt 14.)))

  [<Test>]
  member o.ofArray() =
    let result = Triple.ofArray [| 1; 2; 3 |]
    Assert.That(result, Is.EqualTo (triple(1,2,3)))

  [<Test>]
  [<ExpectedException(typeof<Triple.NumberOfElementsException>)>]
  member o.ofArray2() =
    Triple.ofArray [| 1; 2; 3; 4 |] |> ignore

  [<Test>]
  [<ExpectedException(typeof<Triple.NumberOfElementsException>)>]
  member o.ofArray3() =
    Triple.ofArray [| 1; 2 |] |> ignore

  [<Test>]
  member o.ofList() =
    let result = Triple.ofList [ 1; 2; 3 ]
    Assert.That(result, Is.EqualTo (triple(1,2,3)))

  [<Test>]
  [<ExpectedException(typeof<Triple.NumberOfElementsException>)>]
  member o.ofList2() =
    Triple.ofList [ 1; 2; 3; 4 ] |> ignore

  [<Test>]
  [<ExpectedException(typeof<Triple.NumberOfElementsException>)>]
  member o.ofList3() =
    Triple.ofList [ 1; 2 ] |> ignore

  [<Test>]
  member o.ofTuple() =
    let result = Triple.ofTuple (1,2,3)
    Assert.That(result, Is.EqualTo (triple(1,2,3)))

  [<Test>]
  member o.outer() =
    let result = Triple.outer(triple(1.,2.,3.), triple(4.,5.,6.))
    let shouldBe =
      array2D [|
        [| 4.; 5.; 6. |]
        [| 8.; 10.; 12. |]
        [| 12.; 15.; 18. |] |]
    Assert.That(result, Is.EqualTo shouldBe)

  [<Test>]
  member o.toArray() =
    let result = Triple.toArray (triple(1.,2.,3.))
    Assert.That(result, Is.EqualTo [| 1.; 2.; 3. |])

  [<Test>]
  member o.toFloat() =
    let result = Triple.toFloat (triple(1,2,3))
    Assert.That(result, Is.EqualTo(triple(1.,2.,3.)))

  [<Test>]
  member o.toList() =
    let result = Triple.toList (triple(1.,2.,3.))
    Assert.That(result, Is.EqualTo [ 1.; 2.; 3. ])

  [<Test>]
  member o.toTuple() =
    let result = Triple.toTuple (triple(1.,2.,3.))
    Assert.That(result, Is.EqualTo (1., 2., 3.))