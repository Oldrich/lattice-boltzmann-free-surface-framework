﻿namespace Kit.Tests

open NUnit.Framework
open Kit

[<TestFixture>]
type Option_Tests() =

  [<Test>]
  member o.map() =
    let input = Some 4.0 |> Option.map (fun v -> v + 5.0)
    Assert.That(input, Is.EqualTo (Some 9.0))

  [<Test>]
  member o.map2() =
    let input = None |> Option.map (fun v -> v + 5.0)
    Assert.That(input, Is.EqualTo None)