﻿module Kit.IO.Resource

open System.IO

let read filePath =
  use stream =
    System.Reflection.Assembly.GetCallingAssembly().GetManifestResourceStream filePath
  use reader = new StreamReader(stream)
  reader.ReadToEnd()