﻿namespace Utilities.IO

open System.Text
open System.Text.RegularExpressions
open System.Collections.Generic

type CMD =

  static member args2string (args: string seq) =
    let arguments = new StringBuilder()
    let invalidChar = new Regex("[\x00\x0a\x0d]")//  these can not be escaped
    let needsQuotes = new Regex(@"\s|""")//          contains whitespace or two quote characters
    let escapeQuote = new Regex(@"(\\*)(""|$)")//    one or more '\' followed with a quote or end of string
    args |> Seq.iteri (fun i arg ->
      if invalidChar.IsMatch arg then failwithf "wrong argument: " arg
      if arg = System.String.Empty then arguments.Append "\"\"" |> ignore
      elif needsQuotes.IsMatch arg = false then arguments.Append arg  |> ignore
      else
        arguments.Append '"' |> ignore
        arguments.Append (escapeQuote.Replace (arg, fun (m: Match) ->
          m.Groups.[1].Value + m.Groups.[1].Value + (if m.Groups.[2].Value = "\"" then "\\\"" else "")
        )) |> ignore
        arguments.Append '"' |> ignore
      if i + 1 < Seq.length args then arguments.Append ' ' |> ignore )
    arguments.ToString()

  static member string2args (commandLine: string) =

    let splitBy controller (str: string) =
      let nextPiece = ref 0
      seq { for c in 0 .. str.Length - 1 do
              if controller str.[c] then
                  yield str.Substring (!nextPiece, c - !nextPiece)
                  nextPiece := c + 1
            yield str.Substring !nextPiece }

    let inQuotes = ref false
    commandLine
      |> splitBy (fun c ->
          if c = '\"' then inQuotes := not !inQuotes
          not !inQuotes && c = ' ' )
      |> Seq.choose (fun arg ->
        if arg <> "" then
          let arg, quote = arg.Trim(), '\"'
          if arg.Length >= 2 && arg.[0] = quote && arg.[arg.Length - 1] = quote then
            arg.Substring(1, arg.Length - 2) |> Some
          else Some arg
        else None )
  
  static member toDict (args: string) =
    let dict = Dictionary ()
    let argsA = CMD.string2args args |> Array.ofSeq
    for arg in argsA do
      let a = arg.Split ([|'='|], 2)
      if a.Length = 2 then dict.Add (a.[0], a.[1])
    dict

  static member toDict (args: string []) =
    let dict = Dictionary ()
    for arg in args do
      let a = arg.Split ([|'='|], 2)
      dict.Add (a.[0], a.[1])
    dict

  static member ofDict (dict: IDictionary<string, string>) =
    dict
      |> Seq.map (fun a ->
        if a.Key.Contains "=" then failwithf "The key (%s) cannot contain '=' character." a.Key
        sprintf "%s=%s" a.Key a.Value)
      |> CMD.args2string