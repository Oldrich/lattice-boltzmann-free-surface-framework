﻿module Kit.IO.XML

open Kit
open System.Xml
open System.Xml.Linq

let save (elem: XElement, path: string) =
  let xws = new XmlWriterSettings(Indent = true, NewLineOnAttributes = false)
  use xw = XmlWriter.Create(path, xws)
  elem.Save xw

type System.String with
  member o.xn = XName.Get o
  member o.xa (value: 'T) = box (XAttribute(o.xn, value))
  member o.xe (value: 'T) = box (XElement(o.xn, value))
  member o.xroot (value: 'T) = XElement(o.xn, value)
  member o.xt (t: _ triple) =
    o.xe [ "x".xa t.x; "y".xa t.y; "z".xa t.z ]

